-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 20, 2019 at 08:23 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `contraffic_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `backend_logs`
--

CREATE TABLE `backend_logs` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `class` varchar(255) NOT NULL,
  `method` varchar(255) NOT NULL,
  `messege` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `backend_logs`
--

INSERT INTO `backend_logs` (`id`, `username`, `ip_address`, `class`, `method`, `messege`, `created_at`) VALUES
(1, 'admin', '127.0.0.1', 'users', 'logout', 'Logout : Logouted', '2018-12-18 23:33:24'),
(2, 'admin', '127.0.0.1', 'users', 'login', 'username password ไม่ถูกต้อง', '2018-12-18 23:33:29'),
(3, 'admin', '127.0.0.1', 'users', 'login', 'username password ไม่ถูกต้อง', '2018-12-18 23:33:38'),
(4, 'admin', '127.0.0.1', 'users', 'login', 'Success : LoginPage', '2018-12-18 23:33:46'),
(5, 'admin', '127.0.0.1', 'categorys', 'store', 'เพิ่มข้อมูล ประเภทสินค้า : เสาไฟส่องสว่าง,ประเภท : 0', '2018-12-18 23:38:09'),
(6, 'admin', '127.0.0.1', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : เสาไฟส่องสว่าง,ประเภท : 0', '2018-12-18 23:40:24'),
(7, 'admin', '127.0.0.1', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : เสาไฟส่องสว่าง,ประเภท : 0', '2018-12-18 23:46:49'),
(8, 'admin', '127.0.0.1', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : เสาไฟส่องสว่าง,ประเภท : 0', '2018-12-18 23:50:21'),
(9, 'admin', '127.0.0.1', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : เสาไฟส่องสว่าง,ประเภท : 0', '2018-12-18 23:50:47'),
(10, 'admin', '127.0.0.1', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : เสาไฟส่องสว่าง,ประเภท : 0', '2018-12-18 23:53:13'),
(11, 'admin', '127.0.0.1', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : เสาไฟส่องสว่าง,ประเภท : 0', '2018-12-18 23:54:25'),
(12, 'admin', '127.0.0.1', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : เสาไฟส่องสว่าง,ประเภท : 0', '2018-12-18 23:54:53'),
(13, 'admin', '127.0.0.1', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : เสาไฟส่องสว่าง,ประเภท : 0', '2018-12-18 23:55:25'),
(14, 'admin', '127.0.0.1', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : เสาไฟส่องสว่าง,ประเภท : 0', '2018-12-18 23:55:43'),
(15, 'admin', '127.0.0.1', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : เสาไฟส่องสว่าง,ประเภท : 0', '2018-12-18 23:56:47'),
(16, 'admin', '127.0.0.1', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : เสาไฟส่องสว่าง,ประเภท : 0', '2018-12-18 23:57:47'),
(17, 'admin', '127.0.0.1', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : เสาไฟส่องสว่าง,ประเภท : 0', '2018-12-19 00:11:37'),
(18, 'admin', '127.0.0.1', 'categorys', 'store', 'เพิ่มข้อมูล ประเภทสินค้า : เสาไฟส่องสว่างประติมากรรม,ประเภท : 1', '2018-12-19 00:23:38'),
(19, 'admin', '127.0.0.1', 'categorys', 'store', 'เพิ่มข้อมูล ประเภทสินค้า : ไฟจราจร,ประเภท : 0', '2018-12-19 00:38:40'),
(20, 'admin', '127.0.0.1', 'products', 'store', 'เพิ่มข้อมูล สินค้า : ไฟ ,ประเภท : 2 ,รูปสินค้า : uploads/picture/2018/12/f4353a1c62e46c325378425f620a259c.jpg ,catalog : ', '2018-12-19 02:01:07'),
(21, 'admin', '127.0.0.1', 'users', 'login', 'Success : LoginPage', '2018-12-19 09:31:04'),
(22, 'admin', '127.0.0.1', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ไฟ 2 ,ประเภท : 2 ,รูปสินค้า :  ,catalog : ', '2018-12-19 10:24:16'),
(23, 'admin', '127.0.0.1', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ไฟ 2 ,ประเภท : 2 ,รูปสินค้า : uploads/picture/2018/12/24b8ad3107025714baeec7b0a1986c03.png ,catalog : ', '2018-12-19 10:25:06'),
(24, 'admin', '127.0.0.1', 'products', 'store', 'เพิ่มข้อมูล สินค้า : test ,ประเภท : 2 ,รูปสินค้า : uploads/picture/2018/12/f764a8c2d5432f96118cb8ee00cde4f9.jpg ,catalog : ', '2018-12-19 10:25:56'),
(25, 'admin', '127.0.0.1', 'products', 'store', 'เพิ่มข้อมูล สินค้า : test2 ,ประเภท : 2 ,รูปสินค้า : uploads/picture/2018/12/51fdc36356bf28dfd9a825e36943b2cc.jpg ,catalog : uploads/catalog/2018/12/815f8d5480c164ed4af62fbf1d8c12f5.pdf', '2018-12-19 10:32:02'),
(26, 'admin', '127.0.0.1', 'products', 'store', 'เพิ่มข้อมูล สินค้า : test3 ,ประเภท : 2 ,รูปสินค้า : uploads/picture/2018/12/dcadf34eb917b846300e476b03818810.jpg ,catalog : ', '2018-12-19 10:40:42'),
(27, 'admin', '127.0.0.1', 'products', 'store', 'เพิ่มข้อมูล สินค้า : test4 ,ประเภท : 2 ,รูปสินค้า : uploads/picture/2018/12/ac4df84f79e4a3b4c0fa665d9e4eda54.jpg ,catalog : ', '2018-12-19 10:41:10'),
(28, 'admin', '127.0.0.1', 'products', 'store', 'เพิ่มข้อมูล สินค้า : test5 ,ประเภท : 2 ,รูปสินค้า : uploads/picture/2018/12/d144bd5b4c89b7777591a889f561a23d.jpg ,catalog : ', '2018-12-19 10:42:15'),
(29, 'admin', '127.0.0.1', 'products', 'store', 'เพิ่มข้อมูล สินค้า : test5 ,ประเภท : 2 ,รูปสินค้า : uploads/picture/2018/12/fd060f4db10a3b9562483c7c2e291ce5.jpg ,catalog : ', '2018-12-19 10:47:13'),
(30, 'admin', '127.0.0.1', 'products', 'store', 'เพิ่มข้อมูล สินค้า : test6 ,ประเภท : 2 ,รูปสินค้า : uploads/picture/2018/12/fca21c822ea0c579ba09a6b7efcadd65.jpg ,catalog : ', '2018-12-19 10:47:39'),
(31, 'admin', '127.0.0.1', 'products', 'store', 'เพิ่มข้อมูล สินค้า : test7 ,ประเภท : 2 ,รูปสินค้า : uploads/picture/2018/12/fc94027eec9ea8bc5337b8751acd6f44.jpg ,catalog : ', '2018-12-19 10:50:19'),
(32, 'admin', '127.0.0.1', 'products', 'store', 'เพิ่มข้อมูล สินค้า : test8 ,ประเภท : 2 ,รูปสินค้า : uploads/product/2018/12/ee0e51f651bd1775e3335b198fd7edb0.jpg ,catalog : uploads/product/2018/12/7053e368a56cfae032ba04a8365b62ba.pdf', '2018-12-19 11:01:04'),
(33, 'admin', '127.0.0.1', 'products', 'update', 'แก้ไขข้อมูล สินค้า : test8 ,ประเภท : 2 ,รูปสินค้า : uploads/product/2018/12/fe7b198435be83fe48c54e930924cc3f.jpg ,catalog : uploads/product/2018/12/eb311fd1c52c571ea1c46badc726572a.pdf', '2018-12-19 11:10:55'),
(34, 'admin', '127.0.0.1', 'products', 'destroy', 'ลบข้อมูล สินค้า : test6', '2018-12-19 11:59:38'),
(35, 'admin', '127.0.0.1', 'products', 'destroy', 'ลบข้อมูล สินค้า : test7', '2018-12-19 11:59:43'),
(36, 'admin', '127.0.0.1', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ไฟ 2 ,ประเภท : 2 ,รูปสินค้า : uploads/product/2018/12/2048ebea1d1d286a17d0fe726c580772.jpg ,catalog : uploads/product/2018/12/0c3e537972c284b96cb890f2e52c0f0e.pdf', '2018-12-19 12:00:08'),
(37, 'admin', '127.0.0.1', 'users', 'login', 'Success : LoginPage', '2018-12-19 12:47:14'),
(38, 'admin', '127.0.0.1', 'users', 'login', 'Success : LoginPage', '2018-12-19 17:47:55'),
(39, 'admin', '127.0.0.1', 'users', 'login', 'Success : LoginPage', '2018-12-19 17:48:50'),
(40, 'admin', '127.0.0.1', 'users', 'login', 'Success : LoginPage', '2018-12-20 14:09:37'),
(41, 'admin', '127.0.0.1', 'users', 'login', 'Success : LoginPage', '2018-12-22 00:37:45'),
(42, 'admin', '127.0.0.1', 'users', 'login', 'Success : LoginPage', '2018-12-22 10:45:03'),
(43, 'admin', '127.0.0.1', 'users', 'login', 'Success : LoginPage', '2018-12-22 12:14:33'),
(44, 'admin', '127.0.0.1', 'users', 'login', 'username password ไม่ถูกต้อง', '2018-12-28 06:51:09'),
(45, 'admin', '127.0.0.1', 'users', 'login', 'username password ไม่ถูกต้อง', '2018-12-28 06:51:19'),
(46, 'admin', '127.0.0.1', 'users', 'login', 'Success : LoginPage', '2018-12-28 06:51:26'),
(47, 'admin', '127.0.0.1', 'users', 'logout', 'Logout : Logouted', '2018-12-28 06:51:33'),
(48, 'admin', '127.0.0.1', 'users', 'login', 'Success : LoginPage', '2018-12-28 13:40:43'),
(49, 'admin', '127.0.0.1', 'users', 'login', 'Success : LoginPage', '2019-01-19 14:37:29'),
(50, 'admin', '127.0.0.1', 'users', 'logout', 'Logout : Logouted', '2019-01-19 15:11:10'),
(51, 'admin', '127.0.0.1', 'users', 'login', 'Success : LoginPage', '2019-01-19 15:23:02'),
(52, 'admin', '127.0.0.1', 'products', 'store', 'เพิ่มข้อมูล สินค้า : A ,ประเภท : 2 ,รูปสินค้า : uploads/product/2019/01/dc727509564011faa670218e8b971b0f.jpg ,catalog : ', '2019-01-19 15:28:04');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `icon`, `parent_id`, `sequence`, `create_by`, `update_by`, `active`, `create_at`, `update_at`) VALUES
(1, 'เสาไฟส่องสว่าง', 'เสาไฟส่องสว่าง', 'uploads/icon/2018/12/a1e8a9e3ec9227d9283119fd561060d4.jpg', 0, 0, 1, 1, 0, '2018-12-19 00:11:36', '2018-12-19 00:11:36'),
(2, 'เสาไฟส่องสว่างประติมากรรม', 'เสาไฟส่องสว่างประติมากรรม', '', 1, 0, 1, 1, 0, '2018-12-19 00:23:38', '2018-12-19 00:23:38'),
(3, 'ไฟจราจร', 'ไฟจราจร', 'uploads/icon/2018/12/7a2152dfe3e8cba6bf33ff0326cefd09.png', 0, 0, 1, 1, 0, '2018-12-19 00:38:40', '2018-12-19 00:38:40');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `pic` varchar(255) NOT NULL,
  `detail` longtext NOT NULL,
  `category_id` int(11) NOT NULL,
  `catalog` varchar(255) NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `pic`, `detail`, `category_id`, `catalog`, `create_by`, `update_by`, `active`, `create_at`, `update_at`) VALUES
(1, 'ไฟ 2', 'ไฟ-2', 'uploads/product/2018/12/2048ebea1d1d286a17d0fe726c580772.jpg', '<p><img src=\"/uploads/content/2018/12/b6517b3b98d482f1006ae06a942f13e8.jpg\" style=\"width: 500px;\"><br></p>', 2, 'uploads/product/2018/12/0c3e537972c284b96cb890f2e52c0f0e.pdf', 1, 1, 0, '2018-12-19 12:00:08', '2018-12-19 12:00:08'),
(2, 'A', 'a', 'uploads/product/2019/01/dc727509564011faa670218e8b971b0f.jpg', '', 2, '', 1, 1, 0, '2019-01-19 15:28:04', '2019-01-19 15:28:04');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` int(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `fname` varchar(80) NOT NULL,
  `lname` varchar(80) NOT NULL,
  `created_by` int(11) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `salt`, `fname`, `lname`, `created_by`, `avatar`, `email`, `phone`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 'admin', '38a142d6f6a7b889587a97379cdc0775334169ee', 'gmcZSu9Ce5DhZWl1w.bZ3e', 'admin', '', 1, '', 'admin@admin.com', '', 1, '2018-11-07 00:00:00', '2018-11-07 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backend_logs`
--
ALTER TABLE `backend_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `backend_logs`
--
ALTER TABLE `backend_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
