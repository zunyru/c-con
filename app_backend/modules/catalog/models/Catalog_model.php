<?php

class Catalog_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_CatalogAll() {

		$this->db->select('*');
		$this->db->from('catalog');
		$this->db->order_by('catalog.name', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_CatalogById($id) {
		$this->db->select('*');
		$this->db->from('catalog');
		$this->db->where('id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->id;
			$row->name;
			$row->catalog_img;
			$row->slug;
			$row->catalog_file;
		}

		return $row;
	}

	public function insert($data){
		$query = $this->db->insert('catalog', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
     
	public function update($data){

		$this->db->set('name', $data->name);
		$this->db->set('slug', $data->slug);
		if(!is_null($data->catalog_img)){
			$this->db->set('catalog_img', $data->catalog_img);
		}
		if(!is_null($data->catalog_file)){
			$this->db->set('catalog_file', $data->catalog_file);
		}
		$this->db->set('create_by', $data->create_by);
		$this->db->set('update_by', $data->update_by);
		$this->db->set('create_at', $data->create_at);
		$this->db->set('update_at', $data->update_at);
		$this->db->where('id', $data->id);
		$update = $this->db->update('catalog');

		return $update;
	}

	public function destroy($id){
		$this->db->where('id', $id);
		$delete = $this->db->delete('catalog');

		return $delete;
	}
	
}