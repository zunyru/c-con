<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn','file'));
		$this->load->model('catalog/catalog_model');
		$this->load->library('uploadfile_library');
		$this->load->library('nestable_library');
		$this->load->library('logs_library');
	}
	
	public function index(){   
		$data = array(
			'page_index' 		=> 'catalog',
			'page' 				=> 'browse',
			'title_page' 		=> 'Catalog',
			'description_page' 	=> 'My catalog'
		);
		$data['catalog'] = $this->catalog_model->get_CatalogAll();
	
		//loade view
		$this->load->view('masters/header',$data); 
		$this->load->view('index',$data);
	}

	public function create() {
		$data = array(
			'page_index' 		=> 'catalog',
			'page' 				=> 'create',
			'title_page' 		=> 'Create',
			'description_page' 	=> 'My catalog create',
		);
    
        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function store(){

		$user 					= $this->session->userdata('users');
		$path 					= 'catalog';
		$upload_pic 			= $this->uploadfile_library->do_upload('picture',TRUE,$path);
		$upload_catalog_file 	= $this->uploadfile_library->do_upload('catalog_file',TRUE,$path);
		
        $picture ='';
		if(isset($upload_pic['index'])){
			$picture = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_pic['index']['file_name'];
		}

		$catalog_file = ''; 
		if(isset($upload_catalog_file['index'])){
			$catalog_file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_catalog_file['index']['file_name'];
		}

		$data = array(
			'name'  		 => $this->input->post('name'),
			'slug'      	 => $this->input->post('slug'),
			'catalog_img'    => $picture,
			'catalog_file'   => $catalog_file,
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'active'    	 => 0,
			'create_at' 	 => date('Y-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		$last_id = $this->catalog_model->insert($data);	

		//Log
		$messege = 'เพิ่มข้อมูล : '.$this->input->post('name') 
		.' ,Img : '.$picture
		.' ,File : '.$catalog_file;

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('catalog'));
		}	

	}

	public function edit($id) {
		$data = array(
			'page_index' 		=> 'catalog',
			'page' 				=> 'edit',
			'title_page' 		=> 'Catalog Edit',
			'description_page' 	=> 'My catalog edit',
		);
        //loade data
		$data['catalog'] = $this->catalog_model->get_CatalogById($id);

        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function update($id) {
		
		$user = $this->session->userdata('users');
		
		$path 					= 'catalog';
		$upload_catalog_file 	= $this->uploadfile_library->do_upload('catalog_file',TRUE,$path);
		$upload_pic 			= $this->uploadfile_library->do_upload('picture',TRUE,$path);

		$picture = null;
		if(isset($upload_pic['index'])){
			$picture = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_pic['index']['file_name'];
			$outfile_pic = $this->input->post('outfile_pic');
			if(isset($outfile_pic)){
				$this->load->helper("file");
				unlink($outfile_pic);
			}
		}

		$catalog_file = null; 
		if(isset($upload_catalog_file['index'])){
			$catalog_file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_catalog_file['index']['file_name'];
			$outfile_catalog_file = $this->input->post('outfile_catalog_file');
			if(isset($outfile_catalog_file)){
				$this->load->helper("file");
				unlink($outfile_catalog_file);
			}
		}

		if(isset($upload['error'])){
			$this->session->set_flashdata('status',0);
			redirect(base_url('catalog'));
		}

		$data = array(
			'id'     		 => $id,
			'name'  		 => $this->input->post('name'),
			'slug'      	 => $this->input->post('slug'),
			'catalog_img'    => $picture,
			'catalog_file'   => $catalog_file,
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'active'    	 => 0,
			'create_at' 	 => date('Ycatalog-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		$last_id = $this->catalog_model->update(array_to_obj($data));

		//Log
		$messege = 'แก้ไขข้อมูล : '.$this->input->post('name') 
		.' ,img : '.$picture
		.' ,catalog_file : '.$catalog_file;

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('catalog'));
		}
	} 

	public function destroy($id){
		$user = $this->session->userdata('users');
		
		$data = $this->catalog_model->get_CatalogById($id);

		if(!empty($data->catalog_img)){
			$this->load->helper("file");
			unlink($data->catalog_img);
		}

		if(!empty($data->catalog_file)){
			$this->load->helper("file");
			unlink($data->catalog_file);
		}
		
		$delete = $this->catalog_model->destroy($id);


		if($delete){
			//Log
		$messege = 'ลบข้อมูล : '.$data->name; 

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

			$this->session->set_flashdata('status',1);
			redirect(base_url('catalog'));
		}else{
			$this->session->set_flashdata('status',0);
			redirect(base_url('catalog'));
		}
	}

	public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'catalog';
		$upload = $this->uploadfile_library->do_upload('file',TRUE,$path);

		if(isset($upload['index'])){
			$picture = '/uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
	}	

	public function deletepic($ids){

		$arrayName = array('catalog_file' => $ids);

		echo json_encode($arrayName);
	}
}
