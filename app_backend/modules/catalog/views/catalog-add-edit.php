<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li><a href="<?=base_url('catalog')?>">Catalog</a></li>
                <li class="active"><?=$title_page?></li>
            </ol>
        </div>
        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Catalog</h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <div class="body">
                        <?php 
                        if($page === 'create'): 
                            echo form_open_multipart(base_url().'catalog/store','id="form_validation"');
                        else: 
                            echo form_open_multipart(base_url().'catalog/update/'.$catalog->id,'id="form_validation"');
                        endif;    
                        ?>
                        <input type="hidden" class="form-control" name="db" id="db" value="catalog">
                        <input type="hidden" class="form-control" name="id" id="id" value="<?=isset($catalog->id)? $catalog->id : '';?>">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="name" id="name" value="<?=isset($catalog->name)? $catalog->name : '';?>" required>
                                <label class="form-label">Title</label>
                            </div>
                            <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="slug" id="slug" value="<?=isset($catalog->slug)? $catalog->slug : '';?>" required>
                                <label class="form-label">Slug</label>
                            </div>
                            <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                        </div>
                    <div class="form-group form-float">
                        <p>
                            Img
                        </p>
                        <div class="form-line">
                            <input id="picture" name="picture" type="file" class="file" data-preview-file-type="text">
                            <input type="hidden" name="outfile_pic" value="<?=(isset($catalog->catalog_img)) ? $catalog->catalog_img : ''; ?>">
                        </div>
                    </div> 

                    <div class="form-group form-float">
                        <p>
                            File : (File pdf only)
                        </p>
                        <div class="form-line">
                            <input id="catalog_file" name="catalog_file" type="file" class="file" data-preview-file-type="text">
                            <input type="hidden" name="outfile_catalog_file" value="<?=(isset($catalog->catalog_file)) ? $catalog->catalog_file : ''; ?>">
                        </div>
                    </div>

                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
</div>
</section>





