
<script type="text/javascript">

    //fileinput
    var picture_image = '<?=(!empty($catalog->catalog_img)) ? $this->config->item('backend_uploade').$catalog->catalog_img : ''; ?>';
    $('#picture').fileinput({

        initialPreview: [picture_image],
        initialPreviewAsData: true,
        initialPreviewConfig: [
        { downloadUrl: picture_image  ,extra: {id: '<?=$catalog->id;?>'} },
        ],
        deleteUrl: "<?=base_url().'catalog/deletepic/'.$catalog->id?>",

        maxFileCount: 1,
        validateInitialCount: true,
        overwriteInitial: false,
        showUpload: false,
        showRemove: true,
        required: true,
        initialPreviewAsData: true,
        maxFileSize:500,
        browseOnZoneClick: true,
        allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
        browseLabel: "Picture",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop picture here …",
    });
    

    var catalog = '<?=(!empty($catalog->catalog_file)) ? $this->config->item('backend_uploade').$catalog->catalog_file : ''; ?>';
   // var catalog = 'http://plugins.krajee.com/uploads/sample-2.pdf';
   $('#catalog_file').fileinput({
      <?php if(!empty($catalog->catalog_file)){?>
        initialPreview: [catalog],
        //pdfRendererUrl: 'http://plugins.krajee.com/pdfjs/web/viewer.html',
        overwriteInitial: false,
        initialPreviewAsData: true,
        preferIconicPreview: true,
        previewFileIconSettings: { 
            'pdf': '<i class="material-icons" style="font-size: 100%;color:#fea002;">picture_as_pdf</i>',
        },
        initialPreview: [
        catalog
        ],
        initialPreviewConfig: [
        {type: "pdf", caption: "<?=$catalog->name?>.pdf", downloadUrl: false,extra: {id: '<?=$catalog->id;?>'}},
        ],
        deleteUrl: "<?=base_url().'catalog/deletepic/'.$catalog->id?>",
    <?php }?> 
    maxFileCount: 1,
    validateInitialCount: true,
    overwriteInitial: false,
    showUpload: false,
    showRemove: true,
        //required: false,
        initialPreviewAsData: true,
        maxFileSize:300000,
        browseOnZoneClick: true,
        allowedFileExtensions: ["pdf"],
        browseLabel: "Catalog",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop catalog file here …",
    });

    $('#catalog_file').on('filebatchselected', function(event, files) {
    console.log(files);
});

</script>
