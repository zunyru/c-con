
<hr>
<h3>SEO</h3>
<div class="form-group form-float">
    <div class="form-line">
        <input type="text" class="form-control" name="seo_name" id="seo_name" value="<?=isset($seo->name)? $seo->name : '';?>" required>
        <input type="hidden" class="form-control" name="seo_id"  value="<?=isset($seo->id)? $seo->id : '';?>" required>
        <label class="form-label">Title</label>
    </div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        <textarea class="form-control" rows="3" id="robots" name="robots" required><?=isset($seo->robots)? $seo->robots : '';?></textarea>
        <label class="form-label">Robots</label>
    </div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        <textarea class="form-control" rows="3" id="keywords" name="keywords" required><?=isset($seo->keywords)? $seo->keywords : '';?></textarea>
        <label class="form-label">Keywords</label>
    </div>
</div>
<div class="form-group form-float">
    <div class="form-line">
        <textarea class="form-control" rows="3" id="description" name="description" required><?=isset($seo->description)? $seo->description : '';?></textarea>
        <label class="form-label">Description</label>
    </div>
</div>
