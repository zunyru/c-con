<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li><a href="<?=base_url('seo')?>">Seo</a></li>
                <li class="active"><?=$title_page?></li>
            </ol>
        </div>
        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Seo</h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <div class="body">
                    <?php 
                    if($page === 'create'): 
                        echo form_open_multipart(base_url().'seo/store','id="form_validation"');
                    else: 
                        echo form_open_multipart(base_url().'seo/update/'.$seo->id,'id="form_validation"');
                    endif;    
                    ?>
                    <input type="hidden" class="form-control" name="db" id="db" value="seo">
                    <input type="hidden" class="form-control" name="id" id="id" value="<?=isset($seo->id)? $seo->id : '';?>">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" id="name" value="<?=isset($seo->name)? $seo->name : '';?>" required>
                            <label class="form-label">Title</label>
                        </div>
                        <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="slug" id="slug" value="<?=isset($seo->slug)? $seo->slug : '';?>" required>
                            <label class="form-label">Slug</label>
                        </div>
                        <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea class="form-control" rows="3" id="robots" name="robots" required><?=isset($seo->robots)? $seo->robots : '';?></textarea>
                            <label class="form-label">Robots</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea class="form-control" rows="3" id="keywords" name="keywords" required><?=isset($seo->keywords)? $seo->keywords : '';?></textarea>
                            <label class="form-label">Keywords</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <textarea class="form-control" rows="3" id="description" name="description" required><?=isset($seo->description)? $seo->description : '';?></textarea>
                            <label class="form-label">Description</label>
                        </div>
                    </div>
                   

                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
</div>
</section>





