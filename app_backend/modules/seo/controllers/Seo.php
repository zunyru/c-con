<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seo extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->model('seo/seo_model');
		$this->load->library('uploadfile_library');
		$this->load->library('nestable_library');
		$this->load->library('logs_library');
	}
	
	public function index(){   
		$data = array(
			'page_index' 		=> 'backendlog',
			'page' 				=> 'seo',
			'title_page' 		=> 'Seo',
			'description_page' 	=> 'My Seo'
		);
		$data['seo'] = $this->seo_model->get_SeoAll();
	
		//loade view
		$this->load->view('masters/header',$data); 
		$this->load->view('index',$data);
	}

	public function edit($id) {
		$data = array(
			'page_index' 		=> 'backendlog',
			'page' 				=> 'editseo',
			'title_page' 		=> 'Seo Edit',
			'description_page' 	=> 'My Seo edit',
		);
        //loade data
		$data['seo'] = $this->seo_model->get_SeoById($id);

        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function update($id) {

		$user = $this->session->userdata('users');

		$data = array(
			'id'     		 => $id,
			'name'  		 => $this->input->post('name'),
			'slug'      	 => $this->input->post('slug'),
			'robots'      	 => $this->input->post('robots'),
			'description'    => $this->input->post('description'),
			'keywords'       => $this->input->post('keywords'),
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'active'    	 => 0,
			'create_at' 	 => date('Y-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		$last_id = $this->seo_model->update(array_to_obj($data));

		//Log
		$messege = 'แก้ไขข้อมูล : '.$this->input->post('name');

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('seo'));
		}
	} 
}
