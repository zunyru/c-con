<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li><a href="<?=base_url('categorys')?>">Banners</a></li>
                <li class="active"><?=$title_page?></li>
            </ol>
        </div>
        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Banners</h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <div class="body">
                        <?php 
                        if($page === 'create'): 
                            echo form_open_multipart(base_url().'banners/store','id="form_validation"');
                        else: 
                            echo form_open_multipart(base_url().'banners/update/'.$banners->id,'id="form_validation"');
                        endif;    
                        ?>
                        <input type="hidden" class="form-control" name="db" id="db" value="banners">
                        <input type="hidden" class="form-control" name="id" id="id" value="<?=isset($banners->id)? $banners->id : '';?>">
                  
                        <div class="form-group form-float">
                            <p>
                                รูป 
                            </p>
                            <div class="form-line">
                                <input id="picture" name="picture" type="file" class="file" data-preview-file-type="text">
                                <input type="hidden" name="outfile_pic" value="<?=(isset($banners->banners_img)) ? $banners->banners_img : ''; ?>">
                            </div>
                        </div> 
                        <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
</div>
</section>





