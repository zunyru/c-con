<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banners extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->model('banners/banners_model');
		$this->load->library('uploadfile_library');
		$this->load->library('nestable_library');
		$this->load->library('logs_library');
	}
	
	public function index(){   
		$data = array(
			'page_index' 		=> 'backendlog',
			'page' 				=> 'banners',
			'title_page' 		=> 'Banners Browse',
			'description_page' 	=> 'My Banners'
		);
		$data['banners'] = $this->banners_model->get_BannersAll();

		//loade view
		$this->load->view('masters/header',$data); 
		$this->load->view('index',$data);
	}

	public function create() {
		$data = array(
			'page_index' 		=> 'backendlog',
			'page' 				=> 'create',
			'title_page' 		=> 'Create',
			'description_page' 	=> 'My banners create',
		);
		
        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function store(){

		$user = $this->session->userdata('users');
 
		$path = 'banners';
		$upload_pic = $this->uploadfile_library->do_upload('picture',TRUE,$path);

        $picture ='';
		if(isset($upload_pic['index'])){
			$picture = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_pic['index']['file_name'];
		}

		$data = array(
			'banners_img'   => $picture,
			'create_by' 	=> $user['UID'],
			'update_by' 	=> $user['UID'],
			'create_at' 	=> date('Y-m-d H:i:s'),
			'update_at' 	=> date('Y-m-d H:i:s'),
		);

		$last_id = $this->banners_model->insert($data);	

		//Log
		$messege = 'เพิ่มข้อมูล banners : รูป : '.$picture;

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('banners'));
		}	

	}

	public function edit($id) {
		
		$data = array(
			'page_index' 		=> 'backendlog',
			'page' 				=> 'edit',
			'title_page' 		=> 'Edit',
			'description_page' 	=> 'My banners edit',
		);
		
        //loade data
		$data['banners'] = $this->banners_model->get_ฺBannersById($id);
		
        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function update($id) {

		$user = $this->session->userdata('users');

		$path = 'banners';
		$upload_pic = $this->uploadfile_library->do_upload('picture',TRUE,$path);
		$picture = null;
		if(isset($upload_pic['index'])){
			$picture = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_pic['index']['file_name'];
			$outfile_pic = $this->input->post('outfile_pic');
			if(isset($outfile_pic)){
				$this->load->helper("file");
				unlink($outfile_pic);
			}
		}

		if(isset($upload['error'])){
			$this->session->set_flashdata('status',0);
			redirect(base_url('banners'));
		}

		$data = array(
			'id'        	=> $id,
			'banners_img'   => $picture,
			'create_by' 	=> $user['UID'],
			'update_by' 	=> $user['UID'],
			'create_at' 	=> date('Y-m-d H:i:s'),
			'update_at' 	=> date('Y-m-d H:i:s'),
		);

		$last_id = $this->banners_model->update(array_to_obj($data));

		//Log
		$messege = 'แก้ไขข้อมูล banners : รูป : '.$picture;

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('banners'));
		}
	} 

	public function destroy($id){
		$user = $this->session->userdata('users');
		
		$data = $this->banners_model->get_ฺBannersById($id);
		
		if(!empty($data->banners_img)){
			$this->load->helper("file");
			unlink($data->banners_img);
		}
		
		$delete = $this->banners_model->destroy($id);
		
		if($delete){
			//Log
			$messege = 'ลบข้อมูล banners : รูป '.$data->banners_img; 

			$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

			$this->session->set_flashdata('status',1);
			redirect(base_url('banners'));
		}else{
			$this->session->set_flashdata('status',0);
			redirect(base_url('banners'));
		}
	}

	public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'banners';
		$upload = $this->uploadfile_library->do_upload('file',TRUE,$path);

		if(isset($upload['index'])){
			$picture = '/uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
	}	

	public function deletebanners($ids){

		$arrayName = array('banners_img' => $ids);

		echo json_encode($arrayName);
	}

		
}
