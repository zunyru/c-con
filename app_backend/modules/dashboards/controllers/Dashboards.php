<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboards extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->model('products/product_model');
		$this->load->model('categorys/category_model');
		$this->load->model('jobsregisters/jobsregisters_model');
		$this->load->model('download/download_model');
		$this->load->model('quotations/quotations_model');
		
		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->library('logs_library');
	}

	public function index(){   
		$data = array(
			'page_index' 		=> 'dashboard',
			'page' 				=> 'dashboard',
			'title_page' 		=> 'Dashboard',
			'description_page' 	=> 'My Dashboard'
		);

		//load data
		$data['proCount'] 		= $this->product_model->get_productCount();
		$data['cateCount'] 		= $this->category_model->get_categorieCount();
		$data['jobregisCount'] 	= $this->jobsregisters_model->get_jobsRegisCount();
		$data['downloadCount'] 	= $this->download_model->get_downloadCount();

		$data['download']  		= $this->download_model->getLogLimit();
		$data['jobregis'] 		= $this->jobsregisters_model->getJobsregistersType(0);
		$data['quotations'] 	= $this->quotations_model->getQuotationsType(0);
		
		//loade view
		$this->load->view('masters/header',$data); 
		$this->load->view('index',$data);
	}
}
