<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">สิงค้า</div>
                            <div class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"><?=$proCount?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">bookmarks</i>
                        </div>
                        <div class="content">
                            <div class="text">ประเภทสินค้า</div>
                            <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"><?=$cateCount?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">get_app</i>
                        </div>
                        <div class="content">
                            <div class="text">ผู้ดาวน์โหลด</div>
                            <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20"><?=$downloadCount?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">ผู้สมัคร</div>
                            <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"><?=$jobregisCount?></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->
            <!-- CPU Usage -->
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>รายการผู้ดาวน์โหลด</h2>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                        <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th style="width:20%">วันที่</th>
                                            <th style="width:30%">ชื่อ-สกุล</th>
                                            <th style="width:20%">อีเมล์</th>
                                            <th style="width:20%">ประเภท</th>
                                            <th style="width:10%">ไฟล์</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if(isset($download)){
                                            foreach($download as $item){
                                                if($item->type == 0){
                                                    $text = 'All Catalog';
                                                }else if($item->type == 1){
                                                    $text = 'By Catalog';
                                                }else{
                                                    $text = 'By Product';
                                                }
                                                echo '<tr>';
                                                    echo '<td>'.DateThai($item->create_at).'</td>';
                                                    echo '<td>'.$item->name.'</td>';
                                                    echo '<td>'.$item->email.'</td>';
                                                    echo '<td>'.$text.'</td>';
                                                    echo '<td><a href="'.$this->config->item('backend_uploade').$item->file.'" target="_blank" rel="">file</a></td>';
                                                echo '<tr>';
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <a href="<?=base_url('download')?>">ดูทั้งหมด</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# CPU Usage -->
            <div class="row clearfix">
                <!-- Latest Social Trends -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>รายการใบเสนอราคา</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>วันที่</th>
                                            <th>ผู้ขอ</th>
                                            <th>อีเมล</th>
                                            <th>เบอร์โทรศัทพ์</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(isset($quotations)){
                                            foreach($quotations as $item){
                                                echo '<tr>';
                                                    echo '<td>'.DateThai($item->create_at).'</td>';
                                                    echo '<td>'.$item->name.'</td>';
                                                    echo '<td>'.$item->email.'</td>';
                                                    echo '<td>'.$item->tel.'</td>';
                                                echo '<tr>';
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <a href="<?=base_url('quotations')?>">ดูทั้งหมด</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Latest Social Trends -->
                <!-- Answered Tickets -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-teal">
                            <div class="font-bold m-b--35">
                                รายชื่อผู้สมัคร 
                                <a href="<?=base_url('jobsregisters')?>" style="float: right;color: #fff;">ดูทั้งหมด</a>
                            </div>
                            <ul class="dashboard-stat-list">
                                <?php
                                if(isset($jobregis)){
                                    
                                    foreach($jobregis as $item){
                                        echo '<li>';
                                            echo 'ชื่อ-นามสกุล '.$item->name;
                                            echo '<span class="pull-right"><small>'.DateThai($item->create_at).'</small></span>';
                                        echo '</li>';
                                    }
                                }
                                ?>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Answered Tickets -->
            </div>
        </div>
    </section>