<?php

/**
 *
 */
class Category_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getCategoryAll() {

		$this->db->select('*');
		$this->db->from('categories cat');
		$this->db->order_by('cat.name', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_categories()
	{

		$this->db->select('*');
		$this->db->from('categories');
		$this->db->where('parent_id', 0);
		$this->db->order_by('sequence','ASC');

		$parent = $this->db->get();

		$categories = $parent->result();
		$i=0;
		foreach($categories as $p_cat){

			$categories[$i]->sub = $this->sub_categories($p_cat->id);
			$i++;
		}

		return $categories;
	}

	public function sub_categories($id){

		$this->db->select('*');
		$this->db->from('categories');
		$this->db->where('parent_id', $id);
		$this->db->order_by('sequence','ASC');

		$child = $this->db->get();
		$categories = $child->result();
		$i=0;
		foreach($categories as $p_cat){

			$categories[$i]->sub = $this->sub_categories($p_cat->id);
			$i++;
		}
		return $categories;       
	}


	public function get_CategoryById($id) {

		$this->db->select('*');
		$this->db->from('categories');
		$this->db->where('id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->id;
			$row->name;
			$row->icon;
			$row->parent_id;
		}

		return $row;
	}

	public function getCategoryRelateAll() {

		$this->db->select('*');
		$this->db->from('categories');
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function getCategoryRelateOnMaster() {

		$this->db->select('*');
		$this->db->from('categories cat');
		$this->db->where('cat.parent_id', 0);
		$this->db->order_by('cat.name', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function insert($data)
	{
		$query = $this->db->insert('categories', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}

	public function update($data)
	{ 

		$this->db->set('name', $data->name);
		$this->db->set('slug', $data->slug);
		if(!is_null($data->icon)){
			$this->db->set('icon', $data->icon);
		}

		if(!is_null($data->file)){
			$this->db->set('file', $data->file);
		}
		
		$this->db->set('create_by', $data->create_by);
		$this->db->set('update_by', $data->update_by);
		$this->db->set('parent_id', $data->parent_id);
		$this->db->set('create_at', $data->create_at);
		$this->db->set('update_at', $data->update_at);
		$this->db->where('id', $data->id);
		$update = $this->db->update('categories');

		return $update;
	}

	public function destroy($id)
	{
		$this->db->where('id', $id);
		$delete = $this->db->delete('categories');

		return $delete;
	}

	public function get_categorieCount(){
		return $this->db->count_all_results('categories');
	}
	
}

?>