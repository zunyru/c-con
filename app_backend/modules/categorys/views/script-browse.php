<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript">

    $('.cat-exportable').DataTable({
        dom: 'Bfrtip',
        responsive: true,
        ordering : false,
        columns: [
        { data: 'name' },
        { data: 'position' },
        { data: 'office' },
        { data: 'extn' },
        { data: 'start_date' },
        ],
        buttons: [
            //'copy', 'excel', 'pdf',
            'excel', 
            {
                extend: 'print',
                exportOptions: {
                    columns: ':visible'
                },
                customize: function (win) {
                    $(win.document.body).find('table').addClass('display').css('font-size', '14px');
                    $(win.document.body).find('tr th:nth-child(5)').each(function(index){
                        $(this).css('display','none');
                    });
                    $(win.document.body).find('tr td:nth-child(5)').each(function(index){
                        $(this).css('display','none');
                    });
                    $(win.document.body).find('h1').css('text-align','center');
                }
            },


            ],

        });

    $('.delete').confirm({
        title: 'Delete Confirm!',
        content: 'Are you sure you want to delete this ?',
        buttons: {
            OK: function(){
                location.href = this.$target.attr('href');
            },
            Cancle : function(){
               
            },
        },

    });
    
</script>
