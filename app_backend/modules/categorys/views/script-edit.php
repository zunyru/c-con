
<script type="text/javascript">
    

    var required_icon = true; 
    var icon = <?=$cat->parent_id;?>;
    if(icon === 0){
       $('.icon').show();
    }else{
       $('.icon').hide(); 
    }
    
    $("#category").on("change", function () { 

           if($(this).val() == 0){
              required_icon = true
               $('.icon').show()
           }else{
              required_icon = true
               $('.icon').hide()
           }
    });

    //fileinput
    var icon_image = '<?=(isset($cat->icon)) ? $this->config->item('backend_uploade').$cat->icon : ''; ?>';
    $('#icon').fileinput({

        initialPreview: [icon_image],
        initialPreviewAsData: true,
        initialPreviewConfig: [
          { downloadUrl: icon_image  ,extra: {id: '<?=$cat->id;?>'} },
        ],
        deleteUrl: "<?=base_url().'deleteicon/'.$cat->id?>",

        maxFileCount: 1,
        validateInitialCount: true,
        overwriteInitial: false,
        showUpload: false,
        showRemove: true,
        required: required_icon,
        initialPreviewAsData: true,
        maxFileSize:300,
        browseOnZoneClick: true,
        allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
        browseLabel: "Icon Image",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop icon here …",
    });

    
    var catalog = '<?=(!empty($cat->file)) ? $this->config->item('backend_uploade').$cat->file : ''; ?>';
   // var catalog = 'http://plugins.krajee.com/uploads/sample-2.pdf';
   $('#catalog').fileinput({
      <?php if(!empty($cat->file)){?>
        initialPreview: [catalog],
        //pdfRendererUrl: 'http://plugins.krajee.com/pdfjs/web/viewer.html',
        overwriteInitial: false,
        initialPreviewAsData: true,
        preferIconicPreview: true,
        previewFileIconSettings: { 
            'pdf': '<i class="material-icons" style="font-size: 100%;color:#fea002;">picture_as_pdf</i>',
        },
        initialPreview: [
        catalog
        ],
        initialPreviewConfig: [
        {type: "pdf", caption: "<?=$cat->name?>.pdf", downloadUrl: false,extra: {id: '<?=$cat->id;?>'}},
        ],
        deleteUrl: "<?=base_url().'deleteategory/'.$cat->id?>",
    <?php }?> 
    maxFileCount: 1,
    validateInitialCount: true,
    overwriteInitial: false,
    showUpload: false,
    showRemove: true,
        //required: false,
        initialPreviewAsData: true,
        //maxFileSize:300,
        browseOnZoneClick: true,
        allowedFileExtensions: ["pdf"],
        browseLabel: "Catalog",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop catalog file here …",
    });


</script>
