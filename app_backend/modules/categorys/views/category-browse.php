<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>

                <ol class="breadcrumb">
                    <li class="active">Category</li>
                </ol>
                <!-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> -->
            </h2>
        </div>
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            ข้อมูลกลุ่มสินค้า
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <a href="<?=base_url('categorys/create')?>" class="btn bg-deep-orange waves-effect">Create</a>
                            <a href="<?=base_url('categorys/sort')?>" class="btn bg-deep-orange waves-effect">Sort</a>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">

                            <table class="table table-bordered table-striped table-hover dataTable cat-exportable">
                                <thead>
                                    <tr>
                                        <th width="30%">Name</th>
                                        <th width="25%">Slug</th>
                                        <th width="10%">Icon</th>
                                        <th width="25%">Upage date</th>
                                        <th width="10%">Action</th>
                                        
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                     <th>Name</th>
                                     <th>Slug</th>
                                     <th>Icon</th>
                                     <th>Upage date</th>
                                     <th>Action</th>
                                 </tr>
                             </tfoot>
                             <tbody>

                                <?php 
                                foreach ($categories as $index => $category): 
                                    ?>
                                    <tr>
                                        <td><?=$category->name?></td>
                                        <td><?=$category->slug?></td>
                                        <td>
                                            <?php if(!empty($category->icon)):?>
                                                <img width="50" src="<?=$this->config->item('backend_uploade').$category->icon?>" alt="<?=$category->name?>">
                                            <?php endif;?>

                                        </td>
                                        <td><?=DateThai($category->update_at)?></td>
                                        <td>
                                            <a href="<?=base_url().'categorys/edit/'.$category->id?>" class="btn bg-orange btn-block waves-effect">Edit</a>

                                            <a href="<?=base_url().'categorys/destroy/'.$category->id?>" class="btn bg-red btn-block waves-effect delete">Delete</a>


                                        </td>
                                    </tr>
                                    <?php if(sizeof($category->sub)): 
                                        foreach ($category->sub as $index => $category):?>
                                            <tr>
                                                <td style="padding-left: 3%;"><?="- ".$category->name?></td>
                                                <td><?=$category->slug?></td>
                                                <td>
                                                    <?php if(!empty($category->icon)):?>
                                                        <img width="50" src="<?=$this->config->item('backend_uploade').$category->icon?>" alt="<?=$category->name?>">
                                                    <?php endif;?>
                                                </td>
                                                <td><?=DateThai($category->update_at)?></td>
                                                <td>
                                                 <a href="<?=base_url().'categorys/edit/'.$category->id?>" class="btn bg-orange btn-block waves-effect">Edit</a>

                                                 <a href="<?=base_url().'categorys/destroy/'.$category->id?>" class="btn bg-red btn-block waves-effect delete">Delete</a>
                                             </td>
                                         </tr>
                                         <?php if(sizeof($category->sub) > 0):
                                          foreach ($category->sub as $index => $category):?>

                                            <tr>
                                                <td style="padding-left: 6%;"><?="- ".$category->name?></td>
                                                <td><?=$category->slug?></td>
                                                <td>
                                                    <?php if(!empty($category->icon)):?>
                                                        <img width="50" src="<?=$this->config->item('backend_uploade').$category->icon?>" alt="<?=$category->name?>">
                                                    <?php endif;?>
                                                </td>
                                                <td><?=DateThai($category->update_at)?></td>
                                                <td>
                                                   <a href="<?=base_url().'categorys/edit/'.$category->id?>" class="btn bg-orange btn-block waves-effect">Edit</a>
                                                   <a href="<?=base_url().'categorys/destroy/'.$category->id?>" class="btn bg-red btn-block waves-effect delete">Delete</a>
                                               </td>
                                           </tr>
                                           <?php 
                                       endforeach; 
                                   endif;
                                   ?>
                                   <?php 
                               endforeach; 
                           endif;
                           ?>

                           <?php 
                       endforeach;
                       ?>

                   </tbody>
               </table>
           </div>
       </div>
   </div>
</div>
</div>
<!-- #END# Exportable Table -->
</div>
</section>


