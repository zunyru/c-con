<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li><a href="<?=base_url('categorys')?>">Category</a></li>
                <li class="active">Sort</li>
            </ol>
        </div>
        <div class="row clearfix">
            <!-- Default Example -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            จัดกลุ่มประเภทสินค้า
                            <small>เรียงลำดับประเภทสินค้า</small>
                        </h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <div class="body">
                        <div class="clearfix m-b-20">
                            <?php echo $layout;?>
                        </div>
                        <b>Output JSON</b>
                        <textarea cols="30" rows="3" class="form-control no-resize" readonly></textarea>
                    </div>
                </div>
            </div>
            <!-- #END# Default Example -->
        </div>
        <!-- #END# Draggable Handles -->
    </div>
</section>
