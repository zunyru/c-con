<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li><a href="<?=base_url('categorys')?>">Category</a></li>
                <li class="active"><?=$title_page?></li>
            </ol>
        </div>
        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>กลุ่มสินค้า</h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <div class="body">
                        <?php 
                        if($page === 'create'): 
                            echo form_open_multipart(base_url().'categorys/store','id="form_validation"');
                        else: 
                            echo form_open_multipart(base_url().'categorys/update/'.$cat->id,'id="form_validation"');
                        endif;    
                        ?>
                        <input type="hidden" class="form-control" name="db" id="db" value="categories">
                        <input type="hidden" class="form-control" name="id" id="id" value="<?=isset($cat->id)? $cat->id : '';?>">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="name" id="name" value="<?=isset($cat->name)? $cat->name : '';?>" required>
                                <label class="form-label">ชื่อกลุ่มสินค้า</label>
                            </div>
                            <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="slug" id="slug" value="<?=isset($cat->slug)? $cat->slug : '';?>" required>
                                <label class="form-label">Slug</label>
                            </div>
                            <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                        </div>
                        <div class="form-group form-float">
                            <input type="hidden" name="category" id="category" value="0">
                            <p>
                                รูป Icon (ขนาด 25 x 25 px)
                            </p>
                            <div class="form-line">
                                <input id="icon" name="icon" type="file" class="file" data-preview-file-type="text">
                                <input type="hidden" name="outfile" value="<?=(isset($cat->icon)) ? $cat->icon : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group form-float">
                        <p>
                            Catalog : (File pdf only)
                        </p>
                        <div class="form-line">
                            <input id="catalog" name="catalog" type="file" class="file" data-preview-file-type="text">
                            <input type="hidden" name="outfile_catalog" value="<?=(isset($cat->file)) ? $cat->file : ''; ?>">
                            </div>
                        </div> 


                        <?php
                        // load view seo
                        $this->load->view('seo/seo');
                        ?>

                        <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Validation -->
</div>
</section>





