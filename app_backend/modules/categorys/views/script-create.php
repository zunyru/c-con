
<script type="text/javascript">
    var required_icon = true; 
    $('.icon').hide();
    $("#category").on("change", function () { 

           if($(this).val() == 0){
              required_icon = true
               $('.icon').show()
           }else{
              required_icon = true
               $('.icon').hide()
           }
    });
  
    //fileinput
    $('#icon').fileinput({
        maxFileCount: 1,
        validateInitialCount: true,
        overwriteInitial: false,
        showUpload: false,
        showRemove: true,
        required: required_icon,
        initialPreviewAsData: true,
        maxFileSize:300,
        browseOnZoneClick: true,
        allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
        browseLabel: "Icon Image",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop icon here …",
    });
    
    
    $('#catalog').fileinput({
        maxFileCount: 1,
        validateInitialCount: true,
        overwriteInitial: false,
        showUpload: false,
        showRemove: true,
        //required: false,
        initialPreviewAsData: true,
        //maxFileSize:300,
        browseOnZoneClick: true,
        allowedFileExtensions: ["pdf"],
        browseLabel: "Catalog",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop catalog file here …",
    });
</script>
