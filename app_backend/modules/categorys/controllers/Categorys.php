<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorys extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->model('categorys/category_model');
		$this->load->model('seo/seo_model');
		$this->load->library('uploadfile_library');
		$this->load->library('nestable_library');
		$this->load->library('logs_library');
	}

	public function index() {
		$flash_message = $this->session->flashdata('status');	
		$data = array(
			'page_index' => 'category',
			'page' => 'browse',
			'title_page' => 'ข้อมูลประเภทสินค้า',
			'description_page' => 'My Category',
			'flash_message' => $flash_message,
		);
		
		$data['categories']  =  $this->category_model->get_categories();

		//loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
		
	}

	public function sort() {
		$data = array(
			'page_index' => 'category',
			'page' => 'sort',
			'title_page' => 'Browse',
			'description_page' => 'My Category',
		);
		$data['categories']  =  $this->category_model->get_categories();
		$data['layout'] = $this->nestable_library->get_layout_ol_master($data['categories']);
		//loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	
	public function create() {
		$data = array(
			'page_index' => 'category',
			'page' => 'create',
			'title_page' => 'Create',
			'description_page' => 'My Category create',
		);
        //loade data
		$data['categorys'] = $this->category_model->getCategoryRelateOnMaster();

        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function store(){

		$user = $this->session->userdata('users');

		$path = 'icon';
		$upload = $this->uploadfile_library->do_upload('icon',TRUE,$path);

		$upload_catalog = $this->uploadfile_library->do_upload('catalog',TRUE,$path);

        $icon = '';
		if(isset($upload)){
			$icon = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		$catalog = ''; 
		if(isset($upload_catalog['index'])){
			$catalog = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_catalog['index']['file_name'];
		}

		
       
		if(isset($upload['error'])){
			$this->session->set_flashdata('status',0);
			redirect(base_url('categorys'));
		}

		$dataseo = array(
			'name'  		 => $this->input->post('seo_name'),
			'robots'      	 => $this->input->post('robots'),
			'description'    => $this->input->post('description'),
			'keywords'       => $this->input->post('keywords'),
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'active'    	 => 0,
			'create_at' 	 => date('Y-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		// load add seo

		$seo_id = $this->seo_model->insert($dataseo);

		$data = array(
			'name'      => $this->input->post('name'),
			'slug'      => $this->input->post('slug'),
			'icon'      => $icon,
			'file'      => $catalog,
			'seo_id'	=> $seo_id,
			'create_by' => $user['UID'],
			'update_by' => $user['UID'],
			'parent_id' => $this->input->post('category'),
			'active'    => 0,
			'create_at' => date('Y-m-d H:i:s'),
			'update_at' => date('Y-m-d H:i:s'),
		);

		$last_id = $this->category_model->insert($data);

            //Log
		$messege = 'เพิ่มข้อมูล ประเภทสินค้า : '.$this->input->post('name') .',ประเภท : '.$this->input->post('category');

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('categorys'));
		}

	}

	public function edit($id) {
		$data = array(
			'page_index' => 'category',
			'page' => 'edit',
			'title_page' => 'Edit',
			'description_page' => 'My Category edit',
		);
        //loade data
		$data['categorys'] = $this->category_model->getCategoryRelateOnMaster();
		$data['cat'] = $this->category_model->get_CategoryById($id);
		$data['seo'] = $this->seo_model->get_SeoById($data['cat']->seo_id);

        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function update($id){

		$user = $this->session->userdata('users');

		$path = 'icon';
		$upload = $this->uploadfile_library->do_upload('icon',TRUE,$path);

		$upload_catalog = $this->uploadfile_library->do_upload('catalog',TRUE,$path);

		$icon = null; 
		if(isset($upload['index'])){
			$icon = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];	
			$outfile = $this->input->post('outfile');
			if(isset($outfile)){
				$this->load->helper("file");
				unlink($outfile);
			}
		}

		$catalog = null; 
		if(isset($upload_catalog['index'])){
			$catalog = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_catalog['index']['file_name'];
			$outfile_catalog = $this->input->post('outfile_catalog');
			if(isset($outfile_catalog)){
				$this->load->helper("file");
				unlink($outfile_catalog);
			}
		}

		// if(isset($upload['error'])){
		// 	$this->session->set_flashdata('status',0);
		// 	redirect(base_url('categorys'));
		// }

		$dataseo = array(
			'id'        	 => $this->input->post('seo_id'),
			'name'  		 => $this->input->post('seo_name'),
			'robots'      	 => $this->input->post('robots'),
			'description'    => $this->input->post('description'),
			'keywords'       => $this->input->post('keywords'),
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'active'    	 => 0,
			'type'			 => 1,	
			'create_at' 	 => date('Y-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		//  // load update seo
		$this->seo_model->update(array_to_obj($dataseo));
		

		$data = array(
			'id'        => $id,
			'name'      => $this->input->post('name'),
			'slug'      => $this->input->post('slug'),
			'icon'      => $icon,
			'file'   	=> $catalog,
			'create_by' => $user['UID'],
			'update_by' => $user['UID'],
			'parent_id' => $this->input->post('category'),
			'active'    => 0,
			'create_at' => date('Y-m-d H:i:s'),
			'update_at' => date('Y-m-d H:i:s'),
		);

		$last_id = $this->category_model->update(array_to_obj($data));

		//Log
		$messege = 'แก้ไขข้อมูล ประเภทสินค้า : '.$this->input->post('name') 
		.',ประเภท : '.$this->input->post('category')
		.',Seo :'.$this->input->post('seo_name');

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('categorys'));
		}

	}

	public function destroy($id)
	{
		$user = $this->session->userdata('users');
		
		$data = $this->category_model->get_CategoryById($id);

		if(isset($data->icon)){
			$this->load->helper("file");
			unlink($data->icon);
		}

		if(isset($data->file)){
			$this->load->helper("file");
			unlink($data->file);
		}
		
		$delete = $this->category_model->destroy($id);
		$delete = $this->seo_model->destroy($data->seo_id);

		if($delete){
			$messege = 'ลบข้อมูล ประเภทสินค้า : '.$data->name
			.', Seo id'.$data->seo_id; 

		    $this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

			$this->session->set_flashdata('status',1);
			redirect(base_url('categorys'));
		}else{
			$this->session->set_flashdata('status',0);
			redirect(base_url('categorys'));
		}
	}

	public function ajax_sort()
    {
		$resize  = $this->input->post('resize');
		$arr_ = array();
		foreach($resize as $item): 
			$arr_sequence = array('sequence'=> $item['line']);
			$this->db->update('categories', $arr_sequence, array('id'=>$item['id']));
		endforeach;
        $status = 1;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($status));
    }

	public function deleteIcon($ids)
	{

		$arrayName = array('icon' => $ids);

		echo json_encode($arrayName);
	}

	public function deleteategory($ids)
	{

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
	}


}
