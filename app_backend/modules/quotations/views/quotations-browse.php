<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>

                <ol class="breadcrumb">
                    <li class="active">ใบเสนอราคา</li>
                </ol>
                <!-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> -->
            </h2>
        </div>
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            รายการใบเสนอราคา
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable pro-exportable">
                                <thead>
                                    <tr>
                                        <th width="30%">ชื่อ-สกุล</th>
                                        <th width="20%">ชื่อบริษัท</th>
                                        <th width="20%">เลขที่เสียภาษี</th>
                                        <th width="20%">Email</th>
                                        <th width="20%">Status</th>
                                        <th width="10%">Craet date</th>
                                        <th width="10%">Upage date</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>ชื่อ-สกุล</th>
                                        <th>ชื่อบริษัท</th>
                                        <th>เลขที่เสียภาษี</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Craet date</th>
                                        <th>Upage date</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php 
                                    foreach ($quotations as $index => $item): 
                                        ?>
                                        <tr>
                                            <td><?=$item->name?></td>
                                            <td><?=isset($item->company)? $item->company : 'ไม่มี';?></td>
                                            <td><?=isset($item->company_address)? $item->company_address : 'ไม่มี';?></td>
                                            <td><?=isset($item->email)? $item->email : 'ไม่มี';?></td>
                                            <td>
                                                <?php
                                                if($item->send > 0){ echo 'อ่านแล้ว';}else{ echo 'รออ่าน';}
                                                ?>
                                            </td>
                                            <td>
                                                <span style="display: none;">
                                                    <?=$item->create_at?>
                                                </span>
                                                <?=DateThai($item->create_at)?></td>
                                            <td>
                                                <span style="display: none;">
                                                    <?=$item->update_at?>
                                                </span>
                                                <?=DateThai($item->update_at)?>
                                            </td>
                                            <td>
                                                <a href="<?=base_url().'quotations/edit/'.$item->quotations_id?>" class="btn bg-orange btn-block waves-effect">Privew</a>

                                               <a href="<?=base_url().'quotations/destroy/'.$item->quotations_id?>" class="btn bg-red btn-block waves-effect delete">Delete</a>

                                           </td>
                                       </tr>
                                       <?php 
                                   endforeach; 
                                   ?>    

                               </tbody>
                           </table>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <!-- #END# Exportable Table -->
   </div>
</section>

