<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li><a href="<?=base_url('quotations')?>">Quotations</a></li>
                <li class="active"><?=$title_page?></li>
            </ol>
        </div>
        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Privew quotations</h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <div class="body">
                        <?php 
                            echo form_open_multipart(base_url().'quotations/update/'.$quotations->quotations_id,'id="form_validation"' , 'class="form-horizontal"');  
                        ?>
                        <input type="hidden" class="form-control" name="db" id="db" value="jobs">
                        <input type="hidden" class="form-control" name="quotations_id" id="id" value="<?=isset($quotations->quotations_id)? $quotations->quotations_id : '';?>">
                        
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">ชื่อ-นามสกุล</label>
                                <div class="col-md-10">
                                    <p><?=isset($quotations->name)? $quotations->name : '';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">ตำแหน่ง</label>
                                <div class="col-md-10">
                                    <?=isset($quotations->position)? $quotations->position : 'ว่าง';?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">ชื่อบริษัท</label>
                                <div class="col-md-10">
                                    <?=isset($quotations->company)? $quotations->company : 'ว่าง';?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">ที่อยู่บริษัท</label>
                                <div class="col-md-10">
                                    <?=isset($quotations->company_address)? $quotations->company_address : 'ว่าง';?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">เลขที่เสียภาษี</label>
                                <div class="col-md-10">
                                    <?=isset($quotations->taxpayer_number)? $quotations->taxpayer_number : 'ว่าง';?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">อีเมล์</label>
                                <div class="col-md-10">
                                    <?=isset($quotations->email)? $quotations->email : 'ว่าง';?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">เบอร์โทร</label>
                                <div class="col-md-10">
                                    <?=isset($quotations->tel)? $quotations->tel : 'ว่าง';?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">รายละเอียดสินค้าที่ต้องการ</label>
                                <div class="col-md-10">
                                    <?=isset($quotations->product_detail)? $quotations->product_detail : 'ว่าง';?>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="form-group">
                                <h3 class="col-md-12">สถานะการตรวจสอบ</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">สถานะ</label>
                                <div class="col-md-5">
                                    <?php
                                    if($quotations->send > 0){
                                        $send = 'checked';
                                    }else{
                                        $send = '';
                                    }
                                    ?>
                                    <div class="switch panel-switch-btn">
                                        <label>รออ่าน
                                            <input type="checkbox" id="realtime" name="send" <?=$send?>>
                                            <span class="lever switch-col-cyan"></span>อ่านแล้ว
                                        </label>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
</div>
</section>





