<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotations extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->model(array('quotations/quotations_model'));	
		$this->load->library('nestable_library');
		$this->load->library('logs_library');
	}
	
	public function index(){   
		$data = array(
			'page_index' 		=> 'quotations',
			'page' 				=> 'browse',
			'title_page' 		=> 'ใบเสนอราคา',
			'description_page' 	=> 'My quotations'
		);
		//load quotation
		$data['quotations'] = $this->quotations_model->getQuotationsAll();
	
		//loade view
		$this->load->view('masters/header',$data); 
		$this->load->view('index',$data);
	}

	public function edit($id) {
		$data = array(
			'page_index' 		=> 'quotations',
			'page' 				=> 'send',
			'title_page' 		=> 'Privew quotations',
			'description_page' 	=> 'My privew quotations',
		);
        //loade data
		$data['quotations'] = $this->quotations_model->get_QuotationsById($id);
		
        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function update($id) {

		$user = $this->session->userdata('users');
		if($this->input->post('send') == "on"){$send = 1;}else{$send = 0;}
		$data = array(
			'quotations_id'  => $id,
			'send'      	 => $send ,
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'create_at' 	 => date('Ycatalog-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		$last_id = $this->quotations_model->update(array_to_obj($data));

		//Log
		$messege = 'Quotations : '.$read;

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('quotations'));
		}
	} 

	public function destroy($id){
		$user = $this->session->userdata('users');
		
		$data = $this->quotations_model->get_QuotationsById($id);
		
		$delete = $this->quotations_model->destroy($id);


		if($delete){
			//Log
			$messege = 'ลบข้อมูล quotations : '.$data->name; 

			$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

			$this->session->set_flashdata('status',1);
			redirect(base_url('quotations'));
		}else{
			$this->session->set_flashdata('status',0);
			redirect(base_url('quotations'));
		}
	}
	
}
