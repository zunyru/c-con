<body class="theme-deep-orange">
    <!-- Page Loader -->
    <?php $this->load->view('masters/loader');?>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <?php $this->load->view('masters/search');?>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <?php $this->load->view('masters/navbar');?>
    <!-- #Top Bar -->
    <!-- Left Sidebar -->
    <?php $this->load->view('masters/left-sidebar');?>
    <!-- #END# Left Sidebar -->


    <?php $this->load->view('download-browse');?>

    <!-- script -->
    <?php $this->load->view('masters/script');?>

    <?php 
        $this->load->view('script-browse');     
    ?>
            <!-- #END# script -->

    </body>

</html>