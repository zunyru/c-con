<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                <ol class="breadcrumb">
                    <li class="active">Download</li>
                </ol>
                <!-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> -->
            </h2>
        </div>
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            ข้อมูล Log download catalog
                        </h2>
                        
                    </div>
                    <div class="body">
                        <div class="table-responsive">

                            <table class="table table-bordered table-striped table-hover dataTable log-exportable">
                                <thead>
                                    <tr>
                                        <th width="23%">Date</th>
                                        <th width="20%">User name</th>
                                        <th width="17%">Company</th>
                                        <th width="17%">Email</th>
                                        <th width="10%">Type</th>
                                        <th width="10%">File</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                     <th>Date</th>
                                     <th>User name</th>
                                     <th>Conpany</th>
                                     <th>Email</th>
                                     <th>Type</th>
                                     <th>File</th>
                                 </tr>
                             </tfoot>
                             <tbody>

                                <?php 
                                foreach ($download as $index => $item):

                                    if($item->type == 0){
                                        $text = 'All Catalog';
                                    }else if($item->type == 1){
                                        $text = 'By Catalog';
                                    }else{
                                        $text = 'By Product';
                                    }
                                    ?>
                                    <tr>
                                       <td>
                                        <p style="display: none;"><?=$item->create_at?></p>
                                            <?=DateThai($item->create_at)?>
                                        </td>
                                        <td><?=$item->name?></td>
                                        <td><?=$item->address?></td>
                                        <td><?=$item->email?></td>
                                        <td><?=$text?></td>
                                        <?php
                                        if(!empty($item->file)){
                                        ?>
                                         <td><a href="<?=$this->config->item('backend_uploade').$item->file?>" target="_blank" rel="">file</a></td>
                                        <?php
                                        }else{
                                        ?>
                                         <td>Not File</td>
                                        <?php
                                        }
                                        ?>
                                       
                                    </tr>
                                    <?php 
                                endforeach;
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->
</div>
</section>


