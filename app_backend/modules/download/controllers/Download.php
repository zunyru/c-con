<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->model('download/download_model');
		$this->load->library('logs_library');
	}

	public function index() {
		$flash_message = $this->session->flashdata('status');	
		$data = array(
			'page_index' 		=> 'backendlog',
			'page' 				=> 'download',
			'title_page' 		=> 'Download',
			'description_page'  => 'My Log download',
			'flash_message' 	=> $flash_message,
		);
		
		$data['download']  =  $this->download_model->getLogAll();


		//loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
		
	}

}
