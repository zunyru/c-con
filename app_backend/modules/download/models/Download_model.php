<?php 

/**
 * 
 */
class Download_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function getLogAll() {
		
		$this->db->order_by('create_at', 'DESC');
		$query = $this->db->get('catalog_download_log');

		return $query->result();
	}

	public function getLogLimit() {
		
		$this->db->order_by('create_at', 'DESC');
		$this->db->limit(5);
		$query = $this->db->get('catalog_download_log');

		return $query->result();
	}

	public function get_downloadCount(){
		return $this->db->count_all_results('catalog_download_log');
	}
}

?>