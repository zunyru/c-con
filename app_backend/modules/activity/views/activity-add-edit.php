<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li><a href="<?=base_url('activity')?>">Activity</a></li>
                <li class="active"><?=$title_page?></li>
            </ol>
        </div>
        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Activity</h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <div class="body">
                        <?php 
                        if($page === 'create'): 
                            echo form_open_multipart(base_url().'activity/store','id="form_validation"');
                        else: 
                            echo form_open_multipart(base_url().'activity/update/'.$activity->id,'id="form_validation"');
                        endif;    
                        ?>
                        <input type="hidden" class="form-control" name="db" id="db" value="activity">
                        <input type="hidden" class="form-control" name="id" id="id" value="<?=isset($activity->jobs_id)? $activity->activity_id : '';?>">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="activity_name" id="name" value="<?=isset($activity->name)? $activity->name : '';?>" required>
                                <label class="form-label">Title</label>
                            </div>
                            <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="slug" id="slug" value="<?=isset($activity->slug)? $activity->slug : '';?>" required>
                                <label class="form-label">Slug</label>
                            </div>
                            <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Category</label>
                            <select class="form-control show-tick" required name="activity_category">
                                <option value="">-- Please select --</option>
                                <option value="รีวิว" <?=isset($activity->activity_category) && ($activity->activity_category == 'รีวิว')? 'selected' : '';?>>รีวิว</option>
                                <option value="กิจกรรม" <?=isset($activity->activity_category) && ($activity->activity_category == 'กิจกรรม')? 'selected' : '';?>>กิจกรรม</option>
                                <option value="ข่าวสาร" <?=isset($activity->activity_category) && ($activity->activity_category == 'ข่าวสาร')? 'selected' : '';?>>ข่าวสาร</option>
                            </select>
                        </div>
                        <div class="form-group form-float">
                            <p>
                                Img
                            </p>
                            <div class="form-line">
                                <input id="picture" name="picture" type="file" class="file" data-preview-file-type="text">
                                <input type="hidden" name="outfile_pic" value="<?=(isset($activity->activity_img)) ? $activity->activity_img : ''; ?>">
                            </div>
                        </div> 

                        <div class="form-group">
                            <textarea  class="summernote" name="activity_detail"><?=isset($activity->activity_detail)? $activity->activity_detail :'';?></textarea>
                        </div>

                        <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Validation -->
</div>
</section>





