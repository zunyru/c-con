
<script type="text/javascript">

    //fileinput
    var picture_image = '<?=(!empty($activity->activity_img)) ? $this->config->item('backend_uploade').$activity->activity_img : ''; ?>';
    $('#picture').fileinput({

        initialPreview: [picture_image],
        initialPreviewAsData: true,
        initialPreviewConfig: [
        { downloadUrl: picture_image  ,extra: {id: '<?=$activity->id;?>'} },
        ],
        deleteUrl: "<?=base_url().'activity/deletepic/'.$activity->id?>",

        maxFileCount: 1,
        validateInitialCount: true,
        overwriteInitial: false,
        showUpload: false,
        showRemove: true,
        required: true,
        initialPreviewAsData: true,
        maxFileSize:1024,
        browseOnZoneClick: true,
        allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
        browseLabel: "Picture",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop picture here …",
    });
    

   
</script>
