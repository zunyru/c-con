<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->model('activity/activity_model');
		$this->load->library('uploadfile_library');
		$this->load->library('nestable_library');
		$this->load->library('logs_library');
	}
	
	public function index(){   
		$data = array(
			'page_index' 		=> 'activity',
			'page' 				=> 'browse',
			'title_page' 		=> 'ร่วมสนุก/กิจกรรม',
			'description_page' 	=> 'My Activity'
		);
		$data['activity'] = $this->activity_model->getJobsAll();
	
		//loade view
		$this->load->view('masters/header',$data); 
		$this->load->view('index',$data);
	}

	public function create() {
		$data = array(
			'page_index' => 'activity',
			'page' => 'create',
			'title_page' => 'Create',
			'description_page' => 'My activity create',
		);
    
        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function store(){

		$user 				= $this->session->userdata('users');
		$path 				= 'activity';
		$upload_pic 		= $this->uploadfile_library->do_upload('picture',TRUE,$path);


        $picture ='';
		if(isset($upload_pic['index'])){
			$picture = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_pic['index']['file_name'];
		}

		$data = array(
			'name'      => $this->input->post('activity_name'),
			'slug'      	 => $this->input->post('slug'),
			'activity_img'       => $picture,
			'activity_category'   	 => $this->input->post('activity_category'),
			'activity_detail'	 => $this->input->post('activity_detail'),
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'active'    	 => 0,
			'create_at' 	 => date('Y-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		$last_id = $this->activity_model->insert($data);	

		//Log
		$messege = 'เพิ่มข้อมูล : '.$this->input->post('jobs_name') 
		.' ,Img : '.$picture
		.' ,File : '.$jobs_file;

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('activity'));
		}	

	}

	public function edit($id) {
		$data = array(
			'page_index' 		=> 'activity',
			'page' 				=> 'edit',
			'title_page' 		=> 'Activity Edit',
			'description_page' 	=> 'My Activity edit',
		);
        //loade data
		$data['activity'] = $this->activity_model->get_ActivityById($id);

        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function update($id) {

		$user = $this->session->userdata('users');

		$path = 'activity';
		$upload_pic 		= $this->uploadfile_library->do_upload('picture',TRUE,$path);

		$picture = null;
		if(isset($upload_pic['index'])){
			$picture = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_pic['index']['file_name'];
			$outfile_pic = $this->input->post('outfile_pic');
			if(isset($outfile_pic)){
				$this->load->helper("file");
				unlink($outfile_pic);
			}
		}

		if(isset($upload['error'])){
			$this->session->set_flashdata('status',0);
			redirect(base_url('jobs'));
		}

		$data = array(
			'id'        => $id,
			'name'      => $this->input->post('activity_name'),
			'slug'      	 => $this->input->post('slug'),
			'activity_img'       => $picture,
			'activity_category'   	 => $this->input->post('activity_category'),
			'activity_detail'	 => $this->input->post('activity_detail'),
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'active'    	 => 0,
			'create_at' 	 => date('Ycatalog-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		$last_id = $this->activity_model->update(array_to_obj($data));

		//Log
		$messege = 'แก้ไขข้อมูล : '.$this->input->post('name') 
		.' ,img : '.$picture;

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('activity'));
		}
	} 

	public function destroy($id){
		$user = $this->session->userdata('users');
		
		$data = $this->activity_model->get_ActivityById($id);

		if(!empty($data->activity_img)){
			$this->load->helper("file");
			unlink($data->activity_img);
		}

		$delete = $this->activity_model->destroy($id);


		if($delete){
			//Log
		$messege = 'ลบข้อมูล : '.$data->name; 

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

			$this->session->set_flashdata('status',1);
			redirect(base_url('activity'));
		}else{
			$this->session->set_flashdata('status',0);
			redirect(base_url('activity'));
		}
	}

	public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'content';
		$upload = $this->uploadfile_library->do_upload('file',TRUE,$path);

		if(isset($upload['index'])){
			$picture = '/uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
	}	

	public function deletePic($ids){

		$arrayName = array('pic' => $ids);

		echo json_encode($arrayName);
	}
	
}
