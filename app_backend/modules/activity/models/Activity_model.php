<?php

class Activity_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getJobsAll() {

		$this->db->select('*');
		$this->db->from('activity');
		$this->db->order_by('activity.name', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_ActivityById($id) {
		$this->db->select('*');
		$this->db->from('activity');
		$this->db->where('id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->id;
			$row->name;
			$row->activity_img;
			$row->activity_detail;
			$row->activity_category;
		}


		return $row;
	}

	public function insert($data){
		$query = $this->db->insert('activity', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
     
	public function update($data){

		$this->db->set('name', $data->name);
		$this->db->set('slug', $data->slug);
		if(!is_null($data->jobs_img)){
			$this->db->set('activity_img', $data->activity_img);
		}
		$this->db->set('activity_category', $data->activity_category);
		$this->db->set('activity_detail', $data->activity_detail);
		$this->db->set('create_by', $data->create_by);
		$this->db->set('update_by', $data->update_by);
		$this->db->set('create_at', $data->create_at);
		$this->db->set('update_at', $data->update_at);
		$this->db->where('id', $data->id);
		$update = $this->db->update('activity');

		return $update;
	}

	public function destroy($id){
		$this->db->where('id', $id);
		$delete = $this->db->delete('activity');

		return $delete;
	}
	
}