<?php

class Regis_config_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_RegisConfigAll() {

		$this->db->select('*');
		$this->db->from('regis_config');
		$this->db->order_by('regis_config.regis_config_id', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_RegisConfigById($id) {
		$this->db->select('*');
		$this->db->from('regis_config');
		$this->db->where('regis_config_id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->jobs_id;
			$row->jobs_name;
			$row->jobs_img;
			$row->jobs_detail;
			$row->jobs_file;
		}


		return $row;
	}
	
}