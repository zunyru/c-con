<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobsregisters extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->model(array(
			'jobsregisters/jobsregisters_model'
			,'jobsregisters/regis_config_model'
		));
		
		$this->load->library('nestable_library');
		$this->load->library('logs_library');
	}
	
	public function index(){   
		$data = array(
			'page_index' 		=> 'jobs',
			'page' 				=> 'jobsregisters',
			'title_page' 		=> 'รายชื่อผู้สมัคร',
			'description_page' 	=> 'My jobs Register'
		);
		$data['jobs_register'] = $this->jobsregisters_model->getJobsregistersAll();
	
		//loade view
		$this->load->view('masters/header',$data); 
		$this->load->view('index',$data);
	}

	public function edit($id) {
		$data = array(
			'page_index' 		=> 'jobs',
			'page' 				=> 'approve',
			'title_page' 		=> 'Privew job register',
			'description_page' 	=> 'My Privew job register',
		);
        //loade data
		$data['jobsregisters'] = $this->jobsregisters_model->get_JobsregistersById($id);
		$_regis_arr = array();
		$_obj_regis = $this->regis_config_model->get_RegisConfigAll();
		foreach ($_obj_regis as $item){
			$_regis_arr[$item->type][] = array(
				 'regis_config_id'	 	=> $item->regis_config_id
				,'regis_config_name' 	=> $item->regis_config_name
			);
		}

		unset($_obj_regis);
		$data['regis_config'] = $_regis_arr;
		
        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function update($id) {

		$user = $this->session->userdata('users');
		if($this->input->post('appove') == "on"){$appove = 1;}else{$appove = 0;}
		$data = array(
			'jobs_regis_id'  => $id,
			'appove'      	 => $appove ,
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'create_at' 	 => date('Ycatalog-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		$last_id = $this->jobsregisters_model->update(array_to_obj($data));

		//Log
		$messege = 'Approve : '.$appove;

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('jobsregisters'));
		}
	} 

	public function destroy($id){
		$user = $this->session->userdata('users');
		
		$data = $this->jobs_model->get_JobsById($id);

		if(!empty($data->jobs_img)){
			$this->load->helper("file");
			unlink($data->jobs_img);
		}

		if(!empty($data->jobs_file)){
			$this->load->helper("file");
			unlink($data->jobs_file);
		}
		
		$delete = $this->jobs_model->destroy($id);


		if($delete){
			//Log
		$messege = 'ลบข้อมูล : '.$data->jobs_name; 

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

			$this->session->set_flashdata('status',1);
			redirect(base_url('jobs'));
		}else{
			$this->session->set_flashdata('status',0);
			redirect(base_url('jobs'));
		}
	}

	public function deletepic($ids)
	{

		$arrayName = array('pic' => $ids);

		echo json_encode($arrayName);
	}
	
}
