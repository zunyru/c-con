<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li><a href="<?=base_url('jobsregisters')?>">Jobs</a></li>
                <li class="active"><?=$title_page?></li>
            </ol>
        </div>
        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Privew job register</h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <div class="body">
                        <?php 
                            echo form_open_multipart(base_url().'jobsregisters/update/'.$jobsregisters->jobs_regis_id,'id="form_validation"' , 'class="form-horizontal"');  
                        ?>
                        <input type="hidden" class="form-control" name="db" id="db" value="jobs">
                        <input type="hidden" class="form-control" name="jobs_regis_id" id="id" value="<?=isset($jobsregisters->jobs_regis_id)? $jobsregisters->jobs_regis_id : '';?>">
                        <!-- <div class="row">
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <img src="<?=(!empty($jobsregisters->img)) ? $this->config->item('backend_uploade').$jobsregisters->img : ''; ?>" alt="รูปประจำตัว" class="img-thumbnail" style="width:115px">
                                </div>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">ชื่อ-สกุล</label>
                                <div class="col-md-10">
                                    <p><?=isset($jobsregisters->name)? $jobsregisters->name : '';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">ตำแหน่งที่ต้องการ</label>
                                <div class="col-md-4">
                                    <p>1. <?=isset($jobsregisters->position_one)? $jobsregisters->position_one : 'ว่าง';?></p>
                                    <p>2. <?=isset($jobsregisters->position_two)? $jobsregisters->position_two : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-2 form-label">เงินเดือน บาท/เดือน</label>
                                <div class="col-md-4">
                                    <p>1. <?=isset($jobsregisters->salary_one)? $jobsregisters->salary_one : 'ว่าง';?></p>
                                    <p>2. <?=isset($jobsregisters->salary_two)? $jobsregisters->salary_two : 'ว่าง';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <h3 class="col-md-12">ประวัติส่วนตัว</h3>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 form-label">ที่อยู่ปัจจุบันเลขที่</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->present_address)? $jobsregisters->present_address : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-1 form-label">หมู่ที่</label>
                                <div class="col-md-1">
                                    <p><?=isset($jobsregisters->moo)? $jobsregisters->moo : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-1 form-label">ถนน</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->road)? $jobsregisters->road : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-1 form-label">ตำบล/แขวง</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->district)? $jobsregisters->district : 'ว่าง';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">อำเภอ/เขต</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->present_address)? $jobsregisters->present_address : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-1 form-label">จังหวัด</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->moo)? $jobsregisters->moo : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-2 form-label">รหัสไปรษณีย์</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->road)? $jobsregisters->road : 'ว่าง';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">โทรศัทพ์</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->tel)? $jobsregisters->tel : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-1 form-label">มือถือ</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->mobile)? $jobsregisters->mobile : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-2 form-label">อีเมล์</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->email)? $jobsregisters->email : 'ว่าง';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <?php
                                if(isset($regis_config[1])){
                                    foreach($regis_config[1] as $item){
                                        if($item['regis_config_id'] == $jobsregisters->room){
                                            $room = 'glyphicon glyphicon-check';
                                        }else{
                                            $room = 'glyphicon glyphicon-unchecked';
                                        }
                                        echo ' <p class="col-md-3 form-label"> <span class="'.$room.'" aria-hidden="true"></span> '.$item['regis_config_name'].'</p>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">วัน เดือน ปีเกิด</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->date_birth)? $jobsregisters->date_birth : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-1 form-label">อายุ(ปี)</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->age)? $jobsregisters->age : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-2 form-label">เชื้อชาติ</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->race)? $jobsregisters->race : 'ว่าง';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">สัญชาติ</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->nationality)? $jobsregisters->nationality : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-1 form-label">ศาสนา</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->religion)? $jobsregisters->religion : 'ว่าง';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">บัตรประชาชนเลขที่</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->identity_card_no)? $jobsregisters->identity_card_no : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-2 form-label">บัตรหมดอายุ</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->expiration_date)? $jobsregisters->expiration_date : 'ว่าง';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">ส่วนสูง(ซม.)</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->height)? $jobsregisters->height : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-2 form-label">น้ำหนัก(กก.)</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->weight)? $jobsregisters->weight : 'ว่าง';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">ภาระทางทหาร</label>
                                <?php
                                if(isset($regis_config[2])){
                                    foreach($regis_config[2] as $item){
                                        if($item['regis_config_id'] == $jobsregisters->military_status){
                                            $military_status = 'glyphicon glyphicon-check';
                                        }else{
                                            $military_status = 'glyphicon glyphicon-unchecked';
                                        }
                                        echo ' <p class="col-md-3 form-label"> <span class="'.$military_status.'" aria-hidden="true"></span> '.$item['regis_config_name'].'</p>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">สถานภาพ</label>
                                <?php
                                if(isset($regis_config[3])){
                                    foreach($regis_config[3] as $item){
                                        if($item['regis_config_id'] == $jobsregisters->maritary_status){
                                            $maritary_status = 'glyphicon glyphicon-check';
                                        }else{
                                            $maritary_status = 'glyphicon glyphicon-unchecked';
                                        }
                                        echo ' <p class="col-md-2 form-label"> <span class="'.$maritary_status.'" aria-hidden="true"></span> '.$item['regis_config_name'].'</p>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">เพศ</label>
                                <?php
                                if(isset($regis_config[4])){
                                    foreach($regis_config[4] as $item){
                                        if($item['regis_config_id'] == $jobsregisters->sex){
                                            $sex = 'glyphicon glyphicon-check';
                                        }else{
                                            $sex = 'glyphicon glyphicon-unchecked';
                                        }
                                        echo ' <p class="col-md-2 form-label"> <span class="'.$sex.'" aria-hidden="true"></span> '.$item['regis_config_name'].'</p>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <h3 class="col-md-12">ประวัติการทำงาน</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">ชื่อบริษัท</label>
                                <div class="col-md-10">
                                    <p><?=isset($jobsregisters->office_one)? $jobsregisters->office_one : 'ว่าง';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">ตำแหน่ง/หน้าที่/ประสบการณ์ทำงาน</label>
                                <div class="col-md-10">
                                    <p><?=isset($jobsregisters->experience)? $jobsregisters->experience : 'ว่าง';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <h3 class="col-md-12">ประวัติครอบครัว</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">บิดา ชื่อ-สกุล</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->father_name)? $jobsregisters->father_name : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-2 form-label">อายุ(ปี.)</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->father_age)? $jobsregisters->father_age : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-2 form-label">อาชีพ</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->father_occupation)? $jobsregisters->father_occupation : 'ว่าง';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">มารดา ชื่อ-สกุล</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->mother_name)? $jobsregisters->mother_name : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-2 form-label">อายุ(ปี.)</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->mother_age)? $jobsregisters->mother_age : 'ว่าง';?></p>
                                </div>
                                <label class="col-md-2 form-label">อาชีพ</label>
                                <div class="col-md-2">
                                    <p><?=isset($jobsregisters->mother_occupation)? $jobsregisters->mother_occupation : 'ว่าง';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <h3 class="col-md-12">สถานะการตรวจสอบ</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">สถานะ</label>
                                <div class="col-md-5">
                                    <?php
                                    if($jobsregisters->appove > 0){
                                        $appove = 'checked';
                                    }else{
                                        $appove = '';
                                    }
                                    ?>
                                    <div class="switch panel-switch-btn">
                                        <label>รอตรวจสอบ
                                            <input type="checkbox" id="realtime" name="appove" <?=$appove?>>
                                            <span class="lever switch-col-cyan"></span>ตรวจสอบแล้ว
                                        </label>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
</div>
</section>





