<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>

                <ol class="breadcrumb">
                    <li class="active">รายชื่อผู้สมัครงาน</li>
                </ol>
                <!-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> -->
            </h2>
        </div>
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            รายชื่อผู้สมัครงาน
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable pro-exportable">
                                <thead>
                                    <tr>
                                        <th width="30%">Name</th>
                                        <th width="20%">Position</th>
                                        <th width="20%">Email</th>
                                        <th width="20%">Tel</th>
                                        <th width="20%">Status</th>
                                        <th width="10%">Craet date</th>
                                        <th width="10%">Upage date</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Position</th>
                                        <th>Email</th>
                                        <th>Tel</th>
                                        <th>Status</th>
                                        <th>Craet date</th>
                                        <th>Upage date</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php 
                                    foreach ($jobs_register as $index => $item): 
                                        ?>
                                        <tr>
                                            <td><?=$item->name?></td>
                                            <td>
                                                <p>1. <?=isset($item->position_one)? $item->position_one : 'ไม่มี';?></p>
                                                <p>2. <?=isset($item->position_two)? $item->position_two : 'ไม่มี';?></p>
                                            </td>
                                            <td><?=isset($item->email)? $item->email : 'ไม่มี';?></td>
                                            <td><?=isset($item->mobile)? $item->mobile : 'ไม่มี';?></td>
                                            <td>
                                                <?php
                                                if($item->appove > 0){ echo 'ตรวจสอบแล้ว';}else{ echo 'รอตรวจสอบ';}
                                                ?>
                                            </td>
                                            <td>
                                                <span style="display: none;">
                                                    <?=$item->create_at?>
                                                </span>
                                                <?=DateThai($item->create_at)?></td>
                                            <td>
                                                <span style="display: none;">
                                                    <?=$item->update_at?>
                                                </span>
                                                <?=DateThai($item->update_at)?>
                                            </td>
                                            <td>
                                                <a href="<?=base_url().'jobsregisters/edit/'.$item->jobs_regis_id?>" class="btn bg-orange btn-block waves-effect">Privew</a>

                                               <a href="<?=base_url().'jobsregisters/destroy/'.$item->jobs_regis_id?>" class="btn bg-red btn-block waves-effect delete">Delete</a>

                                           </td>
                                       </tr>
                                       <?php 
                                   endforeach; 
                                   ?>    

                               </tbody>
                           </table>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <!-- #END# Exportable Table -->
   </div>
</section>

