<?php 

/**
 * 
 */
class Users_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function find_users_by_user($username){

		$query = $this->db->where('username',$username)
		         ->get('users');
		return $query->row();         
	}

	public function find_users_by_user_and_salt($username,$salt){

		$query = $this->db->where('username',$username)
						  ->where('salt',$salt)
		                  ->get('users');
		return $query->row();         
	}

	public function get_UsersAll() {

		$this->db->select('users.*,roles.name, roles.display_name');
		$this->db->from('users');
		$this->db->join('roles', 'users.role_id=roles.id','left');
		$this->db->order_by('users.created_at', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_UsersById($id) {
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->id;
			$row->fname;
			$row->lname;
			$row->email;
			$row->phone;
		}

		return $row;
	}

	public function get_UsersNameById($id) {
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('id', $id);
		$query = $this->db->get();  
		return $query;
	}

	public function get_UsersRole() {
		$this->db->select('*');
		$this->db->from('roles'); 
		$query = $this->db->get();  
		return $query;
	}

	public function insert($data){
		$query = $this->db->insert('users', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
     
	public function update($data){

		$this->db->set('fname', $data->fname);
		$this->db->set('lname', $data->lname);
		$this->db->set('email', $data->email);
		$this->db->set('phone', $data->phone); 
		$this->db->set('created_by', $data->created_by); 
		$this->db->set('created_at', $data->created_at);
		$this->db->set('updated_at', $data->updated_at);
		$this->db->set('role_id', $data->role_id);
		if(!empty($data->password)):
			$this->db->set('password', $data->password);
			if(!empty($data->salt)):
				$this->db->set('salt', $data->salt);
			endif;
		endif;
		if(!empty($data->username)):
			$this->db->set('username', $data->username);
		endif;
		$this->db->where('id', $data->id);
		$update = $this->db->update('users');

		return $update;
	}

	public function destroy($id){
		$this->db->where('id', $id);
		$delete = $this->db->delete('users');

		return $delete;
	}

	public function get_rows_check($param) 
    {
        if (!empty($param['id'])):
            $this->db->where('a.id', $param['id']);
		endif;
        if (!empty($param['username'])):
            $this->db->where('a.username', $param['username']);
		endif;
        
        $query = $this->db
                        ->select('a.*')
                        ->from('users a')
                        ->get();
        return $query;
    }
}