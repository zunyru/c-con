<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->library('users_library');
		$this->load->library('logs_library');

		$this->load->helper(array('form', 'url' ,'fn','file'));
		$this->load->library('uploadfile_library');
		$this->load->library('nestable_library');
		$this->load->model('users_model');
		
	}
	
	public function index()
	{
		$this->load->view('sign-in');
	}

	public function login()
    {
		/**
         * test password
         */
		/*$salt = $this->users_library->salt();
		$password = $this->users_library->hash_password('password', $salt);
		echo 'Pass : '.$password;
		echo 'salt : '.$salt ;
		
		$salt = 'd833837a9fd53585ec234e61a901bc71a1a4abd9';
		$password = 'FoyL3gEokVEMRwyWFTJ5xO4422829e0de131db9c96e93c5bb5bfe875a9790e';

		if($this->users_library->hash_password('password',$salt) !== $password){
			echo '<br>'.'NOT';
		}else {
			echo '<br>'.'YES';
		}*/

		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$remember = (bool)$this->input->post('remember');

			$users = $this->users_library->login(
				$username,
				$password,
				$remember
			); 
            //Log
		    $this->logs_library->save_log($username,$this->class,$this->method,$users['message']);

			if($users['status'] === 'success'){
				
				//Go to  dashboard
				redirect(base_url('dashboard'),'refresh');
			}elseif ($users['status'] === 'warning') {
			
				$this->session->set_flashdata('status',$users['status']);
				$this->session->set_flashdata('message',$users['message']);
				//Go to  users/login
				redirect(base_url('users/login'),'refresh');
			}else{
				
				$this->session->set_flashdata('status',$users['status']);
				$this->session->set_flashdata('message',$users['message']);
				//Go to  users/login
				redirect(base_url('users/login'),'refresh');
			}

			
		}
		else
		{
            //loade view
			$this->load->view('sign-in');
		}

	}

	public function Logout()
	{
		$usename = $this->session->userdata('users');
		//Log
		$this->logs_library->save_log($usename['Username'],$this->class,$this->method,'Logout : Logouted');

		delete_cookie('username');
		delete_cookie('salt');

		$this->session->unset_userdata('users');
		redirect(base_url('users/login'),'refresh');
	}

	public function admin(){   
		$data = array(
			'page_index' 		=> 'users',
			'page' 				=> 'browse',
			'title_page' 		=> 'users',
			'description_page' 	=> 'My users'
		);
		
		$data['users'] = $this->users_model->get_UsersAll();
	
		//loade view
		$this->load->view('masters/header',$data); 
		$this->load->view('index',$data);
	}

	public function create() {
		$data = array(
			'page_index' 		=> 'users',
			'page' 				=> 'create',
			'title_page' 		=> 'Create',
			'description_page' 	=> 'My users create',
		);

		$user 			= $this->session->userdata('users');
		$data['roles'] 	= $this->roles_model->get_RolesById($user['RoleId'])->row();
		$data['role']  	= $this->users_model->get_UsersRole()->result(); 
        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function store(){

		$user 		= $this->session->userdata('users');
		$salt 		= $this->users_library->salt();
        $password 	= $this->users_library->hash_password($this->input->post('password'), $salt);
		$data = array( 
			'username'       => $this->input->post('username'),
			'salt'  		 => $salt,
			'password'  	 => $password,
			'fname'  		 => $this->input->post('fname'),
			'lname'      	 => $this->input->post('lname'),
			'email'      	 => $this->input->post('email'),
			'phone'      	 => $this->input->post('phone'),
			'role_id'      	 => $this->input->post('role_id'),
			'created_by' 	 => $user['UID'],  
			'created_at' 	 => date('Y-m-d H:i:s'),
			'updated_at' 	 => date('Y-m-d H:i:s'),
		); 

		$last_id = $this->users_model->insert($data);	

		//Log
		$messege = 'เพิ่มข้อมูล : '.$this->input->post('fname');

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('users/admin'));
		}	

	}

	public function edit($id) 
	{
		$data = array(
			'page_index' 		=> 'users',
			'page' 				=> 'edit',
			'title_page' 		=> 'Users edit',
			'description_page' 	=> 'My users edit',
		);
		//loade data
		
		$user 		= $this->session->userdata('users');

		$data['users'] = $this->users_model->get_UsersById($id);
		$data['roles'] = $this->roles_model->get_RolesById($user['RoleId'])->row();
		$data['role']  = $this->users_model->get_UsersRole()->result();
        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function update($id) 
	{
		
		$user 		= $this->session->userdata('users');
		$salt 		= $this->users_library->salt();
        $password 	= $this->users_library->hash_password($this->input->post('password'), $salt);
		$data = array(
			'id'     		 => $id,
			'salt'  		 => $salt,
			'password'  	 => $password,
			'username'       => $this->input->post('username'),
			'fname'  		 => $this->input->post('fname'),
			'lname'      	 => $this->input->post('lname'),
			'email'      	 => $this->input->post('email'),
			'phone'      	 => $this->input->post('phone'), 
			'role_id'      	 => $this->input->post('role_id'),
			'created_by' 	 => $user['UID'],  
			'created_at' 	 => date('Y-m-d H:i:s'),
			'updated_at' 	 => date('Y-m-d H:i:s'),
		);
		
		$last_id = $this->users_model->update(array_to_obj($data));

		//Log
		$messege = 'แก้ไขข้อมูล : '.$this->input->post('fname');

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('users/admin'));
		}
	} 

	public function destroy($id)
	{
		$user = $this->session->userdata('users');
		 
		$data = $this->users_model->get_UsersById($id);
		$delete = $this->users_model->destroy($id); 
		if($delete){
			//Log
			$messege = 'ลบข้อมูล : '.$data->fname; 

			$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

			$this->session->set_flashdata('status',1);
			redirect(base_url('users/admin'));
		}else{
			$this->session->set_flashdata('status',0);
			redirect(base_url('users/admin'));
		}
	}

	public function check_password() 
	{
        $input = $this->input->post(); 
        $info = $this->users_model->get_UsersNameById($input['id']);
        $row = $info->row(); 
        if($this->users_library->hash_password($input['oldPassword'],$row->salt) !== $row->password){
            $rs = FALSE;
        } else {
            $rs = TRUE;
           
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($rs));
	}
	
	public function check_username()
    {
        $input = $this->input->post();
        $info = $this->users_model->get_rows_check($input);
        if ( $info->num_rows() > 0 ) {
            if ($input['mode'] == 'create') {
                $rs = FALSE;
            } else {
                $row = $info->row();
                if ($row->id == $input['id']) {
                    $rs = TRUE;
                } else {
                    $rs = FALSE;
                }
            }
        } else {
            $rs =  TRUE;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($rs));
    }
}
