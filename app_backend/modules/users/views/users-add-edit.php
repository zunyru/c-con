<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li><a href="<?=base_url('users/admin')?>">Users</a></li>
                <li class="active"><?=$title_page?></li>
            </ol>
        </div>
        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>ข้อมูลส่วนตัว</h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <?php 
                    if($page === 'create'): 
                        echo form_open_multipart(base_url().'users/store','id="form_validation" autocomplete="off"');
                    else: 
                        echo form_open_multipart(base_url().'users/update/'.$users->id,'id="form_validation" autocomplete="off"');
                    endif;    
                    ?>
                    <div class="body">
                       
                        <input type="hidden" class="form-control" name="db" id="db" value="users">
                        <input type="hidden" class="form-control" name="id" id="id" value="<?=isset($users->id)? $users->id : '';?>">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="fname" id="fname" value="<?=isset($users->fname)? $users->fname : '';?>" required>
                                <label class="form-label">Fname</label>
                            </div> 
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="lname" id="lname" value="<?=isset($users->lname)? $users->lname : '';?>" required>
                                <label class="form-label">Lasname</label>
                            </div> 
                        </div> 
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="email" id="email" value="<?=isset($users->email)? $users->email : '';?>">
                                <label class="form-label">Email</label>
                            </div> 
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="phone" id="phone" value="<?=isset($users->phone)? $users->phone : '';?>">
                                <label class="form-label">Phone</label>
                            </div> 
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                               <h4>ชื่อผู้ใช้งานระบบ</h4>
                            </div> 
                        </div>
                       
                        <div class="form-group form-float">
                            <?php 
                            if(!empty($roles->name)): 
                            ?>
                            <div class="form-line"> 
                                <select name="role_id" id="role_id" class="form-control" required>
                                    <option value="">เลือกประเภทผู้ใช้งาน</option>
                                    <?php
                                    if(!empty($role)):
                                        foreach($role as $item):
                                            $role_id = !empty($users->role_id) ? $users->role_id :0;
                                            if($item->id == $role_id):
                                                $selected = 'selected';
                                            else:
                                                $selected = '';
                                            endif;
                                    ?>
                                        <option value="<?php echo !empty($item->id) ? $item->id :'';?>" <?=$selected?>><?php echo !empty($item->display_name) ? $item->display_name :'';?></option>
                                    <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </select>  
                            </div>
                            <?php 
                            else:
                            ?>
                            <div class="form-line">
                                <input type="text" class="form-control" name="" id="" value="<?=isset($roles->display_name)? $roles->display_name: '';?>" required readonly>
                                <input type="hidden" class="form-control" name="role_id" id="role_id" value="<?=isset($users->role_id)? $users->role_id: 0;?>" required readonly>
                                <label class="form-label">User Type</label>
                            </div> 
                            <?php 
                            endif; 
                            ?>
                        </div>
                        <?php
                        if($page === 'create'):
                        ?>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="username" id="username" value="<?=isset($users->username)? $users->username : '';?>" required>
                                <label class="form-label">Username</label>
                            </div> 
                        </div> 
                        <?php
                        else:
                        ?>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="username" id="username" value="<?=isset($users->username)? $users->username : '';?>" readonly>
                                <label class="form-label">Username</label>
                            </div> 
                        </div> 
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control" name="oldPassword" id="input-oldPassword" value="">
                                <label class="form-label">รหัสผ่านเดิม</label>
                            </div> 
                        </div> 
                        <?php
                        endif;
                        ?>
                        
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" id="input-password" value="">
                                <label class="form-label">รหัสผ่าน</label>
                            </div> 
                        </div> 
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control" name="rePassword" id="input-repassword" value="">
                                <label class="form-label">ยืนยันรหัสผ่าน</label>
                            </div> 
                        </div>    

                        <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button> 
                    </div>
                    </form>
                </div>
            </div>
        </div>
    <!-- #END# Basic Validation -->
    </div>
</section>





