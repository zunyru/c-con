<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>

                <ol class="breadcrumb">
                    <li class="active">Users</li>
                </ol>
                <!-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> -->
            </h2>
        </div>
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                        Users
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <a href="<?=base_url('users/create')?>" class="btn bg-deep-orange waves-effect">Create</a>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable pro-exportable">
                                <thead>
                                    <tr>
                                        <th width="30%">Name</th>
                                        <th width="20%">Lasname</th>
                                        <th width="20%">User Type</th> 
                                        <th width="10%">Email</th>
                                        <th width="10%">Tel</th>
                                        <th width="10%">Craet date</th>
                                        <th width="10%">Upage date</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Lasname</th>
                                        <th>User Type</th>
                                        <th>Email</th>
                                        <th>Tel</th>
                                        <th>Craet date</th>
                                        <th>Upage date</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php 
                                    foreach ($users as $index => $item): 
                                        ?>
                                        <tr>
                                            <td><?=$item->fname?></td>
                                            <td><?=$item->lname?></td>
                                            <td><?=$item->display_name?></td> 
                                            <td><?=$item->email?></td>
                                            <td><?=$item->phone?></td>
                                            <td>
                                                <span style="display: none;">
                                                    <?=$item->created_at?>
                                                </span>
                                                <?=DateThai($item->created_at)?></td>
                                            <td>
                                                <span style="display: none;">
                                                    <?=$item->updated_at?>
                                                </span>
                                                <?=DateThai($item->updated_at)?>
                                            </td>
                                            <td>
                                                <a href="<?=base_url().'users/edit/'.$item->id?>" class="btn bg-orange btn-block waves-effect">Edit</a>

                                               <a href="<?=base_url().'users/destroy/'.$item->id?>" class="btn bg-red btn-block waves-effect delete">Delete</a>

                                           </td>
                                       </tr>
                                       <?php 
                                   endforeach; 
                                   ?>    

                               </tbody>
                           </table>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <!-- #END# Exportable Table -->
   </div>
</section>

