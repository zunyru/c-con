
<script type="text/javascript">
    var controller = '<?=base_url('users');?>';
    $('#form_validation').validate({
        rules: {
             oldPassword: {
                remote: {
                    url: controller+"/check_password",
                    type: "post",
                    data: {id: function () {
                            return $('#id').val();
                        }, mode: function () {
                            return $('#db').val();
                        }
                    }
                }
            },
            rePassword: {equalTo: "#input-password"}
        },
        messages: {
            oldPassword: {remote: 'รหัสผ่านปัจจุบันไม่ถูกต้อง'},
            rePassword: {equalTo: 'โปรดระบุรหัสผ่านอีกครั้ง'}
        }
    });
</script>
