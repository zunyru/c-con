
<script type="text/javascript">
    var controller = '<?=base_url('users');?>';
    $('#form_validation').validate({
        rules: {
            username: {
                remote: {
                    url: controller+"/check_username",
                    type: "post",
                    data: {id: function () {
                            return $('#id').val();
                        }, mode: function () {
                            return 'create';
                        }
                    }
                }
            },
            rePassword: {equalTo: "#input-password"}
        },
        messages: {
            username: {remote: 'พบ username ซ้ำในระบบ'},
            rePassword: {equalTo: 'โปรดระบุรหัสผ่านอีกครั้ง'}
        }
    });
    
</script>
