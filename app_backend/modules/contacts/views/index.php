<body class="theme-deep-orange">
    <!-- Page Loader -->
    <?php $this->load->view('masters/loader');?>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <?php $this->load->view('masters/search');?>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <?php $this->load->view('masters/navbar');?>
    <!-- #Top Bar -->
    <!-- Left Sidebar -->
    <?php $this->load->view('masters/left-sidebar');?>
    <!-- #END# Left Sidebar -->
    
    <?php if ($page === 'browse'): ?>
        <?php $this->load->view('contacts-browse');?>
    <?php else: ?>
        <?php $this->load->view('contacts-read');?>
    <?php endif; ?>
    <!-- script -->
    <?php $this->load->view('masters/script');?>
    <!-- #END# script -->

    <?php 
    if($page == 'browse'):
        $this->load->view('script-browse');
    endif;       

    ?>
   </body>

</html>