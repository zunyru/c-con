<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li><a href="<?=base_url('contacts')?>">Contacts</a></li>
                <li class="active"><?=$title_page?></li>
            </ol>
        </div>
        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Privew Contacts</h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <div class="body">
                        <?php 
                            echo form_open_multipart(base_url().'contacts/update/'.$contact->contact_id,'id="form_validation"' , 'class="form-horizontal"');  
                        ?>
                        <input type="hidden" class="form-control" name="db" id="db" value="jobs">
                        <input type="hidden" class="form-control" name="contact_id" id="id" value="<?=isset($contact->contact_id)? $contact->contact_id : '';?>">
                        
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">ชื่อผู้ติดต่อ</label>
                                <div class="col-md-10">
                                    <p><?=isset($contact->user_name)? $contact->user_name : '';?></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">อีเมล์</label>
                                <div class="col-md-10">
                                    <?=isset($contact->email)? $contact->email : 'ว่าง';?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">หัวข้อ</label>
                                <div class="col-md-10">
                                    <?=isset($contact->contact_title)? $contact->contact_title : 'ว่าง';?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">รายละเอียด</label>
                                <div class="col-md-10">
                                    <?=isset($contact->contacts_detail)? $contact->contacts_detail : 'ว่าง';?>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="form-group">
                                <h3 class="col-md-12">สถานะการตรวจสอบ</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-2 form-label">สถานะ</label>
                                <div class="col-md-5">
                                    <?php
                                    if($contact->read > 0){
                                        $read = 'checked';
                                    }else{
                                        $read = '';
                                    }
                                    ?>
                                    <div class="switch panel-switch-btn">
                                        <label>รออ่าน
                                            <input type="checkbox" id="realtime" name="read" <?=$read?>>
                                            <span class="lever switch-col-cyan"></span>อ่านแล้ว
                                        </label>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
</div>
</section>





