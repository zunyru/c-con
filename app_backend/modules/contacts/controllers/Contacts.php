<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->model(array('contacts/contacts_model'));	
		$this->load->library('nestable_library');
		$this->load->library('logs_library');
	}
	
	public function index(){   
		$data = array(
			'page_index' 		=> 'contacts',
			'page' 				=> 'browse',
			'title_page' 		=> 'รายการผู้ติดต่อ',
			'description_page' 	=> 'My contacts'
		);
		//load contacts
		$data['contacts'] = $this->contacts_model->getContactsAll();
	
		//loade view
		$this->load->view('masters/header',$data); 
		$this->load->view('index',$data);
	}

	public function edit($id) {
		$data = array(
			'page_index' 		=> 'contacts',
			'page' 				=> 'read',
			'title_page' 		=> 'Privew contacts',
			'description_page' 	=> 'My privew contacts',
		);
        //loade data
		$data['contact'] = $this->contacts_model->get_ContactsById($id);
		
        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function update($id) {

		$user = $this->session->userdata('users');
		if($this->input->post('read') == "on"){$read = 1;}else{$read = 0;}
		$data = array(
			'contact_id'  	 => $id,
			'read'      	 => $read ,
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'create_at' 	 => date('Ycatalog-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		$last_id = $this->contacts_model->update(array_to_obj($data));

		//Log
		$messege = 'Contact : '.$read;

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('contacts'));
		}
	} 

	public function destroy($id){
		$user = $this->session->userdata('users');
		
		$data = $this->contacts_model->get_ContactsById($id);
		
		$delete = $this->contacts_model->destroy($id);


		if($delete){
			//Log
			$messege = 'ลบข้อมูล Contact : '.$data->name; 

			$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

			$this->session->set_flashdata('status',1);
			redirect(base_url('contacts'));
		}else{
			$this->session->set_flashdata('status',0);
			redirect(base_url('contacts'));
		}
	}
	
}
