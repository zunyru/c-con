
<script type="text/javascript">
    //fileinput
    $('#picture').fileinput({
        maxFileCount: 1,
        validateInitialCount: true,
        overwriteInitial: false,
        showUpload: false,
        showRemove: true,
        required: false,
        initialPreviewAsData: true,
        maxFileSize:500,
        browseOnZoneClick: true,
        allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
        browseLabel: "Picture",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop picture here …",
    });

    $('#catalog_file').fileinput({
        maxFileCount: 1,
        validateInitialCount: true,
        overwriteInitial: false,
        showUpload: false,
        showRemove: true,
        //required: false,
        initialPreviewAsData: true,
        //maxFileSize:300,
        browseOnZoneClick: true,
        allowedFileExtensions: ["pdf"],
        browseLabel: "Catalog",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop catalog file here …",
    });
</script>
