<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li><a href="<?=base_url('abouts')?>">Abouts</a></li>
                <li class="active"><?=$title_page?></li>
            </ol>
        </div>
        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Abouts</h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <div class="body">
                        <?php 
                        if($page === 'create'): 
                            echo form_open_multipart(base_url().'abouts/store','id="form_validation"');
                        else: 
                            echo form_open_multipart(base_url().'abouts/update/'.$abouts->id,'id="form_validation"');
                        endif;    
                        ?>
                        <input type="hidden" class="form-control" name="db" id="db" value="abouts">
                        <input type="hidden" class="form-control" name="id" id="id" value="<?=isset($abouts->id)? $abouts->id : '';?>">
                        <div class="form-group form-float">
                            <p>
                                IMG Banners
                            </p>
                            <div class="form-line">
                                <input id="picture" name="picture" type="file" class="file" data-preview-file-type="text">
                                <input type="hidden" name="outfile_pic" value="<?=(isset($abouts->banners)) ? $abouts->banners : ''; ?>">
                            </div>
                        </div> 
                        <!-- <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="name" id="name" value="<?=isset($abouts->name)? $abouts->name : '';?>" required>
                                <label class="form-label">Name</label>
                            </div>
                            <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                        </div> -->
                        <!-- <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="slug" id="slug" value="<?=isset($abouts->slug)? $abouts->slug : '';?>" required>
                                <label class="form-label">Slug</label>
                            </div>
                            <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                        </div> -->
                        <div class="form-group">
                            <textarea  id="companybg_name" name="companybg_name"><?=isset($abouts->companybg_name)? $abouts->companybg_name :'';?></textarea>
                        </div>
                        <!-- <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="values" id="values" value="<?=isset($abouts->values)? $abouts->values : '';?>" required>
                                <label class="form-label">Values</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea  id="values_name" name="values_name"><?=isset($abouts->values_name)? $abouts->values_name :'';?></textarea>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="mission" id="mission" value="<?=isset($abouts->mission)? $abouts->mission : '';?>" required>
                                <label class="form-label">Mission</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea  id="mission_name" name="mission_name"><?=isset($abouts->mission_name)? $abouts->mission_name :'';?></textarea>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="our_values" id="our_values" value="<?=isset($abouts->our_values)? $abouts->our_values : '';?>" required>
                                <label class="form-label">Mission</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea  id="our_values_name" name="our_values_name"><?=isset($abouts->our_values_name)? $abouts->our_values_name :'';?></textarea>
                        </div> -->
                        <!-- <div class="form-group form-float">
                            <p>
                                IMG our values img 1
                            </p>
                            <div class="form-line">
                                <input id="our_values_img1" name="our_values_img1" type="file" class="file" data-preview-file-type="text">
                                <input type="hidden" name="outfile_our_values_img1" value="<?=(isset($abouts->our_values_img1)) ? $abouts->our_values_img1 : ''; ?>">
                            </div>
                        </div> 
                        <div class="form-group form-float">
                            <p>
                                IMG our values img 2
                            </p>
                            <div class="form-line">
                                <input id="our_values_img2" name="our_values_img2" type="file" class="file" data-preview-file-type="text">
                                <input type="hidden" name="outfile_our_values_img2" value="<?=(isset($abouts->our_values_img2)) ? $abouts->our_values_img2 : ''; ?>">
                            </div>
                        </div> 
                        <div class="form-group form-float">
                            <p>
                                IMG our values img 3
                            </p>
                            <div class="form-line">
                                <input id="our_values_img3" name="our_values_img3" type="file" class="file" data-preview-file-type="text">
                                <input type="hidden" name="outfile_our_values_img3" value="<?=(isset($abouts->our_values_img3)) ? $abouts->our_values_img3 : ''; ?>">
                            </div>
                        </div>  -->
                        <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
</div>
</section>





