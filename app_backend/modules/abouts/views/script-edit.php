
<script type="text/javascript">

    //fileinput
    var picture_image = '<?=(!empty($abouts->banners)) ? $this->config->item('backend_uploade').$abouts->banners : ''; ?>';
    $('#picture').fileinput({

        initialPreview: [picture_image],
        initialPreviewAsData: true,
        initialPreviewConfig: [
        { downloadUrl: picture_image  ,extra: {id: '<?=$abouts->id;?>'} },
        ],
        deleteUrl: "<?=base_url().'abouts/deletepic/'.$abouts->id?>",

        maxFileCount: 1,
        validateInitialCount: false,
        overwriteInitial: false,
        showUpload: false,
        showRemove: true,
        required: false,
        initialPreviewAsData: true,
        maxFileSize:500,
        browseOnZoneClick: true,
        allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
        browseLabel: "Picture",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop picture here …",
    });

    //our_values_img1
    // var picture_image = '<?=(!empty($abouts->our_values_img1)) ? $this->config->item('backend_uploade').$abouts->our_values_img1 : ''; ?>';
    // $('#our_values_img1').fileinput({

    //     initialPreview: [picture_image],
    //     initialPreviewAsData: true,
    //     initialPreviewConfig: [
    //     { downloadUrl: picture_image  ,extra: {id: '<?=$abouts->id;?>'} },
    //     ],
    //     deleteUrl: "<?=base_url().'abouts/deletepic2/'.$abouts->id?>",

    //     maxFileCount: 1,
    //     validateInitialCount: true,
    //     overwriteInitial: false,
    //     showUpload: false,
    //     showRemove: true,
    //     required: true,
    //     initialPreviewAsData: true,
    //     maxFileSize:500,
    //     browseOnZoneClick: true,
    //     allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
    //     browseLabel: "Picture",
    //     browseClass: "btn btn-primary btn-block",
    //     showCaption: false,
    //     showRemove: true,
    //     showUpload: false,
    //     removeClass: 'btn btn-danger',
    //     dropZoneTitle :"Drag & drop picture here …",
    // });

     //our_values_img1
    //  var picture_image = '<?=(!empty($abouts->our_values_img2)) ? $this->config->item('backend_uploade').$abouts->our_values_img2 : ''; ?>';
    // $('#our_values_img2').fileinput({

    //     initialPreview: [picture_image],
    //     initialPreviewAsData: true,
    //     initialPreviewConfig: [
    //     { downloadUrl: picture_image  ,extra: {id: '<?=$abouts->id;?>'} },
    //     ],
    //     deleteUrl: "<?=base_url().'abouts/deletepic3/'.$abouts->id?>",

    //     maxFileCount: 1,
    //     validateInitialCount: true,
    //     overwriteInitial: false,
    //     showUpload: false,
    //     showRemove: true,
    //     required: true,
    //     initialPreviewAsData: true,
    //     maxFileSize:500,
    //     browseOnZoneClick: true,
    //     allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
    //     browseLabel: "Picture",
    //     browseClass: "btn btn-primary btn-block",
    //     showCaption: false,
    //     showRemove: true,
    //     showUpload: false,
    //     removeClass: 'btn btn-danger',
    //     dropZoneTitle :"Drag & drop picture here …",
    // });

     //our_values_img1
    //  var picture_image = '<?=(!empty($abouts->our_values_img3)) ? $this->config->item('backend_uploade').$abouts->our_values_img3 : ''; ?>';
    // $('#our_values_img3').fileinput({

    //     initialPreview: [picture_image],
    //     initialPreviewAsData: true,
    //     initialPreviewConfig: [
    //     { downloadUrl: picture_image  ,extra: {id: '<?=$abouts->id;?>'} },
    //     ],
    //     deleteUrl: "<?=base_url().'abouts/deletepic4/'.$abouts->id?>",

    //     maxFileCount: 1,
    //     validateInitialCount: true,
    //     overwriteInitial: false,
    //     showUpload: false,
    //     showRemove: true,
    //     required: true,
    //     initialPreviewAsData: true,
    //     maxFileSize:500,
    //     browseOnZoneClick: true,
    //     allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
    //     browseLabel: "Picture",
    //     browseClass: "btn btn-primary btn-block",
    //     showCaption: false,
    //     showRemove: true,
    //     showUpload: false,
    //     removeClass: 'btn btn-danger',
    //     dropZoneTitle :"Drag & drop picture here …",
    // });
     
    // $('#values_name').summernote({
    //      fontSizes: ['10', '11', '12', '14', '16','18', '20' ,'22' ,'24', '36', '48' , '64', '82', '100'],
    //     placeholder: 'Detail this',  
    //     height: 400,
    //     tabsize: 2,
    //     toolbar: [
    //     ['style'],
    //     ['style', ['bold', 'italic', 'underline', 'clear']],
    //     ['font', ['strikethrough', 'superscript', 'subscript']],
    //     ['fontsize', ['fontsize']],
    //     ['color', ['color']],
    //     ['para', ['ul', 'ol', 'paragraph']],
    //     ['height', ['height']],
    //     ['link'],
    //     ['table'],
    //     ['media',['picture','video']],
    //     ['hr'],
    //     ['misc',['fullscreen','codeview','undo','redo','help']]
    //     ],
    //     callbacks: {
    //         onImageUpload: function(files) {
    //             sendFile_about(files[0], 'values_name');
    //         }
    //     }, 
    // }); 

    $('#companybg_name').summernote({
         fontSizes: ['10', '11', '12', '14', '16','18', '20' ,'22' ,'24', '36', '48' , '64', '82', '100'],
        placeholder: 'Detail this',  
        height: 400,
        tabsize: 2,
        toolbar: [
        ['style'],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['link'],
        ['table'],
        ['media',['picture','video']],
        ['hr'],
        ['misc',['fullscreen','codeview','undo','redo','help']]
        ],
        callbacks: {
            onImageUpload: function(files) {
                sendFile_about(files[0], 'companybg_name');
            }
        }, 
    });

    // $('#mission_name').summernote({
    //      fontSizes: ['10', '11', '12', '14', '16','18', '20' ,'22' ,'24', '36', '48' , '64', '82', '100'],
    //     placeholder: 'Detail this',  
    //     height: 400,
    //     tabsize: 2,
    //     toolbar: [
    //     ['style'],
    //     ['style', ['bold', 'italic', 'underline', 'clear']],
    //     ['font', ['strikethrough', 'superscript', 'subscript']],
    //     ['fontsize', ['fontsize']],
    //     ['color', ['color']],
    //     ['para', ['ul', 'ol', 'paragraph']],
    //     ['height', ['height']],
    //     ['link'],
    //     ['table'],
    //     ['media',['picture','video']],
    //     ['hr'],
    //     ['misc',['fullscreen','codeview','undo','redo','help']]
    //     ],
    //     callbacks: {
    //         onImageUpload: function(files) {
    //             sendFile_about(files[0], 'mission_name');
    //         }
    //     }, 
    // });

    // $('#our_values_name').summernote({
    //      fontSizes: ['10', '11', '12', '14', '16','18', '20' ,'22' ,'24', '36', '48' , '64', '82', '100'],
    //     placeholder: 'Detail this',  
    //     height: 400,
    //     tabsize: 2,
    //     toolbar: [
    //     ['style'],
    //     ['style', ['bold', 'italic', 'underline', 'clear']],
    //     ['font', ['strikethrough', 'superscript', 'subscript']],
    //     ['fontsize', ['fontsize']],
    //     ['color', ['color']],
    //     ['para', ['ul', 'ol', 'paragraph']],
    //     ['height', ['height']],
    //     ['link'],
    //     ['table'],
    //     ['media',['picture','video']],
    //     ['hr'],
    //     ['misc',['fullscreen','codeview','undo','redo','help']]
    //     ],
    //     callbacks: {
    //         onImageUpload: function(files) {
    //             sendFile_about(files[0], 'our_values_name');
    //         }
    //     }, 
    // });  
    
    
   function sendFile_about(file, editor, welEditable) {
        data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: 'POST',
            url: "<?=base_url('img-upload')?>",
            cache: false,
            contentType: false,
            processData: false,
            success: function(data){
                $('#'+editor).summernote('editor.insertImage', data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus+" "+errorThrown);
            }
        });
    } 

</script>
