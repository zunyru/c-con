<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>

                <ol class="breadcrumb">
                    <li class="active">Abouts</li>
                </ol>
                <!-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> -->
            </h2>
        </div>
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                        Abouts
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable pro-exportable">
                                <thead>
                                    <tr>
                                        <th width="30%">รูป banners</th>
                                        <th width="20%">ความเป็นมา</th>
                                        <th width="10%">Craet date</th>
                                        <th width="10%">Upage date</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>รูป banners</th>
                                        <th>ความเป็นมา</th>
                                        <th>Craet date</th>
                                        <th>Upage date</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php 
                                    foreach ($abouts as $index => $item): 
                                        ?>
                                        <tr>
                                            <td>
                                            <?php if(!empty($item->banners)):?>
                                                <img width="70" src="<?=$this->config->item('backend_uploade').$item->banners?>" alt="<?=$item->banners?>">
                                            <?php endif;?>
                                            </td>
                                            <td><?=$item->name?></td>
                                            <td>
                                                <span style="display: none;">
                                                    <?=$item->create_at?>
                                                </span>
                                                <?=DateThai($item->create_at)?></td>
                                            <td>
                                                <span style="display: none;">
                                                    <?=$item->update_at?>
                                                </span>
                                                <?=DateThai($item->update_at)?>
                                            </td>
                                            <td>
                                                <a href="<?=base_url().'abouts/edit/'.$item->id?>" class="btn bg-orange btn-block waves-effect">Edit</a>
                                           </td>
                                       </tr>
                                       <?php 
                                   endforeach; 
                                   ?>    

                               </tbody>
                           </table>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <!-- #END# Exportable Table -->
   </div>
</section>

