<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Abouts extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->model('abouts/abouts_model');
		$this->load->library('uploadfile_library');
		$this->load->library('nestable_library');
		$this->load->library('logs_library');
	}
	
	public function index(){   
		$data = array(
			'page_index' 		=> 'abouts',
			'page' 				=> 'browse',
			'title_page' 		=> 'abouts',
			'description_page' 	=> 'My abouts'
		);
		
		$data['abouts'] = $this->abouts_model->get_AboutsAll();
	
		//loade view
		$this->load->view('masters/header',$data); 
		$this->load->view('index',$data);
	}

	public function edit($id) {
		$data = array(
			'page_index' 		=> 'abouts',
			'page' 				=> 'edit',
			'title_page' 		=> 'Abouts Edit',
			'description_page' 	=> 'My Abouts edit',
		);
        //loade data
		$data['abouts'] = $this->abouts_model->get_AboutsById($id);

        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function update($id) {

		$user = $this->session->userdata('users');

		$path 					= 'abouts';
		$upload_pic 			= $this->uploadfile_library->do_upload('picture',TRUE,$path);

		// $upload_our_values_img1 = $this->uploadfile_library->do_upload('our_values_img1',TRUE,$path);

		// $upload_our_values_img2 = $this->uploadfile_library->do_upload('our_values_img2',TRUE,$path);

		// $upload_our_values_img3 = $this->uploadfile_library->do_upload('our_values_img3',TRUE,$path);

		
		$picture = null;
		if(isset($upload_pic['index'])){
			$picture = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_pic['index']['file_name'];
			$outfile_pic = $this->input->post('outfile_pic');
			if(isset($outfile_pic)){
				$this->load->helper("file");
				unlink($outfile_pic);
			}
		}

		// $our_values_img1 = null;
		// if(isset($upload_our_values_img1['index'])){
		// 	$our_values_img1 = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_our_values_img1['index']['file_name'];
		// 	$outfile_our_values_img1 = $this->input->post('outfile_our_values_img1');
		// 	if(isset($outfile_our_values_img1)){
		// 		$this->load->helper("file");
		// 		unlink($outfile_our_values_img1);
		// 	}
		// }

		// $our_values_img2 = null;
		// if(isset($upload_our_values_img2['index'])){
		// 	$our_values_img2 = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_our_values_img2['index']['file_name'];
		// 	$outfile_our_values_img2 = $this->input->post('outfile_our_values_img2');
		// 	if(isset($outfile_our_values_img2)){
		// 		$this->load->helper("file");
		// 		unlink($outfile_our_values_img2);
		// 	}
		// } 

		// $our_values_img3 = null;
		// if(isset($upload_our_values_img3['index'])){
		// 	$our_values_img3 = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_our_values_img3['index']['file_name'];
		// 	$outfile_our_values_img3 = $this->input->post('outfile_our_values_img3');
		// 	if(isset($outfile_our_values_img3)){
		// 		$this->load->helper("file");
		// 		unlink($outfile_our_values_img3);
		// 	}
		// }

		// if(isset($upload['error'])){
		// 	$this->session->set_flashdata('status',0);
		// 	redirect(base_url('abouts'));
		// }

		$data = array(
			'id'     		 	=> $id,
			// 'name'  		 	=> $this->input->post('name'),
			// 'slug'      	 	=> $this->input->post('slug'),
			'companybg_name'    => $this->input->post('companybg_name'),
			// 'values'      	 	=> $this->input->post('values'),
			// 'values_name'      	=> $this->input->post('values_name'),
			// 'mission'      	 	=> $this->input->post('mission'),
			// 'mission_name'      => $this->input->post('mission_name'),
			// 'our_values'      	=> $this->input->post('our_values'),
			// 'our_values_name'   => $this->input->post('our_values_name'),
			// 'our_values_img1' 	=> $our_values_img1,
			// 'our_values_img2' 	=> $our_values_img2,
			// 'our_values_img3' 	=> $our_values_img3,
			'banners'     	 	=> $picture,
			'create_by' 	 	=> $user['UID'],
			'update_by' 	 	=> $user['UID'],
			'active'    	 	=> 0,
			'create_at' 	 	=> date('Y-m-d H:i:s'),
			'update_at' 	 	=> date('Y-m-d H:i:s'),
		);
 
		$last_id = $this->abouts_model->update(array_to_obj($data));

		//Log
		$messege = 'แก้ไขข้อมูล : '.$this->input->post('name') 
		.' ,img : '.$picture;
		// .' ,img : '.$our_values_img1
		// .' ,img : '.$our_values_img2
		// .' ,img : '.$our_values_img3;

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('abouts'));
		}
	} 

	public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'abouts';
		$upload = $this->uploadfile_library->do_upload('file',TRUE,$path);

		if(isset($upload['index'])){
			$picture = '/uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
	}	

	public function deletePic($ids){

		$arrayName = array('banners' => $ids);

		echo json_encode($arrayName);
	}

	public function deletePic2($ids){

		$arrayName = array('our_values_img1' => $ids);

		echo json_encode($arrayName);
	}

	public function deletePic3($ids){

		$arrayName = array('our_values_img2' => $ids);

		echo json_encode($arrayName);
	}

	public function deletePic4($ids){

		$arrayName = array('our_values_img3' => $ids);

		echo json_encode($arrayName);
	}
	
}
