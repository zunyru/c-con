<?php

class Product_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getProductAll() {

		$this->db->select('pro.*,cat.name AS cat_name');
		$this->db->from('products pro');
		$this->db->join('categories cat', 'pro.category_id = cat.id', 'left');
		$this->db->order_by('pro.name', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_ProductById($id) {

		$this->db->select('pro.*,cat.name AS cat_name');
		$this->db->from('products pro');
		$this->db->join('categories cat', 'pro.category_id = cat.id', 'left');
		$this->db->where('pro.id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->id;
			$row->name;
			$row->pic;
			$row->detail;
			$row->category_id;
			$row->catalog;
			$row->cat_name;
		}

		return $row;
	}

	public function insert($data)
	{
		$query = $this->db->insert('products', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
     
    public function update($data)
	{
		$this->db->set('name', $data->name);
		$this->db->set('slug', $data->slug);
		if(!is_null($data->pic)){
			$this->db->set('pic', $data->pic);
		}
		if(!is_null($data->catalog)){
			$this->db->set('catalog', $data->catalog);
		}else{
			if(!empty($data->catalog_key)){
				$this->db->set('catalog', $data->catalog);
			}
		}
		
		$this->db->set('detail', $data->detail);
		$this->db->set('create_by', $data->create_by);
		$this->db->set('update_by', $data->update_by);
		$this->db->set('category_id', $data->category_id);
		$this->db->set('create_at', $data->create_at);
		$this->db->set('update_at', $data->update_at);
		$this->db->set('new', $data->new);
		$this->db->where('id', $data->id);
		$update = $this->db->update('products');

		return $update;
	}

	public function destroy($id)
	{
		$this->db->where('id', $id);
		$delete = $this->db->delete('products');

		return $delete;
	}

	public function get_productCount(){
		return $this->db->count_all_results('products');
	}
	
}