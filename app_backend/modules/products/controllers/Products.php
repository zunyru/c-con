<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->model('products/product_model');
		$this->load->model('categorys/category_model');
		$this->load->model('seo/seo_model');
		$this->load->library('uploadfile_library');
		$this->load->library('nestable_library');
		$this->load->library('logs_library');
	}
	
	public function index()
	{   
		$data = array(
			'page_index' => 'products',
			'page' => 'browse',
			'title_page' => 'Product Browse',
			'description_page' => 'My Product'
		);
		$data['products'] = $this->product_model->getProductAll();

		//loade view
		$this->load->view('masters/header',$data); 
		$this->load->view('index',$data);
	}

	public function create() {
		$data = array(
			'page_index' => 'products',
			'page' => 'create',
			'title_page' => 'Create',
			'description_page' => 'My Products create',
		);
        //loade data
		$data['categorys'] = $this->category_model->getCategoryRelateOnMaster();
		$data['categorys_sub'] = $this->category_model->getCategoryRelateAll();

        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function store(){

		$user = $this->session->userdata('users');
 
		$path = 'product';
		$upload_pic = $this->uploadfile_library->do_upload('picture',TRUE,$path);

		$upload_catalog = $this->uploadfile_library->do_upload('catalog',TRUE,$path);

        $picture ='';
		if(isset($upload_pic['index'])){
			$picture = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_pic['index']['file_name'];
		}

		$catalog = ''; 
		if(isset($upload_catalog['index'])){
			$catalog = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_catalog['index']['file_name'];
		}

		$dataseo = array(
			'name'  		 => $this->input->post('seo_name'),
			'robots'      	 => $this->input->post('robots'),
			'description'    => $this->input->post('description'),
			'keywords'       => $this->input->post('keywords'),
			'type'			 => 1,			
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'active'    	 => 0,
			'create_at' 	 => date('Y-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		// load add seo

		$seo_id = $this->seo_model->insert($dataseo);

		$data = array(
			'name'      => $this->input->post('name'),
			'slug'      => $this->input->post('slug'),
			'pic'       => $picture,
			'catalog'   => $catalog,
			'seo_id'	=> $seo_id,
			'detail'	=> $this->input->post('detail'),
			'create_by' => $user['UID'],
			'update_by' => $user['UID'],
			'category_id' => $this->input->post('category'),
			'active'    => 0,
			'create_at' => date('Y-m-d H:i:s'),
			'update_at' => date('Y-m-d H:i:s'),
			'new'       => $this->input->post('new') == 'on' ? 1 : NULL,
		);

		$last_id = $this->product_model->insert($data);	

		//Log
		$messege = 'เพิ่มข้อมูล สินค้า : '.$this->input->post('name') 
		.' ,ประเภท : '.$this->input->post('category')
		.' ,รูปสินค้า : '.$picture
		.' ,catalog : '.$catalog;

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('products'));
		}	

	}

	public function edit($id) {
		$data = array(
			'page_index' => 'products',
			'page' => 'edit',
			'title_page' => 'Edit',
			'description_page' => 'My Products edit',
		);
        //loade data
		$data['categorys'] = $this->category_model->getCategoryRelateOnMaster();
		$data['categorys_sub'] = $this->category_model->getCategoryRelateAll();
		$data['product'] = $this->product_model->get_ProductById($id);
		$data['seo'] = $this->seo_model->get_SeoById($data['product']->seo_id);

        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function update($id) {

		$user = $this->session->userdata('users');

		$path = 'product';
		$upload_pic = $this->uploadfile_library->do_upload('picture',TRUE,$path);


		$upload_catalog = $this->uploadfile_library->do_upload('catalog',TRUE,$path);

		$picture = null;
		if(isset($upload_pic['index'])){
			$picture = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_pic['index']['file_name'];
			$outfile_pic = $this->input->post('outfile_pic');
			if(isset($outfile_pic)){
				$this->load->helper("file");
				unlink($outfile_pic);
			}
		} 

		$catalog = null; 
		if(isset($upload_catalog['index'])){
			$catalog = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_catalog['index']['file_name'];
			$outfile_catalog = $this->input->post('outfile_catalog');
			if(isset($outfile_catalog)){
				$this->load->helper("file");
				unlink($outfile_catalog);
			}
		}

		// if(isset($upload_pic['error'])){
		// 	$this->session->set_flashdata('status',0);
		// 	redirect(base_url('products'));
		// }
		// 
		$data = array(
			'id'        => $id,
			'name'      => $this->input->post('name'),
			'slug'      => $this->input->post('slug'),
			'pic'       => $picture,
			'catalog'   => $catalog,
			'catalog_key' => $this->input->post('catalog_key'),
			'detail'	=> $this->input->post('detail'),
			'create_by' => $user['UID'],
			'update_bys' => $user['UID'],
			'category_id' => $this->input->post('category'),
			'active'    => 0,
			'create_at' => date('Y-m-d H:i:s'),
			'update_at' => date('Y-m-d H:i:s'),
			'new'       => $this->input->post('new') == 'on' ? 1 : NULL,
		); 

		$last_id = $this->product_model->update(array_to_obj($data));
		

		$dataseo = array(
			'id'        	 => $this->input->post('seo_id'),
			'name'  		 => $this->input->post('seo_name'),
			'robots'      	 => $this->input->post('robots'),
			'description'    => $this->input->post('description'),
			'keywords'       => $this->input->post('keywords'),
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'active'    	 => 0,
			'create_at' 	 => date('Y-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);
		 // load update seo
		$this->seo_model->update(array_to_obj($dataseo));

		//Log
		$messege = 'แก้ไขข้อมูล สินค้า : '.$this->input->post('name') 
		.' ,ประเภท : '.$this->input->post('category')
		.' ,รูปสินค้า : '.$picture
		.' ,catalog : '.$catalog
		.' ,Seo :'.$this->input->post('seo_name');


		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('products'));
		}
	} 

	public function destroy($id)
	{
		$user = $this->session->userdata('users');
		
		$data = $this->product_model->get_ProductById($id);

		if(!empty($data->pic)){
			$this->load->helper("file");
			unlink($data->pic);
		}

		if(!empty($data->catalog)){
			$this->load->helper("file");
			unlink($data->catalog);
		}
		
		$delete = $this->product_model->destroy($id);


		if($delete){
			//Log
		$messege = 'ลบข้อมูล สินค้า : '.$data->name; 

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

			$this->session->set_flashdata('status',1);
			redirect(base_url('products'));
		}else{
			$this->session->set_flashdata('status',0);
			redirect(base_url('products'));
		}
	}

	public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'content';
		$upload = $this->uploadfile_library->do_upload('file',TRUE,$path);

		if(isset($upload['index'])){
			$picture = '/uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
	}	

	public function deletepic($ids)
	{

		$arrayName = array('pic' => $ids);

		echo json_encode($arrayName);
	}

	public function deletefile($ids)
	{

		$arrayName = array('catalog' => $ids);

		echo json_encode($arrayName);
	}	
}
