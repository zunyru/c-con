
<script type="text/javascript">

    //fileinput
    var picture_image = '<?=(!empty($product->pic)) ? $this->config->item('backend_uploade').$product->pic : ''; ?>';
    $('#picture').fileinput({

        initialPreview: [picture_image],
        initialPreviewAsData: true,
        initialPreviewConfig: [
        { downloadUrl: picture_image  ,extra: {id: '<?=$product->id;?>'} },
        ],
        deleteUrl: "<?=base_url().'deleteprodpic/'.$product->id?>",

        maxFileCount: 1,
        validateInitialCount: true,
        overwriteInitial: false,
        showUpload: false,
        showRemove: true,
        required: true,
        initialPreviewAsData: true,
        maxFileSize:300,
        browseOnZoneClick: true,
        allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
        browseLabel: "Picture",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop picture here …",
    });
    

    var catalog = '<?=(!empty($product->catalog)) ? $this->config->item('backend_uploade').$product->catalog : ''; ?>';
   // var catalog = 'http://plugins.krajee.com/uploads/sample-2.pdf';
   $('#catalog').fileinput({
      <?php if(!empty($product->catalog)){?>
        initialPreview: [catalog],
        //pdfRendererUrl: 'http://plugins.krajee.com/pdfjs/web/viewer.html',
        overwriteInitial: false,
        initialPreviewAsData: true,
        preferIconicPreview: true,
        previewFileIconSettings: { 
            'pdf': '<i class="material-icons" style="font-size: 100%;color:#fea002;">picture_as_pdf</i>',
        },
        initialPreview: [
        catalog
        ],
        initialPreviewConfig: [
        {type: "pdf", caption: "<?=$product->name?>.pdf", downloadUrl: false,extra: {id: '<?=$product->id;?>'}},
        ],
        deleteUrl: "<?=base_url().'deletefile/'.$product->id?>",
    <?php }?> 
        maxFileCount: 1,
        validateInitialCount: true,
        overwriteInitial: false,
        showUpload: false,
        showRemove: true,
        //required: false,
        initialPreviewAsData: true,
        //maxFileSize:300,
        browseOnZoneClick: true,
        allowedFileExtensions: ["pdf"],
        browseLabel: "Catalog",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop catalog file here …",
    }).on('filepredelete', function(event, key, jqXHR, data) { 
        $('#catalog_key').val(data.id);
    });

</script>
