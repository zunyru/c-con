<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li><a href="<?=base_url('products')?>">Product</a></li>
                <li class="active"><?=$title_page?></li>
            </ol>
        </div>
        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>สินค้า</h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <div class="body">
                        <?php 
                        if($page === 'create'): 
                            echo form_open_multipart(base_url().'products/store','id="form_validation"');
                        else: 
                            echo form_open_multipart(base_url().'products/update/'.$product->id,'id="form_validation"');
                        endif;    
                        ?>
                        <input type="hidden" class="form-control" name="db" id="db" value="products">
                        <input type="hidden" class="form-control" name="id" id="id" value="<?=isset($product->id)? $product->id : '';?>">

                        <div class="form-group">
                            <div class="switch panel-switch-btn">
                                <?php
                                if($product->new > 0){
                                    $new = 'checked';
                                }else{
                                    $new = '';
                                }
                                ?>
                                <label>
                                    <input type="checkbox" id="realtime" name="new" <?=$new?>>
                                    <span class="lever switch-col-cyan"></span>สินค้าใหม่
                                </label>
                            </div> 
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="name" id="name" value="<?=isset($product->name)? $product->name : '';?>" required>
                                <label class="form-label">ชื่อสินค้า</label>
                            </div>
                            <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="slug" id="slug" value="<?=isset($product->slug)? $product->slug : '';?>" required>
                                <label class="form-label">Slug</label>
                            </div>
                            <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                        </div>

                        <div class="form-group">
                            <p>
                                หมวดหมู่/ประเภท
                            </p>
                            <?php //var_dump($categorys); ?>
                            <select class="form-control show-tick" name="category" required>
                                <option value="">--เลือกหมวดหมู่--</option>
                                <?php 
                                foreach ($categorys as $key => $category) {
                                    if($category->id === $product->category_id){
                                      $selected = 'selected';
                                  }else{
                                      $selected = ''; 
                                  }
                                  echo '<option value="'.$category->id.'" '.$selected.'>'.$category->name.'</option>';

                              }
                              ?>
                          </select>
                      </div>
                      <div class="form-group form-float">
                        <p>
                            รูป สินค้า
                        </p>
                        <div class="form-line">
                            <input id="picture" name="picture" type="file" class="file" data-preview-file-type="text">
                            <input type="hidden" name="outfile_pic" value="<?=(isset($product->pic)) ? $product->pic : ''; ?>">
                        </div>
                    </div> 

                    <div class="form-group form-float">
                        <p>
                            Catalog : (File pdf only)
                        </p>
                        <div class="form-line">
                            <input id="catalog" name="catalog" type="file" class="file" data-preview-file-type="text">
                            <input type="hidden" name="outfile_catalog" value="<?=(isset($product->catalog)) ? $product->catalog : ''; ?>">
                            <input type="hidden" id="catalog_key" name="catalog_key">
                        </div>
                    </div> 

                    <div class="form-group">
                        <textarea  class="summernote" name="detail"><?=isset($product->detail)? $product->detail :'';?></textarea>
                    </div>

                    <?php
                    // load view seo
                    $this->load->view('seo/seo');
                    ?>

                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
</div>
</section>





