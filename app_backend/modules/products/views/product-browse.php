<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>

                <ol class="breadcrumb">
                    <li class="active">Products</li>
                </ol>
                <!-- <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> -->
            </h2>
        </div>
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            ข้อมูลสินค้า
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <a href="<?=base_url('products/create')?>" class="btn bg-deep-orange waves-effect">Create</a>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable pro-exportable">
                                <thead>
                                    <tr>
                                        <th width="30%">Name</th>
                                        <th width="20%">Slug</th>
                                        <th width="20%">Category</th>
                                        <th width="10%">Craet date</th>
                                        <th width="10%">Upage date</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Slug</th>
                                        <th>Category</th>
                                        <th>Craet date</th>
                                        <th>Upage date</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php 
                                    foreach ($products as $index => $product): 
                                        ?>
                                        <tr>
                                            <td><?=$product->name?></td>
                                            <td><?=$product->slug?></td>
                                            <td><?=$product->cat_name?></td>
                                            <td>
                                                <span style="display: none;">
                                                    <?=$product->create_at?>
                                                </span>
                                                <?=DateThai($product->create_at)?></td>
                                            <td>
                                                <span style="display: none;">
                                                    <?=$product->update_at?>
                                                </span>
                                                <?=DateThai($product->update_at)?>
                                            </td>
                                            <td>
                                                <a href="<?=base_url().'products/edit/'.$product->id?>" class="btn bg-orange btn-block waves-effect">Edit</a>

                                               <a href="<?=base_url().'products/destroy/'.$product->id?>" class="btn bg-red btn-block waves-effect delete">Delete</a>

                                           </td>
                                       </tr>
                                       <?php 
                                   endforeach; 
                                   ?>    

                               </tbody>
                           </table>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <!-- #END# Exportable Table -->
   </div>
</section>

