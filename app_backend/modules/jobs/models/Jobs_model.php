<?php

class Jobs_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getJobsAll() {

		$this->db->select('*');
		$this->db->from('jobs');
		$this->db->order_by('jobs.jobs_name', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_JobsById($id) {
		$this->db->select('*');
		$this->db->from('jobs');
		$this->db->where('jobs_id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->jobs_id;
			$row->jobs_name;
			$row->jobs_img;
			$row->jobs_detail;
			$row->jobs_file;
		}


		return $row;
	}

	public function insert($data){
		$query = $this->db->insert('jobs', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
     
	public function update($data){

		$this->db->set('jobs_name', $data->jobs_name);
		$this->db->set('slug', $data->slug);
		if(!is_null($data->jobs_img)){
			$this->db->set('jobs_img', $data->jobs_img);
		}
		if(!is_null($data->jobs_file)){
			$this->db->set('jobs_file', $data->jobs_file);
		}
		$this->db->set('jobs_detail', $data->jobs_detail);
		$this->db->set('create_by', $data->create_by);
		$this->db->set('update_by', $data->update_by);
		$this->db->set('create_at', $data->create_at);
		$this->db->set('update_at', $data->update_at);
		$this->db->where('jobs_id', $data->jobs_id);
		$update = $this->db->update('jobs');

		return $update;
	}

	public function destroy($id){
		$this->db->where('jobs_id', $id);
		$delete = $this->db->delete('jobs');

		return $delete;
	}
	
}