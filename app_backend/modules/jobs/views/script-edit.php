
<script type="text/javascript">

    //fileinput
    var picture_image = '<?=(!empty($jobs->jobs_img)) ? $this->config->item('backend_uploade').$jobs->jobs_img : ''; ?>';
    $('#picture').fileinput({

        initialPreview: [picture_image],
        initialPreviewAsData: true,
        initialPreviewConfig: [
        { downloadUrl: picture_image  ,extra: {id: '<?=$jobs->jobs_id;?>'} },
        ],
        deleteUrl: "<?=base_url().'jobsregisters/deletepic/'.$jobs->jobs_id?>",

        maxFileCount: 1,
        validateInitialCount: true,
        overwriteInitial: false,
        showUpload: false,
        showRemove: true,
        required: true,
        initialPreviewAsData: true,
        maxFileSize:1024,
        browseOnZoneClick: true,
        allowedFileExtensions: ["jpg", "png", "gif" ,"svg"],
        browseLabel: "Picture",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop picture here …",
    });
    

    var catalog = '<?=(!empty($jobs->jobs_file)) ? $this->config->item('backend_uploade').$jobs->jobs_file : ''; ?>';
   // var catalog = 'http://plugins.krajee.com/uploads/sample-2.pdf';
   $('#jobs_file').fileinput({
      <?php if(!empty($jobs->jobs_file)){?>
        initialPreview: [catalog],
        //pdfRendererUrl: 'http://plugins.krajee.com/pdfjs/web/viewer.html',
        overwriteInitial: false,
        initialPreviewAsData: true,
        preferIconicPreview: true,
        previewFileIconSettings: { 
            'pdf': '<i class="material-icons" style="font-size: 100%;color:#fea002;">picture_as_pdf</i>',
        },
        initialPreview: [
        catalog
        ],
        initialPreviewConfig: [
        {type: "pdf", caption: "<?=$jobs->jobs_name?>.pdf", downloadUrl: false,extra: {id: '<?=$jobs->jobs_id;?>'}},
        ],
        deleteUrl: "<?=base_url().'deletepic/'.$jobs->jobs_id?>",
    <?php }?> 
    maxFileCount: 1,
    validateInitialCount: true,
    overwriteInitial: false,
    showUpload: false,
    showRemove: true,
        //required: false,
        initialPreviewAsData: true,
        maxFileSize:2024,
        browseOnZoneClick: true,
        allowedFileExtensions: ["pdf"],
        browseLabel: "Catalog",
        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        removeClass: 'btn btn-danger',
        dropZoneTitle :"Drag & drop catalog file here …",
    });

</script>
