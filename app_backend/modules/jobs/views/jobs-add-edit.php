<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li><a href="<?=base_url('jobs')?>">Jobs</a></li>
                <li class="active"><?=$title_page?></li>
            </ol>
        </div>
        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Jobs</h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <div class="body">
                        <?php 
                        if($page === 'create'): 
                            echo form_open_multipart(base_url().'jobs/store','id="form_validation"');
                        else: 
                            echo form_open_multipart(base_url().'jobs/update/'.$jobs->jobs_id,'id="form_validation"');
                        endif;    
                        ?>
                        <input type="hidden" class="form-control" name="db" id="db" value="jobs">
                        <input type="hidden" class="form-control" name="jobs_id" id="id" value="<?=isset($jobs->jobs_id)? $jobs->jobs_id : '';?>">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="jobs_name" id="name" value="<?=isset($jobs->jobs_name)? $jobs->jobs_name : '';?>" required>
                                <label class="form-label">Title</label>
                            </div>
                            <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="slug" id="slug" value="<?=isset($jobs->slug)? $jobs->slug : '';?>" required>
                                <label class="form-label">Slug</label>
                            </div>
                            <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                        </div>
                    <div class="form-group form-float">
                        <p>
                            Img
                        </p>
                        <div class="form-line">
                            <input id="picture" name="picture" type="file" class="file" data-preview-file-type="text">
                            <input type="hidden" name="outfile_pic" value="<?=(isset($jobs->jobs_img)) ? $jobs->jobs_img : ''; ?>">
                        </div>
                    </div> 

                    <div class="form-group form-float">
                        <p>
                            File : (File pdf only)
                        </p>
                        <div class="form-line">
                            <input id="jobs_file" name="jobs_file" type="file" class="file" data-preview-file-type="text">
                            <input type="hidden" name="outfile_jobs_file" value="<?=(isset($jobs->jobs_file)) ? $jobs->jobs_file : ''; ?>">
                        </div>
                    </div> 

                    <div class="form-group">
                        <textarea  class="summernote" name="jobs_detail"><?=isset($jobs->jobs_detail)? $jobs->jobs_detail :'';?></textarea>
                    </div>

                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
</div>
</section>





