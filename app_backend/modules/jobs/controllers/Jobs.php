<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->model('jobs/jobs_model');
		$this->load->library('uploadfile_library');
		$this->load->library('nestable_library');
		$this->load->library('logs_library');
	}
	
	public function index(){   
		$data = array(
			'page_index' 		=> 'jobs',
			'page' 				=> 'browse',
			'title_page' 		=> 'ประกาศรับสมัครงาน',
			'description_page' 	=> 'My jobs'
		);
		$data['jobs'] = $this->jobs_model->getJobsAll();
	
		//loade view
		$this->load->view('masters/header',$data); 
		$this->load->view('index',$data);
	}

	public function create() {
		$data = array(
			'page_index' => 'jobs',
			'page' => 'create',
			'title_page' => 'Create',
			'description_page' => 'My jobs create',
		);
    
        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function store(){

		$user 				= $this->session->userdata('users');
		$path 				= 'jobs';
		$upload_pic 		= $this->uploadfile_library->do_upload('picture',TRUE,$path);
		$upload_jobs_file 	= $this->uploadfile_library->do_upload('jobs_file',TRUE,$path);

        $picture ='';
		if(isset($upload_pic['index'])){
			$picture = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_pic['index']['file_name'];
		}

		$jobs_file = ''; 
		if(isset($upload_jobs_file['index'])){
			$jobs_file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_jobs_file['index']['file_name'];
		}

		$data = array(
			'jobs_name'      => $this->input->post('jobs_name'),
			'slug'      	 => $this->input->post('slug'),
			'jobs_img'       => $picture,
			'jobs_file'   	 => $jobs_file,
			'jobs_detail'	 => $this->input->post('jobs_detail'),
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'active'    	 => 0,
			'create_at' 	 => date('Y-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		$last_id = $this->jobs_model->insert($data);	

		//Log
		$messege = 'เพิ่มข้อมูล : '.$this->input->post('jobs_name') 
		.' ,Img : '.$picture
		.' ,File : '.$jobs_file;

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('jobs'));
		}	

	}

	public function edit($id) {
		$data = array(
			'page_index' 		=> 'jobs',
			'page' 				=> 'edit',
			'title_page' 		=> 'Jobs Edit',
			'description_page' 	=> 'My jobs edit',
		);
        //loade data
		$data['jobs'] = $this->jobs_model->get_JobsById($id);

        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function update($id) {

		$user = $this->session->userdata('users');

		$path = 'jobs';
		$upload_pic 		= $this->uploadfile_library->do_upload('picture',TRUE,$path);
		$upload_jobs_file 	= $this->uploadfile_library->do_upload('jobs_file',TRUE,$path);

		$picture = null;
		if(isset($upload_pic['index'])){
			$picture = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_pic['index']['file_name'];
			$outfile_pic = $this->input->post('outfile_pic');
			if(isset($outfile_pic)){
				$this->load->helper("file");
				unlink($outfile_pic);
			}
		}

		$jobs_file = null; 
		if(isset($upload_jobs_file['index'])){
			$jobs_file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload_jobs_file['index']['file_name'];
			$outfile_jobs_file = $this->input->post('outfile_jobs_file');
			if(isset($outfile_jobs_file)){
				$this->load->helper("file");
				unlink($outfile_jobs_file);
			}
		}

		if(isset($upload['error'])){
			$this->session->set_flashdata('status',0);
			redirect(base_url('jobs'));
		}

		$data = array(
			'jobs_id'        => $id,
			'jobs_name'      => $this->input->post('jobs_name'),
			'slug'      	 => $this->input->post('slug'),
			'jobs_img'       => $picture,
			'jobs_file'   	 => $jobs_file,
			'jobs_detail'	 => $this->input->post('jobs_detail'),
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'active'    	 => 0,
			'create_at' 	 => date('Ycatalog-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		$last_id = $this->jobs_model->update(array_to_obj($data));

		//Log
		$messege = 'แก้ไขข้อมูล : '.$this->input->post('jobs_name') 
		.' ,img : '.$picture
		.' ,jobs_file : '.$jobs_file;

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('jobs'));
		}
	} 

	public function destroy($id){
		$user = $this->session->userdata('users');
		
		$data = $this->jobs_model->get_JobsById($id);

		if(!empty($data->jobs_img)){
			$this->load->helper("file");
			unlink($data->jobs_img);
		}

		if(!empty($data->jobs_file)){
			$this->load->helper("file");
			unlink($data->jobs_file);
		}
		
		$delete = $this->jobs_model->destroy($id);


		if($delete){
			//Log
		$messege = 'ลบข้อมูล : '.$data->jobs_name; 

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

			$this->session->set_flashdata('status',1);
			redirect(base_url('jobs'));
		}else{
			$this->session->set_flashdata('status',0);
			redirect(base_url('jobs'));
		}
	}

	public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'content';
		$upload = $this->uploadfile_library->do_upload('file',TRUE,$path);

		if(isset($upload['index'])){
			$picture = '/uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
	}	

	public function deletePic($ids){

		$arrayName = array('pic' => $ids);

		echo json_encode($arrayName);
	}
	
}
