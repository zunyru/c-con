<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li class="active"><?=$title_page?></li>
            </ol>
        </div>
        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Email</h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <div class="body">
                    <?php 
                        echo form_open_multipart(base_url().'email/update/'.$email[0]->id,'id="form_validation"');  
                    ?>
                    <input type="hidden" class="form-control" name="id" id="id" value="<?=isset($email[0]->id)? $email[0]->id : '';?>">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="Host" id="Host" value="<?=isset($email[0]->Host)? $email[0]->Host : '';?>" required>
                            <label class="form-label">Host</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="Username" id="Username" value="<?=isset($email[0]->Username)? $email[0]->Username : '';?>" required>
                            <label class="form-label">Username</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="Password" id="Password" value="<?=isset($email[0]->Password)? $email[0]->Password : '';?>" required>
                            <label class="form-label">Password</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="SMTPSecure" id="SMTPSecure" value="<?=isset($email[0]->SMTPSecure)? $email[0]->SMTPSecure : '';?>" required>
                            <label class="form-label">SMTPSecure</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="text" class="form-control" name="Port" id="Port" value="<?=isset($email[0]->Port)? $email[0]->Port : '';?>" required>
                            <label class="form-label">Port</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <table class="table">
                        <thead>
                            <tr>
                                <td>Page</td>
                                <td>Send To Mail</td>
                            </tr>
                        <thead>
                        <tbody>
                            <?php
                            if(isset($sendemail)){
                                foreach($sendemail as $item){
                            ?>
                            <tr>
                                <td>
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="page[]" value="<?=$item->page;?>" required>
                                        <input type="hidden" name="mail_id[]" value="<?=$item->id;?>" required>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="send_mail[]" value="<?=$item->send_mail;?>" required>
                                    </div>
                                </td>
                            </tr>
                            <?php 
                                }
                            }
                            ?>
                        </tbody>
                        </table>
                    </div>
                    
                    
                    <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
</div>
</section>





