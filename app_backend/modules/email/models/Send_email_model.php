<?php

class Send_email_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_SendEmailAll() {

		$this->db->select('*');
		$this->db->from('config_mail_send');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_SendEmailById($id) {
		$this->db->select('*');
		$this->db->from('config_mail_send');
		$this->db->where('id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->id;
			$row->page;
			$row->send_mail;
		}

		return $row;
	}

	public function insert($data){
		$query = $this->db->insert('config_mail_send', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
     
	public function update($data){

		$this->db->set('page', $data->page);
		$this->db->set('send_mail', $data->send_mail);
		$this->db->set('create_by', $data->create_by);
		$this->db->set('update_by', $data->update_by);
		$this->db->set('create_at', $data->create_at);
		$this->db->set('update_at', $data->update_at);
		$this->db->where('id', $data->id);
		$update = $this->db->update('config_mail_send');

		return $update;
	}

	public function destroy($id){
		$this->db->where('id', $id);
		$delete = $this->db->delete('config_mail_send');

		return $delete;
	}
	
}