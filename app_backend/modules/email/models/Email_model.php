<?php

class Email_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_EmailAll() {

		$this->db->select('*');
		$this->db->from('config_mail');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_EmailById($id) {
		$this->db->select('*');
		$this->db->from('config_mail');
		$this->db->where('id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->id;
			$row->Host;
			$row->Username;
			$row->Password;
			$row->SMTPSecure;
			$row->Port;
		}

		return $row;
	}

	public function insert($data){
		$query = $this->db->insert('config_mail', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
     
	public function update($data){

		$this->db->set('Host', $data->Host);
		$this->db->set('Username', $data->Username);
		$this->db->set('Password', $data->Password);
		$this->db->set('SMTPSecure', $data->SMTPSecure);
		$this->db->set('Port', $data->Port);
		$this->db->set('create_by', $data->create_by);
		$this->db->set('update_by', $data->update_by);
		$this->db->set('create_at', $data->create_at);
		$this->db->set('update_at', $data->update_at);
		$this->db->where('id', $data->id);
		$update = $this->db->update('config_mail');

		return $update;
	}

	public function destroy($id){
		$this->db->where('id', $id);
		$delete = $this->db->delete('config_mail');

		return $delete;
	}
	
}