<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->model('email/email_model');
		$this->load->model('email/send_email_model');
		$this->load->library('uploadfile_library');
		$this->load->library('nestable_library');
		$this->load->library('logs_library');
	}

	public function edit() {
		$data = array(
			'page_index' 		=> 'backendlog',
			'page' 				=> 'email',
			'title_page' 		=> 'Email Edit',
			'description_page' 	=> 'My Email edit',
		);
        //loade data
		$data['email'] 		= $this->email_model->get_EmailAll();
		$data['sendemail'] 	= $this->send_email_model->get_SendEmailAll();
        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function update($id) {

		$user = $this->session->userdata('users');

		$_mail_id	 	= $this->input->post('mail_id[]');
		$_page	 		= $this->input->post('page[]');
		$_send_mail	 	= $this->input->post('send_mail[]');

		$data = array(
			'id'     		 => $id,
			'Host'  		 => $this->input->post('Host'),
			'Username'       => $this->input->post('Username'),
			'Password'       => $this->input->post('Password'),
			'SMTPSecure'     => $this->input->post('SMTPSecure'),
			'Port'       	 => $this->input->post('Port'),
			'create_by' 	 => $user['UID'],
			'update_by' 	 => $user['UID'],
			'active'    	 => 0,
			'create_at' 	 => date('Y-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		$last_id = $this->email_model->update(array_to_obj($data));

		if(isset($_mail_id) && count($_mail_id) > 0 && count($_page) > 0 && count($_send_mail) > 0){
			$i = 0;
			foreach($_mail_id as $item){
				$maildata = array(
					'id'     		 => $item,
					'page'  		 => $_page[$i],
					'send_mail'      => $_send_mail[$i],
					'create_by' 	 => $user['UID'],
					'update_by' 	 => $user['UID'],
					'active'    	 => 0,
					'create_at' 	 => date('Y-m-d H:i:s'),
					'update_at' 	 => date('Y-m-d H:i:s'),
				);

				$this->send_email_model->update(array_to_obj($maildata));

				$i++;
			}

		}

		//Log
		$messege = 'แก้ไขข้อมูล : '.$this->input->post('Username');

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('email/edit'));
		}
	} 
}
