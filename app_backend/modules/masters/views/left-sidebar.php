 <section>

    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <?php
            
            $user = $this->session->userdata('users');
            $RolesSystem = explode(',',$user['RolesSystem']);
            ?>
            <div class="image">
                <img src="<?php echo $this->config->item('backend'); ?>images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=$user['Name']?></div>
                <!-- <div class="email">john.doe@example.com</div> -->
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="<?php echo base_url('users/edit/'.$user['UID'])?>"><i class="material-icons">person</i>Profile</a></li>
                        <li role="seperator" class="divider"></li>
                        <li><a href="<?=base_url('logout')?>"><i class="material-icons">input</i>Log Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <?php
                if(in_array("1001", $RolesSystem)){ 
                ?> 
                    <li class="<?=$page == 'dashboard' ? 'active' : '';?>">
                        <a href="<?=base_url('dashboard')?>">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li> 
                <?php 
                }
                ?> 
                <?php
                if(in_array("1002", $RolesSystem)){ 
                ?> 
                    <li class="<?=($page == 'browse' || $page == 'sort' || $page == 'create' || $page == 'edit') && ($page_index == 'products') ? 'active' : '';?>">
                        <a href="<?=base_url('products')?>">
                            <i class="material-icons">view_module</i>
                            <span>Products</span>
                        </a>
                    </li>
                <?php 
                }
                ?> 
                <?php
                if(in_array("1015", $RolesSystem)){ 
                ?> 
                    <li class="<?=($page == 'browse' || $page == 'sort' || $page == 'create' || $page == 'edit') && ($page_index == 'activity') ? 'active' : '';?>">
                        <a href="<?=base_url('activity')?>">
                            <i class="material-icons">picture_in_picture</i>
                            <span>Activity</span>
                        </a>
                    </li>
                <?php 
                }
                ?> 
                <?php
                if(in_array("1003", $RolesSystem)){ 
                ?> 
                    <li class="<?=($page == 'browse' || $page == 'sort' || $page == 'create' || $page == 'edit') && ($page_index == 'category') ? 'active' : '';?>">
                        <a href="<?=base_url('categorys')?>">
                            <i class="material-icons">layers</i>
                            <span>Category</span>
                        </a>
                    </li>
                <?php 
                }
                ?> 
                <?php
                if(in_array("1008", $RolesSystem) 
                    || in_array("1009", $RolesSystem)
                    || in_array("1010", $RolesSystem)
                    || in_array("1011", $RolesSystem)
                    || in_array("1012", $RolesSystem)){
                ?>
                    <li class="<?=($page_index == 'backendlog') ? 'active' : '';?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">build</i>
                            <span>Setting</span>
                        </a>
                        <ul class="ml-menu">
                            <?php
                            if(in_array("1008", $RolesSystem)){ 
                            ?> 
                                <li class="<?=($page == 'seo' || $page == 'editseo') && ($page_index == 'backendlog') ? 'active' : '';?>">
                                    <a href="<?=base_url('seo')?>">SEO </a>
                                </li>
                            <?php 
                            }
                            ?>
                            <?php
                            if(in_array("1009", $RolesSystem)){ 
                            ?> 
                                <li class="<?=($page == 'email') && ($page_index == 'backendlog') ? 'active' : '';?>">
                                    <a href="<?=base_url('email/edit')?>">Email </a>
                                </li>
                            <?php 
                            }
                            ?>
                            <?php
                            if(in_array("1010", $RolesSystem)){ 
                            ?> 
                                <li class="<?=($page == 'download') && ($page_index == 'backendlog') ? 'active' : '';?>">
                                    <a href="<?=base_url('download')?>">Download Log </a>
                                </li>
                            <?php 
                            }
                            ?>
                            <?php
                            if(in_array("1011", $RolesSystem)){ 
                            ?> 
                                <li class="<?=($page == 'banners' || $page == 'create' || $page == 'edit') && ($page_index == 'backendlog') ? 'active' : '';?>">
                                    <a href="<?=base_url('banners')?>">Banners </a>
                                </li>
                            <?php 
                            }
                            ?>
                            <?php
                            if(in_array("1012", $RolesSystem)){ 
                            ?> 
                                <li class="<?=($page == 'browse') && ($page_index == 'backendlog') ? 'active' : '';?>">
                                    <a href="<?=base_url('logs')?>">Backend Log </a>
                                </li> 
                            <?php 
                            }
                            ?>
                        </ul>
                    </li>
                <?php
                }
                ?>
                <?php
                if(in_array("1013", $RolesSystem) || in_array("1014", $RolesSystem)){
                ?>
                    <li class="<?=($page_index == 'jobs') ? 'active' : '';?>">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">gavel</i>
                            <span>Job</span>
                        </a>
                        <ul class="ml-menu">
                            <?php
                            if(in_array("1013", $RolesSystem)){ 
                            ?> 
                                <li class="<?=($page == 'browse' || $page == 'create' || $page == 'edit') && ($page_index == 'jobs') ? 'active' : '';?>">
                                    <a href="<?=base_url('jobs')?>">สร้างงาน</a>
                                </li>
                            <?php 
                            }
                            ?>
                            <?php
                            if(in_array("1014", $RolesSystem)){ 
                            ?> 
                                <li class="<?=($page == 'jobsregisters' || $page == 'approve') && ($page_index == 'jobs') ? 'active' : '';?>">
                                    <a href="<?=base_url('jobsregisters')?>">ผู้สมัครงาน</a>
                                </li>
                            <?php 
                            }
                            ?>
                        </ul>
                    </li>
                <?php
                }
                ?>
                <?php
                if(in_array("1004", $RolesSystem)){ 
                ?> 
                    <li class="<?=($page == 'browse' || $page == 'read') && ($page_index == 'contacts') ? 'active' : '';?>">
                        <a href="<?=base_url('contacts')?>">
                            <i class="material-icons">local_phone</i>
                            <span>Contacts</span>
                        </a>
                    </li>
                <?php 
                }
                ?> 
                <?php
                if(in_array("1015", $RolesSystem)){ 
                ?> 
                    <li class="<?=($page == 'browse' || $page == 'send') && ($page_index == 'quotations') ? 'active' : '';?>">
                        <a href="<?=base_url('quotations')?>">
                            <i class="material-icons">list_alt</i>
                            <span>Quotations</span>
                        </a>
                    </li>
                <?php 
                }
                ?> 
                <?php
                if(in_array("1016", $RolesSystem)){ 
                ?> 
                    <li class="<?=($page == 'browse' || $page == 'create' || $page == 'edit') && ($page_index == 'catalog') ? 'active' : '';?>">
                        <a href="<?=base_url('catalog')?>">
                            <i class="material-icons">inbox</i>
                            <span>Catalog</span>
                        </a>
                    </li>
                <?php 
                }
                ?>
                <?php
                if(in_array("1005", $RolesSystem)){ 
                ?> 
                    <li class="<?=($page == 'browse' || $page == 'edit') && ($page_index == 'abouts') ? 'active' : '';?>">
                        <a href="<?=base_url('abouts')?>">
                            <i class="material-icons">account_balance</i>
                            <span>Abouts</span>
                        </a>
                    </li>
                <?php 
                }
                ?>
                <?php
                if(in_array("1006", $RolesSystem)){ 
                ?> 
                    <li class="<?=($page == 'browse' || $page == 'create' || $page == 'edit') && ($page_index == 'users') ? 'active' : '';?>">
                        <a href="<?=base_url('users/admin')?>">
                            <i class="material-icons">group</i>
                            <span>Users</span>
                        </a>
                    </li>
                <?php 
                }
                ?>
                <?php
                if(in_array("1007", $RolesSystem)){ 
                ?> 
                    <li class="<?=($page == 'browse' || $page == 'create' || $page == 'edit') && ($page_index == 'roles') ? 'active' : '';?>">
                        <a href="<?=base_url('roles')?>">
                            <i class="material-icons">settings</i>
                            <span>Role</span>
                        </a>
                    </li>
                <?php 
                }
                ?>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <?php $this->load->view('masters/footer');?>
        <!-- #Footer -->
    </aside>

</section>