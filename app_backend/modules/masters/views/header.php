<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?=isset($title_page) ? $title_page : "Admin | c-contraffic";?></title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo $this->config->item('backend'); ?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo $this->config->item('backend'); ?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo $this->config->item('backend'); ?>plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Colorpicker Css -->
    <link href="<?php echo $this->config->item('backend'); ?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />

    <!-- fileinput -->
    <link href="<?php echo $this->config->item('backend'); ?>plugins/bootstrap-fileinput-master/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

    <!-- Multi Select Css -->
    <link href="<?php echo $this->config->item('backend'); ?>plugins/multi-select/css/multi-select.css" rel="stylesheet">

    <!-- Bootstrap Spinner Css -->
    <link href="<?php echo $this->config->item('backend'); ?>plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">

    <!-- Bootstrap Tagsinput Css -->
    <link href="<?php echo $this->config->item('backend'); ?>plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="<?php echo $this->config->item('backend'); ?>plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- noUISlider Css -->
    <link href="<?php echo $this->config->item('backend'); ?>plugins/nouislider/nouislider.min.css" rel="stylesheet" />

    <!-- Sweet Alert Css -->
    <link href="<?php echo $this->config->item('backend'); ?>plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- JQuery Nestable Css -->
    <link href="<?php echo $this->config->item('backend'); ?>plugins/nestable/jquery-nestable.css" rel="stylesheet" />

    <!-- JQuery Confirm Css -->
    <link href="<?php echo $this->config->item('backend'); ?>plugins/jquery-confirm-v3.3.0/jquery-confirm.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?php echo $this->config->item('backend'); ?>plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- My Custom Css -->
    <link href="<?php echo $this->config->item('backend'); ?>css/materialize.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?php echo $this->config->item('backend'); ?>css/style.css" rel="stylesheet">

    <link href="<?php echo $this->config->item('assets'); ?>summernote-bs4/summernote.css" rel="stylesheet" />

    <!-- My Custom Css -->
    <link href="<?php echo $this->config->item('backend'); ?>css/custom-style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo $this->config->item('backend'); ?>css/themes/all-themes.css" rel="stylesheet" />



    <!-- Jquery Core Js -->
    <script src="<?php echo $this->config->item('backend'); ?>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo $this->config->item('backend'); ?>plugins/bootstrap/js/bootstrap.js"></script>

</head>