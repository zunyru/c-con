

<!-- Select Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Jquery Validation Plugin Css -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-validation/jquery.validate.js"></script>

<!-- JQuery Steps Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-steps/jquery.steps.js"></script>

<!-- Bootstrap Colorpicker Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>

<!-- Dropzone Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/dropzone/dropzone.js"></script>

<!-- Input Mask Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

<!-- Multi Select Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>/plugins/multi-select/js/jquery.multi-select.js"></script>

<!-- Jquery Spinner Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-spinner/js/jquery.spinner.js"></script>

<!-- Bootstrap Tags Input Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<!-- noUISlider Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/nouislider/nouislider.js"></script>

<!-- Sweet Alert Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/sweetalert/sweetalert.min.js"></script>



<!-- Waves Effect Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/node-waves/waves.js"></script>

<!-- Jquery Nestable -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/nestable/jquery.nestable.js"></script>

<!-- Custom Js -->
<script src="<?php echo $this->config->item('backend'); ?>js/admin.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>js/pages/forms/form-validation.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>js/pages/tables/jquery-datatable.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>js/pages/ui/sortable-nestable.js"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-countto/jquery.countTo.js"></script>

<!-- Morris Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/raphael/raphael.min.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/morrisjs/morris.js"></script>

<!-- ChartJs -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/chartjs/Chart.bundle.js"></script>

<!-- Flot Charts Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/flot-charts/jquery.flot.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/flot-charts/jquery.flot.resize.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/flot-charts/jquery.flot.pie.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/flot-charts/jquery.flot.categories.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/flot-charts/jquery.flot.time.js"></script>

<!-- Sparkline Chart Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-sparkline/jquery.sparkline.js"></script>

<!-- fileinput -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/bootstrap-fileinput-master/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/bootstrap-fileinput-master/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/bootstrap-fileinput-master/js/fileinput.min.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/bootstrap-fileinput-master/js/locales/th.js"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<!-- Slug -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/slug/slug.js"></script>

<!--Notify Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>js/pages/ui/notify.min.js"></script>

<!-- Bootstrap Notify Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/bootstrap-notify/bootstrap-notify.js"></script>

<!-- JQuery Confirm  Plugin Js -->
<script src="<?php echo $this->config->item('backend'); ?>plugins/jquery-confirm-v3.3.0/jquery-confirm.js"></script>

<!-- Demo Js -->
<script src="<?php echo $this->config->item('backend'); ?>js/demo.js"></script>

<!-- summernote -->
<script type="text/javascript" src="<?php echo $this->config->item('assets'); ?>summernote-bs4/summernote.js"></script>

<script type="text/javascript">
	$( document ).ready(function() {
		<?php if(isset($flash_message) && $flash_message === 1){?>
			showMessegeSuccess();
		<?php }else if(isset($flash_message) && $flash_message === 0){ ?>
         showMessegeError();
      <?php } ?>

      check_dup_slug = true;
      check_dup_name = true;
      slugURL();

      EditorTool();

   });

   //Skin changer
   function slugURL() {

   	var typingTimer;                
   	var doneTypingInterval = 300;  
   	var $slug = $('#slug');
   	var $name = $('#name');
   	var $id = $('#id');

   	$slug.on('keyup', function () {
   		clearTimeout(typingTimer);
   		typingTimer = setTimeout(doneTypingSlug, doneTypingInterval);
   		$('#slug').val(slug($slug.val()));
   		$('#slug').parent().addClass('focused');
   	});

   	$slug.on('keydown', function () {
   		clearTimeout(typingTimer);
   	});

   	$name.on('keyup', function () {
   		clearTimeout(typingTimer);
   		typingTimer = setTimeout(doneTypingName, doneTypingInterval);
   		stypingTimer = setTimeout(doneTypingSlug, doneTypingInterval);
   		$('#slug').val(slug($name.val()));
   		$('#slug').parent().addClass('focused');
   	});

   	$name.on('keydown', function () {
   		clearTimeout(typingTimer);
   	});
   } 

   function doneTypingSlug () {
   	$.ajax({
   		url: '<?=base_url()."slugs/check_slugs"?>',
   		type: 'POST',
   		dataType: 'json',
   		data: {name: $('#slug').val(), id: $('#id').val() , db: $('#db').val()},
   	})
   	.done(function(data){
   		setTimeout(function(){

   			if(data){
   				$('#slug').parent().removeClass('error');  
   				$('#slug-error-dup').hide()
   				check_dup_slug=true;

   			}else{
   				$('#slug-error-dup').show()
   				$('#slug').parent().addClass('error'); 
   				check_dup_slug=false;
   			}
   		}, 300);    
   	})
   	.fail(function(e) {
   		console.log(e);
   	})
   	.always(function(c) {
   		console.log(c);
   	});
   }  

   function doneTypingName ($name,$id = null) {
   	$.ajax({
   		url: '<?=base_url()."slugs/check_name"?>',
   		type: 'POST',
   		dataType: 'json',
   		data: {name: $('#name').val(), id: $('#id').val() , db: $('#db').val()},
   	})
   	.done(function(data){
   		setTimeout(function(){

   			if(data){
   				$('#name').parent().removeClass('error');  
   				$('#name-error-dup').hide()
   				check_dup_slug=true;

   			}else{
   				$('#name-error-dup').show()
   				$('#name').parent().addClass('error'); 
   				check_dup_slug=false;
   			}
   		}, 300);    
   	})
   	.fail(function(e) {
   		console.log(e);
   	})
   	.always(function(c) {
   		console.log(c);
   	});
   } 
   
   //summernote
   function EditorTool()
   {
      $('.summernote').summernote({
         fontSizes: ['10', '11', '12', '14', '16','18', '20' ,'22' ,'24', '36', '48' , '64', '82', '100'],
        placeholder: 'Detail this',  
        height: 400,
        tabsize: 2,
        toolbar: [
        ['style'],
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['link'],
        ['table'],
        ['media',['picture','video']],
        ['hr'],
        ['misc',['fullscreen','codeview','undo','redo','help']]
        ],
        callbacks: {
            onImageUpload: function(files) {
                sendFile(files[0]);
            }
        },

     });
   } 

   function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: 'POST',
            url: "<?=base_url('img-upload')?>",
            cache: false,
            contentType: false,
            processData: false,
            success: function(data){
                $('.summernote').summernote('editor.insertImage', data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus+" "+errorThrown);
            }
        });
    } 
</script>