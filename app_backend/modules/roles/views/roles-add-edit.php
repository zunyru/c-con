<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <ol class="breadcrumb">
                <li><a href="<?=base_url('roles')?>">Roles</a></li>
                <li class="active"><?=$title_page?></li>
            </ol>
        </div>
        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Roles</h2>
                        <ul class="header-dropdown m-r--5">

                        </ul>
                    </div>
                    <div class="body">
                        <?php 
                        if($page === 'create'): 
                            echo form_open_multipart(base_url().'roles/store','id="form_validation"');
                        else: 
                            echo form_open_multipart(base_url().'roles/update/'.$roles->id,'id="form_validation"');
                        endif;    
                        ?>
                        <input type="hidden" class="form-control" name="db" id="db" value="roles">
                        <input type="hidden" class="form-control" name="id" id="id" value="<?=isset($roles->id)? $roles->id : '';?>">
                  
                        <div class="form-group form-float">
                            <?php
                            $checkbox_name = '';
                            if(!empty($roles->name)):
                                $checkbox_name = 'checked';
                            endif;
                            ?>
                            <div class="form-line">
                                <input type="checkbox" id="checkbox_name" name="name" value="1" <?=$checkbox_name;?>/>
                                <label for="checkbox_name">Top role</label>
                            </div> 
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="display_name" id="display_name" value="<?=isset($roles->display_name)? $roles->display_name : '';?>" required>
                                <label class="form-label">Role name</label>
                            </div> 
                        </div>
                       
                        <div class="form-group form-float">
                            <?php 
                            $roles_system = array(); 
                            if(!empty($roles->roles_system)):
                                $roles_system = explode(',',$roles->roles_system);
                            endif;
                            
                            ?>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Menu</th>
                                        <th class="text-center">Privilege</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Home</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1001" name="role[]" value="1001" <?=(in_array("1001", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1001"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Products</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1002" name="role[]" value="1002" <?=(in_array("1002", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1002"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Category</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1003" name="role[]" value="1003" <?=(in_array("1003", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1003"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Activity</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1015" name="role[]" value="1015" <?=(in_array("1015", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1015"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Contacts</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1004" name="role[]" value="1004" <?=(in_array("1004", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1004"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Abouts</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1005" name="role[]" value="1005" <?=(in_array("1005", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1005"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Users</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1006" name="role[]" value="1006" <?=(in_array("1006", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1006"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Role</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1007" name="role[]" value="1007" <?=(in_array("1007", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1007"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Quotations</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1015" name="role[]" value="1015" <?=(in_array("1015", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1015"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Catalog</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1016" name="role[]" value="1016" <?=(in_array("1016", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1016"></label>
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <th>Setting</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td>SEO</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1008" name="role[]" value="1008" <?=(in_array("1008", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1008"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1009" name="role[]" value="1009" <?=(in_array("1009", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1009"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Download Log</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1010" name="role[]" value="1010" <?=(in_array("1010", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1010"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Banners</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1011" name="role[]" value="1011" <?=(in_array("1011", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1011"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Backend Log</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1012" name="role[]" value="1012" <?=(in_array("1012", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1012"></label>
                                        </td>
                                    </tr>
                                    <tr class="active">
                                        <th>Job</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td>สร้างงาน</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1013" name="role[]" value="1013" <?=(in_array("1013", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1013"></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ผู้สมัครงาน</td>
                                        <td class="text-center">
                                            <input type="checkbox" id="checkbox_1014" name="role[]" value="1014" <?=(in_array("1014", $roles_system)) ? 'checked' : '';?>/>
                                            <label for="checkbox_1014"></label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- #END# Basic Validation -->
</div>
</section>





