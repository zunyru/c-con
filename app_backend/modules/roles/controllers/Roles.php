<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends MY_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url' ,'fn'));
		$this->load->model('roles/roles_model');
		$this->load->library('uploadfile_library');
		$this->load->library('nestable_library');
		$this->load->library('logs_library');
	}
	
	public function index(){   
		$data = array(
			'page_index' 		=> 'roles',
			'page' 				=> 'browse',
			'title_page' 		=> 'Roles Browse',
			'description_page' 	=> 'My Roles'
		);
		$data['roles'] = $this->roles_model->get_RolesAll();
		//loade view
		$this->load->view('masters/header',$data); 
		$this->load->view('index',$data);
	}

	public function create() {
		$data = array(
			'page_index' 		=> 'roles',
			'page' 				=> 'create',
			'title_page' 		=> 'Create',
			'description_page' 	=> 'My roles create',
		);
		
        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function store(){

		$user = $this->session->userdata('users');
 
		$role = $this->input->post('role');
		$name = 0;
		if(!empty($this->input->post('name'))):
			$name = $this->input->post('name');
		endif;
		$role_arr = '';
		if(!empty($role)):
			foreach($role as $key => $list):
				if($key == 0):
					$role_arr.= $list;
				else:
					$role_arr.= ','.$list;
				endif;
			endforeach;
		endif;
		$data = array( 
			'name'   		=> $name,
			'display_name'	=> $this->input->post('display_name'),
			'roles_system'  => $role_arr,
			'created_by' 	=> $user['UID'],
			'updated_by' 	=> $user['UID'],
			'created_at' 	=> date('Y-m-d H:i:s'),
			'updated_at' 	=> date('Y-m-d H:i:s'),
		);

		$last_id = $this->roles_model->insert($data);	

		//Log
		$messege = 'เพิ่มข้อมูล roles : '.$this->input->post('display_name');

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('roles'));
		}	

	}

	public function edit($id) {
		
		$data = array(
			'page_index' 		=> 'roles',
			'page' 				=> 'edit',
			'title_page' 		=> 'Edit',
			'description_page' 	=> 'My roles edit',
		);
		
        //loade data 
		$data['roles'] = $this->roles_model->get_RolesById($id)->row();
        //loade view
		$this->load->view('masters/header', $data);
		$this->load->view('index', $data);
	}

	public function update($id) {

		$user = $this->session->userdata('users');

		$role = $this->input->post('role');
		$name = 0;
		if(!empty($this->input->post('name'))):
			$name = $this->input->post('name');
		endif;
		$role_arr = '';
		if(!empty($role)):
			foreach($role as $key => $list):
				if($key == 0):
					$role_arr.= $list;
				else:
					$role_arr.= ','.$list;
				endif;
			endforeach;
		endif;
		$data = array(
			'id'        	=> $id,
			'name'   		=> $name,
			'display_name'	=> $this->input->post('display_name'),
			'roles_system'  => $role_arr,
			'created_by' 	=> $user['UID'],
			'updated_by' 	=> $user['UID'],
			'created_at' 	=> date('Y-m-d H:i:s'),
			'updated_at' 	=> date('Y-m-d H:i:s'),
		);

		$last_id = $this->roles_model->update(array_to_obj($data));

		//Log
		$messege = 'แก้ไขข้อมูล roles : '.$this->input->post('display_name');

		$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('roles'));
		}
	} 

	public function destroy($id){
		$user = $this->session->userdata('users');
		
		$data = $this->banners_model->get_ฺBannersById($id);
		
		if(!empty($data->banners_img)){
			$this->load->helper("file");
			unlink($data->banners_img);
		}
		
		$delete = $this->banners_model->destroy($id);
		
		if($delete){
			//Log
			$messege = 'ลบข้อมูล banners : รูป '.$data->banners_img; 

			$this->logs_library->save_log($user['Username'],$this->class,$this->method,$messege);

			$this->session->set_flashdata('status',1);
			redirect(base_url('banners'));
		}else{
			$this->session->set_flashdata('status',0);
			redirect(base_url('banners'));
		}
	}

	public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'banners';
		$upload = $this->uploadfile_library->do_upload('file',TRUE,$path);

		if(isset($upload['index'])){
			$picture = '/uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
	}	

	public function deletebanners($ids){

		$arrayName = array('banners_img' => $ids);

		echo json_encode($arrayName);
	}

		
}
