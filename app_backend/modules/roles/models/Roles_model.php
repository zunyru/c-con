<?php

class Roles_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_RolesAll() {

		$this->db->select('*');
		$this->db->from('roles');
		$this->db->order_by('roles.display_name', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_RolesById($id) {

		$this->db->select('*');
		$this->db->from('roles');
		$this->db->where('roles.id', $id);
		$query = $this->db->get();
		return $query;
	}

	public function insert($data){
		$query = $this->db->insert('roles', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
     
    public function update($data){
		$this->db->set('name', $data->name);
		$this->db->set('display_name', $data->display_name);
		$this->db->set('roles_system', $data->roles_system);
		$this->db->set('created_by', $data->created_by);
		$this->db->set('updated_by', $data->updated_by);
		$this->db->set('created_at', $data->created_at);
		$this->db->set('updated_at', $data->updated_at);
		$this->db->where('id', $data->id);
		$update = $this->db->update('roles');

		return $update;
	}

	public function destroy($id){
		$this->db->where('id', $id);
		$delete = $this->db->delete('banners');

		return $delete;
	}
	
}