<?php

/**
 *
 */
class Slug_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getNameCount($name, $id = null, $db) {

		if ($id === null) {
			$query = $this->db->where('name', $name)
				->get($db);
		} else {
			$query = $this->db->where('name', $name)
				->where('id != ', $id)
				->get($db);
		}

		return $query->num_rows();

	}

	public function getSlugCount($name, $id = null, $db) {

		if ($id === null) {
			$query = $this->db->where('slug', $name)
				->get($db);
		} else {
			$query = $this->db->where('slug', $name)
				->where('id != ', $id)
				->get($db);
		}

		return $query->num_rows();

	}
}

?>