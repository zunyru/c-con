<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'users';

$route['logout'] = 'users/logout';

$route['login'] = 'users';
$route['users/login'] = 'users/login';
$route['dashboard'] = 'dashboards';

//Backend log
$route['backned-log'] = 'logs';

//check name and slug
$route['slugs/check_slugs'] = 'slugs/check_slugs';
$route['slugs/check_name'] = 'slugs/check_name';

//Products
$route['products'] = 'products';
//$route['products/img-upload'] = 'products/sumernote_img_upload';
$route['img-upload'] = 'products/sumernote_img_upload';
$route['deleteprodpic/(:num)'] = 'products/deletepic/$1';
$route['deletefile/(:num)']      = 'products/deletefile/$1';
$route['products/destroy/(:num)'] = 'products/destroy/$1';

//Categorys
$route['categorys'] = 'categorys';
$route['categorys/create'] = 'categorys/create';
$route['categorys/sort'] = 'categorys/sort';
$route['categorys/store'] = 'categorys/store';

$route['categorys/edit/(:num)']     = 'categorys/edit/$1';
$route['deleteicon/(:num)']         = 'categorys/deleteicon/$1';
$route['deleteategory/(:num)']      = 'categorys/deleteategory/$1';
$route['categorys/update/(:num)']   = 'categorys/update/$1';
$route['categorys/destroy/(:num)']  = 'categorys/destroy/$1';


//Jobs
$route['jobs']                      = 'jobs';
$route['jobs/img-upload']           = 'jobs/sumernote_img_upload';
$route['deletepic/(:num)']          = 'jobs/deletepic/$1';
$route['jobs/destroy/(:num)']       = 'jobs/destroy/$1';

//Registers
$route['jobsregisters']                 = 'jobsregisters';
$route['jobsregisters/img-upload']      = 'jobsregisters/sumernote_img_upload';
$route['jobsregisters/deletepic/(:num)']= 'jobsregisters/deletepic/$1';
$route['jobsregisters/destroy/(:num)']  = 'jobsregisters/destroy/$1';

//contacts
$route['contacts']                  = 'contacts';
$route['contacts/img-upload']       = 'contacts/sumernote_img_upload';
$route['deletepic/(:num)']          = 'contacts/deletepic/$1';
$route['contacts/destroy/(:num)']   = 'contacts/destroy/$1';

//catalog
$route['catalog']                   = 'catalog';
$route['catalog/img-upload']        = 'catalog/sumernote_img_upload';
$route['deletepic/(:num)']          = 'catalog/deletepic/$1';
$route['catalog/destroy/(:num)']    = 'catalog/destroy/$1';

//catalog
$route['users']                   = 'users';
$route['users/img-upload']        = 'users/sumernote_img_upload';
$route['deletepic/(:num)']        = 'users/deletepic/$1';
$route['users/destroy/(:num)']    = 'users/destroy/$1';



//banners
$route['banners']                   = 'banners';
$route['banners/img-upload']        = 'banners/sumernote_img_upload';
$route['deletebanners/(:num)']      = 'banners/deletebanners/$1';
$route['banners/destroy/(:num)']    = 'banners/destroy/$1';

//abouts
$route['abouts']                    = 'abouts';
$route['abouts/img-upload']         = 'abouts/sumernote_img_upload';
$route['abouts/deletepic/(:num)']   = 'abouts/deletepic/$1';
$route['abouts/deletepic2/(:num)']  = 'abouts/deletepic2/$1';
$route['abouts/deletepic3/(:num)']  = 'abouts/deletepic3/$1';
$route['abouts/deletepic4/(:num)']  = 'abouts/deletepic4/$1';
$route['abouts/destroy/(:num)']     = 'abouts/destroy/$1';

//seo
$route['seo']                   = 'seo';
$route['seo/img-upload']        = 'seo/sumernote_img_upload';
$route['deletepic/(:num)']      = 'seo/deletepic/$1';
$route['seo/destroy/(:num)']    = 'seo/destroy/$1';

//email
$route['email']                   = 'email';
$route['email/img-upload']        = 'email/sumernote_img_upload';
$route['deletepic/(:num)']        = 'email/deletepic/$1';
$route['email/destroy/(:num)']    = 'email/destroy/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
