<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//product
$route['products']                   = 'product';
$route['product/category/(:any)']   = 'product/index/$1';
$route['product/category']          = 'product/index';

$route['activity'] = 'activity';
$route['activity/category/(:any)/detail/(:any)']   = 'activity/detail/$1/$2';
$route['activity/category/(:any)']  = 'activity/category/$1';


//contacts
$route['contact']   = 'contact';

$route['catalog/download']              = 'catalog/download';

$route['joinus']                 = 'joinus';
$route['joinus/detail/(:any)']   = 'joinus/detail/$1';

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
