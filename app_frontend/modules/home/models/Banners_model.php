<?php

class Banners_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_BannersAll() {

		$this->db->select('*');
		$this->db->from('banners');
		$this->db->order_by('banners.create_at', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_ฺBannersById($id) {

		$this->db->select('*');
		$this->db->from('banners');
		$this->db->where('banners.id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->id;
			$row->banners_img;
		}

		return $row;
	}

	public function insert($data){
		$query = $this->db->insert('banners', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
     
    public function update($data){
		$this->db->set('banners_img', $data->banners_img);
		$this->db->set('create_by', $data->create_by);
		$this->db->set('update_by', $data->update_by);
		$this->db->set('create_at', $data->create_at);
		$this->db->set('update_at', $data->update_at);
		$this->db->where('id', $data->id);
		$update = $this->db->update('banners');

		return $update;
	}

	public function destroy($id){
		$this->db->where('id', $id);
		$delete = $this->db->delete('banners');

		return $delete;
	}
	
}