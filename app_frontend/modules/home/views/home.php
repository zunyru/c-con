<section class="SectionHome">
	<div class="container-fluid">
		<div class="row">
			<div class="owl-carousel owl-theme">
				<?php
				foreach ($banners as $index => $item): 
				?>
					<div class="item">
						<img src="<?=$this->config->item('backend_uploade').$item->banners_img?>" class="img-fluid" alt="">
					</div>
				<?php
				endforeach; 
				?>
			</div>
		</div>
	</div>

	<div class="container-fluid MenuSite" style='background-image: url("<?=base_url('image/world.jpg');?>");'>
		<div class="box">
			<?php 
				
			foreach ($category as $index => $categorys): 
			?>
			<a href="<?=site_url('product/category/')?><?=$categorys->slug?>">
				<div class="blog">
					<img src="<?=$this->config->item('backend_uploade').$categorys->icon?>" class="img-fluid" alt="">
					<span><?=$categorys->name?></span>
				</div>
			</a>
			<?php
			endforeach; 
			?>
		</div>
	</div>
</section>
