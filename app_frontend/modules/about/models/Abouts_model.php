<?php

class Abouts_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_AboutsAll() {

		$this->db->select('*');
		$this->db->from('abouts');
		$this->db->order_by('abouts.create_at', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_AboutsById($id) {
		$this->db->select('*');
		$this->db->from('abouts');
		$this->db->where('id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->id;
			$row->banners;
			$row->slug;
			$row->name;
			$row->companybg_name;
			$row->values;
			$row->values_name;
			$row->mission;
			$row->mission_name;
			$row->our_values;
			$row->our_values_name;
			$row->our_values_img1;
			$row->our_values_img2;
			$row->our_values_img3;
		}

		return $row;
	}

	public function insert($data){
		$query = $this->db->insert('abouts', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
     
	public function update($data){

		$this->db->set('name', $data->name);
		$this->db->set('slug', $data->slug);
		$this->db->set('companybg_name', $data->companybg_name);
		$this->db->set('values', $data->values);
		$this->db->set('values_name', $data->values_name);
		$this->db->set('mission', $data->mission);
		$this->db->set('mission_name', $data->mission_name);
		$this->db->set('our_values', $data->our_values);
		$this->db->set('our_values_name', $data->our_values_name);
		if(!is_null($data->banners)){
			$this->db->set('banners', $data->banners);
		}
		if(!is_null($data->our_values_img1)){
			$this->db->set('our_values_img1', $data->our_values_img1);
		}
		if(!is_null($data->our_values_img2)){
			$this->db->set('our_values_img2', $data->our_values_img2);
		}
		
		if(!is_null($data->our_values_img3)){
			$this->db->set('our_values_img3', $data->our_values_img3);
		}
		$this->db->set('create_by', $data->create_by);
		$this->db->set('update_by', $data->update_by);
		$this->db->set('create_at', $data->create_at);
		$this->db->set('update_at', $data->update_at);
		$this->db->where('id', $data->id);
		$update = $this->db->update('abouts');

		return $update;
	}
	
	public function destroy($id){
		$this->db->where('id', $id);
		$delete = $this->db->delete('catalog');

		return $delete;
	}
	
}