<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('about/abouts_model');
		$this->load->model('template/seo_model');
		$this->seo_arr = $this->seo_model->get_SeoAll();

	}

    private function seo(){
		$title          = $this->seo_arr[0]->name;
		$robots         = $this->seo_arr[0]->robots;
		$description    = $this->seo_arr[0]->description;
		$keywords       = $this->seo_arr[0]->keywords;
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		return $meta;
	}

	public function index(){
        $data = array(
            'menu'    => '0',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'about',
            'footer'  => 'footer',
            'function'=>  array('home'),
		);
		//load data
		$data['about'] = $this->abouts_model->get_AboutsAll();
		//load view
        $this->load->view('template/content', $data);
	}
}