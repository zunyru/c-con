<?php 

/**
 * 
 */
class Download_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function getLogAll() {

		$query = $this->db->get('catalog_download_log');

		return $query->result();
	}

	public function getLogKey($code) 
	{ 
		$this->db->where('id', $code);
		$query = $this->db->get('catalog_download_log'); 
		return $query;
	}

	public function insert($data){
		$query = $this->db->insert('catalog_download_log', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
}

?>