<section class="SectionCatalog">
	<div class="container-fluid SubCatgory">
		<div class="">
			<div class="box">
				<span class="s1">ดาวโหลด Catalog</span>
			</div>
		</div>
	</div>

	<div class="container-fluid banner">
		<div class="row">
			<div class="col-sm-8">
				<div>
					<img src="<?=base_url('image/catalog.jpg')?>" class="img-fluid" alt="">
				</div>
			</div>
			<div class="col-sm-4">
				<div class="MenuSite box">
					<div class="mt-5">
						<?php 
					foreach ($category as $index => $categorys): 
					?>
						<a href="javascript:void(0);" class="click-downloadcatalog" data-id="<?=$categorys->id?>" data-type="1">
							<div class="blog">
								<img src="<?=base_url($categorys->icon);?>" class="img-fluid" alt="">
								<span>
									<?='คลิกดาวโหลด Catalog '.$categorys->name?></span>
							</div>
						</a>
						<?php
					endforeach; 
					?>
						<a href="javascript:void(0);" class="click-downloadcatalog" data-id="ADD" data-type="0">
							<div class="blog">
								<img src="<?=base_url('image/menu2.png');?>" class="img-fluid" alt="">
								<span>คลิกดาวโหลด Catalog ทั้งหมด</span>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="modal-download-catalog" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Download file catalog</h4>
			</div>
			<div class="modal-body">
				
				<form id="form_validation" action="<?=base_url('catalog/downloadfile')?>" target="_self" method="post">
					<div class="form-group">
						<label for="user_name">User name:</label>
						<input type="text" class="form-control text-require" id="user_name" name="name" autocomplete="off">
					</div>
					<div class="form-group">
						<label for="email">Email address:</label>
						<input type="email" class="form-control text-require" id="email" name="email" autocomplete="off">
					</div>
					<div class="form-group">
						<label for="company">Company:</label>
						<input type="text" class="form-control text-require" id="company" name="company" autocomplete="off">
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" id="catalog-id">
					<input type="hidden" name="type" id="calalog-type">
					<button type="submit" id="btn-click-form"  class="btn btn-primary">Send file to mail</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
