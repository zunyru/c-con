<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('catalog/catalog_model');
		$this->load->model('catalog/download_model');
		$this->load->model('template/seo_model');
        $this->load->model('product/category_model');
		$this->load->model('product/product_model');
		$this->load->model('template/email_model');
		$this->seo_arr = $this->seo_model->get_SeoAll();
		$this->load->library('zip');

	}

    private function seo(){
		$title          = $this->seo_arr[0]->name;
		$robots         = $this->seo_arr[0]->robots;
		$description    = $this->seo_arr[0]->description;
		$keywords       = $this->seo_arr[0]->keywords;
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		return $meta;
	}

	public function index(){
        $data = array(
            'menu'    => '0',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'catalog',
            'footer'  => 'footer',
            'function'=>  array('home'),
		);
		//load data
        $data['category']  = $this->category_model->getCategoryAll();
		$data['catalog']   = $this->catalog_model->get_CatalogAll();
		//load view
        $this->load->view('template/content', $data);
	}

	public function downloadfile(){
		$this->output->set_content_type('application/json');
        $this->load->library('form_validation');
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('type', 'type', 'required');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('company', 'company', 'required');

		$message = '';
		$status  = 0;
        if ($this->form_validation->run() == TRUE){
            
			$id      	= $this->input->post('id');
			$type      	= $this->input->post('type');
			$name      	= $this->input->post('name');
			$email      = $this->input->post('email');
			$company   	= $this->input->post('company');

			if($id != 'ADD'){ 
				if($type==0){
					$data['file']  	= $this->catalog_model->get_CatalogById($id);
					$_full_path 	= $data['file']->catalog_file;
				}else if($type==1){
					$data['file']  	= $this->category_model->get_CategoryByKey($id);
					$_full_path 	= $data['file']->file;
				}else if($type==2){
					$data['file']  	= $this->product_model->get_ProductByKey($id);
					$_full_path 	= $data['file']->catalog;
				}
				$file = $_full_path;
			
				if(isset($file)){
					$data = array(
						'name'      => $name,
						'email'     => $email,
						'type'      => $type,
						'ref_id'    => $id,
						'file'		=> $file,
						'address'	=> $company,
						'create_by' => 0,
						'update_by' => 0,
						'active'    => 0,
						'create_at' => date('Y-m-d H:i:s'),
						'update_at' => date('Y-m-d H:i:s'),
					);
			
					$last_id = $this->download_model->insert($data);

					if($last_id){ 
						 $data['last_id'] = $last_id;
						 $this->sentMail($data);
						 $message = 'ระบบทำการส่งข้อมูลไปยังอีเมลของท่าน';
						 $status  = 1;
					}
				}else{
					$message = 'ไม่พบข้อมูล...........';
				}
			}else{
				
				$file_name 		= date('YmdHis').'.zip';
				$createdzipname = 'catalog_download';
				$path = 'uploads/'.$createdzipname.'/'.date('Y').'/'.date('m').'/';
				if (!is_dir($path))
				{
					mkdir('./'.$path, 0777, true);
				}
				$catalog 		= $this->catalog_model->get_CatalogAll();
				if(!empty($catalog)){
					foreach($catalog as $item){
						$_full_path = $item->catalog_file; 
						// add data own data into the folder created
						$this->zip->read_file($_full_path);
					} 

					$data = array(
						'name'      => $name,
						'email'     => $email,
						'type'      => $type,
						'ref_id'    => $id,
						'file'		=> $path.$file_name,
						'address'	=> $company,
						'create_by' => 0,
						'update_by' => 0,
						'active'    => 0,
						'create_at' => date('Y-m-d H:i:s'),
						'update_at' => date('Y-m-d H:i:s'),
					);
					$last_id = $this->download_model->insert($data);
					if($last_id){
						$this->zip->archive($path.$file_name);
						$data['last_id'] = $last_id;
						$this->sentMail($data);
						$message = 'ระบบทำการส่งข้อมูลไปยังอีเมลของท่าน';
						$status  = 1;
					} else{
						$message = 'ไม่พบข้อมูล...........';
					}
				}
			}
        }else{
			$message = 'กรุณากรอกข้อมูล...........';
		}

		$_data = array(
			'message'  => $message
		   ,'status'   => $status 
	   );

		$this->output->set_output(json_encode($_data));

	} 

	private function sentMail($data){

		// load mail config
		$obj_email  = $this->email_model->get_EmailAll();
		$Host 		= $obj_email[0]->Host;
		$Username 	= $obj_email[0]->Username;
		$Password 	= $obj_email[0]->Password;
		$SMTPSecure = $obj_email[0]->SMTPSecure;
		$Port 		= $obj_email[0]->Port;

		$data['url'] = base_url('catalog/download?code='.urlencode(base64_encode($data['last_id'])));

		$viewMail = $this->load->view('email/email', $data, TRUE);

		// load mail send config 
		
		require 'app_frontend/third_party/phpmailer/PHPMailerAutoload.php';
		$mail = new PHPMailer;
		$mail->SMTPDebug = 0;                               	// Enable verbose debug output
		
		$mail->isSMTP();                                      	// Set mailer to use SMTP
		$mail->Host 		= $Host;              				// Specify main and backup SMTP servers
		$mail->SMTPAuth 	= true;                             // Enable SMTP authentication
		$mail->Username 	= $Username;                		// SMTP username
		$mail->Password 	= $Password;                        // SMTP password
		$mail->SMTPSecure 	= $SMTPSecure;                      // Enable TLS encryption, `ssl` also accepted
		$mail->Port 		= $Port;                            // TCP port to connect to
		$mail->CharSet 		= 'UTF-8';
		
		$mail->From 		= $Username;
		$mail->FromName 	= $Username;

		$email_to 			= $data['email'];

		$mail->addAddress($email_to);               			// Name is optional
		$mail->isHTML(false);                                  	// Set email format to HTML

		$mail->Subject = $email_to;
		$mail->Body    = $viewMail;
		$mail->AltBody = $viewMail;

		if(!$mail->Send()) {
			echo 'ยังไม่สามารถส่งเมลล์ได้ในขณะนี้ ' . $mail->ErrorInfo;
			exit;
		}
	}
	
	public function download(){
		 
		$id      		= urldecode(base64_decode($this->input->get('code'))); 
		$this->load->helper('download');
		$catalog_log 	= $this->download_model->getLogKey($id)->row();
		if(!empty($catalog_log)){
			$file = !empty($catalog_log->file) ? $catalog_log->file :''; 
			if (file_exists($file)) {
				force_download($file, NULL);
			} 
		}else{
			echo 'ไม่พบข้อมูลกรุณาลองใหม่อีกครั้ง !!!!';
		} 
	}
	
	public function downloadss(){

        $this->load->library('form_validation');
		$this->form_validation->set_rules('code', 'id', 'required');
		$this->form_validation->set_rules('type', 'type', 'required');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('company', 'company', 'required');
        if ($this->form_validation->run() == TRUE){
            
			$id      	= $this->input->post('id');
			$type      	= $this->input->post('type');
			$name      	= $this->input->post('name');
			$email      = $this->input->post('email');
			$company   	= $this->input->post('company');

			if($id != 'ADD'){ 
				if($type==0){
					$data['file']  	= $this->catalog_model->get_CatalogById($id);
					$_full_path 	= $data['file']->catalog_file;
				}else if($type==1){
					$data['file']  	= $this->category_model->get_CategoryByKey($id);
					$_full_path 	= $data['file']->file;
				}else if($type==2){
					$data['file']  	= $this->product_model->get_ProductByKey($id);
					$_full_path 	= $data['file']->catalog;
				}
				$file = $_full_path;
			
				if(isset($file)){
					$data = array(
						'name'      => $name,
						'email'     => $email,
						'type'      => $type,
						'ref_id'    => $id,
						'file'		=> $file,
						'address'	=> $company,
						'create_by' => 0,
						'update_by' => 0,
						'active'    => 0,
						'create_at' => date('Y-m-d H:i:s'),
						'update_at' => date('Y-m-d H:i:s'),
					);
			
					$last_id = $this->download_model->insert($data);

					if($last_id){
						if (file_exists($file)) {
							header("Pragma: public");
							header("Expires: 0");
							header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
							header("Content-Type: application/force-download");
							header("Content-Type: application/octet-stream");
							header("Content-Type: application/download");;
							header('Content-Disposition: attachment; filename="catalog'.date('Ymdhis').'.pdf"');
							header("Content-Transfer-Encoding: binary ");
							readfile($file);
							exit;
						}
					}
				}else{
					echo '<p>ไม่พบข้อมูล...........</p>';
				}
			}else{
				$createdzipname = 'catalog';
				$catalog 		= $this->catalog_model->get_CatalogAll();
				if(!empty($catalog)){
					foreach($catalog as $item){
						$_full_path = $item->catalog_file; 
						// add data own data into the folder created
						$this->zip->add_data($_full_path);
					} 

					$data = array(
						'name'      => $name,
						'email'     => $email,
						'type'      => $type,
						'ref_id'    => $id,
						'file'		=> '',
						'address'	=> $company,
						'create_by' => 0,
						'update_by' => 0,
						'active'    => 0,
						'create_at' => date('Y-m-d H:i:s'),
						'update_at' => date('Y-m-d H:i:s'),
					);
					$last_id = $this->download_model->insert($data);
					if($last_id){
						$this->zip->download($createdzipname.'.zip');
					} else{
						echo '<p>ไม่พบข้อมูล...........</p>';
					}
				}
			}
        }else{
			echo '<p>กรุณากรอกข้อมูล...........</p>';
		}
    }
}