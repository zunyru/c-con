<section class="SectionFacebook">
	<div class="container-fluid banner">
		<div class="row">
			<img src="<?=base_url('image/facebook.jpg');?>" class="img-fluid text-edit" alt="">
			<div class="facebook">
				<a href="https://facebook.com" rel="" target="_blank">
					<div class="BtnCustom gray">
						<i class="fab fa-facebook-f"></i>
						<span>รับขาวสารจากเราดวย Facebook</span>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
