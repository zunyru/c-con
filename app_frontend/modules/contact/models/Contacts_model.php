<?php

class Contacts_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getContactsAll() {

		$this->db->select('*');
		$this->db->from('contacts');
		$this->db->order_by('contacts.create_at', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_ContactsById($id) {
		$this->db->select('*');
		$this->db->from('contacts');
		$this->db->where('contact_id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->contact_id;
			$row->user_name;
			$row->email;
			$row->contact_title;
			$row->contacts_detail;
		}

		return $row;
	}

	public function insert($data){
		$query = $this->db->insert('contacts', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
     
	public function update($data){

		$this->db->set('read', $data->read);
		$this->db->set('create_by', $data->create_by);
		$this->db->set('update_by', $data->update_by);
		$this->db->set('create_at', $data->create_at);
		$this->db->set('update_at', $data->update_at);
		$this->db->where('contact_id', $data->contact_id);
		$update = $this->db->update('contacts');

		return $update;
	}

	public function destroy($id){
		$this->db->where('contact_id', $id);
		$delete = $this->db->delete('contacts');

		return $delete;
	}
	
}