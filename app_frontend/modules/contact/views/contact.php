<style>
	.SectionContact .SubCatgory {
		background: #F2B242;
		padding: 5px 3%;
	}

	.SectionContact .box .s-24 {
		font-size: 24px;
	}

	.SectionContact p {
		margin-bottom: 0rem !important;
	}

</style>

<style>
	#map {
		height: 300px;
	}

</style>

<section class="SectionContact">
	<div class="container-fluid SubCatgory">
		<div class="">
			<div class="box">
				<span class="text-white s-24">ติดต่อเรา</span>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row mt-4 mb-4">
			<div class="col-md-6 col-12">
				<h1 class="">ส่งข้อความให้เรา</h1>
				<?php 
					echo form_open_multipart(base_url().'contact/store','id="form_validation"' , 'class="form-horizontal"');  
				
					if($this->session->flashdata('status') === 1){
						echo '<div class="alert alert-success" role="alert">บันทึกเรียบร้อยแล้ว</div>';
					}
					if($this->session->flashdata('status') === 0){
						echo '<div class="alert alert-danger" role="alert">ไม่สามารถบันทึกได้ กรุณาลองใหม่อีกครั้ง</div>';
					}
				?>
					<div class="form-row mt-2">
						<div class="col">
							<input type="text" class="form-control text-require" name="user_name" placeholder="ชื่อผู้ติดต่อ" autocomplete="off">
						</div>
						<div class="col">
							<input type="email" class="form-control text-require" name="email" placeholder="อีเมล" autocomplete="off">
						</div>
					</div>
					<div class="form-row mt-2">
						<div class="col">
							<input type="text" class="form-control text-require" name="contact_title" placeholder="หัวข้อ" autocomplete="off">
						</div>
					</div>
					<div class="form-row mt-2">
						<div class="col">
							<textarea class="form-control" id="exampleFormControlTextarea1" rows="5" name="contacts_detail" placeholder="รายละเอียด" autocomplete="off"></textarea>
						</div>
					</div>
					<div class="row mt-2">
						<div class="col">
							<button type="button" id="btn-click-form" class="btn btn-outline-dark">ส่งข้อความ </button>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-6 col-12">
				<p>บ.ซี-คอน ซิสเต็ม เทคโนโลยี จำกัด</p>
				<p>ฃอยบางเพ็ง ตำบล คลองข่อย อำเภอ ปากเกร็ด นนทบุรี 11120</p>
				<!-- <div id="map"></div> -->
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3871.687098556715!2d100.42679841455531!3d13.97722119020187!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e288afda854981%3A0xb88a89f17c4f00ef!2z4LiaLuC4i-C4tS3guITguK3guJkg4LiL4Li04Liq4LmA4LiV4LmH4LihIOC5gOC4l-C4hOC5guC4meC5guC4peC4ouC4tSDguIjguLPguIHguLHguJQ!5e0!3m2!1sth!2sth!4v1548169097248" height="300" frameborder="0" style="border:0;width: 100%;" allowfullscreen></iframe>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="ContatcSocial">
					<a href="tel:021033274">
						<div class="blog">
							<div class="img">
								<img src="<?=base_url('image/footer2.png');?>" alt="">
							</div>
							<div class="text">
								<span>02-1033273-75 , 063-4647729</span>
							</div>
						</div>
					</a>
					<a href="mailto:c-con-support@c-contraffic.com" target="">
						<div class="blog">
							<div class="img">
								<img src="<?=base_url('image/footer3.png');?>" alt="">
							</div>
							<div class="text">
								<span>c-con-support@c-contraffic.com</span>
							</div>
						</div>
					</a>
					<a href="https://line.me/en-US/" target="_blank">
						<div class="blog">
							<div class="img">
								<img src="<?=base_url('image/footer5.png');?>" alt="">
							</div>
							<div class="text">
								<span>@c-contraffic</span>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<script>

	window.setTimeout(function() {
		$(".alert").fadeTo(1000, 0).slideUp(500, function(){
			$(this).hide();
		});
	}, 4000);
	
</script>