<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

    private function seo()
	{
		$title          = "titleweb";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		return $meta;
	}

	public function index()
	{
        $data = array(
            'menu'    => '0',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'news',
            'footer'  => 'footer',
            'function'=>  array('home'),
        );
        $this->load->view('template/content', $data);
	}
}