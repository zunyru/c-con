<section class="SectionProduct">

	<div class="container-fluid SubCatgory">
		<div class="">
			<div class="box">
				<span class="s1">สินคา</span>
				<span class="">กลุมสินคา</span>
				<?php
				if(isset($teb_name->name)){?>
					<span class="s2"><?=isset($teb_name->name) ? $teb_name->name : '' ?></span>
					<?php
				}
				?>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-12">
				<div class="MenuSite box">
					<?php 
					foreach ($category as $index => $categorys): 
						$active = null;
						if(isset($teb_name->name)){
							if($teb_name->name == $categorys->name){
								$active = "active";
							} 
						}
						?>
						<a href="<?=site_url('product/category/')?><?=$categorys->slug?>" class="<?=$active?>">
							<div class="blog">
								<img src="<?=base_url($categorys->icon);?>" class="img-fluid" alt="">
								<span><?=$categorys->name?></span>
							</div>
						</a>
						<?php
					endforeach; 
					?>
					
				</div>
				<div id="SubMenuSite">
					<?php 
					if(isset($teb_name)):
						foreach ($products as $product): 
							?>
							<a href="<?=site_url('product/detail');?>/<?=$product->slug?>" rel="">
								<div class="blog">
									<img src="<?=base_url($teb_name->icon);?>" class="img-fluid" alt="">
									<span><?=$product->name;?></span>
									<?php 
									if($product->new == 1){
										?>
										<img src="<?=base_url('image/new.gif');?>" class="img-fluid new" alt="">
									<?php } ?>
								</div>
							</a>
							<?php
						endforeach;
					endif;
					?>
				</div>
			</div>

			<div class="col-md-9 col-12 ProductList">

				<?php 
				foreach ($products as $product): 
					?>
					<div class="box">
						<?php 
						if($product->new == 1){
							?>
							<img src="<?=base_url('image/new-icon.gif');?>" class="img-fluid new-product" alt="">
						<?php } ?>
						<div class="img">
							<img src="<?=base_url($product->pic);?>" class="img-fluid" alt="">
						</div>
						<a href="<?=site_url('product/detail');?>/<?=$product->slug?>" rel="">
							<div class="text">
								<p><?=$product->name;?></p>
								<img src="<?=base_url('image/click.png');?>" class="img-fluid" alt="">
							</div>
						</a>
					</div>
					<?php
				endforeach; 
				?>
			</div>
		</div>
	</div>
</section>
