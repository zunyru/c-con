<section class="SectionProduct">

	<div class="container-fluid SubCatgory">
		<div class="row">
			<div class="box">
				<span class="s1">สินคา</span>
				<span class="">กลุมสินคา</span>
				<?php
				if(isset($product->cat_name)){?>
					<span class="s2"><?=isset($product->cat_name) ? $product->cat_name : '' ?></span>
					<?php
				}
				?>
				<?php
				if(isset($product->name)){?>
					<span class="s2"><?=isset($product->name) ? $product->name : '' ?></span>
					<?php
				}
				?>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3 col-12">
				<div class="MenuSite box ">
					<?php 
					foreach ($category as $index => $categorys):
						$active = null;
						if($product->cat_name == $categorys->name){
							$active = "active";
						} 
						?>
						<a href="<?=site_url('product/category/')?><?=$categorys->slug?>" class="<?=$active?>">
							<div class="blog">
								<img src="<?=base_url($categorys->icon);?>" class="img-fluid" alt="">
								<span><?=$categorys->name?></span>
							</div>
						</a>
						<?php
					endforeach; 
					?>
				</div>
				<div id="SubMenuSite">
					<?php  
					if(!empty($teb_name)):
						foreach ($products as $item): 
							$active_sub = null;
							if($product->name == $item->name){
								$active_sub = "active";
							} 
							?>
							<a href="<?=site_url('product/detail');?>/<?=$item->slug?>" rel="" class="<?=$active_sub?>">
								<div class="blog">
									<img src="<?=base_url($teb_name->icon);?>" class="img-fluid" alt="">
									<span><?=$item->name;?></span>
									<?php 
									if($item->new == 1){
										?>
										<img src="<?=base_url('image/new.gif');?>" class="img-fluid new" alt="">
									<?php } ?>
								</div>
							</a>
							<?php
						endforeach;
					endif;
					?>
				</div>
			</div>

			<div class="col-md-9 col-12 ProductList">
				<div class="col-12 text-edit">
					<?php 
					if($product->new == 1){
						?>
						<img src="<?=base_url('image/new-icon.gif');?>" class="img-fluid new-product-detail" alt="">
					<?php } ?>
					<?=isset($product->detail)? $product->detail :'ไม่พบข้อมูล'; ?>
				</div>
				<div class="col-12 AllButton">
					<a href="<?=site_url('product');?>" rel="">
						<div class="blog">
							<img src="<?=base_url('image/back.png');?>" class="img-fluid" alt="">
							<span>กลับไปกลุมลาสุด</span>
						</div>
					</a>
					<a href="javascript:void(0);" class="click-downloadcatalog" data-id="<?=$product->id?>" data-type="2">
						<div class="blog download">
							<span>ดาวโหลด Catalog</span>
						</div>
					</a>
					<img src="<?=base_url('image/file.png');?>" class="img" alt=""> <br>
					<span style="margin-right: 5%;"><?=isset($product->name) ? $product->name : 'c-con' ?></span>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="modal-download-catalog" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Download file catalog</h4>
			</div>
			<div class="modal-body">
				<form id="form_validation" action="<?=base_url('catalog/downloadfile')?>" target="_self" method="post">
					<div class="form-group">
						<label for="user_name">User name:</label>
						<input type="text" class="form-control text-require" id="user_name" name="name" autocomplete="off">
					</div>
					<div class="form-group">
						<label for="email">Email address:</label>
						<input type="email" class="form-control text-require" id="email" name="email" autocomplete="off">
					</div>
					<div class="form-group">
						<label for="company">Company:</label>
						<input type="text" class="form-control text-require" id="company" name="company" autocomplete="off">
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" id="catalog-id">
					<input type="hidden" name="type" id="calalog-type">
					<button type="submit" id="btn-click-form" class="btn btn-primary">Send file to mail</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
