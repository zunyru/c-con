<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    function __construct() {
		parent::__construct();
		$this->load->model('home/banners_model');
        $this->load->model('product/category_model');
        $this->load->model('product/product_model');
        $this->load->model('template/seo_model');
	}

    private function seo($id=1){
        $this->seo_arr = $this->seo_model->get_SeoById($id);
		$title          = $this->seo_arr->name;
		$robots         = $this->seo_arr->robots;
		$description    = $this->seo_arr->description;
		$keywords       = $this->seo_arr->keywords;
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		return $meta;
	}

	public function detail($id){
        $data = array(
            'menu'    => '0',
            'header'  => 'header',
            'content' => 'product_detail',
            'footer'  => 'footer',
            'function'=>  array('home'),
        );
        
        //load data
        $data['category']  = $this->category_model->getCategoryAll();
        $data['product']   = $this->product_model->get_ProductById($id); 
        
        $data['products']  = $this->product_model->get_CategoryById($data['product']->cat_slug);
        $data['teb_name']   = $this->category_model->get_CategoryById($data['product']->cat_slug); 
        $data['seo']       = $this->seo($data['product']->seo_id);
        //load view
        $this->load->view('template/content', $data);
    }

    public function index($id=null){

        $data = array(
            'menu'    => '0',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'product',
            'footer'  => 'footer',
            'function'=>  array('home'),
        );
        //load data

        $data['category']  = $this->category_model->getCategoryAll();

        if(!is_null($id)){

            $data['products']   = $this->product_model->get_CategoryById($id);
            $data['teb_name']   = $this->category_model->get_CategoryById($id);
            $data['seo']       = $this->seo($data['teb_name']->seo_id);
        }else{
            $data['products']   = $this->product_model->getProductAll();
        }
        
        //load view
        $this->load->view('template/content', $data);
    }
    
    public function product_ajax(){
        $this->load->view('product_ajax');
	}
}