<?php

class Quotations_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getQuotationsAll() {

		$this->db->select('*');
		$this->db->from('quotations');
		$this->db->order_by('quotations.create_at', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_QuotationsById($id) {
		$this->db->select('*');
		$this->db->from('quotations');
		$this->db->where('quotations_id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->quotations_id;
			$row->name;
			$row->position;
			$row->company;
			$row->company_address;
			$row->taxpayer_number;
			$row->email;
			$row->tel;
			$row->product_detail;
			$row->send;
		}

		return $row;
	}

	public function insert($data){
		$query = $this->db->insert('quotations', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
     
	public function update($data){

		$this->db->set('send', $data->send);
		$this->db->set('create_by', $data->create_by);
		$this->db->set('update_by', $data->update_by);
		$this->db->set('create_at', $data->create_at);
		$this->db->set('update_at', $data->update_at);
		$this->db->where('quotations_id', $data->quotations_id);
		$update = $this->db->update('quotations');

		return $update;
	}

	public function destroy($id){
		$this->db->where('quotations_id', $id);
		$delete = $this->db->delete('quotations');

		return $delete;
	}
	
}