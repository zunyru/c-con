<section class="SectionQuotation">

	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1>ขอใบเสนอราคาออนไลน</h1>
				<hr>

				<?php 
					echo form_open_multipart(base_url().'quotation/store','id="form_validation"' , 'class="form-horizontal"');  
				
					if($this->session->flashdata('status') === 1){
						echo '<div class="alert alert-success" role="alert">บันทึกเรียบร้อยแล้ว</div>';
					}
					if($this->session->flashdata('status') === 0){
						echo '<div class="alert alert-danger" role="alert">ไม่สามารถบันทึกได้ กรุณาลองใหม่อีกครั้ง</div>';
					}
				?>

				<p>ในกรณีที่ต้องการเอกสารใบเสนอราคาในรูปแบบบริษัท ลูกค้าสามารถกรอกแบบฟอร์มด้านล่าง ระบบจะส่งใบเสนอราคาในรูปแบบ PDF ให้คุณทันที</p>
			</div>
		</div>
		<form>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3 label">
						<label>ชื่อ - นามสกุล</label>
					</div>
					<div class="col-sm-9">
						<input type="text" class="form-control text-require" name="name" autocomplete="off" placeholder="ชื่อ - นามสกุล" autocomplete="off">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3 label">
						<label>ตำแหนง</label>
					</div>
					<div class="col-sm-9">
						<input type="text" class="form-control text-require" name="position" autocomplete="off" placeholder="ตำแหนง" autocomplete="off">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3 label">
						<label>ชื่อบริษัท</label>
					</div>
					<div class="col-sm-9">
						<input type="text" class="form-control text-require" name="company" autocomplete="off" placeholder="ชื่อบริษัท" autocomplete="off">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3 label">
						<label>ที่อยูบริษัท</label>
					</div>
					<div class="col-sm-9">
						<textarea class="form-control text-require" rows="3" name="company_address" autocomplete="off" placeholder="ที่อยูบริษัท" autocomplete="off"></textarea>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3 label">
						<label>เลขที่เสียภาษี</label>
					</div>
					<div class="col-sm-9">
						<input type="number" class="form-control text-require" name="taxpayer_number" autocomplete="off" placeholder="เลขที่เสียภาษี" autocomplete="off">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3 label">
						<label>อีเมล</label>
					</div>
					<div class="col-sm-9">
						<input type="email" class="form-control text-require" name="email" autocomplete="off" placeholder="อีเมล" autocomplete="off">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3 label">
						<label>เบอรโทร</label>
					</div>
					<div class="col-sm-9">
						<input type="number" class="form-control text-require" name="tel" autocomplete="off" placeholder="เบอรโทร" autocomplete="off">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3 label">
						<label>รายละเอียดสินคาที่ตองการ</label>
					</div>
					<div class="col-sm-9">
						<textarea class="form-control text-require" rows="3" name="product_detail" autocomplete="off" placeholder="รายละเอียดสินคาที่ตองการ" autocomplete="off"></textarea>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="offset-sm-3 col-sm-9">
						<button type="button"  id="btn-click-form" class="btn btn-light">สงขอความ</button>
					</div>
				</div>
			</div>
		</form>

		<div class="row">
			<div class="col-sm-12">
				<div class="ContatcSocial">
					<a href="tel:021033274">
						<div class="blog">
							<div class="img">
								<img src="<?=base_url('image/footer2.png');?>" alt="">
							</div>
							<div class="text">
								<span>02-1033273-75 , 063-4647729</span>
							</div>
						</div>
					</a>
					<a href="mailto:c-con-support@c-contraffic.com" target="">
						<div class="blog">
							<div class="img">
								<img src="<?=base_url('image/footer3.png');?>" alt="">
							</div>
							<div class="text">
								<span>c-con-support@c-contraffic.com</span>
							</div>
						</div>
					</a>
					<a href="https://line.me/en-US/" target="_blank">
						<div class="blog">
							<div class="img">
								<img src="<?=base_url('image/footer5.png');?>" alt="">
							</div>
							<div class="text">
								<span>@c-contraffic</span>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>
<script>

	window.setTimeout(function() {
		$(".alert").fadeTo(1000, 0).slideUp(500, function(){
			$(this).hide();
		});
	}, 4000);
	
</script>
