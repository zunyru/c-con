<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quotation extends CI_Controller {

	function __construct() {
		parent::__construct();

		$this->load->helper(array('form', 'url'));
		$this->load->model(array('quotation/quotations_model'));	
		$this->load->model('template/seo_model');
		$this->load->model('template/email_model');
		$this->load->model('template/send_email_model');
		$this->seo_arr = $this->seo_model->get_SeoAll();

	}

    private function seo(){
		$title          = $this->seo_arr[0]->name;
		$robots         = $this->seo_arr[0]->robots;
		$description    = $this->seo_arr[0]->description;
		$keywords       = $this->seo_arr[0]->keywords;
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		return $meta;
	}

	public function index(){
        $data = array(
            'menu'    => '0',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'quotation',
            'footer'  => 'footer',
            'function'=>  array('home'),
        );
        $this->load->view('template/content', $data);
	}

	public function store() {

		$data = array(
			'name'  	 	 => $this->input->post('name'),
			'email'      	 => $this->input->post('email'),
			'position'	 	 => $this->input->post('position'),
			'company'		 => $this->input->post('company'),
			'company_address'=> $this->input->post('company_address'),
			'taxpayer_number'=> $this->input->post('taxpayer_number'),
			'tel'			 => $this->input->post('tel'),
			'product_detail' => $this->input->post('product_detail'),
			'create_by' 	 => 0,
			'update_by' 	 => 0,
			'create_at' 	 => date('Y-m-d H:i:s'),
			'update_at' 	 => date('Y-m-d H:i:s'),
		);

		$last_id = $this->quotations_model->insert($data);

		
		$this->sentMail($data); // เรียก function send mail

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('quotation'));
		}else{
			$this->session->set_flashdata('status',0);
			redirect(base_url('quotation'));
		}
	} 

	private function sentMail($data){

		// load mail config
		$obj_email  = $this->email_model->get_EmailAll();
		$Host 		= $obj_email[0]->Host;
		$Username 	= $obj_email[0]->Username;
		$Password 	= $obj_email[0]->Password;
		$SMTPSecure = $obj_email[0]->SMTPSecure;
		$Port 		= $obj_email[0]->Port;

		// load mail send config
		$obj_send_email  = $this->send_email_model->get_SendEmailByPage('quotation');
		
		require 'app_frontend/third_party/phpmailer/PHPMailerAutoload.php';
		$mail = new PHPMailer;
		$mail->SMTPDebug = 0;                               	// Enable verbose debug output
		
		$mail->isSMTP();                                      	// Set mailer to use SMTP
		$mail->Host 		= $Host;              				// Specify main and backup SMTP servers
		$mail->SMTPAuth 	= true;                             // Enable SMTP authentication
		$mail->Username 	= $Username;                		// SMTP username
		$mail->Password 	= $Password;                        // SMTP password
		$mail->SMTPSecure 	= $SMTPSecure;                      // Enable TLS encryption, `ssl` also accepted
		$mail->Port 		= $Port;                            // TCP port to connect to
		$mail->CharSet 		= 'UTF-8';
		
		$mail->From 		= $data['email'];
		$mail->FromName 	= $data['user_name'];

		$email_to 			= $obj_send_email->send_mail;

		$mail->addAddress($email_to);               			// Name is optional
		$mail->isHTML(true);                                  	// Set email format to HTML

		$mail->Subject = $email_to.' : ขอใบเสนอราคาออนไลน์';
		$mail->Body    = $data['name'];
		$mail->AltBody = $data['product_detail'];

		if(!$mail->Send()) {
			echo 'ยังไม่สามารถส่งเมลล์ได้ในขณะนี้ ' . $mail->ErrorInfo;
			exit;
		}
	}

}