<section class="SectionAbout">
	<div class="container-fluid banner ">
		<div class="row">
			<img src="<?=base_url('image/slide.jpg');?>" class="img-fluid " alt="">
		</div>
	</div>

	<div class="container-fluid content">
		<div class="row mt-3">
			<div class="col-sm-4">
				<a href="<?=base_url('activity/category/รีวิว');?>" >
					<div class="card" >
						<img class="card-img-top" src="<?=base_url('image/review.jpg');?>" alt="Card image cap">
						<div class="card-body">
							<h2 class="card-title text-center">รีวิว</h2>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-4">
				<a href="<?=base_url('activity/category/กิจกรรม');?>" >
					<div class="card" >
						<img class="card-img-top" src="<?=base_url('image/activity.jpg');?>" alt="Card image cap">
						<div class="card-body">
							<h2 class="card-title text-center">กิจกรรม</h2>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-4">
				<a href="<?=base_url('activity/category/ข่าวสาร');?>" >
					<div class="card" >
						<img class="card-img-top" src="<?=base_url('image/news.jpg');?>" alt="Card image cap">
						<div class="card-body">
							<h2 class="card-title text-center">ข่าวสาร</h2>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
