<style type="">
	.card {
		transition: 0.5s;
		cursor: pointer;
	}
	.card-block {
		-webkit-box-flex: 1;
		-webkit-flex: 1 1 auto;
		-ms-flex: 1 1 auto;
		flex: 1 1 auto;
		padding: 1.25rem;
	}
	img {
		height: auto;
		width: 100%;
	}
	.card-title {
		font-size: 15px;
		transition: 1s;
		cursor: pointer;
	}
	.card-text {
		height: 80px;
	}
	.SectionAbout .content p{
		line-height: 1;
	}
</style>
<section class="SectionAbout">
	<div class="container-fluid banner ">
		<div class="row">
			<img src="<?=base_url('image/slide.jpg');?>" class="img-fluid " alt="">
		</div>
	</div>	
	<div class="container-fluid content">
		<div class="row mt-3">
			<?php
			foreach ($activity as $item):
				?>
				<div class="col-md-3 col-sm-6">
					<a href="<?=base_url('activity/category/').$current[5].'/detail/'.$item->slug;?>">
						<div class="card card-block">
							<img src="<?=base_url($item->activity_img)?>" alt="c-contraffic">
							<p class="card-text mt-2"><?=$item->name?></p>
						</div>
					</a>
				</div>
				<?php
			endforeach;
			?>
		</div>
	</div>

</section>
