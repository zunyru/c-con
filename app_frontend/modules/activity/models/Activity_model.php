<?php

class Activity_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function get_ActivityAll() {

		$this->db->select('*');
		$this->db->from('activity');
		$this->db->order_by('activity.create_at', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_ActivityByCat($category) {
		$this->db->select('*');
		$this->db->from('activity');
		$this->db->where('activity_category', $category);
		$query = $this->db->get();

		return $query->result_object();
	}

	public function get_ActivityBySlug($category,$slug) {
		$this->db->select('*');
		$this->db->from('activity');
		$this->db->where('activity_category', $category);
		$this->db->where('slug', $slug);
		$query = $this->db->get();

		return $query->result_object();
	}

	
}