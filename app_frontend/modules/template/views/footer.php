<footer>
    <div class="footer">
        <?php 
            $_footer_arr = array(
                 '0' => array(
                    'title' => 'แผนที่'
                    ,'text' => 'ซอยบางเพ็ง ตำบล คลองข่อย อำเภอ ปากเกร็ด นนทบุรี 11120'
                    ,'link' => 'https://www.google.com/maps/dir/13.9776062,100.4290299/13.9772212,100.4289871/@13.977221,100.428987,16z?hl=en-US'
                    ,'target' => ''
                    )
                ,'1' => array(
                    'title' => 'ติดต่อเรา'
                    ,'text' => '02-1033273-75<br>063-4647729'
                    ,'link' => base_url('contact')
                    ,'target' => ''
                    )
                ,'2' => array(
                    'title' => 'อีเมล์'
                    ,'text' => 'c-con-support@c-contraffic.com'
                    ,'link' => 'mailto:c-con-support@c-contraffic.com'
                    ,'target' => ''
                    )
                ,'3' => array(
                    'title' => 'Facebook'
                    ,'text' => 'c-contraffic'
                    ,'link' => 'https://www.facebook.com/ccontraffic1/'
                    ,'target' => '_blank'
                    )
                ,'4' => array(
                    'title' => 'Line'
                    ,'text' => '@c-contraffic'
                    ,'link' => 'https://line.me/en-US/'
                    ,'target' => '_blank'
                    )
           );
            for ($i=1; $i < 6; $i++) { ?>
            <a href="<?=$_footer_arr[$i-1]['link']?>" target="<?=$_footer_arr[$i-1]['target']?>">
                <div class="blog">
                    <div class="img">
                        <img src="<?=base_url('image/footer'.$i.'.png');?>" class="img-fluid" alt="">
                    </div>
                    <div class="text">
                        <h1><?=$_footer_arr[$i-1]['title']?></h1>
                        <p><?=$_footer_arr[$i-1]['text']?></p>
                    </div>
                </div>
            </a>
        <?php } ?>
    </div>
    <div class="copyrigth">
        <p>ลิขสิทธิ์ของ บริษัท ซี-คอน ซิสเต็ม เทคโนโลยี @ 2019</p>
    </div>
</footer>