<script>
	$('.owl-carousel').owlCarousel({
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:false,
		loop: true,
		margin: 10,
		items: 1,
		nav: true,
		navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>']
	})

</script>

<script>
	// $(document).ready(function () {
	// 	$(".MenuSite a").click(function () {
    //         // alert(11);
	// 		$.ajax({
	// 			url: "<?=site_url('product/product_ajax');?>",
	// 			success: function (result) {
	// 				$("#SubMenuSite").html(result);
	// 			}
	// 		});
	// 	});
	// });
</script>
