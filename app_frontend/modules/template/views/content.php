<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?=$seo;?>
	<meta name="robots" content="noindex,nofollow">
	<meta property="og:image" content="http://c-contraffic.com/uploads/banners/2019/01/5f6074bd7d8cd2f26a5ccf76e3fbe9e2.jpg" />
	<meta property="og:image:secure_url" content="http://c-contraffic.com/uploads/banners/2019/01/5f6074bd7d8cd2f26a5ccf76e3fbe9e2.jpg" /> 
	<meta property="og:image:type" content="image/jpeg" /> 
	<meta property="og:image:width" content="400" /> 
	<meta property="og:image:height" content="300" />
	<link rel="shortcut icon" href="<?=base_url();?>image/logo/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="<?=base_url('plugin/bootstrap/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?=base_url('class/css/app.css');?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<!-- OwlCarousel2 -->
	<link rel="stylesheet" href="<?=base_url('plugin/OwlCarousel2-2.3.4/owl.carousel.min.css');?>">
	<!-- <link rel="stylesheet" href="<?=base_url('plugin/OwlCarousel2-2.3.4/owl.theme.default.minsel.min.css');?>"> -->
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">

	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-147357019-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-147357019-1');
	</script>

	<!-- sharethis.js -->
	<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5df0662d5f28dd001220421c&product=inline-share-buttons&cms=sop" async></script>
	<script type="text/javascript" src ="https://platform-api.sharethis.com/js/sharethis.js#property=5df0662d5f28dd001220421c&cms=sop" async> </script>


</head>

<body>

	<?php $this->load->view('template/'.$header);?>
	<?php $this->load->view($content);?>
	<?php $this->load->view('template/'.$footer);?>

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url('plugin/bootstrap/bootstrap.min.js');?>"></script>
	<!-- OwlCarousel2 -->
	<script type="text/javascript" src="<?=base_url('plugin/OwlCarousel2-2.3.4/owl.carousel.min.js');?>"></script>
	<?php
	if(!empty($function)){
		$length2 = count($function);
		for ($x = 0; $x < $length2; $x++) {
			$this->load->view('function/'.$function[$x].'.php');
		}
	}
	?>

	<script>
		$(".click-downloadcatalog").click(function () {
			$('#catalog-id').val($(this).attr('data-id'));
			$('#calalog-type').val($(this).attr('data-type'));
			$('#btn-click-form').attr('disabled', false).html('Send file to mail');
			$("#modal-download-catalog").modal('show');
		});

		$(document).on("submit", "#form_validation", function(event)
		{
			event.preventDefault();
			setButtonLoading("#btn-click-form");  
			$(".text-require + span.require").remove();  
			$(".text-require").each(function(){  
				$(this).each(function(){
					if($(this).val()==""){  
						$(this).after("<span class=require ><font color=red>« จำเป็นต้องกรอก</font></span>");  
					}
				});
			});

			if($(".text-require").next().is(".require")==false){
				var i=0;
				$('[type="email"]').each(function(){
					if(!isEmail($(this).val())){
						$(this).after("<span class=require ><font color=red>« กรุณาตรวจสอบ</font></span>");  
						i++;
					}
				}); 

			}else{ 
				setButtonReset("#btn-click-form",1000);
				return false; 
				
			}

			$.ajax({
				url: $(this).attr("action"),
				type: $(this).attr("method"), 
				data: new FormData(this),
				processData: false,
				contentType: false,
				success: function (data)
				{ 
					if(data.status > 0){
						$('#modal-download-catalog').modal('hide');
						$('#user_name').val('');
						$('#email').val('');
						$('#company').val(''); 
						setButtonReset("#btn-click-form",1000);
						alert(data.message);
					}
				},
				error: function (data)
				{ 
					console.log('An error occurred.');
					setButtonReset("#btn-click-form",1000);
				}
			});        
		});
		/*
		$(document).on('click', '#btn-click-form', function(){
			$(".text-require + span.require").remove();  
			$(".text-require").each(function(){  
				$(this).each(function(){
					if($(this).val()==""){  
						$(this).after("<span class=require ><font color=red>« จำเป็นต้องกรอก</font></span>");  
					}
				});
			});

			if($(".text-require").next().is(".require")==false){
				var i=0;
				$('[type="email"]').each(function(){
					if(!isEmail($(this).val())){
						$(this).after("<span class=require ><font color=red>« กรุณาตรวจสอบ</font></span>");  
						i++;
					}
				});

				if(i == 0){
					$('#form_validation').submit();
				}

			}else{  
				return false;  
			}  
		});
		*/
		function isEmail(email) {
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			return regex.test(email);
		}

		$('[type="number"]').bind('keyup paste', function(){
			this.value = this.value.replace(/[^0-9]/g, '');
		});

		// set funtion loading button > class or id
		function setButtonLoading(action){
			$(action).html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>กำลังประมวลผล...').attr('disabled', true)
		}

		// set funtion reset button > class or id and time
		function setButtonReset(action,time){
			setTimeout(function(){
				$(action).attr('disabled', false).html('Send file to mail');
			}, time);
		}
	</script>
</body>
</html>