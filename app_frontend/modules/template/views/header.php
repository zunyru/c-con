<header>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 NavBar">
				<div class="BoxLeft">
					<a href="<?=site_url();?>">
						<img src="<?=base_url('image/logo.png');?>" class="img-fluid" alt="">
					</a>
				</div>
				<div class="BoxRight">
                    <div class="row">
						<?php
						$_menu_arr = array(
							 'หน้าหลัก'
							,'เกี่ยวกับเรา'
							,'สินค้า'
							,'ติดต่อเรา'
							,'ดาวโหลดแคตตาล็อก'
							,'ขอใบเสนอราคาด่วน!'
							,'ร่วมงานกับเรา'
							,'ร่วมสนุก/กิจกรรม'
						);
						$_menu_link_arr = array(
							 ''
							,'about'
							,'products'
							,'contact'
							,'catalog'
							,'quotation'
							,'joinus'
							,'activity'
						);
						for ($i=1; $i < 9; $i++) { 
						?>
                        <div class="coll">
                        	<?php if($i != 9){?>
                            <a href="<?=site_url($_menu_link_arr[$i-1]);?>">
                            <?php }else{ ?>
								<a href="<?=$_menu_link_arr[$i-1];?>">
                             <?php }?>
                                <div class="blog">
									<div class="img">
										<img src="<?=base_url('image/menu'.$i.'.png');?>" class="img-fluid" alt="">
									</div>
                                    <span><?=$_menu_arr[$i-1]?></span>
                                </div>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
				</div>
			</div>
		</div>
	</div>
</header>
