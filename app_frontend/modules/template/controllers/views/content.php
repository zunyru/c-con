<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?=$seo;?>
	<link rel="shortcut icon" href="<?=base_url();?>image/logo/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="<?=base_url('plugin/bootstrap/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?=base_url('class/css/app.css');?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<!-- OwlCarousel2 -->
	<link rel="stylesheet" href="<?=base_url('plugin/OwlCarousel2-2.3.4/owl.carousel.min.css');?>">
	<link rel="stylesheet" href="<?=base_url('plugin/OwlCarousel2-2.3.4/owl.theme.default.min.css');?>">

</head>

<body>

	<?php $this->load->view('template/'.$header);?>
	<?php $this->load->view($content);?>
	<?php $this->load->view('template/'.$footer);?>

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="<?=base_url('plugin/bootstrap/bootstrap.min.js');?>"></script>
	<!-- OwlCarousel2 -->
	<script type="text/javascript" src="<?=base_url('plugin/OwlCarousel2-2.3.4/owl.carousel.min.js');?>"></script>
	<?php
	if(!empty($function)){
		$length2 = count($function);
		for ($x = 0; $x < $length2; $x++) {
			$this->load->view('function/'.$function[$x].'.php');
		}
	}
	?>

</body>
</html>