<header>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 NavBar">
				<div class="BoxLeft">
					<a href="<?=site_url();?>">
						<img src="<?=base_url('image/logo.png');?>" class="img-fluid" alt="">
					</a>
				</div>
				<div class="BoxRight">
                    <div class="row">
						<?php
						$_menu_arr = array(
							 'หน้าหลัก'
							,'เกี่ยวกับเรา'
							,'สินค้า'
							,'ติดต่อเรา'
							,'ดาวโหลดแคตตาล็อก'
							,'ขอใบเสนอราคาด่วน!'
							,'ร่วมงานกับเรา'
							,'Facebook'
						);
						$_menu_link_arr = array(
							 'home'
							,'about'
							,'product'
							,'contact'
							,'catalog'
							,'quotation'
							,'joinus'
							,'facebook'
						);
						for ($i=1; $i < 9; $i++) { 
						
						?>
                        <div class="coll">
                            <a href="<?=site_url($_menu_link_arr[$i-1]);?>">
                                <div class="blog">
                                    <img src="<?=base_url('image/menu'.$i.'.png');?>" class="img-fluid" alt="">
                                    <span><?=$_menu_arr[$i-1]?></span>
                                </div>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
				</div>
			</div>
		</div>
	</div>
</header>
