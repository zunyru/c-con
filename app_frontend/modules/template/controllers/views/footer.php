<footer>
    <div class="footer">
        <?php 
            $_footer_arr = array(
                 '0' => array(
                    'title' => 'แผนที่'
                    ,'text' => 'ขอยบางเพ็ง ตำบล คลองข่อย อำเภอ ปากเกร็ด นนทบุรี 11120')
                ,'1' => array(
                    'title' => 'ติดต่อเรา'
                    ,'text' => '02 103 3274')
                ,'2' => array(
                    'title' => 'อีเมล์'
                    ,'text' => 'ccon-support@c-contrafﬁc.com')
                ,'3' => array(
                    'title' => 'Facebook'
                    ,'text' => 'ccontraffic')
                ,'4' => array(
                    'title' => 'Line'
                    ,'text' => 'ccontraffic')
           );
            for ($i=1; $i < 6; $i++) { ?>
            <div class="blog">
                <div class="img">
                    <img src="<?=base_url('image/footer'.$i.'.png');?>" class="img-fluid" alt="">
                </div>
                <div class="text">
                    <h1><?=$_footer_arr[$i-1]['title']?></h1>
                    <p><?=$_footer_arr[$i-1]['text']?></p>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="copyrigth">
        <p>ลิขสิทธิ์ของ บริษัท ซีคอน ซิตี้ กรุป จำกัด @ 2019</p>
    </div>
</footer>