<?php

class Jobsregisters_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getJobsregistersAll() {

		$this->db->select('*');
		$this->db->from('jobs_regis');
		$this->db->order_by('jobs_regis.create_at', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_JobsregistersById($id) {
		$this->db->select('*');
		$this->db->from('jobs_regis');
		$this->db->where('jobs_regis_id', $id);
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			$row->jobs_id;
			$row->jobs_name;
			$row->jobs_img;
			$row->jobs_detail;
			$row->jobs_file;
		}


		return $row;
	}

	public function insert($data){
		$query = $this->db->insert('jobs_regis', $data);
		$insertId = $this->db->insert_id();

		return $insertId;
	}
     
	public function update($data){

		$this->db->set('appove', $data->appove);
		$this->db->set('create_by', $data->create_by);
		$this->db->set('update_by', $data->update_by);
		$this->db->set('create_at', $data->create_at);
		$this->db->set('update_at', $data->update_at);
		$this->db->where('jobs_regis_id', $data->jobs_regis_id);
		$update = $this->db->update('jobs_regis');

		return $update;
	}

	public function destroy($id){
		$this->db->where('jobs_regis_id', $id);
		$delete = $this->db->delete('jobs_regis');

		return $delete;
	}
	
}