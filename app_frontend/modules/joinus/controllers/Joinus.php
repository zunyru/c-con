<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Joinus extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('joinus/jobs_model');
		$this->load->model('joinus/regis_config_model');
		$this->load->model('joinus/jobsregisters_model');
		$this->load->model('template/seo_model');
		$this->load->helper(array('form', 'url'));
		$this->seo_arr = $this->seo_model->get_SeoAll();

	}

    private function seo(){
		$title          = $this->seo_arr[0]->name;
		$robots         = $this->seo_arr[0]->robots;
		$description    = $this->seo_arr[0]->description;
		$keywords       = $this->seo_arr[0]->keywords;
		$meta  			= "<TITLE>".$title."</TITLE>";
		$meta 		   .= "<META name='robots' content='".$robots."'/>";
		$meta		   .= "<META name='description' content='".$description."'/>";
		$meta 		   .= "<META name='keywords' content='".$keywords."'/>";
		return $meta;
	}

	public function index(){
        $data = array(
            'menu'    => '0',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'joinus',
            'footer'  => 'footer',
            'function'=>  array('home'),
		);
		//load jobs
		$data['jobs'] = $this->jobs_model->get_JobsAll();
		//load view
        $this->load->view('template/content', $data);
	}

	public function store() {

		$data = array(
			'name'  	 		=> $this->input->post('name'),
			'position_one'      => $this->input->post('position_one'),
			'position_two'	 	=> $this->input->post('position_two'),
			'salary_one'		=> $this->input->post('salary_one'),
			'salary_two'		=> $this->input->post('salary_two'),
			'present_address'	=> $this->input->post('present_address'),
			'moo'				=> $this->input->post('moo'),
			'road'				=> $this->input->post('road'),
			'district'			=> $this->input->post('district'),
			'amphur'			=> $this->input->post('amphur'),
			'province'			=> $this->input->post('province'),
			'post_code'			=> $this->input->post('post_code'),
			'tel'				=> $this->input->post('tel'),
			'mobile'			=> $this->input->post('mobile'),
			'email'				=> $this->input->post('email'),
			'room'				=> $this->input->post('room'),
			'age'				=> $this->input->post('age'),
			'race'				=> $this->input->post('race'),
			'nationality'		=> $this->input->post('nationality'),
			'religion'			=> $this->input->post('religion'),
			'identity_card_no'	=> $this->input->post('identity_card_no'),
			'expiration_date'	=> $this->input->post('expiration_date'),
			'height'			=> $this->input->post('height'),
			'weight'			=> $this->input->post('weight'),
			'military_status'	=> $this->input->post('military_status'),
			'maritary_status'	=> $this->input->post('maritary_status'),
			'sex'				=> $this->input->post('sex'),
			'office_one'		=> $this->input->post('office_one'),
			'experience'		=> $this->input->post('experience'),
			'father_name'		=> $this->input->post('father_name'),
			'father_age'		=> $this->input->post('father_age'),
			'father_occupation' => $this->input->post('father_occupation'),
			'mother_name'		=> $this->input->post('mother_name'),
			'mother_age'		=> $this->input->post('mother_age'),
			'mother_occupation' => $this->input->post('mother_occupation'),
			'create_by' 	 	=> 0,
			'update_by' 	 	=> 0,
			'create_at' 	 	=> date('Y-m-d H:i:s'),
			'update_at' 	 	=> date('Y-m-d H:i:s'),
		);

		$last_id = $this->jobsregisters_model->insert($data);

		if($last_id){
			$this->session->set_flashdata('status',1);
			redirect(base_url('joinus/detail/').$this->input->post('slug'));
		}else{
			$this->session->set_flashdata('status',0);
			redirect(base_url('joinus/detail/').$this->input->post('slug'));
		}
	} 

	public function detail($id){
        $data = array(
            'menu'    => '0',
            'seo'     => $this->seo(),
            'header'  => 'header',
            'content' => 'detail',
            'footer'  => 'footer',
            'function'=>  array('home'),
		);
		//load jobs
		$data['jobs'] = $this->jobs_model->get_JobsBySlug($id);

		$_obj_regis = $this->regis_config_model->get_RegisConfigAll();
		foreach ($_obj_regis as $item){
			$_regis_arr[$item->type][] = array(
				 'regis_config_id'	 	=> $item->regis_config_id
				,'regis_config_name' 	=> $item->regis_config_name
			);
		}

		unset($_obj_regis);
		$data['regis_config'] = $_regis_arr;

		$data['slug'] = $id;
		
		//load view
        $this->load->view('template/content', $data);
	}
}