
<section class="SectionJoinus">

	<div class="container-fluid banner">
		<div class="row">
			<div class="BannerContent">
				<img src="<?=base_url('image/joinus.jpg');?>" class="img-fluid" alt="">
				<div class="text">
					<h2>รวมงานกับเรา</h2>
					<p>ก้าวหน้าในอาชีพของคุณ</p>
					<p>มาร่วมเป็นทีมผู้คิดค้นระดับโลกที่มีความหลากหลายด้านวัฒนธรรมและขนบธรรมเนียม
						ที่ซึ่งคุณได้พบความท้าทายใหม่ๆ ในทุกๆ วัน ในสภาพแวดล้อมด้วยความร่วมมือ
						ที่ซึ่งความคิดริเริ่มจะได้รับการส่งเสริม และตอบแทน
					</p>
				</div>
			</div>
		</div>
		<div class="row">

			<?php
			foreach ($jobs as $index => $item): 
			?>
			<div class="col-md-3 col-6">
				<a href="<?=base_url('joinus/detail/')?><?=$item->slug?>" rel="">
					<div class="selects">
						<img src="<?=base_url().$item->jobs_img?>" class="img-fluid" alt="">
						<span><?=$item->jobs_name?></span>
					</div>
				</a>
			</div>
			<?php
			endforeach; 
			?>
		</div>
		<div class="row">
			<img src="<?=base_url('image/joinus_banner.jpg');?>" class="img-fluid text-edit" alt="">
		</div>
	</div>
</section>
