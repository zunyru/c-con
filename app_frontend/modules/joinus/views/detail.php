<style>
 .text-address p, label{
	margin-right: 10px;
 }
</style>
<section class="SectionJoinus">

	<div class="container-fluid banner">
		<div class="row">
			<div class="BannerContent">
				<img src="<?=base_url('image/joinus.jpg');?>" class="img-fluid" alt="">
				<div class="text">
					<h2>รวมงานกับเรา</h2>
					<p>ก้าวหน้าในอาชีพของคุณ</p>
					<p>มาร่วมเป็นทีมผู้คิดค้นระดับโลกที่มีความหลากหลายด้านวัฒนธรรมและขนบธรรมเนียม
						ที่ซึ่งคุณได้พบความท้าทายใหม่ๆ ในทุกๆ วัน ในสภาพแวดล้อมด้วยความร่วมมือ
						ที่ซึ่งความคิดริเริ่มจะได้รับการส่งเสริม และตอบแทน
					</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<br>
				<h3>รายละเอียดงาน</h3>
				<p>ตำแหน่งงาน : <?=$jobs->jobs_name?></p>
				<p><?=$jobs->jobs_detail?></p>
			</div>
			
			<div class="col-md-6 text-address">
				<br>
				<h3>ฟอร์มสมัครงาน</h3>
				
				<?php 
					echo form_open_multipart(base_url().'Joinus/store','id="form_validation"' , 'class="form-horizontal"');  
					
					if($this->session->flashdata('status') === 1){
						echo '<div class="alert alert-success" role="alert">บันทึกเรียบร้อยแล้ว</div>';
					}
					if($this->session->flashdata('status') === 0){
						echo '<div class="alert alert-danger" role="alert">ไม่สามารถบันทึกได้ กรุณาลองใหม่อีกครั้ง</div>';
					}
				?>
					
					<div class="form-group">
						<label class="form-label">ชื่อ-สกุล</label>
						<input type="text" class="form-control text-require" name="name" placeholder="ชื่อ-สกุล" autocomplete="off">
						<input type="hidden" name="slug" value="<?=$slug?>">
					</div>
					<div class="form-group">
						<label class="form-label">ตำแหน่งที่ต้องการ</label>
						<p><input type="text" class="form-control text-require" name="position_one" placeholder="1. ตำแหน่งที่ต้องการ" autocomplete="off"></p>
						<p><input type="text" class="form-control text-require" name="position_two" placeholder="2. ตำแหน่งที่ต้องการ" autocomplete="off"></p>
					</div>
					<div class="form-group">
						<label class="form-label">เงินเดือน บาท/เดือน</label>
						<p><input type="number" class="form-control text-require" name="salary_one" placeholder="1. เงินเดือน บาท/เดือน" autocomplete="off"></p>
						<p><input type="number" class="form-control text-require" name="salary_two" placeholder="2. เงินเดือน บาท/เดือน" autocomplete="off"></p>
					</div>
					<div class="form-group">
						<h4>ประวัติส่วนตัว</h4>
					</div>
					<div class="form-group">
						<label class="form-label">ที่อยู่ปัจจุบันเลขที่</label>
						<div class="row col-md-12">
							<p><input type="text" class="form-control text-require" name="present_address" placeholder="ที่อยู่ปัจจุบันเลขที่" autocomplete="off"></p>
							<p><input type="text" class="form-control text-require" name="moo" placeholder="หมู่ที่" autocomplete="off"></p>
							<p><input type="text" class="form-control text-require" name="road" placeholder="ถนน" autocomplete="off"></p>
							<p><input type="text" class="form-control text-require" name="district" placeholder="ตำบล/แขวง" autocomplete="off"></p>
							<p><input type="text" class="form-control text-require" name="amphur" placeholder="อำเภอ/เขต" autocomplete="off"></p>
							<p><input type="text" class="form-control text-require" name="province" placeholder="จังหวัด" autocomplete="off"></p>
							<p><input type="number" class="form-control text-require" name="post_code" placeholder="รหัสไปรษณีย์" autocomplete="off"></p>
						</div>
					</div>
					
					<div class="form-group">
						<label class="form-label">ช่องทางติดต่อ</label>
						<div class="row col-md-12">
							<p><input type="number" class="form-control text-require" name="tel" placeholder="โทรศัพท์" autocomplete="off"></p>
							<p><input type="number" class="form-control text-require" name="mobile" placeholder="มือถือ" autocomplete="off"></p>
							<p><input type="email" class="form-control text-require" name="email" placeholder="อีเมล์" autocomplete="off"></p>
						</div>
					</div>
					
					<div class="form-group">
						<label class="form-label">ที่พักปัจจุบัน</label>
						<?php
						if(isset($regis_config[1])){
							echo '<div class="radio">';
							foreach($regis_config[1] as $item){
									echo '<label><input type="radio" class="text-require" name="room" value="'.$item['regis_config_id'].'"> '.$item['regis_config_name'].'</label>';
							}
							echo '</div>';
						}
						?>
					</div>
					<div class="form-group">
						<label class="form-label">วัน เดือน ปีเกิด</label>
						<div class="row col-md-12">
							<p><input type="date" class="form-control text-require" name="date_birth" placeholder="วัน เดือน ปีเกิด" autocomplete="off"></p>
							<p><input type="number" class="form-control text-require" name="age" placeholder="อายุ(ปี)" autocomplete="off"></p>
							<p><input type="text" class="form-control text-require" name="race" placeholder="เชื้อชาติ" autocomplete="off"></p>
							<p><input type="text" class="form-control text-require" name="nationality" placeholder="สัญชาติ" autocomplete="off"></p>
							<p><input type="text" class="form-control text-require" name="religion" placeholder="ศาสนา" autocomplete="off"></p>
						</div>
					</div>
					<div class="form-group">
						<label class="form-label">บัตรประชาชนเลขที่</label>
						<div class="row col-md-12">
							<p><input type="number" class="form-control text-require" name="identity_card_no" placeholder="บัตรประชาชนเลขที่" autocomplete="off"></p>
							<p><input type="date" class="form-control text-require" name="expiration_date" placeholder="บัตรหมดอายุ" autocomplete="off"></p>
							<p><input type="number" class="form-control text-require" name="height" placeholder="ส่วนสูง(ซม.)" autocomplete="off"></p>
							<p><input type="number" class="form-control text-require" name="weight" placeholder="น้ำหนัก(กก.)" autocomplete="off"></p>
						</div>
					</div>
					<div class="form-group">
						<label class="form-label">ภาระทางทหาร</label>
						<?php
						if(isset($regis_config[2])){
							echo '<div class="radio">';
							foreach($regis_config[2] as $item){
									echo '<label><input type="radio" class="text-require" name="military_status" value="'.$item['regis_config_id'].'"> '.$item['regis_config_name'].'</label>';
							}
							echo '</div>';
						}
						?>
					</div>
					<div class="form-group">
						<label class="form-label">สถานภาพ</label>
						<?php
						if(isset($regis_config[3])){
							echo '<div class="radio">';
							foreach($regis_config[3] as $item){
									echo '<label><input type="radio" class="text-require" name="maritary_status" value="'.$item['regis_config_id'].'"> '.$item['regis_config_name'].'</label>';
							}
							echo '</div>';
						}
						?>
					</div>
					<div class="form-group">
						<label class="form-label">เพศ</label>
						<?php
						if(isset($regis_config[4])){
							echo '<div class="radio">';
							foreach($regis_config[4] as $item){

									echo '<label><input type="radio" class="text-require" name="sex" value="'.$item['regis_config_id'].'"> '.$item['regis_config_name'].'</label>';
							}
							echo '</div>';
						}
						?>
					</div>
					<div class="form-group">
						<h4>ประวัติการทำงาน</h4>
					</div>
					<div class="form-group">
						<label class="form-label">ชื่อบริษัท</label>
						<div class="row col-md-12">
							<input type="text" class="form-control text-require" name="office_one" placeholder="ชื่อบริษัท" autocomplete="off">
						</div>
					</div>
					<div class="form-group">
						<label class="form-label">ตำแหน่ง/หน้าที่/ประสบการณ์ทำงาน</label>
						<div class="row col-md-12">
							<textarea cols="10" rows="5" class="form-control text-require" name="experience" placeholder="ตำแหน่ง/หน้าที่/ประสบการณ์ทำงาน" autocomplete="off"></textarea>
						</div>
					</div>
					<div class="form-group">
						<h4>ประวัติครอบครัว</h4>
					</div>
					<div class="form-group">
						<label class="form-label">บิดา ชื่อ-สกุล</label>
						<div class="row col-md-12">
							<p><input type="text" class="form-control text-require" name="father_name" placeholder="บิดา ชื่อ-สกุล" autocomplete="off"></p>
							<p><input type="number" class="form-control text-require" name="father_age" placeholder="อายุ(ปี)" autocomplete="off"></p>
							<p><input type="text" class="form-control text-require" name="father_occupation" placeholder="อาชีพ" autocomplete="off"></p>
						</div>
					</div>
					<div class="form-group">
						<label class="form-label">มารดา ชื่อ-สกุล</label>
						<div class="row col-md-12">
							<p><input type="text" class="form-control text-require" name="mother_name" placeholder="มารดา ชื่อ-สกุล" autocomplete="off"></p>
							<p><input type="number" class="form-control text-require" name="mother_age" placeholder="อายุ(ปี)" autocomplete="off"></p>
							<p><input type="text" class="form-control text-require" name="mother_occupation" placeholder="อาชีพ" autocomplete="off"></p>
						</div>
					</div>
					<div class="form-group">
						<div class="row col-md-12">
							<button type="button" id="btn-click-form" class="btn btn-primary waves-effect">บันทึกข้อมูล</button>
						</div>
					</div>
                </form>
			</div>
		</div>
		
	</div>
</section>

<script>

	window.setTimeout(function() {
		$(".alert").fadeTo(1000, 0).slideUp(500, function(){
			$(this).hide();
		});
	}, 4000);

</script>
