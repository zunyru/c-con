$(function () {
  skinChanger();
  activateNotificationAndTasksScroll();

  setSkinListHeightAndScroll(true);
  setSettingListHeightAndScroll(true);
  $(window).resize(function () {
    setSkinListHeightAndScroll(false);
    setSettingListHeightAndScroll(false);
  });
  
  $( document ).ready(function() {
    check_dup_slug = true;
    check_dup_name = true;
    slugURL();


  });
  
});

//Skin changer
function slugURL() {

 var typingTimer;                
 var doneTypingInterval = 300;  
 var $slug = $('#slug');
 var $name = $('#name');
 var $id = $('#id');

 $slug.on('keyup', function () {
  clearTimeout(typingTimer);
  typingTimer = setTimeout(doneTypingSlug, doneTypingInterval);
  $('#slug').val(slug($slug.val()));
  $('#slug').parent().addClass('focused');
});

 $slug.on('keydown', function () {
  clearTimeout(typingTimer);
});

 $name.on('keyup', function () {
  clearTimeout(typingTimer);
  typingTimer = setTimeout(doneTypingName, doneTypingInterval);
  stypingTimer = setTimeout(doneTypingSlug, doneTypingInterval);
  $('#slug').val(slug($name.val()));
  $('#slug').parent().addClass('focused');
});

 $name.on('keydown', function () {
  clearTimeout(typingTimer);
});
} 

function doneTypingSlug () {
  $.ajax({
    url: 'check_slugs',
    type: 'POST',
    dataType: 'json',
    data: {name: $('#slug').val(), id: $('#id').val() , db: $('#db').val()},
  })
  .done(function(data){
   setTimeout(function(){

    if(data){
      $('#slug').parent().removeClass('error');  
      $('#slug-error-dup').hide()
      check_dup_slug=true;

    }else{
      $('#slug-error-dup').show()
      $('#slug').parent().addClass('error'); 
      check_dup_slug=false;
    }
  }, 300);    
 })
  .fail(function(e) {
    console.log(e);
  })
  .always(function(c) {
    console.log(c);
  });
}  

function doneTypingName ($name,$id = null) {
  $.ajax({
    url: 'check_name',
    type: 'POST',
    dataType: 'json',
    data: {name: $('#name').val(), id: $('#id').val() , db: $('#db').val()},
  })
  .done(function(data){
   setTimeout(function(){

    if(data){
      $('#name').parent().removeClass('error');  
      $('#name-error-dup').hide()
      check_dup_slug=true;

    }else{
      $('#name-error-dup').show()
      $('#name').parent().addClass('error'); 
      check_dup_slug=false;
    }
  }, 300);    
 })
  .fail(function(e) {
    console.log(e);
  })
  .always(function(c) {
    console.log(c);
  });
}   

//Skin changer
function skinChanger() {
  $('.right-sidebar .demo-choose-skin li').on('click', function () {
    var $body = $('body');
    var $this = $(this);

    var existTheme = $('.right-sidebar .demo-choose-skin li.active').data('theme');
    $('.right-sidebar .demo-choose-skin li').removeClass('active');
    $body.removeClass('theme-' + existTheme);
    $this.addClass('active');

    $body.addClass('theme-' + $this.data('theme'));
  });
}

//Skin tab content set height and show scroll
function setSkinListHeightAndScroll(isFirstTime) {
  var height = $(window).height() - ($('.navbar').innerHeight() + $('.right-sidebar .nav-tabs').outerHeight());
  var $el = $('.demo-choose-skin');

  if (!isFirstTime){
    $el.slimScroll({ destroy: true }).height('auto');
    $el.parent().find('.slimScrollBar, .slimScrollRail').remove();
  }

  $el.slimscroll({
    height: height + 'px',
    color: 'rgba(0,0,0,0.5)',
    size: '6px',
    alwaysVisible: false,
    borderRadius: '0',
    railBorderRadius: '0'
  });
}

//Setting tab content set height and show scroll
function setSettingListHeightAndScroll(isFirstTime) {
  var height = $(window).height() - ($('.navbar').innerHeight() + $('.right-sidebar .nav-tabs').outerHeight());
  var $el = $('.right-sidebar .demo-settings');

  if (!isFirstTime){
    $el.slimScroll({ destroy: true }).height('auto');
    $el.parent().find('.slimScrollBar, .slimScrollRail').remove();
  }

  $el.slimscroll({
    height: height + 'px',
    color: 'rgba(0,0,0,0.5)',
    size: '6px',
    alwaysVisible: false,
    borderRadius: '0',
    railBorderRadius: '0'
  });
}

//Activate notification and task dropdown on top right menu
function activateNotificationAndTasksScroll() {
  $('.navbar-right .dropdown-menu .body .menu').slimscroll({
    height: '254px',
    color: 'rgba(0,0,0,0.5)',
    size: '4px',
    alwaysVisible: false,
    borderRadius: '0',
    railBorderRadius: '0'
  });
}

//Google Analiytics ======================================================================================
addLoadEvent(loadTracking);
var trackingId = 'UA-30038099-6';

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function () {
      oldonload();
      func();
    }
  }
}

function loadTracking() {
  (function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
      (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date(); a = s.createElement(o),
    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

  ga('create', trackingId, 'auto');
  ga('send', 'pageview');
}
//========================================================================================================

function showMessegeSuccess(){
  $.notify({
      // options
      icon: 'glyphicon glyphicon-ok',
      title: 'Success',
      message: 'Complete',
    },
    {
      // settings
      element: 'body',
      position: null,
      type: "success",
      allow_dismiss: true,
      newest_on_top: false,
      showProgressbar: false,
      placement: {
        from: "top",
        align: "right"
      },
      offset: 20,
      spacing: 5,
      z_index: 1031,
      delay: 5000,
      timer: 1000,
      url_target: '_blank',
      mouse_over: null,
      animate: {
        enter: 'animated fadeInDown',
        exit: 'animated fadeOutUp'
      },
      onShow: null,
      onShown: null,
      onClose: null,
      onClosed: null,
      icon_type: 'class',
      template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
        '<span data-notify="icon"></span> ' +
        '<span data-notify="title">{1}</span> ' +
        '<span data-notify="message">{2}</span>' +
        '<div class="progress" data-notify="progressbar">' +
          '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
        '</div>' +
        '<a href="{3}" target="{4}" data-notify="url"></a>' +
      '</div>'
    });
}