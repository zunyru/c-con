LICENSE CERTIFICATE: Innostudio.de
==============================================

Thank you for using an innostudio.de article!

Item Title:
Fileuploader

Item URL:
https://innostudio.de/fileuploader/

Item ID:
0001

Permissions:
You are allowed only to test the product on your website or application.

Missing features:
- themes
- templates
- canvas render for thumbnails
- speed of image renderer
- upload progress bar
- synchron upload and chunks
- paste from clipboard
- exif orientation
- video thumbnails
- image editor
- sorter
- API
- captions
- rest of examples

For any queries related to this document or license please contact Help Team via contact@innostudio.de

Innostudio.de
Iulian Galciuc
Germany