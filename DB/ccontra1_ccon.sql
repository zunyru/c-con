-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 18, 2019 at 10:18 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ccontra1_ccon`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(11) NOT NULL,
  `banners` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `companybg_name` text,
  `values` varchar(255) DEFAULT NULL,
  `values_name` text,
  `mission` varchar(255) DEFAULT NULL,
  `mission_name` text,
  `our_values` varchar(255) DEFAULT NULL,
  `our_values_name` text,
  `our_values_img1` varchar(255) DEFAULT NULL,
  `our_values_img2` varchar(255) DEFAULT NULL,
  `our_values_img3` varchar(255) DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `banners`, `slug`, `name`, `companybg_name`, `values`, `values_name`, `mission`, `mission_name`, `our_values`, `our_values_name`, `our_values_img1`, `our_values_img2`, `our_values_img3`, `create_by`, `update_by`, `active`, `create_at`, `update_at`) VALUES
(1, 'uploads/abouts/2019/09/21b8e9f36e9d7f5a20bfe8b8b266818b.jpg', 'd', '.', '<div style=\"text-align: center;\"></div><div style=\"text-align: center;\"><img src=\"/uploads/content/2019/10/22f078e96e10d58a86d0f36b8f6059d0.jpg\" style=\"width: 100%;\"></div><div style=\"text-align: center;\"><br></div><div style=\"text-align: center;\"><br></div><div style=\"text-align: center;\"><br></div><div style=\"text-align: center;\"><img src=\"/uploads/content/2019/10/9ea7e66d65c9595682438edeab4e0480.jpg\" style=\"width: 100%;\"></div><div style=\"text-align: center;\"><br></div><div style=\"text-align: center;\"><img src=\"/uploads/content/2019/10/5cdc3e2fcb560c7913d1b37237640cb2.jpg\" style=\"width: 100%;\"></div><div style=\"text-align: center;\"><br></div><div style=\"text-align: center;\"><img src=\"/uploads/content/2019/10/3825540fa75b09219eb948f122831907.jpg\" style=\"width: 100%;\"></div><div style=\"text-align: center;\"><br></div><div style=\"text-align: center;\"><img src=\"/uploads/content/2019/10/a03aa83f1ce3391f0c1078d98ad38058.jpg\" style=\"width: 100%;\"></div><div style=\"text-align: center;\"><br></div><div style=\"text-align: center;\"><img src=\"/uploads/content/2019/10/5f9e8bf9d71d392017cbcf711e7a9a61.jpg\" style=\"width: 100%;\"></div><div style=\"text-align: center;\"><img src=\"/uploads/content/2019/11/7e1502d0d54e7f4ba8336287fc85b659.jpg\" style=\"width: 100%;\"></div><div style=\"text-align: center;\"><img src=\"/uploads/content/2019/11/d47ab30d48e98cd7a666ae266256bc0e.jpg\" style=\"width: 100%;\"></div><div style=\"text-align: center;\"><img src=\"/uploads/content/2019/11/76dc3947e603eb8572c3b472cbaf498d.jpg\" style=\"width: 100%;\"></div><div style=\"text-align: center;\"><img src=\"/uploads/content/2019/11/88aeba7ea2c28f54dd20932f5e4139af.jpg\" style=\"width: 100%;\"><br></div>', '.', '<p class=\"MsoNormal\" style=\"line-height: 1;\"><br></p>', '.', '', '.', '', 'uploads/abouts/2019/09/ceff43008e281e876fdb5a79b68b19b1.jpg', 'uploads/abouts/2019/09/87b1f86a7c53a740d114b4f3833712f1.jpg', 'uploads/abouts/2019/09/53d0b0e8271b3ab6ad9f42c65b5887ca.jpg', 1, 1, 0, '2019-11-12 22:13:10', '2019-11-12 22:13:10');

-- --------------------------------------------------------

--
-- Table structure for table `backend_logs`
--

CREATE TABLE `backend_logs` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `class` varchar(255) NOT NULL,
  `method` varchar(255) NOT NULL,
  `messege` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `backend_logs`
--

INSERT INTO `backend_logs` (`id`, `username`, `ip_address`, `class`, `method`, `messege`, `created_at`) VALUES
(1, 'admin', '49.49.233.14', 'users', 'login', 'Success : LoginPage', '2019-10-15 22:12:48'),
(2, 'admin', '223.24.62.225', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาไฮแมสแบบเหลี่ยม สูง15-45 ม.ชุบกัลวาไนซ์ ,ประเภท : 1 ,รูปสินค้า :  ,catalog :  ,Seo :เสาไฮแมสแบบเหลี่ยม สูง15-45 ม.ชุบกัลวาไนซ์', '2019-10-15 22:17:27'),
(3, 'admin', '223.24.62.225', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสากลมปลายเรียว สูง 4-17 ม.ชุบกัลวาไนซ์ ,ประเภท : 1 ,รูปสินค้า :  ,catalog :  ,Seo :เสากลมปลายเรียว สูง 4-17 ม.ชุบกัลวาไนซ์', '2019-10-15 22:18:26'),
(4, 'admin', '223.24.62.225', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาเหลี่ยมปลายเรียว สูง 4-17 ม.ชุบกัลวาไนซ์ ,ประเภท : 1 ,รูปสินค้า :  ,catalog :  ,Seo :เสาเหลี่ยมปลายเรียว สูง 4-17 ม.ชุบกัลวาไนซ์', '2019-10-15 22:19:43'),
(5, 'admin', '223.24.62.225', 'email', 'update', 'แก้ไขข้อมูล :  ccontraffic@gmail.com', '2019-10-15 23:49:43'),
(6, 'admin', '223.24.62.225', 'quotations', 'update', 'Quotations : ', '2019-10-16 00:12:10'),
(7, 'admin', '124.120.116.47', 'users', 'login', 'Success : LoginPage', '2019-10-16 21:03:45'),
(8, 'admin', '27.55.90.248', 'users', 'login', 'Success : LoginPage', '2019-10-16 21:46:34'),
(9, 'admin', '27.55.90.248', 'email', 'update', 'แก้ไขข้อมูล : ccontraffic@gmail.com', '2019-10-16 22:07:35'),
(10, 'admin', '27.55.90.248', 'email', 'update', 'แก้ไขข้อมูล : ccontraffic@gmail.com', '2019-10-16 22:20:47'),
(11, 'admin', '49.49.243.81', 'users', 'login', 'username password ไม่ถูกต้อง', '2019-10-16 22:58:47'),
(12, 'admin', '49.49.243.81', 'users', 'login', 'Success : LoginPage', '2019-10-16 22:59:00'),
(13, 'admin', '27.55.90.248', 'contacts', 'destroy', 'ลบข้อมูล Contact : ', '2019-10-16 23:07:57'),
(14, 'admin', '27.55.90.248', 'contacts', 'destroy', 'ลบข้อมูล Contact : ', '2019-10-16 23:08:02'),
(15, 'admin', '27.55.90.248', 'email', 'update', 'แก้ไขข้อมูล : ccontraffic@gmail.com', '2019-10-16 23:24:18'),
(16, 'admin', '27.55.90.248', 'email', 'update', 'แก้ไขข้อมูล : ccontraffic@gmail.com', '2019-10-16 23:26:02'),
(17, 'admin', '27.55.90.248', 'email', 'update', 'แก้ไขข้อมูล : ccontraffic@gmail.com', '2019-10-16 23:34:14'),
(18, 'admin', '49.230.5.137', 'users', 'login', 'Success : LoginPage', '2019-10-17 15:20:29'),
(19, 'admin', '49.230.5.137', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ไฟเตือนทางแยกโซล่าเซลล์ ,ประเภท : 6 ,รูปสินค้า :  ,catalog :  ,Seo :ไฟเตือนทางแยกโซล่าเซลล์', '2019-10-17 15:21:05'),
(20, 'admin', '49.230.5.137', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ไฟเตือนจราจรโซล่าเซลล์ CCTV ,ประเภท : 6 ,รูปสินค้า :  ,catalog :  ,Seo :ไฟเตือนจราจรโซล่าเซลล์ CCTV', '2019-10-17 15:21:19'),
(21, 'admin', '49.230.5.137', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ไฟเตือนจราจรโซล่าเซลล์ ,ประเภท : 6 ,รูปสินค้า :  ,catalog :  ,Seo :ไฟเตือนจราจรโซล่าเซลล์', '2019-10-17 15:21:31'),
(22, 'admin', '49.230.5.137', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ไฟเตือนกระพริบทางเบี่ยง ,ประเภท : 6 ,รูปสินค้า :  ,catalog :  ,Seo :ไฟเตือนกระพริบทางเบี่ยง', '2019-10-17 15:21:43'),
(23, 'admin', '49.230.5.137', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ไฟจราจรปุ่มกดคนเดินข้าม ,ประเภท : 5 ,รูปสินค้า :  ,catalog :  ,Seo :ไฟจราจรปุ่มกดคนเดินข้าม', '2019-10-17 15:25:14'),
(24, 'admin', '49.230.5.137', 'products', 'destroy', 'ลบข้อมูล สินค้า : ไฟจราจรปุ่มกดคนเดินข้าม', '2019-10-17 15:27:54'),
(25, 'admin', '49.230.5.137', 'products', 'update', 'แก้ไขข้อมูล สินค้า : โคมไฟจราจรแบบ3ดวงโคม ,ประเภท : 5 ,รูปสินค้า :  ,catalog :  ,Seo :โคมไฟจราจรแบบ3ดวงโคม', '2019-10-17 15:28:34'),
(26, 'admin', '49.230.5.137', 'products', 'update', 'แก้ไขข้อมูล สินค้า : โคมไฟจราจรแบบมีช่องทางเดินรถ ,ประเภท : 5 ,รูปสินค้า :  ,catalog :  ,Seo :โคมไฟจราจรแบบมีช่องทางเดินรถ', '2019-10-17 15:33:49'),
(27, 'admin', '49.230.5.137', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : กลุ่มไฟจราจร,ประเภท : 0,Seo :ไฟจราจร', '2019-10-17 15:39:52'),
(28, 'admin', '49.230.5.137', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : กลุ่มป้ายจราจร,ประเภท : 0,Seo :ป้ายจราจร', '2019-10-17 15:40:03'),
(29, 'admin', '49.230.5.137', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : กลุ่มอุปกรณ์ความปลอดภัยจราจร,ประเภท : 0,Seo :อุปกรณ์ความปลอดภัยจราจร', '2019-10-17 15:40:20'),
(30, 'admin', '49.230.5.137', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : กลุ่มไฟเตือนจราจรโซล่าเซลล์,ประเภท : 0,Seo :ไฟเตือนจราจรโซล่าเซลล์', '2019-10-17 15:40:43'),
(31, 'admin', '49.230.5.137', 'categorys', 'update', 'แก้ไขข้อมูล ประเภทสินค้า : กลุ่มอุปกรณ์ความปลอดภัย,ประเภท : 0,Seo :อุปกรณ์ความปลอดภัยจราจร', '2019-10-17 15:41:15'),
(32, 'admin', '49.230.5.137', 'quotations', 'update', 'Quotations : ', '2019-10-17 15:42:30'),
(33, 'admin', '49.230.5.137', 'quotations', 'destroy', 'ลบข้อมูล quotations : ', '2019-10-17 15:42:41'),
(34, 'admin', '49.49.234.146', 'users', 'login', 'Success : LoginPage', '2019-10-21 00:16:42'),
(35, 'admin', '49.49.234.146', 'users', 'update', 'แก้ไขข้อมูล : admin', '2019-10-21 00:20:41'),
(36, 'admin', '49.49.234.146', 'users', 'store', 'เพิ่มข้อมูล : test name', '2019-10-21 00:21:50'),
(37, 'admin', '49.49.234.146', 'users', 'logout', 'Logout : Logouted', '2019-10-21 00:21:55'),
(38, 'test_admin', '49.49.234.146', 'users', 'login', 'Success : LoginPage', '2019-10-21 00:22:05'),
(39, 'test_admin', '49.49.234.146', 'users', 'destroy', 'ลบข้อมูล : test name', '2019-10-21 00:22:22'),
(40, 'test_admin', '49.49.234.146', 'users', 'logout', 'Logout : Logouted', '2019-10-21 00:22:42'),
(41, 'admin', '49.49.234.146', 'users', 'login', 'username password ไม่ถูกต้อง', '2019-10-21 00:29:16'),
(42, 'admin', '49.49.234.146', 'users', 'login', 'username password ไม่ถูกต้อง', '2019-10-21 00:29:23'),
(43, 'admin', '49.49.234.146', 'users', 'login', 'username password ไม่ถูกต้อง', '2019-10-21 00:29:32'),
(44, 'admin', '49.49.234.146', 'users', 'login', 'username password ไม่ถูกต้อง', '2019-10-21 00:29:56'),
(45, 'admin', '49.49.234.146', 'users', 'login', 'username password ไม่ถูกต้อง', '2019-10-21 00:30:18'),
(46, 'admin', '49.49.234.146', 'users', 'login', 'username password ไม่ถูกต้อง', '2019-10-21 00:30:32'),
(47, 'admin', '49.49.234.146', 'users', 'login', 'username password ไม่ถูกต้อง', '2019-10-21 00:30:57'),
(48, 'admin', '49.49.234.146', 'users', 'login', 'Success : LoginPage', '2019-10-21 00:40:07'),
(49, 'admin', '49.49.234.146', 'users', 'store', 'เพิ่มข้อมูล : test', '2019-10-21 00:41:19'),
(50, 'admin', '49.49.234.146', 'users', 'update', 'แก้ไขข้อมูล : test', '2019-10-21 00:41:39'),
(51, 'admin', '49.49.234.146', 'users', 'logout', 'Logout : Logouted', '2019-10-21 00:41:47'),
(52, 'admina', '49.49.234.146', 'users', 'login', 'username password ไม่ถูกต้อง', '2019-10-21 00:41:58'),
(53, 'admina', '49.49.234.146', 'users', 'login', 'username password ไม่ถูกต้อง', '2019-10-21 00:42:05'),
(54, 'admina', '49.49.234.146', 'users', 'login', 'username password ไม่ถูกต้อง', '2019-10-21 00:42:25'),
(55, 'admin', '49.49.234.146', 'users', 'login', 'Success : LoginPage', '2019-10-21 00:42:35'),
(56, 'admin', '49.49.234.146', 'users', 'logout', 'Logout : Logouted', '2019-10-21 00:43:01'),
(57, 'admina', '49.49.234.146', 'users', 'login', 'username password ไม่ถูกต้อง', '2019-10-21 00:43:10'),
(58, 'admina', '49.49.234.146', 'users', 'login', 'username password ไม่ถูกต้อง', '2019-10-21 00:44:42'),
(59, 'admina', '49.49.234.146', 'users', 'login', 'Success : LoginPage', '2019-10-21 00:44:51'),
(60, 'admina', '49.49.234.146', 'users', 'logout', 'Logout : Logouted', '2019-10-21 00:45:01'),
(61, 'admin', '49.49.234.146', 'users', 'login', 'Success : LoginPage', '2019-10-21 00:45:17'),
(62, 'admin', '49.49.234.146', 'users', 'update', 'แก้ไขข้อมูล : test', '2019-10-21 00:46:13'),
(63, 'admin', '49.49.234.146', 'users', 'update', 'แก้ไขข้อมูล : test', '2019-10-21 00:47:22'),
(64, 'admin', '49.49.234.146', 'users', 'logout', 'Logout : Logouted', '2019-10-21 00:48:04'),
(65, 'admina', '49.49.234.146', 'users', 'login', 'Success : LoginPage', '2019-10-21 00:48:13'),
(66, 'admina', '49.49.234.146', 'users', 'logout', 'Logout : Logouted', '2019-10-21 00:51:12'),
(67, 'admin', '171.97.98.143', 'users', 'login', 'Success : LoginPage', '2019-10-21 21:39:56'),
(68, 'admin', '171.97.98.143', 'users', 'store', 'เพิ่มข้อมูล : ton', '2019-10-21 21:40:47'),
(69, 'admin', '171.97.98.143', 'users', 'logout', 'Logout : Logouted', '2019-10-21 21:40:55'),
(70, 'ton', '171.97.98.143', 'users', 'login', 'Success : LoginPage', '2019-10-21 21:41:00'),
(71, 'admin', '49.49.240.82', 'users', 'login', 'Success : LoginPage', '2019-10-27 15:03:34'),
(72, 'admin', '49.49.240.82', 'users', 'update', 'แก้ไขข้อมูล : test', '2019-10-27 15:04:19'),
(73, 'admin', '49.49.240.82', 'roles', 'update', 'แก้ไขข้อมูล roles : เจ้าหน้าที่ปรับปรุงข้อมูล', '2019-10-27 15:16:04'),
(74, 'admin', '61.91.4.5', 'users', 'login', 'username password ไม่ถูกต้อง', '2019-10-27 16:08:11'),
(75, 'admin', '61.91.4.5', 'users', 'login', 'Success : LoginPage', '2019-10-27 16:08:16'),
(76, 'admin', '61.91.4.5', 'users', 'store', 'เพิ่มข้อมูล : tonn', '2019-10-27 16:15:13'),
(77, 'admin', '61.91.4.5', 'users', 'logout', 'Logout : Logouted', '2019-10-27 16:15:35'),
(78, 'tonn', '61.91.4.5', 'users', 'login', 'Success : LoginPage', '2019-10-27 16:15:41'),
(79, 'tonn', '61.91.4.5', 'users', 'logout', 'Logout : Logouted', '2019-10-27 16:15:50'),
(80, 'admin', '61.91.4.5', 'users', 'login', 'Success : LoginPage', '2019-10-27 16:16:01'),
(81, 'admin', '61.91.4.5', 'roles', 'update', 'แก้ไขข้อมูล roles : เจ้าหน้าที่ปรับปรุงข้อมูล', '2019-10-27 16:16:45'),
(82, 'admin', '61.91.4.5', 'users', 'logout', 'Logout : Logouted', '2019-10-27 16:16:51'),
(83, 'tonn', '61.91.4.5', 'users', 'login', 'Success : LoginPage', '2019-10-27 16:16:58'),
(84, 'admin', '223.24.154.58', 'users', 'login', 'Success : LoginPage', '2019-10-30 18:37:28'),
(85, 'admin', '49.49.245.114', 'users', 'login', 'Success : LoginPage', '2019-10-30 21:43:07'),
(86, 'admin', '183.89.151.87', 'users', 'login', 'Success : LoginPage', '2019-11-12 11:45:42'),
(87, 'admin', '183.89.151.87', 'users', 'store', 'เพิ่มข้อมูล : sakid', '2019-11-12 11:50:52'),
(88, 'admin', '183.89.151.87', 'users', 'logout', 'Logout : Logouted', '2019-11-12 11:51:03'),
(89, 'sakidlo', '183.89.151.87', 'users', 'login', 'Success : LoginPage', '2019-11-12 11:51:12'),
(90, 'sakidlo', '183.89.151.87', 'users', 'logout', 'Logout : Logouted', '2019-11-12 12:11:26'),
(91, 'admin', '183.89.151.87', 'users', 'login', 'Success : LoginPage', '2019-11-12 12:11:30'),
(92, 'admin', '183.89.151.87', 'users', 'logout', 'Logout : Logouted', '2019-11-12 12:13:20'),
(93, 'sakidlo', '183.89.151.87', 'users', 'login', 'Success : LoginPage', '2019-11-12 12:13:37'),
(94, 'sakidlo', '183.89.151.87', 'users', 'logout', 'Logout : Logouted', '2019-11-12 12:47:22'),
(95, 'admin', '183.89.151.87', 'users', 'login', 'Success : LoginPage', '2019-11-12 12:47:27'),
(96, 'admin', '183.89.151.87', 'roles', 'store', 'เพิ่มข้อมูล roles : job', '2019-11-12 13:00:50'),
(97, 'admin', '184.22.226.176', 'users', 'login', 'Success : LoginPage', '2019-11-12 21:58:12'),
(98, 'admin', '184.22.226.176', 'abouts', 'update', 'แก้ไขข้อมูล :  ,img : ', '2019-11-12 22:02:13'),
(99, 'admin', '184.22.226.176', 'abouts', 'update', 'แก้ไขข้อมูล :  ,img : ', '2019-11-12 22:03:56'),
(100, 'admin', '184.22.226.176', 'abouts', 'update', 'แก้ไขข้อมูล :  ,img : ', '2019-11-12 22:12:30'),
(101, 'admin', '184.22.226.176', 'abouts', 'update', 'แก้ไขข้อมูล :  ,img : ', '2019-11-12 22:13:10'),
(102, 'admin', '184.22.234.115', 'users', 'login', 'Success : LoginPage', '2019-11-12 22:33:00'),
(103, 'admin', '184.22.234.115', 'users', 'logout', 'Logout : Logouted', '2019-11-12 22:33:16'),
(104, 'admin', '184.22.234.115', 'users', 'login', 'Success : LoginPage', '2019-11-12 22:34:22'),
(105, 'admin', '184.22.233.70', 'users', 'login', 'Success : LoginPage', '2019-11-13 00:43:32'),
(106, 'admin', '184.22.233.70', 'users', 'login', 'Success : LoginPage', '2019-11-13 00:43:38'),
(107, 'sakidlo', '184.22.233.70', 'users', 'login', 'Success : LoginPage', '2019-11-13 00:48:29'),
(108, 'admin', '184.22.227.101', 'users', 'login', 'Success : LoginPage', '2019-11-13 00:49:12'),
(109, 'admin', '184.22.233.70', 'users', 'login', 'Success : LoginPage', '2019-11-13 00:57:11'),
(110, 'admin', '184.22.227.101', 'products', 'destroy', 'ลบข้อมูล สินค้า : เสาประติมากรรม', '2019-11-13 02:23:34'),
(111, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาประติมากรรม ,ประเภท : 1 ,รูปสินค้า :  ,catalog :  ,Seo :เสาปฏิมากรรม', '2019-11-13 02:24:57'),
(112, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาประติมากรรม ,ประเภท : 1 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/a59c228818a368e675fc80c57a0c4ab9.pdf ,Seo :เสาปฏิมากรรม', '2019-11-13 02:25:40'),
(113, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาประติมากรรม ,ประเภท : 1 ,รูปสินค้า :  ,catalog :  ,Seo :เสาปฏิมากรรม', '2019-11-13 02:41:31'),
(114, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : กรวยจราจร ,ประเภท : 8 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/c31e692b95aad794fd0d42619deabd28.pdf ,Seo :กรวยจราจร', '2019-11-13 02:43:58'),
(115, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : กระจกโค้งจราจร ,ประเภท : 8 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/14e779839f38a76a3fc49ee7368ee6e8.pdf ,Seo :กระจกโค้งจราจร', '2019-11-13 02:45:07'),
(116, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : กล้อง CCTV ,ประเภท : 8 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/b35fded9a991a8c0798884690eee7b89.pdf ,Seo :กล้อง CCTV', '2019-11-13 02:45:45'),
(117, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : กำแพงน้ำพลาสติก แบริเออร์น้ำ หรือแผงกั้นชนิดบรรจุน้ำ ,ประเภท : 8 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/a4e7f8a8176fc38f501cb7a5e04032e8.pdf ,Seo :กำแพงน้ำพลาสติก แบริเออร์น้ำ หรือแผงกั้นชนิดบรรจุน้ำ', '2019-11-13 02:46:39'),
(118, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ตู้ควบคุมสัญญาณไฟจราจร ,ประเภท : 5 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/83a9e9c31266e03adb3cf43f70f99ddc.pdf ,Seo :ตู้ควบคุมสัญญาณไฟจราจร', '2019-11-13 02:49:05'),
(119, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้าย Overhang ,ประเภท : 7 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/d6d83203dd8aa7d88568235280d04466.pdf ,Seo :ป้าย Overhang', '2019-11-13 02:49:43'),
(120, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้าย Overhead จราจร ,ประเภท : 7 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/37ac8e4d71a0b40ae82aebff80c70b7d.pdf ,Seo :ป้าย Overhead จราจร', '2019-11-13 02:50:10'),
(121, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายจอ LED VMS,SMS,MMS ,ประเภท : 8 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/a50a5cfad800a33a642d14a2e77070c8.pdf ,Seo :ป้ายจอ LED VMS,SMS,MMS', '2019-11-13 02:53:18'),
(122, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายซอยประติมากรรม ,ประเภท : 7 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/a3d5629aee5e75f7b63f4356c1f9d91e.pdf ,Seo :ป้ายซอยประติมากรรม', '2019-11-13 02:53:58'),
(123, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายบังคับจราจร ,ประเภท : 7 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/e0afae9c4ed34c5e158427f69c649f90.pdf ,Seo :ป้ายบังคับจราจร', '2019-11-13 02:54:28'),
(124, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายเตือนจราจร ,ประเภท : 7 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/524784f52d4ff09e3b2a77b08757e656.pdf ,Seo :ป้ายเตือนจราจร', '2019-11-13 02:55:08'),
(125, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายแนะนำ ,ประเภท : 7 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/9af7692c440acaad6b2a59da6a60e4d6.pdf ,Seo :ป้ายแนะนำ', '2019-11-13 02:56:14'),
(126, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ยางชะลอความเร็ว ,ประเภท : 8 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/22e3849ba950cc1cd1e0aff84388a56e.pdf ,Seo :ยางชะลอความเร็ว', '2019-11-13 02:57:07'),
(127, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ราวกั้นอันตราย (การ์ดเรล) ,ประเภท : 1 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/07e395583731bd8e9ff30017c31706dd.pdf ,Seo :ราวกั้นอันตราย (การ์ดเรล)', '2019-11-13 02:57:50'),
(128, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : หมุดแก้ว 360 องศา ,ประเภท : 8 ,รูปสินค้า :  ,catalog :  ,Seo :หมุดแก้ว 360 องศา', '2019-11-13 03:00:20'),
(129, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : หมุดจราจร ,ประเภท : 8 ,รูปสินค้า :  ,catalog :  ,Seo :หมุดจราจร', '2019-11-13 03:03:35'),
(130, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : หมุดอลูมิเนียม ,ประเภท : 8 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/93f5752c4c34555f07906819d475c15d.pdf ,Seo :หมุดอลูมิเนียม', '2019-11-13 03:04:29'),
(131, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : หมุดแก้ว 360 องศา ,ประเภท : 8 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/b5002f61b71496e1f43211a2eef5105b.pdf ,Seo :หมุดแก้ว 360 องศา', '2019-11-13 03:05:45'),
(132, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : โคมไฟจราจรนับเวลาถอยหลัง ,ประเภท : 5 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/7714f98330ca20887ee90099d9f8d6f1.pdf ,Seo :โคมไฟจราจรนับเวลาถอยหลัง', '2019-11-13 03:08:22'),
(133, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เป้าสะท้อนแสง ,ประเภท : 8 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/4a9ac7021a1f019dacb1d65a702dd38f.pdf ,Seo :เป้าสะท้อนแสง', '2019-11-13 03:09:41'),
(134, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสากลมปลายเรียว ชุบกัลวาไนซ์ สูง 4-14 เมตร ,ประเภท : 1 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/4291f27f779f29a42c0218012e2ca166.pdf ,Seo :เสากลมปลายเรียว ชุบกัลวาไนซ์ สูง 4-14 เมตร', '2019-11-13 03:12:58'),
(135, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาธงแบบกลมปลายเรียว ชุบกัลวาไนซ์  สูง 3-30 เมตร ,ประเภท : 1 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/9941343cf8ed42b24a46b2d9f052eea9.pdf ,Seo :เสาธงแบบกลมปลายเรียว ชุบกัลวาไนซ์  สูง 3-30 เมตร', '2019-11-13 03:15:36'),
(136, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร ,ประเภท : 1 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/7ce7968068a933d48623aa6936e9102b.pdf ,Seo :เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร', '2019-11-13 03:17:20'),
(137, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาประติมากรรม ,ประเภท : 1 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/82a8a042d8d258f964880f0ed73cffc5.pdf ,Seo :เสาปฏิมากรรม', '2019-11-13 03:18:28'),
(138, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาประติมากรรม ,ประเภท : 1 ,รูปสินค้า :  ,catalog :  ,Seo :เสาประติมากรรม', '2019-11-13 03:19:47'),
(139, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาล้มลุก ,ประเภท : 8 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/25c8e157ebbdd9c3e705824c45edaa6b.pdf ,Seo :เสาล้มลุก', '2019-11-13 03:20:22'),
(140, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาสนามกีฬา ชุบกัลวาไนซ์ สูง 15-40 เมตร ,ประเภท : 1 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/863f5dcb20f2f22954d49b2aea777360.pdf ,Seo :เสาสนามกีฬา ชุบกัลวาไนซ์ สูง 15-40 เมตร', '2019-11-13 03:22:04'),
(141, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาเหลี่ยมปลายตรง ชุบกัลวาไนซ์ สูง 4-14 เมตร ,ประเภท : 1 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/cd1f828fa418385ffde18878ad5379f3.pdf ,Seo :เสาเหลี่ยมปลายตรง ชุบกัลวาไนซ์ สูง 4-14 เมตร', '2019-11-13 03:23:07'),
(142, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร ,ประเภท : 1 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/4988a85eeae57807cc89ca8bc3c5bb46.pdf ,Seo :เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร', '2019-11-13 03:24:24'),
(143, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาไฟส่องสว่าง โซล่าเซลล์ ,ประเภท : 1 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/da6596d7306906af3af9616a569cdf30.pdf ,Seo :เสาไฟส่องสว่าง โซล่าเซลล์', '2019-11-13 03:25:48'),
(144, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาไฟไฮแมสแบบกลม ชุบกัลวาไนซ์ สูง 15-40 เมตร ,ประเภท : 1 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/780ff30891d056e345e2d5d3e16abc48.pdf ,Seo :เสาไฟไฮแมสแบบกลม ชุบกัลวาไนซ์ สูง 15-40 เมตร', '2019-11-13 03:26:49'),
(145, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสาไฟไฮแมสแบบเหลี่ยม ชุบกัลวาไนซ์ สูง 15-40 เมตร ,ประเภท : 1 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/14eaf310fe7a96530303d993b491fc6d.pdf ,Seo :เสาไฟไฮแมสแบบเหลี่ยม ชุบกัลวาไนซ์ สูง 15-40 เมตร', '2019-11-13 03:28:04'),
(146, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : เสื้อจราจร ,ประเภท : 8 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/27089f39b5bc1f871ac8f9d1db9b5532.pdf ,Seo :เสื้อจราจร', '2019-11-13 03:28:40'),
(147, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : แผงกั้นจราจร ,ประเภท : 8 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/4457488b35936bc29e659419c09e13a5.pdf ,Seo :แผงกั้นจราจร', '2019-11-13 03:29:31'),
(148, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : โคมไฟจราจรแบบ 3 ดวงโคม ,ประเภท : 5 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/2030e7db51552600523f08cf7b8073c6.pdf ,Seo :โคมไฟจราจรแบบ 3 ดวงโคม', '2019-11-13 03:30:53'),
(149, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : โคมไฟจราจรแบบ3ดวงโคม ,ประเภท : 5 ,รูปสินค้า :  ,catalog :  ,Seo :โคมไฟจราจรแบบ3ดวงโคม', '2019-11-13 03:31:43'),
(150, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : โคมไฟจราจร ,ประเภท : 5 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/3ac0a6798a3f75d13dce03ba7edb5dd7.pdf ,Seo :โคมไฟจราจร', '2019-11-13 03:34:29'),
(151, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : โคมไฟจราจรนับเวลาถอยหลัง ,ประเภท : 5 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/c6630765ddd13518b09e4f2ce5134d44.pdf ,Seo :โคมไฟจราจรนับเวลาถอยหลัง', '2019-11-13 03:35:05'),
(152, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : โคมไฟจราจรแบบ 4/6 ดวงโคม ,ประเภท : 5 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/57d84d90a025a4407eace27e17e3b4f6.pdf ,Seo :โคมไฟจราจรแบบ 4/6 ดวงโคม', '2019-11-13 03:35:59'),
(153, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : โคมไฟจราจรแบ่งช่องทางเดินรถ ,ประเภท : 5 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/9801f8d1d609fbb92ae29fb56094840d.pdf ,Seo :โคมไฟจราจรแบ่งช่องทางเดินรถ', '2019-11-13 03:36:50'),
(154, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : โคมไฟถนน ชนิดหลอดไส้ ,ประเภท : 4 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/509c9a47f72d94ecca68f041fc01d496.pdf ,Seo :โคมไฟถนน ชนิดหลอดไส้', '2019-11-13 03:38:49'),
(155, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : โคมไฟถนน ชนิดหลอด LED ,ประเภท : 4 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/28e9c9f7fa6dd9c6957752463fea45bb.pdf ,Seo :โคมไฟถนน ชนิดหลอด LED', '2019-11-13 03:39:51'),
(156, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : โคมไฟสนาม ชนิดหลอดไส้ ,ประเภท : 4 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/062d83c29a08ef395fdeabe84b956215.pdf ,Seo :โคมไฟสนาม ชนิดหลอดไส้', '2019-11-13 03:41:00'),
(157, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : โคมไฟสนาม ชนิดหลอด LED ,ประเภท : 4 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/f47fadcb40ac2a92b8052e809d38ed9f.pdf ,Seo :โคมไฟสนาม ชนิดหลอด LED', '2019-11-13 03:42:03'),
(158, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายไฟเตือนลดความเร็วโซล่าเซลล์ ,ประเภท : 6 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/c0557234220b3ff6931952f0f8cf2164.pdf ,Seo :ป้ายไฟเตือนลดความเร็วโซล่าเซลล์', '2019-11-13 03:44:04'),
(159, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายไฟเตือนเขตโรงเรียนโซล่าเซลล์ ,ประเภท : 6 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/373f443bc839e0ac6b90ba8a40b4e7d2.pdf ,Seo :ป้ายไฟเตือนเขตโรงเรียนโซล่าเซลล์', '2019-11-13 03:44:52'),
(160, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายไฟเตือนทางเบี่ยงโซล่าเซลล์ ,ประเภท : 6 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/7008e9ba24cd543e4bd88942c92b0e7c.pdf ,Seo :ป้ายไฟเตือนทางเบี่ยงโซล่าเซลล์', '2019-11-13 03:45:48'),
(161, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายไฟเตือนกระพริบโซล่าเซลล์ ,ประเภท : 6 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/1088bad5f6d5d55f288934f39686c308.pdf ,Seo :ป้ายไฟเตือนกระพริบโซล่าเซลล์', '2019-11-13 03:46:55'),
(162, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายไฟเตือนเกาะกลางโซล่าเซลล์ ,ประเภท : 6 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/d577cadd7e02892d4d91fdadc579c11e.pdf ,Seo :ป้ายไฟเตือนเกาะกลางโซล่าเซลล์', '2019-11-13 03:48:31'),
(163, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายไฟเตือนทางเบี่ยงโซล่าเซลล์ ,ประเภท : 6 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/c526416d529283946fdeca8788ee0c91.pdf ,Seo :ป้ายไฟเตือนทางเบี่ยงโซล่าเซลล์', '2019-11-13 03:49:50'),
(164, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายไฟเตือนเกาะกลางโซล่าเซลล์ ,ประเภท : 6 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/b94317011b1fcce8559dbaf0384ecaad.pdf ,Seo :ป้ายไฟเตือนเกาะกลางโซล่าเซลล์', '2019-11-13 03:50:16'),
(165, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายไฟเตือนรถบรรทุกเข้า-ออกโซล่าเซลล์ ,ประเภท : 6 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/305d74923d588f92a186beaff646d370.pdf ,Seo :ป้ายไฟเตือนรถบรรทุกเข้า-ออกโซล่าเซลล์', '2019-11-13 03:51:29'),
(166, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายไฟเตือนจราจร CCTV โซล่าเซลล์ ,ประเภท : 6 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/645022c066541b142b0d0d6add8a4d17.pdf ,Seo :ป้ายไฟเตือนจราจร CCTV โซล่าเซลล์', '2019-11-13 03:52:40'),
(167, 'admin', '184.22.227.101', 'products', 'update', 'แก้ไขข้อมูล สินค้า : ป้ายไฟเตือนทางแยกโซล่าเซลล์ ,ประเภท : 6 ,รูปสินค้า :  ,catalog : uploads/product/2019/11/444b6d7938bcb8af8f248ee603b425ec.pdf ,Seo :ป้ายไฟเตือนทางแยกโซล่าเซลล์', '2019-11-13 03:53:29');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `banners_img` varchar(255) DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `banners_img`, `create_by`, `update_by`, `active`, `create_at`, `update_at`) VALUES
(3, 'uploads/banners/2019/01/5f6074bd7d8cd2f26a5ccf76e3fbe9e2.jpg', 1, 1, 0, '2019-01-26 16:05:08', '2019-01-26 16:05:08'),
(4, 'uploads/banners/2019/01/ec66d3cd616e4006a0cefb62251d6030.jpg', 1, 1, 0, '2019-01-27 16:33:16', '2019-01-27 16:33:16'),
(5, 'uploads/banners/2019/01/02e9c15684fc49debd2d11b31f65e7b5.jpg', 1, 1, 0, '2019-01-27 16:33:27', '2019-01-27 16:33:27'),
(6, 'uploads/banners/2019/01/ebafda35f29cf6ea3bf44186ad26b97e.jpg', 1, 1, 0, '2019-01-27 16:33:39', '2019-01-27 16:33:39'),
(7, 'uploads/banners/2019/01/dece7dcc3fdfa1566c003c42011d1f52.jpg', 1, 1, 0, '2019-01-27 16:33:52', '2019-01-27 16:33:52');

-- --------------------------------------------------------

--
-- Table structure for table `catalog`
--

CREATE TABLE `catalog` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `catalog_img` varchar(255) DEFAULT NULL,
  `catalog_file` varchar(255) DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `catalog`
--

INSERT INTO `catalog` (`id`, `name`, `slug`, `catalog_img`, `catalog_file`, `create_by`, `update_by`, `active`, `create_at`, `update_at`) VALUES
(9, 'กลุ่มเสาประเภทต่างๆและงานการ์ดเรล', 'กลุ่มเสาประเภทต่างๆและงานการ์ดเรล', 'uploads/catalog/2019/10/6e5940ee83b84d1a77d746b4073cf3a5.jpg', 'uploads/catalog/2019/10/757d92ceb0f9300739c64d4a6d2e76fe.pdf', 1, 1, 0, '2019-10-13 20:05:49', '2019-10-13 20:05:49'),
(10, 'กลุ่มโคมไฟส่องสว่าง', 'กลุ่มโคมไฟส่องสว่าง', 'uploads/catalog/2019/10/ae25344b1d148ef57a485baad91ee0af.jpg', 'uploads/catalog/2019/10/23539992cc496e88a230ed8e7c62856d.pdf', 1, 1, 0, '2019-10-13 20:07:38', '2019-10-13 20:07:38'),
(11, 'ไฟจราจร', 'ไฟจราจร', 'uploads/catalog/2019/10/2e2ab896a1ff1f80ad800d7a10470c30.jpg', 'uploads/catalog/2019/10/e2bd2734d8777b895c0a76ff60cd80a6.pdf', 1, 1, 0, '2019-10-13 20:09:53', '2019-10-13 20:09:53'),
(12, 'ป้ายไฟเตือนจราจรโซล่าเซลล์', 'ป้ายไฟเตือนจราจรโซล่าเซลล์', 'uploads/catalog/2019/10/182693b402950f2d468b982ee1066e0b.jpg', 'uploads/catalog/2019/10/d352822c440133b6d0bea322903b0932.pdf', 1, 1, 0, '2019-10-13 20:12:43', '2019-10-13 20:12:43'),
(13, 'ป้ายจราจร', 'ป้ายจราจร', 'uploads/catalog/2019/10/958ae03a541d0f28961144da21e90d05.jpg', 'uploads/catalog/2019/10/9b497587620e3e99f63b8543c7a7d1d1.pdf', 1, 1, 0, '2019-10-13 20:15:34', '2019-10-13 20:15:34'),
(14, 'อุปกรณ์ความปลอดภัย', 'อุปกรณ์ความปลอดภัย', 'uploads/catalog/2019/10/b7de0b04cac70f56ba2d84bcea03cd56.jpg', 'uploads/catalog/2019/10/6d7999cce94351085c1c0c6a0ff6670d.pdf', 1, 1, 0, '2019-10-13 20:16:31', '2019-10-13 20:16:31');

-- --------------------------------------------------------

--
-- Table structure for table `catalog_download_log`
--

CREATE TABLE `catalog_download_log` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `address` text,
  `type` int(11) DEFAULT '0' COMMENT '0 : ดาวน์โหลดทั้งหมด\n1 : ดาวน์โหลดตามประเภท\n2 : ดาวน์โหลดสินค้า',
  `create_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `catalog_download_log`
--

INSERT INTO `catalog_download_log` (`id`, `name`, `email`, `ref_id`, `file`, `address`, `type`, `create_by`, `update_by`, `active`, `create_at`, `update_at`) VALUES
(1, 'Test', 'test@gmail.com', 1, 'uploads/catalog/2019/01/b938dc94876a391c69788d329239e5de.pdf', 'Test', 0, 0, 0, 0, '2019-02-07 11:32:31', '2019-02-07 11:32:31'),
(2, 'sukij', 'sukij@gmail.com', 4, '', 'sukij', 2, 0, 0, 0, '2019-02-25 14:25:09', '2019-02-25 14:25:09'),
(3, 'sukij23', 'sukij@gmail.com', 4, '', 'friendly6design', 2, 0, 0, 0, '2019-02-25 15:41:02', '2019-02-25 15:41:02'),
(4, 'กกก', 'saroch@gmail.com', 6, '', 'กกกก', 2, 0, 0, 0, '2019-08-08 22:54:50', '2019-08-08 22:54:50'),
(5, 'sakidlo', 'ryankij@gmail.com', 6, 'uploads/product/2019/09/54b71bd528dc3e74cda53f3fc4b504b2.pdf', 'sakidlocode', 2, 0, 0, 0, '2019-09-01 00:14:56', '2019-09-01 00:14:56'),
(6, 'mama', 'ryankij@gmail.com', 34, 'uploads/product/2019/09/70e0b51f866664ab174bfcf1a85fb73f.pdf', 'mama', 2, 0, 0, 0, '2019-09-01 01:57:57', '2019-09-01 01:57:57'),
(7, 'Ruslee Seebu', 'lee.yam_1214@hotmail.com', 27, 'uploads/product/2019/09/5d67b23ebbc1510ba072dd9c10afe703.pdf', 'test', 2, 0, 0, 0, '2019-09-08 00:22:45', '2019-09-08 00:22:45'),
(8, 'Thanapol', 'rittichai4659@gmail.com', 12, 'uploads/product/2019/09/0c2d3f0ddad8460ca497fd9046cc4c25.pdf', 'หจก.รุ่งเจริญพาณิชย์ การไฟฟ้า', 2, 0, 0, 0, '2019-09-10 13:15:01', '2019-09-10 13:15:01'),
(9, 'Thanapol', 'rittichai4659@gmail.com', 12, 'uploads/product/2019/09/0c2d3f0ddad8460ca497fd9046cc4c25.pdf', 'หจก.รุ่งเจริญพาณิชย์ การไฟฟ้า', 2, 0, 0, 0, '2019-09-10 13:15:08', '2019-09-10 13:15:08'),
(10, 'pattaratida', 'kongsankamchak@gmail.com', 41, 'uploads/product/2019/09/168b73e2c6e762909c66fb661a5dd47f.pdf', 'ศิริไพบูลย์พัฒนาการ', 2, 0, 0, 0, '2019-09-18 10:26:47', '2019-09-18 10:26:47'),
(11, 'pattaratida', 'kongsankamchak@gmail.com', 17, 'uploads/product/2019/09/0cdaa9786c95fac018e004f8944ded8c.pdf', 'ศิริไพบูลย์พัฒนาการ', 2, 0, 0, 0, '2019-09-18 10:30:18', '2019-09-18 10:30:18'),
(12, 'anan', 'anan.kasr@gmail.com', 24, 'uploads/product/2019/09/f9029cec05b21aff54cfa368b2a5d6bb.pdf', 'บริาัท วพิตา', 2, 0, 0, 0, '2019-09-21 13:41:47', '2019-09-21 13:41:47'),
(13, 'anan', 'anan.kasr@gmail.com', 26, 'uploads/product/2019/09/eea0dcf308e64ee7d6a0ef0a9370cf8b.pdf', 'วพิตา', 2, 0, 0, 0, '2019-09-21 13:45:25', '2019-09-21 13:45:25'),
(14, 'ทองดี', 'thongdeeudon@hotmail.com', 17, 'uploads/product/2019/09/0cdaa9786c95fac018e004f8944ded8c.pdf', 'หจก.นภากร', 2, 0, 0, 0, '2019-10-04 00:39:21', '2019-10-04 00:39:21'),
(15, 'ทองดี', 'thongdeeudon@hotmail.com', 17, 'uploads/product/2019/09/0cdaa9786c95fac018e004f8944ded8c.pdf', 'หจก.นภากร', 2, 0, 0, 0, '2019-10-04 11:24:24', '2019-10-04 11:24:24'),
(16, 'กก', 's@gmail.com', 1, 'uploads/icon/2019/10/ec726bd74ff7712ce387a8fc25b10ef5.pdf', 'กก', 1, 0, 0, 0, '2019-10-13 20:10:22', '2019-10-13 20:10:22'),
(17, 'pemika', 'pleumd@gmail.com', 47, 'uploads/product/2019/10/cd9937dc7dcdb7c7033a00eaa46fc256.pdf', 'C-Con', 2, 0, 0, 0, '2019-10-14 10:06:57', '2019-10-14 10:06:57'),
(18, 'pemika', 'pleumd@gmail.com', 47, 'uploads/product/2019/10/cd9937dc7dcdb7c7033a00eaa46fc256.pdf', 'C-Con', 2, 0, 0, 0, '2019-10-14 10:06:58', '2019-10-14 10:06:58'),
(19, 'pemika', 'pleumd@gmail.com', 47, 'uploads/product/2019/10/cd9937dc7dcdb7c7033a00eaa46fc256.pdf', 'C-Con', 2, 0, 0, 0, '2019-10-14 10:07:03', '2019-10-14 10:07:03'),
(20, 'pemika', 'pleumd@gmail.com', 47, 'uploads/product/2019/10/cd9937dc7dcdb7c7033a00eaa46fc256.pdf', 'C-Con', 2, 0, 0, 0, '2019-10-14 10:07:04', '2019-10-14 10:07:04'),
(21, 'pemika', 'pleumd@gmail.com', 47, 'uploads/product/2019/10/cd9937dc7dcdb7c7033a00eaa46fc256.pdf', 'C-Con', 2, 0, 0, 0, '2019-10-14 10:07:04', '2019-10-14 10:07:04'),
(22, 'pemika', 'pleumd@gmail.com', 47, 'uploads/product/2019/10/cd9937dc7dcdb7c7033a00eaa46fc256.pdf', 'C-Con', 2, 0, 0, 0, '2019-10-14 10:07:04', '2019-10-14 10:07:04'),
(23, 'pemika', 'pleumd@gmail.com', 47, 'uploads/product/2019/10/cd9937dc7dcdb7c7033a00eaa46fc256.pdf', 'C-Con', 2, 0, 0, 0, '2019-10-14 10:07:04', '2019-10-14 10:07:04'),
(24, 'pemika', 'pleumd@gmail.com', 47, 'uploads/product/2019/10/cd9937dc7dcdb7c7033a00eaa46fc256.pdf', 'C-Con', 2, 0, 0, 0, '2019-10-14 10:07:04', '2019-10-14 10:07:04'),
(25, 'pemika', 'pleumd@gmail.com', 47, 'uploads/product/2019/10/cd9937dc7dcdb7c7033a00eaa46fc256.pdf', 'C-Con', 2, 0, 0, 0, '2019-10-14 10:07:05', '2019-10-14 10:07:05'),
(26, 'pemika', 'pleumd@gmail.com', 47, 'uploads/product/2019/10/cd9937dc7dcdb7c7033a00eaa46fc256.pdf', 'C-Con', 2, 0, 0, 0, '2019-10-14 10:07:05', '2019-10-14 10:07:05'),
(27, 'pemika', 'pleumd@gmail.com', 47, 'uploads/product/2019/10/cd9937dc7dcdb7c7033a00eaa46fc256.pdf', 'C-Con', 2, 0, 0, 0, '2019-10-14 10:07:05', '2019-10-14 10:07:05'),
(28, 'pemika', 'pleumd@gmail.com', 47, 'uploads/product/2019/10/cd9937dc7dcdb7c7033a00eaa46fc256.pdf', 'C-Con', 2, 0, 0, 0, '2019-10-14 10:07:11', '2019-10-14 10:07:11'),
(29, 'pemika', 'pleumd@gmail.com', 47, 'uploads/product/2019/10/cd9937dc7dcdb7c7033a00eaa46fc256.pdf', 'C-Con', 2, 0, 0, 0, '2019-10-14 10:07:13', '2019-10-14 10:07:13'),
(30, 'pemika', 'pleumd@gmail.com', 47, 'uploads/product/2019/10/cd9937dc7dcdb7c7033a00eaa46fc256.pdf', 'C-Con', 2, 0, 0, 0, '2019-10-14 10:07:13', '2019-10-14 10:07:13'),
(31, 'jiggy', 'kritsakorn_p@hotmail.com', 1, 'uploads/icon/2019/10/ec726bd74ff7712ce387a8fc25b10ef5.pdf', 'enlighten', 1, 0, 0, 0, '2019-10-14 15:36:59', '2019-10-14 15:36:59'),
(32, 'jiggy', 'kritsakorn_p@hotmail.com', 5, 'uploads/icon/2019/10/db56e7ca5085cb4011b2238cae7c335d.pdf', 'enlighten', 1, 0, 0, 0, '2019-10-14 15:41:21', '2019-10-14 15:41:21'),
(33, 'jiggy', 'kritsakorn_p@hotmail.com', 7, 'uploads/icon/2019/10/fbbaa1f55604b8d673bfbe63a21358e9.pdf', 'enlighten', 1, 0, 0, 0, '2019-10-14 15:46:30', '2019-10-14 15:46:30'),
(34, 'jiggy', 'kritsakorn_p@hotmail.com', 4, 'uploads/icon/2019/10/e7ad87b0c585f7596e0671bd496248cd.pdf', 'enlighten', 1, 0, 0, 0, '2019-10-14 15:56:11', '2019-10-14 15:56:11'),
(35, 'jiggy', 'kritsakorn_p@hotmail.com', 8, 'uploads/icon/2019/10/f64af584cf00c5cb9736c710bb50e14f.pdf', 'enlighten', 1, 0, 0, 0, '2019-10-14 16:00:51', '2019-10-14 16:00:51'),
(36, 'jiggy', 'kritsakorn_p@hotmail.com', 5, 'uploads/icon/2019/10/db56e7ca5085cb4011b2238cae7c335d.pdf', 'enlighten', 1, 0, 0, 0, '2019-10-14 16:21:58', '2019-10-14 16:21:58'),
(37, 'jiggy', 'kritsakorn_p@hotmail.com', 6, 'uploads/icon/2019/10/45ff3c86a7681c1c7a3ad97db677e71e.pdf', 'enlighten', 1, 0, 0, 0, '2019-10-14 16:22:51', '2019-10-14 16:22:51'),
(38, 'jiggy', 'kritsakorn_p@hotmail.com', 8, 'uploads/icon/2019/10/f64af584cf00c5cb9736c710bb50e14f.pdf', 'Enlighten', 1, 0, 0, 0, '2019-10-14 17:04:53', '2019-10-14 17:04:53'),
(39, 'กก', 't@gmail.com', 1, 'uploads/icon/2019/10/ec726bd74ff7712ce387a8fc25b10ef5.pdf', '111', 1, 0, 0, 0, '2019-10-30 19:09:43', '2019-10-30 19:09:43'),
(40, 'กก', 't@gmail.com', 7, 'uploads/icon/2019/10/fbbaa1f55604b8d673bfbe63a21358e9.pdf', '111', 1, 0, 0, 0, '2019-10-30 19:10:25', '2019-10-30 19:10:25'),
(41, 'test', 'test@gmail.com', 5, 'uploads/icon/2019/10/db56e7ca5085cb4011b2238cae7c335d.pdf', 'test', 1, 0, 0, 0, '2019-10-30 21:49:42', '2019-10-30 21:49:42'),
(42, 'test', 'test@gmail.com', 0, '', 'th', 0, 0, 0, 0, '2019-11-03 12:41:57', '2019-11-03 12:41:57'),
(43, 'ddd', 'd@gnail.com', 0, '', '1234', 0, 0, 0, 0, '2019-11-03 19:52:12', '2019-11-03 19:52:12'),
(44, 'kritsakorn', 'c-conlt05@c-contraffic.com', 18, 'uploads/product/2019/10/d16dc717b7e92c55a8beb663b49d396e.pdf', 'c-con', 2, 0, 0, 0, '2019-11-12 08:41:16', '2019-11-12 08:41:16'),
(45, 'kritsakorn', 'c-conlt05@c-contraffic.com', 37, 'uploads/product/2019/10/af30d9e432fa34d714ff9b26681b8a11.pdf', 'c-con', 2, 0, 0, 0, '2019-11-12 08:52:20', '2019-11-12 08:52:20'),
(46, 'kritsakorn', 'c-conlt05@c-contraffic.com', 35, 'uploads/product/2019/10/06ecf33932ebce94df4d2a872ff02745.pdf', 'c-con', 2, 0, 0, 0, '2019-11-12 08:53:39', '2019-11-12 08:53:39'),
(47, 'test', 'test@gmail.com', 6, 'uploads/product/2019/10/7a75d8d34cf80e208cccd486c1ffa111.pdf', 'test', 2, 0, 0, 0, '2019-11-12 12:17:48', '2019-11-12 12:17:48'),
(48, 'kritsakorn', 'c-conlt05@c-contraffic.com', 37, 'uploads/product/2019/11/524784f52d4ff09e3b2a77b08757e656.pdf', 'c-con', 2, 0, 0, 0, '2019-11-13 14:00:04', '2019-11-13 14:00:04'),
(49, 'kritsakorn', 'c-conlt05@c-contraffic.com', 8, 'uploads/product/2019/11/4988a85eeae57807cc89ca8bc3c5bb46.pdf', 'c-con', 2, 0, 0, 0, '2019-11-14 14:19:15', '2019-11-14 14:19:15'),
(50, 'kritsakorn', 'c-conlt05@c-contraffic.com', 11, 'uploads/product/2019/11/9941343cf8ed42b24a46b2d9f052eea9.pdf', 'c-con', 2, 0, 0, 0, '2019-11-14 15:48:23', '2019-11-14 15:48:23'),
(51, 'kritsakorn', 'c-conlt05@c-contraffic.com', 11, 'uploads/product/2019/11/9941343cf8ed42b24a46b2d9f052eea9.pdf', 'c-con', 2, 0, 0, 0, '2019-11-14 15:48:27', '2019-11-14 15:48:27');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `seo_id` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `icon`, `parent_id`, `sequence`, `file`, `create_by`, `update_by`, `seo_id`, `active`, `create_at`, `update_at`) VALUES
(1, 'กลุ่มงานเสาประเภทต่างๆและงานการ์ดเรล', 'กลุ่มงานเสาประเภทต่างๆและงานการ์ดเรล', 'uploads/icon/2019/01/3778007836ba4a4f6c663f65362acf43.png', 0, 1, 'uploads/icon/2019/10/ec726bd74ff7712ce387a8fc25b10ef5.pdf', 1, 1, 4, 0, '2019-10-10 23:17:17', '2019-10-10 23:17:17'),
(4, 'กลุ่มโคมไฟส่องสว่าง', 'กลุ่มโคมไฟส่องสว่าง', 'uploads/icon/2019/01/d0a0679cbd6a4bdd35194c6b447c980c.png', 0, 4, 'uploads/icon/2019/10/e7ad87b0c585f7596e0671bd496248cd.pdf', 1, 1, 5, 0, '2019-10-10 23:18:08', '2019-10-10 23:18:08'),
(5, 'กลุ่มไฟจราจร', 'กลุ่มไฟจราจร', 'uploads/icon/2019/01/087d2f95bba81b490e44864e7bfb7c6c.png', 0, 2, 'uploads/icon/2019/10/db56e7ca5085cb4011b2238cae7c335d.pdf', 1, 1, 6, 0, '2019-10-17 15:39:52', '2019-10-17 15:39:52'),
(6, 'กลุ่มไฟเตือนจราจรโซล่าเซลล์', 'กลุ่มไฟเตือนจราจรโซล่าเซลล์', 'uploads/icon/2019/01/1ecefd00a85de5889729bfabd019d70d.png', 0, 6, 'uploads/icon/2019/10/45ff3c86a7681c1c7a3ad97db677e71e.pdf', 1, 1, 7, 0, '2019-10-17 15:40:43', '2019-10-17 15:40:43'),
(7, 'กลุ่มป้ายจราจร', 'กลุ่มป้ายจราจร', 'uploads/icon/2019/01/da22f54aa4acbf4ed7f634370c4e1d6e.png', 0, 3, 'uploads/icon/2019/10/fbbaa1f55604b8d673bfbe63a21358e9.pdf', 1, 1, 8, 0, '2019-10-17 15:40:03', '2019-10-17 15:40:03'),
(8, 'กลุ่มอุปกรณ์ความปลอดภัย', 'กลุ่มอุปกรณ์ความปลอดภัย', 'uploads/icon/2019/01/32856dcadc1b69c89780292084ea853f.png', 0, 5, 'uploads/icon/2019/10/f64af584cf00c5cb9736c710bb50e14f.pdf', 1, 1, 9, 0, '2019-10-17 15:41:15', '2019-10-17 15:41:15');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) CHARACTER SET latin1 NOT NULL,
  `ip_address` varchar(45) CHARACTER SET latin1 NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('000h34192e15vlul5mrftkraq8epq9kd', '184.22.227.101', 1573590228, '__ci_last_regenerate|i:1573589907;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('0tchtuev8p011vqufl4r5guq60s6m0lj', '184.22.227.101', 1573587811, '__ci_last_regenerate|i:1573587488;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('2abulc0id8vgbnniqtbbb9lk2bo2gnjd', '184.22.227.101', 1573588772, '__ci_last_regenerate|i:1573588444;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('49npfdp8nfp6ne9nhmmooeidj9fda431', '184.22.225.99', 1573646883, '__ci_last_regenerate|i:1573646883;'),
('49oc0rrtv53u8042182guao9tlacq0r9', '184.22.227.101', 1573589105, '__ci_last_regenerate|i:1573588808;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('4q3o2hgg6l049be8v215mdalr0s4d6bo', '184.22.225.99', 1573646882, '__ci_last_regenerate|i:1573646882;'),
('5ttrc1ahbjesg00lbllgokvnv6i0eifi', '184.22.227.101', 1573589569, '__ci_last_regenerate|i:1573589145;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('7rgb35kj4i030g12udnighllb611a3gq', '184.22.233.70', 1573584379, '__ci_last_regenerate|i:1573584306;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('896obrosnisnumrnan7ms5uofjm6r7m6', '184.22.227.101', 1573586991, '__ci_last_regenerate|i:1573586991;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('8gos1cbu2jt72g6o4v9v06r9md39aa4t', '184.22.227.101', 1573591791, '__ci_last_regenerate|i:1573591493;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}status|i:1;__ci_vars|a:1:{s:6:\"status\";s:3:\"old\";}'),
('aibuiajmvhoumbe2tn3dgvt984tl1et1', '184.22.227.101', 1573588103, '__ci_last_regenerate|i:1573587838;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('amuiuiskr3ktae26b743vlsqri43hs0t', '184.22.227.101', 1573591493, '__ci_last_regenerate|i:1573591190;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}status|i:1;__ci_vars|a:1:{s:6:\"status\";s:3:\"new\";}'),
('atth3n6ln7a2acl04eg0t51cn4vkmhp9', '184.22.233.70', 1573583322, '__ci_last_regenerate|i:1573583023;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('bdjqas6md1ds7r45jhbk289edi094gs1', '184.22.233.70', 1573582947, '__ci_last_regenerate|i:1573582714;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('bssvue6mdarspsb65us0q00hgfa4sjk8', '184.22.227.101', 1573592010, '__ci_last_regenerate|i:1573591800;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}status|i:1;__ci_vars|a:1:{s:6:\"status\";s:3:\"old\";}'),
('dp0sehn6f234e6lbhmgof1f66b6c59gq', '184.22.227.101', 1573590555, '__ci_last_regenerate|i:1573590259;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('gr4eeihea7nmkk9tso52i7giv5abrgrl', '184.22.233.70', 1573582455, '__ci_last_regenerate|i:1573582269;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('h320lkou9f6m2rgm427hcngp5ht1s7uv', '184.22.233.70', 1573588617, '__ci_last_regenerate|i:1573588617;'),
('m5h8p4su78pvije4cqiekah9qh1h1h79', '184.22.227.101', 1573591153, '__ci_last_regenerate|i:1573590882;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('q68vg2v5479cqgh59apsc39kt55haukv', '184.22.227.101', 1573590870, '__ci_last_regenerate|i:1573590571;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}status|i:1;__ci_vars|a:1:{s:6:\"status\";s:3:\"old\";}'),
('qerq1ohuruol8rjgl4jub0mklku1riv5', '184.22.233.70', 1573583797, '__ci_last_regenerate|i:1573583701;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('sk2f5mm77bbpkqj8iiubbfjh0liksemd', '184.22.233.70', 1573585051, '__ci_last_regenerate|i:1573584789;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('t4r93cfj0m5d6b4fj1v2fs6c75h63qho', '184.22.227.101', 1573588439, '__ci_last_regenerate|i:1573588140;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}status|i:1;__ci_vars|a:1:{s:6:\"status\";s:3:\"old\";}'),
('tqptsh2aamn76t04oo7ni2ujte2i0vt6', '184.22.227.101', 1573586748, '__ci_last_regenerate|i:1573586613;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('u3nimv3tqlr9g872lgoa27p98v82nb73', '184.22.233.70', 1573583531, '__ci_last_regenerate|i:1573583331;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}'),
('ue6d3se0ic8bkk2rj51fe8ufam5ch7cg', '184.22.227.101', 1573589872, '__ci_last_regenerate|i:1573589577;users|a:5:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:5:\"admin\";s:4:\"Name\";s:12:\"admin system\";s:6:\"RoleId\";s:1:\"1\";s:11:\"RolesSystem\";s:79:\"1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014\";}');

-- --------------------------------------------------------

--
-- Table structure for table `config_mail`
--

CREATE TABLE `config_mail` (
  `id` int(11) NOT NULL,
  `Host` varchar(255) DEFAULT NULL,
  `Username` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `SMTPSecure` varchar(255) DEFAULT NULL,
  `Port` int(11) DEFAULT '0',
  `create_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `config_mail`
--

INSERT INTO `config_mail` (`id`, `Host`, `Username`, `Password`, `SMTPSecure`, `Port`, `create_by`, `update_by`, `active`, `create_at`, `update_at`) VALUES
(1, 'ssl://smtp.gmail.com', 'ccontraffic@gmail.com', 'ccontraffic@8', 'tls', 465, 1, 1, 0, '2019-10-16 23:34:14', '2019-10-16 23:34:14');

-- --------------------------------------------------------

--
-- Table structure for table `config_mail_send`
--

CREATE TABLE `config_mail_send` (
  `id` int(11) NOT NULL,
  `page` varchar(45) DEFAULT NULL,
  `send_mail` varchar(255) DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `config_mail_send`
--

INSERT INTO `config_mail_send` (`id`, `page`, `send_mail`, `create_by`, `update_by`, `active`, `create_at`, `update_at`) VALUES
(1, 'quotation', 'ccon-support@c-contrafﬁc.com', 1, 1, 0, '2019-10-16 23:34:14', '2019-10-16 23:34:14'),
(2, 'contact', 'ccon-support@c-contrafﬁc.com', 1, 1, 0, '2019-10-16 23:34:14', '2019-10-16 23:34:14'),
(3, 'joinus', 'ccon-support@c-contrafﬁc.com', 1, 1, 0, '2019-10-16 23:34:14', '2019-10-16 23:34:14');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `contact_id` int(11) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact_title` varchar(255) DEFAULT NULL,
  `contacts_detail` text,
  `create_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `read` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '0',
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`contact_id`, `user_name`, `email`, `contact_title`, `contacts_detail`, `create_by`, `update_by`, `read`, `active`, `create_at`, `update_at`) VALUES
(3, 'ต้น', 'saroch.wora@gmail.com', 'tttt', 'tttt', 0, 0, 0, 0, '2019-10-16 23:07:06', '2019-10-16 23:07:06'),
(4, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-10-20 00:35:38', '2019-10-20 00:35:38'),
(5, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-10-22 16:39:00', '2019-10-22 16:39:00'),
(6, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-10-22 21:51:25', '2019-10-22 21:51:25'),
(7, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-10-24 15:00:53', '2019-10-24 15:00:53'),
(8, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-10-25 00:50:14', '2019-10-25 00:50:14'),
(9, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-10-27 14:31:10', '2019-10-27 14:31:10'),
(10, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-10-27 14:31:12', '2019-10-27 14:31:12'),
(11, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-10-27 14:31:13', '2019-10-27 14:31:13'),
(12, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-10-27 14:31:14', '2019-10-27 14:31:14'),
(13, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-11-01 06:19:29', '2019-11-01 06:19:29'),
(14, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-11-06 03:42:55', '2019-11-06 03:42:55'),
(15, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-11-12 13:51:23', '2019-11-12 13:51:23'),
(16, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-11-13 16:37:22', '2019-11-13 16:37:22'),
(17, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-11-14 00:09:26', '2019-11-14 00:09:26'),
(18, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-11-14 00:09:27', '2019-11-14 00:09:27'),
(19, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-11-14 00:09:28', '2019-11-14 00:09:28'),
(20, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-11-14 00:09:29', '2019-11-14 00:09:29'),
(21, NULL, NULL, NULL, NULL, 0, 0, 0, 0, '2019-11-18 21:07:24', '2019-11-18 21:07:24');

-- --------------------------------------------------------

--
-- Table structure for table `footers`
--

CREATE TABLE `footers` (
  `footer_id` int(11) NOT NULL,
  `footer_name` varchar(255) DEFAULT NULL,
  `footer_content` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT '1',
  `icon` varchar(255) DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `footers`
--

INSERT INTO `footers` (`footer_id`, `footer_name`, `footer_content`, `type`, `icon`, `create_by`, `update_by`, `active`, `create_at`, `update_at`) VALUES
(1, 'แผนที่', 'ขอยบางเพ็ง ตำบล คลองข่อย อำเภอ ปากเกร็ด นนทบุรี 11120', 1, 'image/footer1.png', 1, 1, 0, '2019-01-24 23:36:54', '2019-01-24 23:36:54'),
(2, 'ติดต่อเรา', '02 103 3274', 1, 'image/footer2.png', 1, 1, 0, '2019-01-24 23:36:54', '2019-01-24 23:36:54'),
(3, 'อีเมล์', 'ccon-support@c-contrafﬁc.com', 1, 'image/footer3.png', 1, 1, 0, '2019-01-24 23:36:54', '2019-01-24 23:36:54'),
(4, 'Facebook', 'ccontraffic', 1, 'image/footer4.png', 1, 1, 0, '2019-01-24 23:36:54', '2019-01-24 23:36:54'),
(5, 'Line', 'ccontraffic', 1, 'image/footer5.png', 1, 1, 0, '2019-01-24 23:36:54', '2019-01-24 23:36:54'),
(6, '', '02 103 3274', 2, 'image/footer2.png', 1, 1, 0, '2019-01-24 23:36:54', '2019-01-24 23:36:54'),
(7, NULL, 'ccon-support@c-contrafﬁc.com', 2, 'image/footer3.png', 1, 1, 0, '2019-01-24 23:36:54', '2019-01-24 23:36:54'),
(8, NULL, 'ccontrafﬁc', 2, 'image/footer5.png', 1, 1, 0, '2019-01-24 23:36:54', '2019-01-24 23:36:54');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `jobs_id` int(11) NOT NULL,
  `jobs_name` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `jobs_detail` text NOT NULL,
  `jobs_img` varchar(255) NOT NULL,
  `jobs_file` varchar(255) NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`jobs_id`, `jobs_name`, `slug`, `jobs_detail`, `jobs_img`, `jobs_file`, `create_by`, `update_by`, `active`, `create_at`, `update_at`) VALUES
(3, 'ฝ่ายบริหาร', 'ฝ่ายบริหาร', '<p>detail</p>', 'uploads/jobs/2019/01/56d2ef3b7735c311ee0557e539be6b16.png', 'uploads/jobs/2019/01/88714a1b0b46f39d5f7ad9a47a4426ba.pdf', 1, 1, 0, '0000-00-00 00:00:00', '2019-01-27 16:49:21'),
(4, 'พนักงานออฟฟิต', 'พนักงานออฟฟิต', '', 'uploads/jobs/2019/01/a13ccd328c8eb539191139d85354c639.png', '', 1, 1, 0, '2019-01-27 16:50:42', '2019-01-27 16:50:42'),
(5, 'วิศวกร', 'วิศวกร', '', 'uploads/jobs/2019/01/a888941dd1762b3ddfdda6cbf0af4dbf.png', '', 1, 1, 0, '2019-01-27 16:51:01', '2019-01-27 16:51:01'),
(6, 'ช่าง', 'ช่าง', '', 'uploads/jobs/2019/01/316376a4bacf9c08555f13b13149dd0e.png', '', 1, 1, 0, '2019-01-27 16:52:07', '2019-01-27 16:52:07');

-- --------------------------------------------------------

--
-- Table structure for table `jobs_regis`
--

CREATE TABLE `jobs_regis` (
  `jobs_regis_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `position_one` varchar(255) DEFAULT NULL,
  `position_two` varchar(255) DEFAULT NULL,
  `salary_one` int(11) DEFAULT NULL,
  `salary_two` int(11) DEFAULT NULL,
  `present_address` varchar(255) DEFAULT NULL,
  `moo` varchar(255) DEFAULT NULL,
  `road` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `amphur` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `post_code` int(11) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `room` int(11) DEFAULT NULL,
  `date_birth` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `race` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `identity_card_no` varchar(13) DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `military_status` int(11) DEFAULT NULL,
  `maritary_status` int(11) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `office_one` text,
  `experience` text,
  `father_name` varchar(225) DEFAULT NULL,
  `father_age` int(11) DEFAULT NULL,
  `father_occupation` varchar(255) DEFAULT NULL,
  `mother_name` varchar(255) DEFAULT NULL,
  `mother_age` int(11) DEFAULT NULL,
  `mother_occupation` varchar(255) DEFAULT NULL,
  `img` text,
  `appove` int(11) DEFAULT '0',
  `create_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `pic` varchar(255) NOT NULL,
  `detail` longtext NOT NULL,
  `category_id` int(11) NOT NULL,
  `catalog` varchar(255) NOT NULL,
  `seo_id` int(11) DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `pic`, `detail`, `category_id`, `catalog`, `seo_id`, `create_by`, `update_by`, `active`, `create_at`, `update_at`) VALUES
(6, 'เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร', 'เสาเหลี่ยมปลายเรียว-ชุบกัลวาไนซ์-สูง-4-17-เมตร', 'uploads/product/2019/08/93c2dd260b392e8fb1d3e1a432bfcf39.jpg', '<p><b><span style=\"font-size: 24px;\">เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร</span></b><span style=\"font-size: 24px;\"><b><br></b></span></p><p><img src=\"/uploads/content/2019/10/8e04b2a9a2c7bdca5e1e142473e98c01.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><b><br></b></span><img src=\"/uploads/content/2019/10/2b56af969c722890f01d2e5764518d9c.jpg\" style=\"width: 100%;\"><br></p><p><img src=\"/uploads/content/2019/10/6eb3dfa47787f60aa1d85ca3bd124e5c.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><br></p>', 1, 'uploads/product/2019/11/7ce7968068a933d48623aa6936e9102b.pdf', 12, 1, 0, 0, '2019-11-13 03:17:20', '2019-11-13 03:17:20'),
(8, 'เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร', 'เสาเหลี่ยมปลายเรียว-ชุบกัลวาไนซ์-สูง-4-17-เมตร', 'uploads/product/2019/08/a69d2ff033c9ee546259e131b68b5836.jpg', '<p><b><span style=\"font-size: 24px;\">เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร</span></b><span style=\"font-size: 24px;\"><b><br></b></span><img src=\"/uploads/content/2019/10/85a8e8aaa353186c4c357e1565f7ee57.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/bf25c76286b7c8e181bed602a3ed56ce.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/7bd0d94fc8fbdf0cb97a807536527a29.jpg\" style=\"width: 100%;\"><br></p>', 1, 'uploads/product/2019/11/4988a85eeae57807cc89ca8bc3c5bb46.pdf', 14, 1, 0, 0, '2019-11-13 03:24:19', '2019-11-13 03:24:19'),
(9, 'เสาไฟไฮแมสแบบเหลี่ยม ชุบกัลวาไนซ์ สูง 15-40 เมตร', 'เสาไฟไฮแมสแบบเหลี่ยม-ชุบกัลวาไนซ์-สูง-15-40-เมตร', 'uploads/product/2019/08/7db3d954ec32e2b26db50b9580c6847e.jpg', '<p><b><span style=\"font-size: 24px;\">เสาไฟไฮแมสแบบเหลี่ยม ชุบกัลวาไนซ์ สูง 15-40 เมตร</span></b><br></p><p><img src=\"/uploads/content/2019/10/bbac43fe1cc5b6dcf15f75781703fdc9.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/97d114e49735fc05fe06489a8b5ac60b.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><br></p>', 1, 'uploads/product/2019/11/14eaf310fe7a96530303d993b491fc6d.pdf', 15, 1, 0, 0, '2019-11-13 03:28:03', '2019-11-13 03:28:03'),
(10, 'เสาไฟไฮแมสแบบกลม ชุบกัลวาไนซ์ สูง 15-40 เมตร', 'เสาไฟไฮแมสแบบกลม-ชุบกัลวาไนซ์-สูง-15-40-เมตร', 'uploads/product/2019/08/2cbdc4a9ad6473c074fe2a1b03da935a.jpg', '<p><b><span style=\"font-size: 24px;\">เสาไฟไฮแมสแบบกลม ชุบกัลวาไนซ์ สูง 15-40 เมตร</span><span style=\"font-size: 24px;\"><br></span></b><img src=\"/uploads/content/2019/10/7eeb1fc2acdbb8772b3c2669474685b8.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/4bb3b45407e8ad136d054f7abea46ba9.jpg\" style=\"width: 100%;\"><br></p>', 1, 'uploads/product/2019/11/780ff30891d056e345e2d5d3e16abc48.pdf', 16, 1, 0, 0, '2019-11-13 03:26:49', '2019-11-13 03:26:49'),
(11, 'เสาธงแบบกลมปลายเรียว ชุบกัลวาไนซ์  สูง 3-30 เมตร', 'เสาธงแบบกลมปลายเรียว-ชุบกัลวาไนซ์-สูง-3-30-เมตร', 'uploads/product/2019/08/103a85ae04fc6d84451a1b6baaed99af.jpg', '<p><span style=\"font-size: 24px;\"><b>เสาธงแบบกลมปลายเรียว ชุบกัลวาไนซ์ &nbsp;สูง 3-30 เมตร</b></span><br></p><p><img src=\"/uploads/content/2019/10/87f4947a08dd81e5f2a07da7236a4677.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><b><br></b></span><br></p>', 1, 'uploads/product/2019/11/9941343cf8ed42b24a46b2d9f052eea9.pdf', 17, 1, 0, 0, '2019-11-13 03:15:35', '2019-11-13 03:15:35'),
(12, 'เสาสนามกีฬา ชุบกัลวาไนซ์ สูง 15-40 เมตร', 'เสาสนามกีฬา-ชุบกัลวาไนซ์-สูง-15-40-เมตร', 'uploads/product/2019/08/c177f34fd733afc3258b6867b6dfa5e2.jpg', '<p><span style=\"font-size: 24px;\"><b>เสาสนามกีฬา ชุบกัลวาไนซ์ สูง 15-40 เมตร</b></span><img src=\"/uploads/content/2019/08/6feb10eb0dccbfc207c3957c0980e8cb.png\" style=\"width: 100%;\"><br></p>', 1, 'uploads/product/2019/11/863f5dcb20f2f22954d49b2aea777360.pdf', 18, 1, 0, 0, '2019-11-13 03:22:04', '2019-11-13 03:22:04'),
(13, 'เสากลมปลายเรียว ชุบกัลวาไนซ์ สูง 4-14 เมตร', 'เสากลมปลายเรียว-ชุบกัลวาไนซ์-สูง-4-14-เมตร', 'uploads/product/2019/08/3c74e862ea6325a17da186a89d4522e9.jpg', '<p><span style=\"font-size: 24px;\"><b>เสากลมปลายเรียว ชุบกัลวาไนซ์ สูง 4-14 เมตร</b></span></p><p><img src=\"/uploads/content/2019/10/efd37529b563b7731b8132798e8f3d34.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><b><br></b></span><br></p>', 1, 'uploads/product/2019/11/4291f27f779f29a42c0218012e2ca166.pdf', 19, 1, 0, 0, '2019-11-13 03:12:57', '2019-11-13 03:12:57'),
(14, 'เสาเหลี่ยมปลายตรง ชุบกัลวาไนซ์ สูง 4-14 เมตร', 'เสาเหลี่ยมปลายตรง-ชุบกัลวาไนซ์-สูง-4-14-เมตร', 'uploads/product/2019/08/d82fc06fe40db2cb25ac211b5f130356.jpg', '<p><b><span style=\"font-size: 24px;\">เสาเหลี่ยมปลายตรง ชุบกัลวาไนซ์ สูง 4-14 เมตร</span></b><br></p><p><img src=\"/uploads/content/2019/10/a0f93e44919627e8f78197b0b7e28e66.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><b><br></b></span><br></p>', 1, 'uploads/product/2019/11/cd1f828fa418385ffde18878ad5379f3.pdf', 20, 1, 0, 0, '2019-11-13 03:23:00', '2019-11-13 03:23:00'),
(15, 'เสาประติมากรรม', 'เสาประติมากรรม', 'uploads/product/2019/08/4c0e6c85f11831b1c11571efa040c080.jpg', '<p><span style=\"font-size: 24px;\"><b>เสาประติมากรรม</b></span><br></p><p><img src=\"/uploads/content/2019/11/8f4a5ba66a82d838fd97c194fb2ac387.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><b><br></b></span></p><p><img src=\"/uploads/content/2019/11/fe7f0e7f7774c2cbde59c88558c9d266.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><b><br></b></span></p><p><img src=\"/uploads/content/2019/11/1fbc203c8a5e614c6634effdf2ed1921.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><b><br></b></span></p><p><img src=\"/uploads/content/2019/11/e81d8f3e64fb3daa29a0dab138570646.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><b><br></b></span></p><p><img src=\"/uploads/content/2019/11/b37dffad680d160d848bb4221e3af430.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><b><br></b></span></p><p><img src=\"/uploads/content/2019/11/2aba73804bc78ad2c2ae81c43ad54984.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><b><br></b></span></p><p><img src=\"/uploads/content/2019/11/afe280372c1b314dd6c2ff0d1770bfa1.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><b><br></b></span></p><p><img src=\"/uploads/content/2019/11/53c52b889c06d288fee27be77fe561c3.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><b><br></b></span></p>', 1, 'uploads/product/2019/11/82a8a042d8d258f964880f0ed73cffc5.pdf', 21, 1, 0, 0, '2019-11-13 03:19:45', '2019-11-13 03:19:45'),
(16, 'เสาไฟส่องสว่าง โซล่าเซลล์', 'เสาไฟส่องสว่าง-โซล่าเซลล์', 'uploads/product/2019/08/c0b4b741db6641d89ffd64ee7983df4a.jpg', '<p><b><span style=\"font-size: 24px;\">เสาไฟส่องสว่าง โซล่าเซลล์</span></b></p><p><img src=\"/uploads/content/2019/10/342e2fe11ad839940d291ab808a74c12.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><img src=\"/uploads/content/2019/10/3beffc3f812ab3a84283c141245ec645.jpg\" style=\"width: 100%;\"><br></p>', 1, 'uploads/product/2019/11/da6596d7306906af3af9616a569cdf30.pdf', 22, 1, 0, 0, '2019-11-13 03:25:47', '2019-11-13 03:25:47'),
(17, 'ราวกั้นอันตราย (การ์ดเรล)', 'ราวกั้นอันตราย-การ์ดเรล', 'uploads/product/2019/08/49a2efc006100fee2d8f30947c05f7f9.jpg', '<p><b><span style=\"font-size: 24px;\">ราวกั้นอันตราย (การ์ดเรล)</span></b></p><p><img src=\"/uploads/content/2019/10/bf96e544609492bd0565280c2920f9b2.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><img src=\"/uploads/content/2019/10/12a3b03d53aeda547f97d8f46c28c10a.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/ea08d6ab6a91337767d5e3ab4994f718.jpg\" style=\"width: 100%;\"><br></p>', 1, 'uploads/product/2019/11/07e395583731bd8e9ff30017c31706dd.pdf', 23, 1, 0, 0, '2019-11-13 02:57:50', '2019-11-13 02:57:50'),
(18, 'โคมไฟถนน ชนิดหลอดไส้', 'โคมไฟถนน-ชนิดหลอดไส้', 'uploads/product/2019/09/750d5d4486453990bfea1e5532e396be.jpg', '<p><b><span style=\"font-size: 24px;\">โคมไฟถนน ชนิดหลอดไส้</span></b><br></p><p><img src=\"/uploads/content/2019/10/3d60c001dcf0cd82b77c9ed531810bd3.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><img src=\"/uploads/content/2019/10/672f4b595e54ad64313a7757dbe2333a.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/429cd9ca760dce1f8b44cb04ba64e8e2.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/90c2f5e31d25ecdea20d40e9b8d3c90c.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/b067fd51e5c675297d2640c3bd6de6f6.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/d6fc999baa7704987f6592979930c250.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/b6954f130373ee5e680e30643d4d8a6c.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/6042d58f00c4a29a41e18b5c78d0e813.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/51252acd4956d6833f3c35d2f8d8598f.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/f14f74a6b888c06321422a32f2f29463.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/8863295c9ac1028858081561fb2eb11e.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/7f29904490b06e2102fe2cd821ed4b8e.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/21ac71ad37c17b6708ce106a6c2804a8.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/45c3317057d3cb74f286c01401346bcb.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><br></span></p>', 4, 'uploads/product/2019/11/509c9a47f72d94ecca68f041fc01d496.pdf', 24, 1, 0, 0, '2019-11-13 03:38:48', '2019-11-13 03:38:48'),
(19, 'โคมไฟสนาม ชนิดหลอดไส้', 'โคมไฟสนาม-ชนิดหลอดไส้', 'uploads/product/2019/09/691b2a422f218dcdd95dbc9da1c1fffa.jpg', '<p><b><span style=\"font-size: 24px;\">โคมไฟสนาม ชนิดหลอดไส้</span></b><br></p><p><img src=\"/uploads/content/2019/10/c415e80938e0ae9b35c6f1e25fba19d1.jpg\" style=\"width: 100%; float: left;\" class=\"note-float-left\"><b><span style=\"font-size: 24px;\"><br></span></b><img src=\"/uploads/content/2019/10/be5298027aa25689ebc29abc431d9ee2.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/47c468af27e3937fbb3e83a6b50515c3.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/a2f1b400459e1d385464706a5875a0fb.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/64c6053f5236bd32f927aac6b076bd11.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/adca1916181de6db30f4d19a6ad7c9d5.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/58fba936e177b3cb78e964e5555a8599.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/916c28130a8021c1eb8cfe64a0dcab38.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/4bc420ffddf34cac88c3ef9b1fef1389.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/f263cab885a9c5759aef74e11aae52c5.jpg\" style=\"width: 100%;\"><br></p>', 4, 'uploads/product/2019/11/062d83c29a08ef395fdeabe84b956215.pdf', 25, 1, 0, 0, '2019-11-13 03:40:59', '2019-11-13 03:40:59'),
(20, 'โคมไฟถนน ชนิดหลอด LED', 'โคมไฟถนน-ชนิดหลอด-led', 'uploads/product/2019/09/d6f1d638781059bc796417bd13a35377.jpg', '<p><b><span style=\"font-size: 24px;\">โคมไฟถนน ชนิดหลอด LED</span></b><br></p><p><img src=\"/uploads/content/2019/10/f76285ff80934c50d5720b8ddb5737a5.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><img src=\"/uploads/content/2019/10/29987b6dd38bb7a990ef59c8041547c2.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/6fee5a20b63ae73c47281b77de9c9f02.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/9ef13bd353f6e891428296005e2fb678.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/7a8ff8997ee8f064c1611378d4dbf5fc.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/bd94df91146fa6f13fe9b0b8828779fc.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/40c2c7f8198642dba102c8e7c918aa41.jpg\" style=\"width: 100%;\"><br></p>', 4, 'uploads/product/2019/11/28e9c9f7fa6dd9c6957752463fea45bb.pdf', 26, 1, 0, 0, '2019-11-13 03:39:50', '2019-11-13 03:39:50'),
(21, 'โคมไฟสนาม ชนิดหลอด LED', 'โคมไฟสนาม-ชนิดหลอด-led', 'uploads/product/2019/09/c00f7950d5aa830cdca36dd908861e36.jpg', '<p><b><span style=\"font-size: 24px;\">โคมไฟสนาม ชนิดหลอด LED</span></b><br></p><p><img src=\"/uploads/content/2019/10/36ca7f68cc6e06509249f4810519da65.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><img src=\"/uploads/content/2019/10/fc6bfadd47272dd90f422e39ec391071.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/0a5edf3d5c0b3c01b30e3b7a2f0ac6cd.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/6b6e8a792214dca41dd56224d57b2f44.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/42065d16351bee40f023ef6842d4d5c9.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/43705bf8c06fa01716313e02510155c0.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/3f331fe6d2a1e5840f3da9e154d0909f.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/ab08460b098ed4cf0dbe1fb784f414a8.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/0a095249a6b3dc76acef8fa58ce71b43.jpg\" style=\"width: 100%;\"><br></p>', 4, 'uploads/product/2019/11/f47fadcb40ac2a92b8052e809d38ed9f.pdf', 27, 1, 0, 0, '2019-11-13 03:42:02', '2019-11-13 03:42:02'),
(22, 'ตู้ควบคุมสัญญาณไฟจราจร', 'ตู้ควบคุมสัญญาณไฟจราจร', 'uploads/product/2019/10/06a067b78bea2d838db1060bfae382f2.jpg', '<p><span style=\"font-size: 24px;\"><b>ตู้ควบคุมสัญญาณไฟจราจร</b></span></p><p><img src=\"/uploads/content/2019/10/7e4f147dd824e406422baf52f6d5b3f4.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><b><br></b></span><img src=\"/uploads/content/2019/10/65852d74767e2ddb38b5e4fa5d49cc8d.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/5c5b2d3809e8fd2e09cc9e057cdb782c.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/726b21018e8deced979f26d7983956c8.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/ce4de081612f49601340b92333a39280.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/bb39321d22266cf8d35ac207ac0258ed.jpg\" style=\"width: 100%;\"><br></p>', 5, 'uploads/product/2019/11/83a9e9c31266e03adb3cf43f70f99ddc.pdf', 28, 1, 0, 0, '2019-11-13 02:49:00', '2019-11-13 02:49:00'),
(23, 'โคมไฟจราจร', 'โคมไฟจราจร', 'uploads/product/2019/09/c1f0284e99485e8ef24108c6e00a9ac0.jpg', '<p><span style=\"font-size: 24px;\"><b>โคมไฟจราจร</b></span><img src=\"/uploads/content/2019/09/ea25dbe42d9c4ed60d50a886e06dc0d8.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/aad152aff3b451cdc37ce00d1cc8e7c9.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/0f01977b6b4be2ed81518f1b2ffe1e49.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/1876d05268659036a03b05ecfc39b24d.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/8745252eccdb59dadfbd7e05016c970c.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/090971910cd007d7ee4bdf372a707aa3.png\" style=\"width: 100%;\"><br></p>', 5, 'uploads/product/2019/11/3ac0a6798a3f75d13dce03ba7edb5dd7.pdf', 29, 1, 0, 0, '2019-11-13 03:34:29', '2019-11-13 03:34:29'),
(24, 'โคมไฟจราจรนับเวลาถอยหลัง', 'โคมไฟจราจรนับเวลาถอยหลัง', 'uploads/product/2019/10/9ef74e828f8d7b4e561d3adb6ef9dc82.jpg', '<p><b><span style=\"font-size: 24px;\">โคมไฟจราจรนับเวลาถอยหลัง</span></b></p><p><img src=\"/uploads/content/2019/10/54f73d1b0dd50e8f65d9f13ced18f82b.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><br></p>', 5, 'uploads/product/2019/11/c6630765ddd13518b09e4f2ce5134d44.pdf', 30, 1, 0, 0, '2019-11-13 03:35:04', '2019-11-13 03:35:04'),
(25, 'โคมไฟจราจรแบ่งช่องทางเดินรถ', 'โคมไฟจราจรแบ่งช่องทางเดินรถ', 'uploads/product/2019/10/6709977898f2b5026fb7284b1be0a03a.jpg', '<p><b><span style=\"font-size: 24px;\">﻿</span><span style=\"font-size: 24px;\">โคมไฟจราจรแบบมีช่องทางเดินรถ</span></b><br></p><p><img src=\"/uploads/content/2019/10/66ee1838c6dc906733c2a39f27b9e536.jpg\" style=\"width: 100%;\"><br></p>', 5, 'uploads/product/2019/11/9801f8d1d609fbb92ae29fb56094840d.pdf', 31, 1, 0, 0, '2019-11-13 03:36:49', '2019-11-13 03:36:49'),
(27, 'ป้ายไฟเตือนทางเบี่ยงโซล่าเซลล์', 'ป้ายไฟเตือนทางเบี่ยงโซล่าเซลล์', 'uploads/product/2019/10/cb865148eb4bff8094cde9c6f8224002.jpg', '<p><span style=\"font-size: 24px;\">﻿</span><span style=\"font-size: 24px;\"><b>ป้ายไฟเตือนทางเบี่ยงโซล่าเซลล์</b></span><img src=\"/uploads/content/2019/10/00971882bf4edfe7610aafd8a65f647a.jpg\" style=\"font-size: 14px; width: 100%;\"><br></p>', 6, 'uploads/product/2019/11/c526416d529283946fdeca8788ee0c91.pdf', 33, 1, 0, 0, '2019-11-13 03:49:47', '2019-11-13 03:49:47'),
(28, 'ป้ายไฟเตือนทางแยกโซล่าเซลล์', 'ป้ายไฟเตือนทางแยกโซล่าเซลล์', 'uploads/product/2019/10/63d515755cc1ddda627c53c2607b5e0d.jpg', '<p><b><span style=\"font-size: 24px;\">ป้ายไฟเตือนทางแยกโซล่าเซลล์</span></b><br></p><p><img src=\"/uploads/content/2019/10/b2fbd3d3d2d969aeaaf20b080eb056dd.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><br></p><p><br></p>', 6, 'uploads/product/2019/11/444b6d7938bcb8af8f248ee603b425ec.pdf', 34, 1, 0, 0, '2019-11-13 03:53:28', '2019-11-13 03:53:28'),
(29, 'ป้ายไฟเตือนเขตโรงเรียนโซล่าเซลล์', 'ป้ายไฟเตือนเขตโรงเรียนโซล่าเซลล์', 'uploads/product/2019/10/bacb6976bce2df02279f03b7dafc8bd1.jpg', '<p><b><span style=\"font-size: 24px;\">ป้ายไฟเตือนเขตโรงเรียนโซล่าเซลล์</span></b><br></p><p><img src=\"/uploads/content/2019/10/a5ca085bd485006a35d850a1d8c0dc09.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><br></p>', 6, 'uploads/product/2019/11/373f443bc839e0ac6b90ba8a40b4e7d2.pdf', 35, 1, 0, 0, '2019-11-13 03:44:50', '2019-11-13 03:44:50'),
(30, 'ป้ายไฟเตือนเกาะกลางโซล่าเซลล์', 'ป้ายไฟเตือนเกาะกลางโซล่าเซลล์', 'uploads/product/2019/10/c46787ec657ba03514d036282351f1bd.jpg', '<p><b><span style=\"font-size: 24px;\">ป้ายไฟเตือนเกาะกลางโซล่าเซลล์</span></b><br></p><p><img src=\"/uploads/content/2019/10/c1b34138579cd38ed9f62acbb4887d14.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><br></p>', 6, 'uploads/product/2019/11/b94317011b1fcce8559dbaf0384ecaad.pdf', 36, 1, 0, 0, '2019-11-13 03:50:15', '2019-11-13 03:50:15'),
(31, 'ป้ายไฟเตือนลดความเร็วโซล่าเซลล์', 'ป้ายไฟเตือนลดความเร็วโซล่าเซลล์', 'uploads/product/2019/10/81c1de02e536582e969ff869bf72073c.jpg', '<p><b><span style=\"font-size: 24px;\">ป้ายไฟเตือนลดความเร็วโซล่าเซลล์</span></b><br></p><p><img src=\"/uploads/content/2019/10/5ab0177f91ec5ef776bca85bacaac043.jpg\" style=\"width: 100%;\"><br></p>', 6, 'uploads/product/2019/11/c0557234220b3ff6931952f0f8cf2164.pdf', 37, 1, 0, 0, '2019-11-13 03:44:03', '2019-11-13 03:44:03'),
(32, 'ป้ายไฟเตือนกระพริบโซล่าเซลล์', 'ป้ายไฟเตือนกระพริบโซล่าเซลล์', 'uploads/product/2019/10/832751dfbcefbe4d38c247504729e7ed.jpg', '<p><b><span style=\"font-size: 24px;\">ป้ายไฟเตือนกระพริบโซล่าเซลล์</span></b><br></p><p><img src=\"/uploads/content/2019/10/c928c7307864d8647c6e098af3d8ebaf.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><br></p>', 6, 'uploads/product/2019/11/1088bad5f6d5d55f288934f39686c308.pdf', 38, 1, 0, 0, '2019-11-13 03:46:55', '2019-11-13 03:46:55'),
(33, 'ป้ายไฟเตือนรถบรรทุกเข้า-ออกโซล่าเซลล์', 'ป้ายไฟเตือนรถบรรทุกเข้า-ออกโซล่าเซลล์', 'uploads/product/2019/10/0fbf5840303a80aea15bf57881d7426c.jpg', '<p><b><span style=\"font-size: 24px;\">ป้ายไฟเตือนรถบรรทุกเข้า-ออกโซล่าเซลล์</span></b><br></p><p><img src=\"/uploads/content/2019/10/7d4321d53ba6acd3c05a7525dc30afb6.jpg\" style=\"width: 100%; float: left;\" class=\"note-float-left\"><b><span style=\"font-size: 24px;\"><br></span></b><br></p>', 6, 'uploads/product/2019/11/305d74923d588f92a186beaff646d370.pdf', 39, 1, 0, 0, '2019-11-13 03:51:29', '2019-11-13 03:51:29'),
(34, 'ป้ายไฟเตือนจราจร CCTV โซล่าเซลล์', 'ป้ายไฟเตือนจราจร-cctv-โซล่าเซลล์', 'uploads/product/2019/10/05706abf28ad1156bd4258ba2665996a.jpg', '<p><b><span style=\"font-size: 24px;\">ป้ายไฟเตือนจราจร CCTV โซล่าเซลล์</span></b><br></p><p><img src=\"/uploads/content/2019/10/dc31e2580cbe2c7889ef455614a4f542.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><br></p>', 6, 'uploads/product/2019/11/645022c066541b142b0d0d6add8a4d17.pdf', 40, 1, 0, 0, '2019-11-13 03:52:39', '2019-11-13 03:52:39'),
(35, 'ป้ายบังคับจราจร', 'ป้ายบังคับจราจร', 'uploads/product/2019/10/09c51d0e3e9655a7c9487e87568fc823.jpg', '<p><b><span style=\"font-size: 24px;\">ป้ายบังคับจราจร</span></b></p><p><img src=\"/uploads/content/2019/10/2e15aca377c82a818da27b7bb13ea530.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><img src=\"/uploads/content/2019/10/2a26bbb67388431993ee980c30db9734.jpg\" style=\"width: 100%;\"><br></p>', 7, 'uploads/product/2019/11/e0afae9c4ed34c5e158427f69c649f90.pdf', 41, 1, 0, 0, '2019-11-13 02:54:27', '2019-11-13 02:54:27'),
(36, 'ป้ายแนะนำ', 'ป้ายแนะนำ', 'uploads/product/2019/10/27e2e70a1f49289a66b6939d9e0d4023.jpg', '<p><b><span style=\"font-size: 24px;\">ป้ายแนะนำ</span></b></p><p><img src=\"/uploads/content/2019/10/36c0eff14efdfc96b10d95a9e237a53d.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><img src=\"/uploads/content/2019/10/5838fc1aed2ae1c06c5099c56a03f488.jpg\" style=\"width: 100%;\"><br></p>', 7, 'uploads/product/2019/11/9af7692c440acaad6b2a59da6a60e4d6.pdf', 42, 1, 0, 0, '2019-11-13 02:56:13', '2019-11-13 02:56:13'),
(37, 'ป้ายเตือนจราจร', 'ป้ายเตือนจราจร', 'uploads/product/2019/10/9e55dda0cef058e59843a8c3146139c3.jpg', '<p><b><span style=\"font-size: 24px;\">﻿</span><span style=\"font-size: 24px;\">ป้ายเตือนจราจร</span></b></p><p><img src=\"/uploads/content/2019/10/02c7714d2a03ab89a50b0d4df43d6f4e.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><img src=\"/uploads/content/2019/10/ea8a6c2fe59680b5ef195d57857caa7e.jpg\" style=\"width: 100%;\"><br></p>', 7, 'uploads/product/2019/11/524784f52d4ff09e3b2a77b08757e656.pdf', 43, 1, 0, 0, '2019-11-13 02:55:08', '2019-11-13 02:55:08'),
(38, 'ป้าย Overhang', 'ป้าย-overhang', 'uploads/product/2019/10/d1b037a6c491750daf802b281ead3de4.jpg', '<p><b><span style=\"font-size: 24px;\">ป้าย Overhang</span></b><span style=\"font-size: 24px;\">﻿</span></p><p><img src=\"/uploads/content/2019/10/3ae17e133d817352d9f705596f110e15.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><br></span><br></p>', 7, 'uploads/product/2019/11/d6d83203dd8aa7d88568235280d04466.pdf', 44, 1, 0, 0, '2019-11-13 02:49:42', '2019-11-13 02:49:42'),
(39, 'ป้ายซอยประติมากรรม', 'ป้ายซอยประติมากรรม', 'uploads/product/2019/10/f44e9cdc067446172068b3c2998b64d1.jpg', '<p><b><span style=\"font-size: 24px;\">ป้ายซอยประติมากรรม</span></b></p><p><img src=\"/uploads/content/2019/10/c25950f7145c4b1f718e130f0d1d8f52.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><img src=\"/uploads/content/2019/10/145d8f52b26415212c07eec06ebb902f.jpg\" style=\"width: 100%;\"><br></p>', 7, 'uploads/product/2019/11/a3d5629aee5e75f7b63f4356c1f9d91e.pdf', 45, 1, 0, 0, '2019-11-13 02:53:58', '2019-11-13 02:53:58'),
(40, 'ป้าย Overhead จราจร', 'ป้าย-overhead-จราจร', 'uploads/product/2019/10/c5d4fa82cd878a7de7028933c5327aa9.jpg', '<p><b><span style=\"font-size: 24px;\">ป้าย Overhead จราจร</span></b></p><p><img src=\"/uploads/content/2019/10/b7b9e6a72deda7d4c14e6e2fa7b7e6aa.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><br></p>', 7, 'uploads/product/2019/11/37ac8e4d71a0b40ae82aebff80c70b7d.pdf', 46, 1, 0, 0, '2019-11-13 02:50:10', '2019-11-13 02:50:10'),
(41, 'หมุดจราจร', 'หมุดจราจร', 'uploads/product/2019/09/d0f78f9c3efd97a1cd63973dd452bde4.jpg', '<p><b><span style=\"font-size: 24px;\">หมุดจราจร</span></b><img src=\"/uploads/content/2019/09/7de2c591375de58b8ca13f6865f4ebc6.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/7d86347e086ec2cdd96b3ae1728d491f.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/a4e63c57333365ad8ade36904e6e3f23.png\" style=\"width: 100%;\"><br></p>', 8, 'uploads/product/2019/09/168b73e2c6e762909c66fb661a5dd47f.pdf', 47, 1, 0, 0, '2019-11-13 03:03:32', '2019-11-13 03:03:32'),
(42, 'หมุดแก้ว 360 องศา', 'หมุดแก้ว-360-องศา', 'uploads/product/2019/10/1d2a4beb3d13b4688ea6871af4130cb0.jpg', '<p><b><span style=\"font-size: 24px;\">หมุดแก้ว 360 องศา</span></b></p><p><img src=\"/uploads/content/2019/10/89b2aa623fa04c6b442dde99590f880d.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><img src=\"/uploads/content/2019/10/b5f3708506d838d59cc188d8e63d12a8.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/be7df2244d277028bbbd6bb662ea43bc.jpg\" style=\"width: 100%;\"><br></p>', 8, 'uploads/product/2019/11/b5002f61b71496e1f43211a2eef5105b.pdf', 48, 1, 0, 0, '2019-11-13 03:05:45', '2019-11-13 03:05:45'),
(43, 'กรวยจราจร', 'กรวยจราจร', 'uploads/product/2019/10/36ad2822d30b1923282b47304001e9cd.jpg', '<p><b><span style=\"font-size: 24px;\">กรวยจราจร</span></b></p><p><img src=\"/uploads/content/2019/10/863bc29c572dc05760ca122d8e0d81a4.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><br></p>', 8, 'uploads/product/2019/11/c31e692b95aad794fd0d42619deabd28.pdf', 49, 1, 0, 0, '2019-11-13 02:43:58', '2019-11-13 02:43:58'),
(44, 'เสื้อจราจร', 'เสื้อจราจร', 'uploads/product/2019/10/44a88403b0e28ba0b0b3f7ead6a41a75.jpg', '<p><b><span style=\"font-size: 24px;\">เสื้อจราจร</span></b><span style=\"font-size: 24px;\">﻿</span></p><p><img src=\"/uploads/content/2019/10/52e1079cf931488b40dbcf9448c93905.jpg\" style=\"width: 100%;\"><span style=\"font-size: 24px;\"><br></span><img src=\"/uploads/content/2019/10/9f086c615ae3ed2cd53b89426597b60b.jpg\" style=\"width: 100%;\"><br></p>', 8, 'uploads/product/2019/11/27089f39b5bc1f871ac8f9d1db9b5532.pdf', 50, 1, 0, 0, '2019-11-13 03:28:39', '2019-11-13 03:28:39'),
(45, 'อุปกรณ์ความปลอดภัยจราจร', 'อุปกรณ์ความปลอดภัยจราจร', 'uploads/product/2019/09/12e7f2db398009fbc557d170b9c85bef.jpg', '<p><b><span style=\"font-size: 24px;\">อุปกรณ์ความปลอดภัยจราจร</span></b><img src=\"/uploads/content/2019/09/62296235b547cad78a9be33973886aad.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/fc94f4b5c8165b7e160a2e6542b0b79e.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/b45082eb9e318399573b79d55c7f1724.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/aa60a504bbd8268031e7469af6647770.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/1f76c555ab3e38ec7a2a6ec9f987d13c.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/d01131384d1e15e980bb1c5544211717.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/9e1d7b8ede07e4ac35fcb5a5d67d6512.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/489a9861d4d92da5e31d9c9e29e16105.png\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/09/588b85dac7cc8b5be8a69b4483defd1a.png\" style=\"width: 100%;\"><br></p>', 8, 'uploads/product/2019/09/41fbf9894a9ed9caf706e9ce9b403569.pdf', 51, 1, 1, 0, '2019-09-01 02:27:46', '2019-09-01 02:27:46'),
(46, 'กล้อง CCTV', 'กล้อง-cctv', 'uploads/product/2019/10/a5df1717aa3081be133e64a165b0061a.jpg', '<p><b><span style=\"font-size: 24px;\">กล้อง CCTV</span></b></p><p><img src=\"/uploads/content/2019/10/6c0ff0639f405384199171f28fa2ed74.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><br></p>', 8, 'uploads/product/2019/11/b35fded9a991a8c0798884690eee7b89.pdf', 52, 1, 0, 0, '2019-11-13 02:45:45', '2019-11-13 02:45:45'),
(47, 'ป้ายจอ LED VMS,SMS,MMS', 'ป้ายจอ-led-vms-sms-mms', 'uploads/product/2019/10/ccdbbdeeb502533a68ccfd4d405ab706.jpg', '<p><b><span style=\"font-size: 24px;\">ป้ายจอ LED VMS,SMS,MMS</span></b></p><p><img src=\"/uploads/content/2019/10/99cc8af29b808144444c4d3974a62131.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 24px;\"><br></span></b><img src=\"/uploads/content/2019/10/42caebead3a28b964d8e22c463802ca5.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/a5171a66e2e12d77bcfd0c644bfb37d0.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/0b1117bc51ed3098ab1ff47de1219ff6.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/4da203df9182954ea16da87d36509601.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/e4a16cbd94e5ea84194c3eed357e6f14.jpg\" style=\"width: 100%;\"><br></p>', 8, 'uploads/product/2019/11/a50a5cfad800a33a642d14a2e77070c8.pdf', 53, 1, 0, 0, '2019-11-13 02:53:18', '2019-11-13 02:53:18'),
(48, 'โคมไฟจราจรแบบ3ดวงโคม', 'โคมไฟจราจรแบบ3ดวงโคม', 'uploads/product/2019/10/28be43eae4989ccba3cd3d95356c58a8.jpg', '<p><b><span style=\"font-size: 22px;\">โคมไฟจราจรแบบ3ดวงโคม</span><br></b></p><p><img src=\"/uploads/content/2019/10/cbb2b68a347c48802545842935fa0836.jpg\" style=\"width: 100%;\"><br></p>', 5, 'uploads/product/2019/10/cd249baf4f5c200ba1933d2d1ef3460b.pdf', 54, 1, 0, 0, '2019-11-13 03:31:43', '2019-11-13 03:31:43'),
(49, 'โคมไฟจราจรแบบ 4/6 ดวงโคม', 'โคมไฟจราจรแบบ-4-6-ดวงโคม', 'uploads/product/2019/10/8882f006887ded9e20a539081d0d847d.jpg', '<p><b><span style=\"font-size: 24px;\">โคมไฟจราจรแบบ 4/6 ดวงโคม</span><br></b></p><p><img src=\"/uploads/content/2019/10/8111fe2a8a031939c30a6b7d05a12232.jpg\" style=\"width: 100%;\"><br></p>', 5, 'uploads/product/2019/11/57d84d90a025a4407eace27e17e3b4f6.pdf', 55, 1, 0, 0, '2019-11-13 03:35:59', '2019-11-13 03:35:59'),
(50, 'หมุดอลูมิเนียม', 'หมุดอลูมิเนียม', 'uploads/product/2019/10/b05fcfd062371484d707131e27e3c874.jpg', '<p><b><span style=\"font-size: 22px;\">หมุดอลูมิเนียม</span></b></p><p><img src=\"/uploads/content/2019/10/dbd50ed110f4e42a7f9b9e7cdaa32227.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/3b8c9d83ff521dd2cf1a1390d6e4b479.jpg\" style=\"width: 100%;\"><br></p>', 8, 'uploads/product/2019/11/93f5752c4c34555f07906819d475c15d.pdf', 56, 1, 0, 0, '2019-11-13 03:04:28', '2019-11-13 03:04:28'),
(51, 'เป้าสะท้อนแสง', 'เป้าสะท้อนแสง', 'uploads/product/2019/10/33e10302435d38468142096337ea49a3.jpg', '<p><b><span style=\"font-size: 22px;\">เป้าสะท้อนแสง</span></b></p><p><img src=\"/uploads/content/2019/10/0cd115039cdd779ce562064197c448b3.jpg\" style=\"width: 100%;\"><img src=\"/uploads/content/2019/10/a65d82fefd5e0e463094160dfdc2e2bb.jpg\" style=\"width: 100%;\"><br></p>', 8, 'uploads/product/2019/11/4a9ac7021a1f019dacb1d65a702dd38f.pdf', 57, 1, 0, 0, '2019-11-13 03:09:32', '2019-11-13 03:09:32'),
(52, 'กำแพงน้ำพลาสติก แบริเออร์น้ำ หรือแผงกั้นชนิดบรรจุน้ำ', 'กำแพงน้ำพลาสติก-แบริเออร์น้ำ-หรือแผงกั้นชนิดบรรจุน้ำ', 'uploads/product/2019/10/fc93214ebed4cb1711bc355d5cc7f760.jpg', '<p><b><span style=\"font-size: 22px;\">กำแพงน้ำพลาสติก แบริเออร์น้ำ หรือแผงกั้นชนิดบรรจุน้ำ</span></b><br></p><p><img src=\"/uploads/content/2019/10/ad3463128f66d84afb39ad611a1c2aab.jpg\" style=\"width: 100%;\"></p><p><b><span style=\"font-size: 22px;\"><br></span></b><img src=\"/uploads/content/2019/10/e25d10df8c394c14998216fc322a3f36.jpg\" style=\"width: 100%;\"><br></p>', 8, 'uploads/product/2019/11/a4e7f8a8176fc38f501cb7a5e04032e8.pdf', 58, 1, 0, 0, '2019-11-13 02:46:38', '2019-11-13 02:46:38'),
(53, 'แผงกั้นจราจร', 'แผงกั้นจราจร', 'uploads/product/2019/10/4c91f46e892fa10a0f63320f914c46f1.jpg', '<p><b><span style=\"font-size: 22px;\">แผงกั้นจราจร</span></b></p><p><img src=\"/uploads/content/2019/10/36df7e9ed5ea2f1971c29afc6b9a3409.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 22px;\"><br></span></b><img src=\"/uploads/content/2019/10/20dac19ee8ec8e9f679f0d05933eeda8.jpg\" style=\"width: 100%;\"><br></p>', 8, 'uploads/product/2019/11/4457488b35936bc29e659419c09e13a5.pdf', 59, 1, 0, 0, '2019-11-13 03:29:31', '2019-11-13 03:29:31'),
(54, 'เสาล้มลุก', 'เสาล้มลุก', 'uploads/product/2019/10/27f1066550e2fe0362b311d45c3fea19.jpg', '<p><b><span style=\"font-size: 22px;\">เสาล้มลุก</span></b></p><p><img src=\"/uploads/content/2019/10/45fc254fa355e5ab29404b20fed73164.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 22px;\"><br></span></b><br></p>', 8, 'uploads/product/2019/11/25c8e157ebbdd9c3e705824c45edaa6b.pdf', 60, 1, 0, 0, '2019-11-13 03:20:22', '2019-11-13 03:20:22'),
(55, 'ยางชะลอความเร็ว', 'ยางชะลอความเร็ว', 'uploads/product/2019/10/e02294527b9deb64225c5e95381ed6ae.jpg', '<p><b><span style=\"font-size: 22px;\">ยางชะลอความเร็ว</span></b></p><p><img src=\"/uploads/content/2019/10/08a8ab2e2353dceb798fdf5bbdc0d460.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 22px;\"><br></span></b><br></p>', 8, 'uploads/product/2019/11/22e3849ba950cc1cd1e0aff84388a56e.pdf', 61, 1, 0, 0, '2019-11-13 02:57:07', '2019-11-13 02:57:07'),
(56, 'กระจกโค้งจราจร', 'กระจกโค้งจราจร', 'uploads/product/2019/10/e1e5efc7422d0b3e3279f0897fb87d34.jpg', '<p><b><span style=\"font-size: 22px;\">กระจกโค้งจราจร</span></b></p><p><img src=\"/uploads/content/2019/10/126c06fdd80e46c4d0982b8b39ed0a6e.jpg\" style=\"width: 100%;\"><b><span style=\"font-size: 22px;\"><br></span></b><br></p>', 8, 'uploads/product/2019/11/14e779839f38a76a3fc49ee7368ee6e8.pdf', 62, 1, 0, 0, '2019-11-13 02:45:01', '2019-11-13 02:45:01');

-- --------------------------------------------------------

--
-- Table structure for table `quotations`
--

CREATE TABLE `quotations` (
  `quotations_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `company_address` text,
  `taxpayer_number` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tel` varchar(13) DEFAULT NULL,
  `product_detail` text,
  `send` int(11) DEFAULT '0',
  `create_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quotations`
--

INSERT INTO `quotations` (`quotations_id`, `name`, `position`, `company`, `company_address`, `taxpayer_number`, `email`, `tel`, `product_detail`, `send`, `create_by`, `update_by`, `active`, `create_at`, `update_at`) VALUES
(1, 'รุสลี สีบู้', 'โปรแกรมเมอร์', 'IT', 'รามคำแหง 53', '12233243244', 'Test@gmail.com', '0867867867', 'หลอกไฟฟ้า', 1, 1, 1, 0, '0000-00-00 00:00:00', '2019-01-31 03:43:08'),
(4, 'Test', 'Test', '12 ', 'sdasdasd', '1212131313', 'lee.yam_1214@gmail.com', '9809808908', 'fdfgdfgdfg', 0, 0, 0, 0, '2019-09-07 23:59:43', '2019-09-07 23:59:43'),
(5, 'นายสาโรช วรดี', 'support', 'TEST GROUP', '13', '11111111111111111', 'saroch.wora@gmail.com', '097873827939', 'เสาไฟ', 0, 0, 0, 0, '2019-09-08 00:07:46', '2019-09-08 00:07:46'),
(6, 'Ruslee Seebu', 'test', 'test', 'test', '342342423', 'lee.yam_1214@gmail.com', '078678678678', 'sdasdasdad', 0, 0, 0, 0, '2019-09-08 00:11:17', '2019-09-08 00:11:17'),
(7, 'นายสาโรช วรดี', 'support', 'TEST GROUP', '13', '11111111111111111', 'saroch.wora@gmail.com', '097873827939', 'เสาไฟ', 0, 0, 0, 0, '2019-09-08 00:12:41', '2019-09-08 00:12:41'),
(8, 'ton woradee', 'support', 'test group', '12', '2222222222', 'saroch.wora@gmail.com', '0894724830', 'เสาไฟ', 0, 0, 0, 0, '2019-09-08 00:13:37', '2019-09-08 00:13:37'),
(9, 'Ruslee Seebu', '23 test', 'test', 'test', '0768678768', 'lee.yam_1214@hotmail.com', '9789789789', 'asdasdasd', 0, 0, 0, 0, '2019-09-08 00:13:56', '2019-09-08 00:13:56'),
(10, 'aaaa', 'aaa', 'aaa', 'aa', '2222222222222', 'saroch.wora@gmail.com', '32049024', 'dspld', 1, 1, 1, 0, '0000-00-00 00:00:00', '2019-10-17 15:42:30'),
(11, 'www', 'ww', 'ww', 'ww', '122222222222', 'saroch.wora@gmail.com', '1230920323', 'd', 0, 0, 0, 0, '2019-09-08 00:33:46', '2019-09-08 00:33:46'),
(12, 'ต้น วรดี', 'พนักงาน', 'test ', 'test', '11111111111', 'saroch.wora@gmail.com', '30293082938', 'เสาไฟเตือน', 1, 1, 1, 0, '0000-00-00 00:00:00', '2019-10-16 00:12:10'),
(13, 'ต้น ', 'ไอที', 'test', '13 ', '44444444444444444', 'saroch.wora@gmail.com', '0839289383', 'ddddd', 0, 0, 0, 0, '2019-10-16 22:14:27', '2019-10-16 22:14:27'),
(14, 'กกก', 'กกก', 'กกก', 'กกก', '31212121212', 'saroch.wora@gmail.com', '092049023', 'dd', 0, 0, 0, 0, '2019-10-16 22:29:26', '2019-10-16 22:29:26'),
(15, 'กก', 'กก', 'กก', 'กก', '2121212121', 'saroch.wora@gmail.com', '32302930920', 'dwdw', 0, 0, 0, 0, '2019-10-16 23:24:49', '2019-10-16 23:24:49'),
(16, 'ddd', 'dd', 'dd', 'd', '22222222', 'ww@gmail.com', '134123123', 'ww', 0, 0, 0, 0, '2019-10-16 23:27:56', '2019-10-16 23:27:56');

-- --------------------------------------------------------

--
-- Table structure for table `regis_config`
--

CREATE TABLE `regis_config` (
  `regis_config_id` int(11) NOT NULL,
  `regis_config_name` varchar(255) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `active` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `regis_config`
--

INSERT INTO `regis_config` (`regis_config_id`, `regis_config_name`, `type`, `active`) VALUES
(1, 'อาศัยกับครอบครัว', 1, 0),
(2, 'บ้านตัวเอง', 1, 0),
(3, 'บ้านเช่า', 1, 0),
(4, 'หอพัก', 1, 0),
(5, 'ได้รับการยกเว้น', 2, 0),
(6, 'ปลดเป็นทหารกองหนุน', 2, 0),
(7, 'ยังไม่ได้รับการเกณฑ์', 2, 0),
(8, 'โสด', 3, 0),
(9, 'แต่งงาน', 3, 0),
(10, 'หม้าย', 3, 0),
(11, 'แยกกัน', 3, 0),
(12, 'ชาย', 4, 0),
(13, 'หญิง', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` int(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `roles_system` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `roles_system`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'ผู้ดูแลระบบ', '1001,1002,1003,1004,1005,1006,1007,1015,1016,1008,1009,1010,1011,1012,1013,1014', 1, 1, '2019-10-27 12:48:27', '2019-10-27 12:48:27'),
(2, 0, 'เจ้าหน้าที่ปรับปรุงข้อมูล', '1001,1002,1003,1005,1010,1011', 1, 1, '2019-10-27 16:16:45', '2019-10-27 16:16:45'),
(3, 0, 'job', '1013,1014', 1, 1, '2019-11-12 13:00:50', '2019-11-12 13:00:50');

-- --------------------------------------------------------

--
-- Table structure for table `seo`
--

CREATE TABLE `seo` (
  `id` int(11) NOT NULL,
  `name` text,
  `slug` varchar(255) DEFAULT NULL,
  `robots` text,
  `description` text,
  `keywords` text,
  `create_by` int(11) DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `active` int(11) DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seo`
--

INSERT INTO `seo` (`id`, `name`, `slug`, `robots`, `description`, `keywords`, `create_by`, `update_by`, `type`, `active`, `create_at`, `update_at`) VALUES
(1, 'C-CON SYSTEM TECHNOLOGY', 'c-con', 'C-CON SYSTEM TECHNOLOGY', 'โรงงานผู้ผลิต ค้าส่งสินค้าเทคโนโลยี เพื่อความปลอดภัยบนท้องถนน ไฟจราจร  ไฟส่องสว่าง ป้ายจราจร  ราวกันอันตราย (Guard Rail)  และอุปกรณ์จราจร  ตามมาตรฐาน\r\nนวัตกรรมประหยัดพลังงานโซล่าเซลส์  มุ่งเน้นผลิตสินค้าคุณภาพเยี่ยม ภายใต้ตราสินค้า  C-Con', 'c-con', 1, 1, 0, NULL, '2019-01-30 19:53:18', '2019-01-30 19:53:18'),
(2, 'หมวดหมู่/ประเภท', NULL, 'หมวดหมู่/ประเภท', 'หมวดหมู่/ประเภท', 'หมวดหมู่/ประเภท', 1, 1, 1, 0, '2019-01-30 21:20:44', '2019-01-30 21:20:44'),
(4, 'กลุ่มงานเสาประเภทต่างๆและงานการ์ดเรล', NULL, 'กลุ่มงานเสาประเภทต่างๆและงานการ์ดเรล', 'กลุ่มงานเสาประเภทต่างๆและงานการ์ดเรล', 'กลุ่มงานเสาประเภทต่างๆและงานการ์ดเรล', 1, 1, 1, 0, '2019-10-10 23:17:17', '2019-10-10 23:17:17'),
(5, 'กลุ่มโคมไฟส่องสว่าง', NULL, 'กลุ่มโคมไฟส่องสว่าง', 'กลุ่มโคมไฟส่องสว่าง', 'กลุ่มโคมไฟส่องสว่าง', 1, 1, 1, 0, '2019-10-10 23:18:07', '2019-10-10 23:18:07'),
(6, 'ไฟจราจร', NULL, 'ไฟจราจร', 'ไฟจราจร', 'ไฟจราจร', 1, 1, 1, 0, '2019-10-17 15:39:52', '2019-10-17 15:39:52'),
(7, 'ไฟเตือนจราจรโซล่าเซลล์', NULL, 'ไฟเตือนจราจรโซล่าเซลล์', 'ไฟเตือนจราจรโซล่าเซลล์', 'ไฟเตือนจราจรโซล่าเซลล์', 1, 1, 1, 0, '2019-10-17 15:40:42', '2019-10-17 15:40:42'),
(8, 'ป้ายจราจร', NULL, 'ป้ายจราจร', 'ป้ายจราจร', 'ป้ายจราจร', 1, 1, 1, 0, '2019-10-17 15:40:02', '2019-10-17 15:40:02'),
(9, 'อุปกรณ์ความปลอดภัยจราจร', NULL, 'อุปกรณ์ความปลอดภัยจราจร', 'อุปกรณ์ความปลอดภัยจราจร', 'อุปกรณ์ความปลอดภัยจราจร', 1, 1, 1, 0, '2019-10-17 15:41:14', '2019-10-17 15:41:14'),
(10, 'เสาไฟส่องสว่างแบบกลมปลายเรียว 4-16 เมตร ชุปกัลป์วาไนซ์', NULL, 'เสาไฟส่องสว่างแบบกลมปลายเรียว 4-16 เมตร ชุปกัลป์วาไนซ์', 'เสาไฟส่องสว่างแบบกลมปลายเรียว 4-16 เมตร ชุปกัลป์วาไนซ์', 'เสาไฟส่องสว่างแบบกลมปลายเรียว 4-16 เมตร ชุปกัลป์วาไนซ์', 1, 1, 1, 0, '2019-01-30 21:16:43', '2019-01-30 21:16:43'),
(11, 'อุปกรณ์ความปลอดภัย', NULL, 'อุปกรณ์ความปลอดภัย', 'อุปกรณ์ความปลอดภัย', 'อุปกรณ์ความปลอดภัย', 1, 1, 1, 0, '2019-01-30 21:04:53', '2019-01-30 21:04:53'),
(12, 'เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร', NULL, 'เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร', 'เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร', 'เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร', 1, 1, 1, 0, '2019-11-13 03:17:20', '2019-11-13 03:17:20'),
(13, 'โคมไฟถนนแบบชนนิดหลอดไส้', NULL, 'โคมไฟถนนแบบชนนิดหลอดไส้', 'โคมไฟถนนแบบชนนิดหลอดไส้', 'โคมไฟถนนแบบชนนิดหลอดไส้', 1, 1, 1, 0, '2019-08-08 22:52:31', '2019-08-08 22:52:31'),
(14, 'เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร', NULL, 'เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร', 'เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร', 'เสาเหลี่ยมปลายเรียว ชุบกัลวาไนซ์ สูง 4-17 เมตร', 1, 1, 1, 0, '2019-11-13 03:24:23', '2019-11-13 03:24:23'),
(15, 'เสาไฟไฮแมสแบบเหลี่ยม ชุบกัลวาไนซ์ สูง 15-40 เมตร', NULL, 'เสาไฟไฮแมสแบบเหลี่ยม ชุบกัลวาไนซ์ สูง 15-40 เมตร', 'เสาไฟไฮแมสแบบเหลี่ยม ชุบกัลวาไนซ์ สูง 15-40 เมตร', 'เสาไฟไฮแมสแบบเหลี่ยม ชุบกัลวาไนซ์ สูง 15-40 เมตร', 1, 1, 1, 0, '2019-11-13 03:28:03', '2019-11-13 03:28:03'),
(16, 'เสาไฟไฮแมสแบบกลม ชุบกัลวาไนซ์ สูง 15-40 เมตร', NULL, 'เสาไฟไฮแมสแบบกลม ชุบกัลวาไนซ์ สูง 15-40 เมตร', 'เสาไฟไฮแมสแบบกลม ชุบกัลวาไนซ์ สูง 15-40 เมตร', 'เสาไฟไฮแมสแบบกลม ชุบกัลวาไนซ์ สูง 15-40 เมตร', 1, 1, 1, 0, '2019-11-13 03:26:49', '2019-11-13 03:26:49'),
(17, 'เสาธงแบบกลมปลายเรียว ชุบกัลวาไนซ์  สูง 3-30 เมตร', NULL, 'เสาธงแบบกลมปลายเรียว ชุบกัลวาไนซ์  สูง 3-30 เมตร', 'เสาธงแบบกลมปลายเรียว ชุบกัลวาไนซ์  สูง 3-30 เมตร', 'เสาธงแบบกลมปลายเรียว ชุบกัลวาไนซ์  สูง 3-30 เมตร', 1, 1, 1, 0, '2019-11-13 03:15:36', '2019-11-13 03:15:36'),
(18, 'เสาสนามกีฬา ชุบกัลวาไนซ์ สูง 15-40 เมตร', NULL, 'เสาสนามกีฬา ชุบกัลวาไนซ์ สูง 15-40 เมตร', 'เสาสนามกีฬา ชุบกัลวาไนซ์ สูง 15-40 เมตร', 'เสาสนามกีฬา ชุบกัลวาไนซ์ สูง 15-40 เมตร', 1, 1, 1, 0, '2019-11-13 03:22:04', '2019-11-13 03:22:04'),
(19, 'เสากลมปลายเรียว ชุบกัลวาไนซ์ สูง 4-14 เมตร', NULL, 'เสากลมปลายเรียว ชุบกัลวาไนซ์ สูง 4-14 เมตร', 'เสากลมปลายเรียว ชุบกัลวาไนซ์ สูง 4-14 เมตร', 'เสากลมปลายเรียว ชุบกัลวาไนซ์ สูง 4-14 เมตร', 1, 1, 1, 0, '2019-11-13 03:12:57', '2019-11-13 03:12:57'),
(20, 'เสาเหลี่ยมปลายตรง ชุบกัลวาไนซ์ สูง 4-14 เมตร', NULL, 'เสาเหลี่ยมปลายตรง ชุบกัลวาไนซ์ สูง 4-14 เมตร', 'เสาเหลี่ยมปลายตรง ชุบกัลวาไนซ์ สูง 4-14 เมตร', 'เสาเหลี่ยมปลายตรง ชุบกัลวาไนซ์ สูง 4-14 เมตร', 1, 1, 1, 0, '2019-11-13 03:23:07', '2019-11-13 03:23:07'),
(21, 'เสาประติมากรรม', NULL, 'เสาประติมากรรม', 'เสาประติมากรรม', 'เสาประติมากรรม', 1, 1, 1, 0, '2019-11-13 03:19:46', '2019-11-13 03:19:46'),
(22, 'เสาไฟส่องสว่าง โซล่าเซลล์', NULL, 'เสาไฟส่องสว่าง โซล่าเซลล์', 'เสาไฟส่องสว่าง โซล่าเซลล์', 'เสาไฟส่องสว่าง โซล่าเซลล์', 1, 1, 1, 0, '2019-11-13 03:25:48', '2019-11-13 03:25:48'),
(23, 'ราวกั้นอันตราย (การ์ดเรล)', NULL, 'ราวกั้นอันตราย (การ์ดเรล)', 'ราวกั้นอันตราย (การ์ดเรล)', 'ราวกั้นอันตราย (การ์ดเรล)', 1, 1, 1, 0, '2019-11-13 02:57:50', '2019-11-13 02:57:50'),
(24, 'โคมไฟถนน ชนิดหลอดไส้', NULL, 'โคมไฟถนน ชนิดหลอดไส้', 'โคมไฟถนน ชนิดหลอดไส้', 'โคมไฟถนน ชนิดหลอดไส้', 1, 1, 1, 0, '2019-11-13 03:38:49', '2019-11-13 03:38:49'),
(25, 'โคมไฟสนาม ชนิดหลอดไส้', NULL, 'โคมไฟสนาม ชนิดหลอดไส้', 'โคมไฟสนาม ชนิดหลอดไส้', 'โคมไฟสนาม ชนิดหลอดไส้', 1, 1, 1, 0, '2019-11-13 03:40:59', '2019-11-13 03:40:59'),
(26, 'โคมไฟถนน ชนิดหลอด LED', NULL, 'โคมไฟถนน ชนิดหลอด LED', 'โคมไฟถนน ชนิดหลอด LED', 'โคมไฟถนน ชนิดหลอด LED', 1, 1, 1, 0, '2019-11-13 03:39:51', '2019-11-13 03:39:51'),
(27, 'โคมไฟสนาม ชนิดหลอด LED', NULL, 'โคมไฟสนาม ชนิดหลอด LED', 'โคมไฟสนาม ชนิดหลอด LED', 'โคมไฟสนาม ชนิดหลอด LED', 1, 1, 1, 0, '2019-11-13 03:42:02', '2019-11-13 03:42:02'),
(28, 'ตู้ควบคุมสัญญาณไฟจราจร', NULL, 'ตู้ควบคุมสัญญาณไฟจราจร', 'ตู้ควบคุมสัญญาณไฟจราจร', 'ตู้ควบคุมสัญญาณไฟจราจร', 1, 1, 1, 0, '2019-11-13 02:49:04', '2019-11-13 02:49:04'),
(29, 'โคมไฟจราจร', NULL, 'โคมไฟจราจร', 'โคมไฟจราจร', 'โคมไฟจราจร', 1, 1, 1, 0, '2019-11-13 03:34:29', '2019-11-13 03:34:29'),
(30, 'โคมไฟจราจรนับเวลาถอยหลัง', NULL, 'โคมไฟจราจรนับเวลาถอยหลัง', 'โคมไฟจราจรนับเวลาถอยหลัง', 'โคมไฟจราจรนับเวลาถอยหลัง', 1, 1, 1, 0, '2019-11-13 03:35:04', '2019-11-13 03:35:04'),
(31, 'โคมไฟจราจรแบ่งช่องทางเดินรถ', NULL, 'โคมไฟจราจรแบ่งช่องทางเดินรถ', 'โคมไฟจราจรแบ่งช่องทางเดินรถ', 'โคมไฟจราจรแบ่งช่องทางเดินรถ', 1, 1, 1, 0, '2019-11-13 03:36:49', '2019-11-13 03:36:49'),
(32, 'ไฟจราจรปุ่มกดคนเดินข้าม', NULL, 'ไฟจราจรปุ่มกดคนเดินข้าม', 'ไฟจราจรปุ่มกดคนเดินข้าม', 'ไฟจราจรปุ่มกดคนเดินข้าม', 1, 1, 1, 0, '2019-10-17 15:25:14', '2019-10-17 15:25:14'),
(33, 'ป้ายไฟเตือนทางเบี่ยงโซล่าเซลล์', NULL, 'ป้ายไฟเตือนทางเบี่ยงโซล่าเซลล์', 'ป้ายไฟเตือนทางเบี่ยงโซล่าเซลล์', 'ป้ายไฟเตือนทางเบี่ยงโซล่าเซลล์', 1, 1, 1, 0, '2019-11-13 03:49:48', '2019-11-13 03:49:48'),
(34, 'ป้ายไฟเตือนทางแยกโซล่าเซลล์', NULL, 'ป้ายไฟเตือนทางแยกโซล่าเซลล์', 'ป้ายไฟเตือนทางแยกโซล่าเซลล์', 'ป้ายไฟเตือนทางแยกโซล่าเซลล์', 1, 1, 1, 0, '2019-11-13 03:53:29', '2019-11-13 03:53:29'),
(35, 'ป้ายไฟเตือนเขตโรงเรียนโซล่าเซลล์', NULL, 'ป้ายไฟเตือนเขตโรงเรียนโซล่าเซลล์', 'ป้ายไฟเตือนเขตโรงเรียนโซล่าเซลล์', 'ป้ายไฟเตือนเขตโรงเรียนโซล่าเซลล์', 1, 1, 1, 0, '2019-11-13 03:44:52', '2019-11-13 03:44:52'),
(36, 'ป้ายไฟเตือนเกาะกลางโซล่าเซลล์', NULL, 'ป้ายไฟเตือนเกาะกลางโซล่าเซลล์', 'ป้ายไฟเตือนเกาะกลางโซล่าเซลล์', 'ป้ายไฟเตือนเกาะกลางโซล่าเซลล์', 1, 1, 1, 0, '2019-11-13 03:50:15', '2019-11-13 03:50:15'),
(37, 'ป้ายไฟเตือนลดความเร็วโซล่าเซลล์', NULL, 'ป้ายไฟเตือนลดความเร็วโซล่าเซลล์', 'ป้ายไฟเตือนลดความเร็วโซล่าเซลล์', 'ป้ายไฟเตือนลดความเร็วโซล่าเซลล์', 1, 1, 1, 0, '2019-11-13 03:44:04', '2019-11-13 03:44:04'),
(38, 'ป้ายไฟเตือนกระพริบโซล่าเซลล์', NULL, 'ป้ายไฟเตือนกระพริบโซล่าเซลล์', 'ป้ายไฟเตือนกระพริบโซล่าเซลล์', 'ป้ายไฟเตือนกระพริบโซล่าเซลล์', 1, 1, 1, 0, '2019-11-13 03:46:55', '2019-11-13 03:46:55'),
(39, 'ป้ายไฟเตือนรถบรรทุกเข้า-ออกโซล่าเซลล์', NULL, 'ป้ายไฟเตือนรถบรรทุกเข้า-ออกโซล่าเซลล์', 'ป้ายไฟเตือนรถบรรทุกเข้า-ออกโซล่าเซลล์', 'ป้ายไฟเตือนรถบรรทุกเข้า-ออกโซล่าเซลล์', 1, 1, 1, 0, '2019-11-13 03:51:29', '2019-11-13 03:51:29'),
(40, 'ป้ายไฟเตือนจราจร CCTV โซล่าเซลล์', NULL, 'ป้ายไฟเตือนจราจร CCTV โซล่าเซลล์', 'ป้ายไฟเตือนจราจร CCTV โซล่าเซลล์', 'ป้ายไฟเตือนจราจร CCTV โซล่าเซลล์', 1, 1, 1, 0, '2019-11-13 03:52:40', '2019-11-13 03:52:40'),
(41, 'ป้ายบังคับจราจร', NULL, 'ป้ายบังคับจราจร', 'ป้ายบังคับจราจร', 'ป้ายบังคับจราจร', 1, 1, 1, 0, '2019-11-13 02:54:28', '2019-11-13 02:54:28'),
(42, 'ป้ายแนะนำ', NULL, 'ป้ายแนะนำ', 'ป้ายแนะนำ', 'ป้ายแนะนำ', 1, 1, 1, 0, '2019-11-13 02:56:13', '2019-11-13 02:56:13'),
(43, 'ป้ายเตือนจราจร', NULL, 'ป้ายเตือนจราจร', 'ป้ายเตือนจราจร', 'ป้ายเตือนจราจร', 1, 1, 1, 0, '2019-11-13 02:55:08', '2019-11-13 02:55:08'),
(44, 'ป้าย Overhang', NULL, 'ป้าย Overhang', 'ป้าย Overhang', 'ป้าย Overhang', 1, 1, 1, 0, '2019-11-13 02:49:42', '2019-11-13 02:49:42'),
(45, 'ป้ายซอยประติมากรรม', NULL, 'ป้ายซอยประติมากรรม', 'ป้ายซอยประติมากรรม', 'ป้ายซอยประติมากรรม', 1, 1, 1, 0, '2019-11-13 02:53:58', '2019-11-13 02:53:58'),
(46, 'ป้าย Overhead จราจร', NULL, 'ป้าย Overhead จราจร', 'ป้าย Overhead จราจร', 'ป้าย Overhead จราจร', 1, 1, 1, 0, '2019-11-13 02:50:10', '2019-11-13 02:50:10'),
(47, 'หมุดจราจร', NULL, 'หมุดจราจร', 'หมุดจราจร', 'หมุดจราจร', 1, 1, 1, 0, '2019-11-13 03:03:35', '2019-11-13 03:03:35'),
(48, 'หมุดแก้ว 360 องศา', NULL, 'หมุดแก้ว 360 องศา', 'หมุดแก้ว 360 องศา', 'หมุดแก้ว 360 องศา', 1, 1, 1, 0, '2019-11-13 03:05:45', '2019-11-13 03:05:45'),
(49, 'กรวยจราจร', NULL, 'กรวยจราจร', 'กรวยจราจร', 'กรวยจราจร', 1, 1, 1, 0, '2019-11-13 02:43:58', '2019-11-13 02:43:58'),
(50, 'เสื้อจราจร', NULL, 'เสื้อจราจร', 'เสื้อจราจร', 'เสื้อจราจร', 1, 1, 1, 0, '2019-11-13 03:28:39', '2019-11-13 03:28:39'),
(51, 'อุปกรณ์ความปลอดภัยจราจร', NULL, 'อุปกรณ์ความปลอดภัยจราจร', 'อุปกรณ์ความปลอดภัยจราจร', 'อุปกรณ์ความปลอดภัยจราจร', 1, 1, 1, 0, '2019-09-01 02:27:46', '2019-09-01 02:27:46'),
(52, 'กล้อง CCTV', NULL, 'กล้อง CCTV', 'กล้อง CCTV', 'กล้อง CCTV', 1, 1, 1, 0, '2019-11-13 02:45:45', '2019-11-13 02:45:45'),
(53, 'ป้ายจอ LED VMS,SMS,MMS', NULL, 'ป้ายจอ LED VMS,SMS,MMS', 'ป้ายจอ LED VMS,SMS,MMS', 'ป้ายจอ LED VMS,SMS,MMS', 1, 1, 1, 0, '2019-11-13 02:53:18', '2019-11-13 02:53:18'),
(54, 'โคมไฟจราจรแบบ3ดวงโคม', NULL, 'โคมไฟจราจรแบบ3ดวงโคม', 'โคมไฟจราจรแบบ3ดวงโคม', 'โคมไฟจราจรแบบ3ดวงโคม', 1, 1, 1, 0, '2019-11-13 03:31:43', '2019-11-13 03:31:43'),
(55, 'โคมไฟจราจรแบบ 4/6 ดวงโคม', NULL, 'โคมไฟจราจรแบบ 4/6 ดวงโคม', 'โคมไฟจราจรแบบ 4/6 ดวงโคม', 'โคมไฟจราจรแบบ 4/6 ดวงโคม', 1, 1, 1, 0, '2019-11-13 03:35:59', '2019-11-13 03:35:59'),
(56, 'หมุดอลูมิเนียม', NULL, 'หมุดอลูมิเนียม', 'หมุดอลูมิเนียม', 'หมุดอลูมิเนียม', 1, 1, 1, 0, '2019-11-13 03:04:29', '2019-11-13 03:04:29'),
(57, 'เป้าสะท้อนแสง', NULL, 'เป้าสะท้อนแสง', 'เป้าสะท้อนแสง', 'เป้าสะท้อนแสง', 1, 1, 1, 0, '2019-11-13 03:09:41', '2019-11-13 03:09:41'),
(58, 'กำแพงน้ำพลาสติก แบริเออร์น้ำ หรือแผงกั้นชนิดบรรจุน้ำ', NULL, 'กำแพงน้ำพลาสติก แบริเออร์น้ำ หรือแผงกั้นชนิดบรรจุน้ำ', 'กำแพงน้ำพลาสติก แบริเออร์น้ำ หรือแผงกั้นชนิดบรรจุน้ำ', 'กำแพงน้ำพลาสติก แบริเออร์น้ำ หรือแผงกั้นชนิดบรรจุน้ำ', 1, 1, 1, 0, '2019-11-13 02:46:39', '2019-11-13 02:46:39'),
(59, 'แผงกั้นจราจร', NULL, 'แผงกั้นจราจร', 'แผงกั้นจราจร', 'แผงกั้นจราจร', 1, 1, 1, 0, '2019-11-13 03:29:31', '2019-11-13 03:29:31'),
(60, 'เสาล้มลุก', NULL, 'เสาล้มลุก', 'เสาล้มลุก', 'เสาล้มลุก', 1, 1, 1, 0, '2019-11-13 03:20:22', '2019-11-13 03:20:22'),
(61, 'ยางชะลอความเร็ว', NULL, 'ยางชะลอความเร็ว', 'ยางชะลอความเร็ว', 'ยางชะลอความเร็ว', 1, 1, 1, 0, '2019-11-13 02:57:07', '2019-11-13 02:57:07'),
(62, 'กระจกโค้งจราจร', NULL, 'กระจกโค้งจราจร', 'กระจกโค้งจราจร', 'กระจกโค้งจราจร', 1, 1, 1, 0, '2019-11-13 02:45:07', '2019-11-13 02:45:07'),
(63, 'เสาประติมากรรม', NULL, 'เสาประติมากรรม', 'เสาประติมากรรม', 'เสาประติมากรรม', 1, 1, 1, 0, '2019-10-12 13:00:22', '2019-10-12 13:00:22');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `fname` varchar(80) NOT NULL,
  `lname` varchar(80) NOT NULL,
  `created_by` int(11) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `salt`, `fname`, `lname`, `created_by`, `avatar`, `email`, `phone`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 'admin', '38a142d6f6a7b889587a97379cdc0775334169ee', 'gmcZSu9Ce5DhZWl1w.bZ3e', 'admin', 'system', 1, '', 'admin@admin.com', '', 1, '2019-10-21 00:20:41', '2019-10-21 00:20:41'),
(3, 'admina', '8f89ca155ed51376c705f37f5b561c6b00e33a11', 'T0wulygnJwjffVMbViKrZe', 'test', 'test', 1, '', 'test@gmail.com', '089089089', 2, '2019-10-27 15:04:19', '2019-10-27 15:04:19'),
(4, 'ton', 'af64a14a09db5fe045e21228eaff0d2fb0b7757c', '6oh23fCvmk4oWPE2EP692O', 'ton', 'ton', 1, '', 'ton@gmail.com', '098080218', 1, '2019-10-21 21:40:47', '2019-10-21 21:40:47'),
(5, 'tonn', '0ab104c8ab4b8392192beb3edce84a7e9fbead73', 'E7jXD97JRj82cplBQrYD2O', 'tonn', 'tonn', 1, '', 'ton@gmail.com', '', 2, '2019-10-27 16:15:13', '2019-10-27 16:15:13'),
(6, 'sakidlo', 'dcb12ef1d2778a6c19386682aa66852a5e6b79ca', '6kpcpRxmjg8z1FNmucX9x.', 'sakid', 'lo', 1, '', 'sakidlo@gmail.com', '0898155825', 2, '2019-11-12 11:50:52', '2019-11-12 11:50:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_logs`
--
ALTER TABLE `backend_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `catalog_download_log`
--
ALTER TABLE `catalog_download_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `ci_sessions_timestamp` (`timestamp`) USING BTREE;

--
-- Indexes for table `config_mail`
--
ALTER TABLE `config_mail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config_mail_send`
--
ALTER TABLE `config_mail_send`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `footers`
--
ALTER TABLE `footers`
  ADD PRIMARY KEY (`footer_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`jobs_id`);

--
-- Indexes for table `jobs_regis`
--
ALTER TABLE `jobs_regis`
  ADD PRIMARY KEY (`jobs_regis_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotations`
--
ALTER TABLE `quotations`
  ADD PRIMARY KEY (`quotations_id`);

--
-- Indexes for table `regis_config`
--
ALTER TABLE `regis_config`
  ADD PRIMARY KEY (`regis_config_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `backend_logs`
--
ALTER TABLE `backend_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `catalog`
--
ALTER TABLE `catalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `catalog_download_log`
--
ALTER TABLE `catalog_download_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `config_mail`
--
ALTER TABLE `config_mail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `config_mail_send`
--
ALTER TABLE `config_mail_send`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `footers`
--
ALTER TABLE `footers`
  MODIFY `footer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `jobs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jobs_regis`
--
ALTER TABLE `jobs_regis`
  MODIFY `jobs_regis_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `quotations`
--
ALTER TABLE `quotations`
  MODIFY `quotations_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `regis_config`
--
ALTER TABLE `regis_config`
  MODIFY `regis_config_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `seo`
--
ALTER TABLE `seo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
