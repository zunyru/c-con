<?php

/**
 * 
 */
class Products_model extends CI_Model
{   

	protected $CI;

	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('categorys/categorys_model');
	}

	public function get_ProductsAll()
	{

		$query = $this->db->get('products');

		$products = $query->result();
		$i=0;
		foreach($products as $product){

			$products[$i]->category_product = $this->CI->categorys_model->get_CategoriesByProduct($product->category_id);


			foreach ($products[$i]->category_product as $key => $sub_cat) {

				$products[$i]->category_product[$key]->category_main = $this->CI->categorys_model->get_categories($sub_cat->parent_id);
			}

			$i++;
		}

		return $products;

		//return $query->result_array();
	}

}

?>