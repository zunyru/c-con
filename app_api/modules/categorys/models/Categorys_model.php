<?php

/**
 *
 */
class Categorys_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getCategoryAll() {

		$this->db->select('*');
		$this->db->from('categories cat');
		$this->db->order_by('cat.name', 'ASC');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_categories($id = null)
	{

		$this->db->select('*');
		$this->db->from('categories');
		$this->db->where('parent_id', 0);
		if(!is_null($id))
		   $this->db->where('id', $id);	 
		$this->db->order_by('sequence','ASC');

		$parent = $this->db->get();

		$categories = $parent->result();
		$i=0;
		foreach($categories as $p_cat){

			$categories[$i]->catogory_sub = $this->sub_categories($p_cat->id);
			$i++;
		}

		return $categories;
	}

	public function get_CategoriesByProduct($category_id)
	{
		$query = $this->db->where('id',$category_id)->get('categories');

		return $query->result();
	}

	public function sub_categories($id){

		$this->db->select('id,name,slug,icon');
		$this->db->from('categories');
		$this->db->where('parent_id', $id);
		$this->db->order_by('sequence','ASC');

		$child = $this->db->get();
		$categories = $child->result();
		$i=0;
		foreach($categories as $p_cat){

			$categories[$i]->sub = $this->sub_categories($p_cat->id);
			$i++;
		}
		return $categories;       
	}

	
	
}

?>